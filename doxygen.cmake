find_package(Doxygen)

macro (create_doxygen outputdir inputdir name)
    foreach (tag ${ARGN})
        string(REGEX REPLACE "=.*" "" tagfile ${tag})
        set(tagdeps
            ${tagdeps}
            ${tagfile}
        )
    endforeach (tag)
    
    add_custom_command(
        OUTPUT
            ${outputdir}/${name}
        COMMAND
            cmake -E make_directory ${outputdir}/${name}
        COMMENT
            "Creating documentation directory for ${name}"
    )
    add_custom_command(
        OUTPUT
            ${outputdir}/${name}/Doxyfile
        COMMAND
            sed ${CMAKE_SOURCE_DIR}/Doxyfile
                -e 's!PROJECT_SOURCE_DIR!${inputdir}!g'
                -e 's!DOCUMENTATION_OUTPUT_PATH!${outputdir}!g'
                -e 's!_PROJECT_NAME!${name}!g'
                -e 's!TAG_FILES!${ARGN}!g'
                > Doxyfile
        DEPENDS
            ${CMAKE_SOURCE_DIR}/Doxyfile
            ${outputdir}/${name}
        WORKING_DIRECTORY
            ${outputdir}/${name}
        COMMENT
            "Generating Doxyfile for ${name}"
    )
    add_custom_command(
        OUTPUT
            ${outputdir}/${name}.tag
        COMMAND
            ${DOXYGEN_EXECUTABLE}
        DEPENDS
            ${outputdir}/${name}/Doxyfile
        WORKING_DIRECTORY
            ${outputdir}/${name}
        COMMENT
            "Creating tag for ${name}"
    )
    add_custom_command(
        OUTPUT
            ${outputdir}/${name}/index.html
        COMMAND
            ${DOXYGEN_EXECUTABLE}
        DEPENDS
            ${outputdir}/${name}.tag
            ${tagdeps}
        WORKING_DIRECTORY
            ${outputdir}/${name}
        COMMENT
            "Creating HTML documentation for ${name}"
    )
    add_custom_target(doxygen-${name}
        DEPENDS
            ${outputdir}/${name}/index.html
    )
    
#     FIXME
#     install(
#         DIRECTORY
#             ${outputdir}/${name}
#         DESTINATION
#             ${CMAKE_INSTALL_PREFIX}/share/doc/${CMAKE_PROJECT_NAME}-${SIGEN_VERSION}/${name}
#         COMPONENT
#             documentation
#     )
endmacro (create_doxygen)
