/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_ROOM
#define SIGNET_ROOM

// Signet includes
#include "ClientHandler.h"
#include "Global.h"

// Qt includes
#include <QtCore/QMap>

namespace Signet
{
// Forward declarations
class Table;
class Server;

class SIGNET_EXPORT Room : public ClientHandler
{
    Q_OBJECT
    
    public:
        Room(const QString& name, Server* server);
        
        QStringList receiveChain() const;
    public slots:
        void packetReceived(QTcpSocket* client, Protocol::Packet* packet);
    signals:
        void globalMessage(const QString& message);
    protected:
        void createTable(const QString& table);
        void closeTable(const QString& table);
    private:
        Server* m_server;
        QString m_name;
        QMap<QString, Table*> m_tables;
};
}

#endif
