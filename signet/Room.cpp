/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Room.h"

// Signet includes
#include "Server.h"
#include "Table.h"

Signet::Room::Room(const QString& name, Server* server) :
        ClientHandler(server),
        m_server(server),
        m_name(name)
{
}

QStringList Signet::Room::receiveChain() const
{
    return m_server->receiveChain() << QString("room-%1").arg(m_name);
}

void Signet::Room::packetReceived(QTcpSocket* client, Protocol::Packet* packet)
{
    QString receiver = packet->received();
    if (receiver.isEmpty())
    {
        // TODO: Handle the packet
    }
    else
    {
        createTable(receiver);
        m_tables[receiver]->packetReceived(client, packet);
    }
}

void Signet::Room::createTable(const QString& table)
{
    if (m_tables.contains(table))
        return;
    m_tables[table] = new Table(table, this);
}

void Signet::Room::closeTable(const QString& table)
{
    if (m_tables.contains(table))
    {
        delete m_tables[table];
        m_tables.remove(table);
    }
}
