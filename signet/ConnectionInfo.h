/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_CONNECTIONINFO
#define SIGNET_CONNECTIONINFO

// Protocol includes
#include "protocol/ChallengeMediator.h"

// Signet includes
#include "Global.h"

// KDE includes
#include <KConfigGroup>

// Qt includes
#include <QtNetwork/QHostAddress>

// Forward declarations
class QTcpSocket;

namespace Signet
{
class SIGNET_EXPORT ConnectionInfo
{
    public:
        ConnectionInfo();
        
        void load(const KConfigGroup& config);
        
        bool isValid() const;
        
        void connectToHost(QTcpSocket* socket) const;
        Protocol::ChallengeMediator::Error authenticate(const QString& username) const;
        
        KConfigGroup config() const;
    private:
        bool m_valid;
        QHostAddress m_host;
        int m_port;
        QByteArray m_secret;
};
}

#endif
