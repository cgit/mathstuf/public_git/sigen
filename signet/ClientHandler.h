/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_CLIENTHANDLER
#define SIGNET_CLIENTHANDLER

// Protocol includes
#include "protocol/Packet.h"

// Signet includes
#include "Global.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QObject>

// Forward declarations
class QTcpSocket;

namespace Signet
{
class SIGNET_EXPORT ClientHandler : public QObject
{
    Q_OBJECT
    
    public:
        ClientHandler(QObject* parent);
        
        virtual QStringList receiveChain() const = 0;
    public slots:
        bool addClient(QTcpSocket* client);
        bool removeClient(QTcpSocket* client);
    protected:
        virtual QByteArray secretForClient(const QByteArray& id) const;
    private:
        QList<QTcpSocket*> m_clients;
};
}

#endif
