/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Server.h"

// Signet includes
#include "Room.h"
#include "ServerConnectionHandler.h"

// KDE includes
#include <KConfig>
#include <KConfigGroup>
#include <KGlobal>

// Qt includes
#include <QtCore/QtGlobal>
#include <QtCore/QMutexLocker>
#include <QtCore/QTimer>
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>

Signet::Server::Server(QObject* parent) :
        ClientHandler(parent),
        m_server(new QTcpServer(this))
{
    loadConfiguration();
    if (m_metaserverInfo.isValid())
        connectToMetaserver();
    connect(m_server, SIGNAL(newConnection()), this, SLOT(newConnection()));
}

QStringList Signet::Server::receiveChain() const
{
    return QStringList() << QString("server-%1").arg(m_name);
}

void Signet::Server::start()
{
    QTcpServer* server = new QTcpServer(this);
    server->listen(QHostAddress::Any, m_port);
}

void Signet::Server::createRoom(const QString& room)
{
    QMutexLocker locker(&m_mutex);
    if (m_rooms.contains(room))
        return;
    m_rooms[room] = new Room(room, this);
}

void Signet::Server::closeRoom(const QString& room)
{
    QMutexLocker locker(&m_mutex);
    if (m_rooms.contains(room))
    {
        delete m_rooms[room];
        m_rooms.remove(room);
    }
}

void Signet::Server::metaserverFound()
{
    qDebug("Metaserver found");
}

void Signet::Server::metaserverConnected()
{
    qDebug("Metaserver connected");
    switch (m_metaserverInfo.authenticate(QString("server-").arg(m_name).toUtf8()))
    {
        case Protocol::ChallengeMediator::NoError:
            qDebug("Authenticated with metaserver");
            // TODO: Send server information
            break;
        case Protocol::ChallengeMediator::UnknownUser:
            qWarning("Server is not registered with the metaserver");
            break;
        case Protocol::ChallengeMediator::ClientFailed:
            qWarning("Could not authenticate against metaserver");
            break;
        case Protocol::ChallengeMediator::ServerFailed:
            qWarning("Metaserver failed authentication");
            break;
        case Protocol::ChallengeMediator::SocketError:
            qWarning("Socket error");
            break;
        case Protocol::ChallengeMediator::UnexpectedError:
            qWarning("Unexpected packet");
            break;
        case Protocol::ChallengeMediator::SideError:
            qCritical("Acted on wrong end of authentication");
            break;
        case Protocol::ChallengeMediator::ReceiverError:
            qCritical("Wrong destination for packet");
            break;
    }
}

void Signet::Server::metaserverDisconnected()
{
    QTimer::singleShot(60000, this, SLOT(connectToMetaserver()));
    qWarning("Metaserver disconnected; will retry in 60 seconds");
}

void Signet::Server::metaserverError()
{
    qWarning("Metaserver socket error: %s", m_metaserver->errorString().toUtf8().constData());
}

void Signet::Server::newConnection()
{
    QTcpSocket* socket = m_server->nextPendingConnection();
    ServerConnectionHandler* handler = new ServerConnectionHandler(socket, this);
    connect(handler, SIGNAL(finished()), socket, SLOT(deleteLater()));
    handler->run();
}

void Signet::Server::loadConfiguration()
{
    if (KGlobal::config()->hasGroup("Metaserver"))
        m_metaserverInfo.load(KGlobal::config()->group("Metaserver"));
    KConfigGroup config = KGlobal::config()->group("Server Info");
    m_name = config.readEntry("Name", "");
    m_port = config.readEntry("Port", 49959);
}

void Signet::Server::connectToMetaserver()
{
    if (!m_metaserver)
    {
        m_metaserver = new QTcpSocket(this);
        connect(m_metaserver, SIGNAL(hostFound()), this, SLOT(metaserverFound()));
        connect(m_metaserver, SIGNAL(connected()), this, SLOT(metaserverConnected()));
        connect(m_metaserver, SIGNAL(disconnected()), this, SLOT(metaserverDisconnected()));
        connect(m_metaserver, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(metaserverError()));
    }
    m_metaserverInfo.connectToHost(m_metaserver);
}
