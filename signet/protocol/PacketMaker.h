/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_PROTOCOL_PACKETMAKER
#define SIGNET_PROTOCOL_PACKETMAKER

// Protocol includes
#include "Packet.h"

// Signet includes
#include "../Global.h"

// Forward declarations
class QIODevice;

namespace Signet
{
namespace Protocol
{
class ProgressMeter;

class SIGNET_IMPORT PacketMaker
{
    public:
        static void rawData(QIODevice* device, const QByteArray& data, const QStringList& receivers = QStringList());
        
        static void deny(QIODevice* device, const QStringList& receivers = QStringList());
        static void accept(QIODevice* device, const QStringList& receivers = QStringList());
        
        static void ack(QIODevice* device, const QStringList& receivers = QStringList());
        
        static void challenge(QIODevice* device, const QByteArray& id, const QStringList& receivers = QStringList());
        static void challengeKey(QIODevice* device, const QByteArray& key, const QStringList& receivers = QStringList());
        static void challengeResponse(QIODevice* device, const QByteArray& response, const QStringList& receivers = QStringList());
        
        static void message(QIODevice* device, const QString& message, const QStringList& receivers = QStringList());
        
        static void makeConnection(QIODevice* device, const QStringList& receivers = QStringList());
        static void dropConnection(QIODevice* device, const QStringList& receivers = QStringList());
        
        static void unknownReceiver(QIODevice* device, const QStringList& receivers = QStringList());
        static void unhandledRequest(QIODevice* device, const QStringList& receivers = QStringList());
        
        static Packet unwrap(QIODevice* device, ProgressMeter* progressMeter = NULL);
};
}
}

#endif
