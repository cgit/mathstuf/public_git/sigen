/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_PROTOCOL_CONNECTMEDIATOR
#define SIGNET_PROTOCOL_CONNECTMEDIATOR

// Signet includes
#include "../Global.h"

// Qt includes
#include <QtCore/QStringList>

// Forward declarations
class QTcpSocket;

namespace Signet
{
namespace Protocol
{
class SIGNET_IMPORT ConnectMediator
{
    public:
        enum Error
        {
            NoError         = 0,
            SocketError     = 1,
            UnexpectedError = 2,
            SideError       = 3,
            ReceiverError   = 4
        };
        
        enum Side
        {
            Client  = 0,
            Server  = 1
        };
        
        ConnectMediator(QTcpSocket* socket, Side side, const QStringList& receivers = QStringList());
        
        Error requestConnection();
        Error replyToConnection();
    protected:
        void init();
        
        void sendAck();
        void recvAck();
        
        void failure();
    private:
        const Side m_side;
        const QStringList& m_receivers;
        Error m_error;
        QTcpSocket* m_socket;
};
}
}

#endif
