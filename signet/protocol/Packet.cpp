/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Packet.h"

// Qt include
#include <QtCore/QtEndian>
#include <QtCore/QBuffer>
#include <QtCore/QIODevice>

Signet::Protocol::Packet::Packet(const qint16 type, const QStringList& receivers) :
        m_type(type),
        m_receivers(receivers)
{
}

qint16 Signet::Protocol::Packet::type() const
{
    return m_type;
}

QStringList Signet::Protocol::Packet::receivers() const
{
    return m_receivers;
}

bool Signet::Protocol::Packet::isValid() const
{
    return m_type == Invalid;
}

QString Signet::Protocol::Packet::received()
{
    if (m_receivers.isEmpty())
        return QString();
    return m_receivers.takeFirst();
}

void Signet::Protocol::Packet::write(const QByteArray& data)
{
    if ((m_type == RawData) || (m_type == Invalid))
        m_rawData.append(data);
}

void Signet::Protocol::Packet::rawDump(QIODevice* device) const
{
    device->write(m_rawData);
}

void Signet::Protocol::Packet::dump(QIODevice* device) const
{
    if (m_type == Invalid)
    {
        qCritical("Cannot send a packet of invalid type");
        return;
    }
    union
    {
        char raw[4];
        qint16 type;
        qint32 size;
    };
    QByteArray data;
    QBuffer buffer(&data);
    type = qToBigEndian(m_type);
    buffer.write(raw, 2);
    foreach (const QString& receiver, m_receivers)
    {
        if (receiver.isEmpty())
            continue;
        QByteArray utf8Data = receiver.toUtf8();
        size = receiver.size();
        size = qToBigEndian(size);
        buffer.putChar('\0');
        buffer.write(raw, 4);
        buffer.write(utf8Data.constData(), utf8Data.size());
    }
    size = m_rawData.size();
    size = qToBigEndian(size);
    buffer.putChar('\x1');
    buffer.write(raw, 4);
    buffer.write(m_rawData);
    device->write(data);
}
