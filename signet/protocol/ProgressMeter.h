/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_PROTOCOL_PROGRESSMETER
#define SIGNET_PROTOCOL_PROGRESSMETER

namespace Signet
{
namespace Protocol
{
class SIGNET_IMPORT ProgressMeter
{
    public:
        virtual ~ProgressMeter();
        
        virtual void setExpectedSize(const int expectedSize) = 0;
        virtual void setValue(const int value) = 0;
        
        virtual void success() = 0;
        virtual void failure() = 0;
};
}
}

#endif
