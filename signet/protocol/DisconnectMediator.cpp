/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "DisconnectMediator.h"

// Protocol includes
#include "PacketMaker.h"

// Qt includes
#include <QtNetwork/QTcpSocket>

Signet::Protocol::DisconnectMediator::DisconnectMediator(QTcpSocket* socket, Side side, const QStringList& receivers) :
        m_side(side),
        m_receivers(receivers),
        m_error(NoError),
        m_socket(socket)
{
}

Signet::Protocol::DisconnectMediator::Error Signet::Protocol::DisconnectMediator::requestDisconnection()
{
    if (m_side != Client)
        return SideError;
    init();
    recvAck();
    return m_error;
}

Signet::Protocol::DisconnectMediator::Error Signet::Protocol::DisconnectMediator::replyToDisconnection()
{
    if (m_side != Server)
        return SideError;
    recvAck();
    if (m_error != NoError)
    {
        failure();
        return m_error;
    }
    sendAck();
    return m_error;
}

void Signet::Protocol::DisconnectMediator::init()
{
    PacketMaker::dropConnection(m_socket, m_receivers);
}

void Signet::Protocol::DisconnectMediator::sendAck()
{
    PacketMaker::ack(m_socket, m_receivers);
}

void Signet::Protocol::DisconnectMediator::recvAck()
{
    Packet packet = PacketMaker::unwrap(m_socket);
    if (packet.isValid())
    {
        if (packet.receivers() == m_receivers)
        {
            if (packet.type() != Packet::Acknowledge)
                m_error = UnexpectedError;
        }
        else
            m_error = ReceiverError;
    }
    else
        m_error = SocketError;
}

void Signet::Protocol::DisconnectMediator::failure()
{
    PacketMaker::deny(m_socket, m_receivers);
}
