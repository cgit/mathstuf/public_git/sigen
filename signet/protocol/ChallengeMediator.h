/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_PROTOCOL_CHALLENGEMEDIATOR
#define SIGNET_PROTOCOL_CHALLENGEMEDIATOR

// Signet includes
#include "../Global.h"

// Qt includes
#include <QtCore/QStringList>

// Forward declarations
class QTcpSocket;

namespace Signet
{
namespace Protocol
{
class SIGNET_IMPORT ChallengeMediator
{
    public:
        enum Error
        {
            NoError         = 0,
            UnknownUser     = 1,
            ClientFailed    = 2,
            ServerFailed    = 3,
            SocketError     = 4,
            UnexpectedError = 5,
            SideError       = 6,
            ReceiverError   = 7
        };
        
        enum Side
        {
            Client = 0,
            Server = 1
        };
        
        ChallengeMediator(QTcpSocket* socket, Side side, const QStringList& receivers = QStringList());
        
        Error challenge(const QByteArray& id, const QByteArray& secret);
        Error challenged(const QByteArray& secret);
    protected:
        static QByteArray generateChallengeKey();
        static QByteArray generateResponse(const QByteArray& key1, const QByteArray& key2, const QByteArray& secret);
        
        void init(const QByteArray& id);
        
        void sendServerKey(const QByteArray& serverKey);
        void recvServerKey(QByteArray* serverKey);
        
        void sendClientResponse(const QByteArray& clientKey, const QByteArray& clientResponse);
        void recvClientResponse(QByteArray* clientKey, QByteArray* clientResponse);
        
        void sendServerResponse(const QByteArray& serverResponse);
        void recvServerResponse(QByteArray* serverResponse);
        
        void recvClientAnswer();
        
        void notifyServerFail();
        void notifyServerPass();
        void notifyClientFail();
        void notifyClientUnknown();
    private:
        const Side m_side;
        const QStringList& m_receivers;
        Error m_error;
        QTcpSocket* m_socket;
};
}
}

#endif
