/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_PROTOCOL_PACKET
#define SIGNET_PROTOCOL_PACKET

// Signet includes
#include "../Global.h"

// Qt includes
#include <QtCore/QByteArray>
#include <QtCore/QStringList>

// Forward declarations
class QIODevice;

namespace Signet
{
namespace Protocol
{
class SIGNET_IMPORT Packet
{
    public:
        enum Type
        {
            // Errors
            UnknownReceiver     = 0x4000,
            UnhandledPacket     = 0x4001,
            LimitedPermissions  = 0x4002,
            Invalid             = 0x40FF,
            // Requests
            ConnectRequest      = 0x0100,
            DisconnectRequest   = 0x0101,
            ChallengeRequest    = 0x0102,
            UserListRequest     = 0x0103,
            ServerListRequest   = 0x0104,
            RoomListRequest     = 0x0105,
            TableListRequest    = 0x0106,
            SigmodListRequest   = 0x0107,
            SigmodRequest       = 0x0108,
            TableState          = 0x0109,
            DecisionRequest     = 0x010A,
            // Data
            RawData             = 0x0200,
            Acceptance          = 0x0201,
            Denial              = 0x0202,
            Acknowledge         = 0x0203,
            ChallengeKey        = 0x0204,
            ChallengeResponse   = 0x0205,
            Message             = 0x0206,
            ServerList          = 0x0207,
            UserList            = 0x0208,
            RoomList            = 0x0209,
            SigmodList          = 0x020A,
            Sigmod              = 0x020B,
            BattleState         = 0x020C,
            MapState            = 0x020D,
            Decision            = 0x020E,
            // Updates
        };
        
        enum SectionType
        {
            Receiver    = 0,
            Payload     = 1
        };
        
        Packet(const qint16 type = Invalid, const QStringList& receivers = QStringList());
        
        qint16 type() const;
        QStringList receivers() const;
        bool isValid() const;
        
        QString received();
        
        void write(const QByteArray& data);
        void rawDump(QIODevice* device) const;
        void dump(QIODevice* device) const;
    private:
        qint16 m_type;
        QStringList m_receivers;
        QByteArray m_rawData;
};
}
}

#endif
