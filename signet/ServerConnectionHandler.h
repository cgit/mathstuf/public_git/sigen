/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_SERVERCONNECTIONHANDLER
#define SIGNET_SERVERCONNECTIONHANDLER

// Signet includes
#include "Global.h"

// Qt includes
#include <QtCore/QThread>

// Forward declarations
class QTcpSocket;

namespace Signet
{
class Server;

class SIGNET_EXPORT ServerConnectionHandler : public QThread
{
    Q_OBJECT
    
    public:
        ServerConnectionHandler(QTcpSocket* socket, Server* server);
        
        void run();
    private:
        QTcpSocket* m_socket;
        Server* m_server;
};
}

#endif
