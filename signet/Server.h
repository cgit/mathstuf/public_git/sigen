/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGNET_SERVER
#define SIGNET_SERVER

// Signet includes
#include "ClientHandler.h"
#include "ConnectionInfo.h"
#include "Global.h"

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QMutex>
#include <QtCore/QObject>
#include <QtCore/QUuid>
#include <QtNetwork/QHostAddress>

// Forward declarations
class KConfigGroup;
class QTcpServer;
class QTcpSocket;

namespace Signet
{
class Room;
class ServerConnectionHandler;

class SIGNET_EXPORT Server : public ClientHandler
{
    Q_OBJECT
    
    public:
        Server(QObject* parent);
        
        QStringList receiveChain() const;
        
        void start();
    signals:
        void globalMessage(const QString& message);
    protected:
        void createRoom(const QString& room);
        void closeRoom(const QString& room);
    protected slots:
        void metaserverFound();
        void metaserverConnected();
        void metaserverDisconnected();
        void metaserverError();
        
        void newConnection();
        
        void loadConfiguration();
        
        void connectToMetaserver();
    private:
        friend class ServerConnectionHandler;
        
        QTcpSocket* m_metaserver;
        QTcpServer* m_server;
        
        QMutex m_mutex;
        
        ConnectionInfo m_metaserverInfo;
        QString m_name;
        quint16 m_port;
        
        QMap<QString, Room*> m_rooms;
};
}

#endif
