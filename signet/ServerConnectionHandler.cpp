/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ServerConnectionHandler.h"

// Protocol includes
#include "protocol/ChallengeMediator.h"
#include "protocol/ConnectMediator.h"
#include "protocol/DisconnectMediator.h"
#include "protocol/Packet.h"
#include "protocol/PacketMaker.h"

// Signet includes
#include "Room.h"
#include "Server.h"

// Qt includes
#include <QtNetwork/QTcpSocket>

Signet::ServerConnectionHandler::ServerConnectionHandler(QTcpSocket* socket, Server* server) :
        QThread(server),
        m_socket(socket),
        m_server(server)
{
}

void Signet::ServerConnectionHandler::run()
{
    Protocol::Packet packet = Protocol::PacketMaker::unwrap(m_socket);
    const QString receiver = packet.received();
    if (receiver.startsWith(QString("server-")) && receiver.endsWith(m_server->m_name))
    {
        switch (packet.type())
        {
            case Protocol::Packet::ConnectRequest:
            {
                Protocol::ConnectMediator mediator(m_socket, Protocol::ConnectMediator::Server, m_server->receiveChain());
                Protocol::ConnectMediator::Error error = mediator.replyToConnection();
                switch (error)
                {
                    case Protocol::ConnectMediator::NoError:
                        qDebug("Client connected from %s:%d", m_socket->peerAddress().toString().toUtf8().constData(), m_socket->peerPort());
                        break;
                    case Protocol::ConnectMediator::SocketError:
                        qWarning("Socket error on connect");
                        break;
                    case Protocol::ConnectMediator::UnexpectedError:
                        qWarning("Unexpected packet from %s:%d on connect", m_socket->peerAddress().toString().toUtf8().constData(), m_socket->peerPort());
                        break;
                    case Protocol::ConnectMediator::SideError:
                        qCritical("Called the wrong method on connect");
                        break;
                    case Protocol::ConnectMediator::ReceiverError:
                        qCritical("Wrong destination for packet");
                        break;
                }
                break;
            }
            case Protocol::Packet::DisconnectRequest:
            {
                Protocol::DisconnectMediator mediator(m_socket, Protocol::DisconnectMediator::Server, m_server->receiveChain());
                Protocol::DisconnectMediator::Error error = mediator.replyToDisconnection();
                switch (error)
                {
                    case Protocol::DisconnectMediator::NoError:
                        qDebug("Client disconnected from %s:%d", m_socket->peerAddress().toString().toUtf8().constData(), m_socket->peerPort());
                        break;
                    case Protocol::DisconnectMediator::SocketError:
                        qWarning("Socket error on disconnect");
                        break;
                    case Protocol::DisconnectMediator::UnexpectedError:
                        qWarning("Unexpected packet from %s:%d on disconnect", m_socket->peerAddress().toString().toUtf8().constData(), m_socket->peerPort());
                        break;
                    case Protocol::DisconnectMediator::SideError:
                        qCritical("Called the wrong method on disconnect");
                        break;
                    case Protocol::DisconnectMediator::ReceiverError:
                        qCritical("Wrong destination for packet");
                        break;
                }
                break;
            }
            case Protocol::Packet::ChallengeRequest:
                // TODO
                break;
            case Protocol::Packet::UserListRequest:
                // TODO
                break;
            case Protocol::Packet::ServerListRequest:
                // TODO
                break;
            case Protocol::Packet::RoomListRequest:
                // TODO
                break;
            case Protocol::Packet::SigmodListRequest:
                // TODO
                break;
            case Protocol::Packet::SigmodRequest:
                // TODO
                break;
            default:
                Protocol::PacketMaker::unhandledRequest(m_socket, m_server->receiveChain());
                break;
        }
    }
    else
    {
        if (receiver.startsWith(QString("room-")))
        {
            const QString room = receiver.mid(receiver.indexOf('-') + 1);
            m_server->createRoom(room);
            if (m_server->m_rooms.contains(room))
                m_server->m_rooms[room]->packetReceived(m_socket, &packet);
            else
                qCritical("Failed to create room %s", room.toUtf8().constData());
        }
    }
}
