/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ConnectionInfo.h"

Signet::ConnectionInfo::ConnectionInfo()
{
}

void Signet::ConnectionInfo::load(const KConfigGroup& config)
{
}

bool Signet::ConnectionInfo::isValid() const
{
}

void Signet::ConnectionInfo::connectToHost(QTcpSocket* socket) const
{
}

Signet::Protocol::ChallengeMediator::Error Signet::ConnectionInfo::authenticate(const QString& username) const
{
}

KConfigGroup Signet::ConnectionInfo::config() const
{
}
