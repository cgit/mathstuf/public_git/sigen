/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Table.h"

// Protocol includes
#include "protocol/PacketMaker.h"

// Signet includes
#include "Room.h"

// Qt includes
#include <QtNetwork/QTcpSocket>

Signet::Table::Table(const QString& name, Room* room) :
        ClientHandler(room),
        m_room(room),
        m_name(name)
{
}

QStringList Signet::Table::receiveChain() const
{
    return m_room->receiveChain() << QString("table-").arg(m_name);
}

void Signet::Table::packetReceived(QTcpSocket* client, Protocol::Packet* packet)
{
    QString receiver = packet->received();
    if (receiver.isEmpty())
    {
        // TODO: Handle the packet
    }
    else
    {
        if (receiver == "spectators")
        {
        }
        else
            Protocol::PacketMaker::unknownReceiver(client);
    }
}
