/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ClientHandler.h"

// Signet includes
#include "Client.h"

Signet::ClientHandler::ClientHandler(QObject* parent) :
        QObject(parent)
{
}

bool Signet::ClientHandler::addClient(QTcpSocket* client)
{
    m_clients.append(client);
}

bool Signet::ClientHandler::removeClient(QTcpSocket* client)
{
    m_clients.removeAll(client);
}

QByteArray Signet::ClientHandler::secretForClient(const QByteArray& id) const
{
    Q_UNUSED(id)
    return QByteArray();
}
