!include "Sections.nsh"

Var AR_SecFlags
Var AR_RegFlags

!macro CheckDependencies SecName
    SectionGetFlags ${${SecName}} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    StrCmp $R0 $${SecName}_state +6
    StrCpy $${SecName}_state $R0
    StrCmp $${SecName}_state ${SF_SELECTED} +3
    Call deselect_${SecName}_dependents
    Goto checked_${SecName}
    Call select_${SecName}_dependencies
  checked_${SecName}:
!macroend

!macro UnselectDependent SecName
    SectionGetFlags ${${SecName}} $R0
    IntOp $R1 $R0 & ${SF_SELECTED}
    StrCmp $R1 ${SF_SELECTED} 0 +4
    IntOp $R0 $R0 ^ ${SF_SELECTED}
    SectionSetFlags ${${SecName}} $R0
    StrCpy $${SecName}_state 0
    Call deselect_${SecName}_dependents
!macroend

!macro SelectDependency SecName
    SectionGetFlags ${${SecName}} $R0
    IntOp $R1 $R0 & ${SF_SELECTED}
    StrCmp $R1 ${SF_SELECTED} +5
    IntOp $R0 $R0 | ${SF_SELECTED}
    SectionSetFlags ${${SecName}} $R0
    StrCpy $${SecName}_state ${SF_SELECTED}
    Call select_${SecName}_dependencies
!macroend

!macro DeclareStates SecName
    Var ${SecName}_state
!macroend

!macro InitStates SecName
    SectionGetFlags ${SecName} $R0
    IntOp $R0 $R0 & ${SF_SELECTED}
    StrCpy $${SecName}_state 0
!macroend

!macro InitSection SecName
    ClearErrors
    ReadRegDWORD $AR_RegFlags HKLM "${REG_UNINSTALL}\Components\${SecName}" "Installed"
    IfErrors "default_${SecName}"
    IntOp $AR_RegFlags $AR_RegFlags & 0x0001
    SectionGetFlags ${${SecName}} $AR_SecFlags
    IntOp $AR_SecFlags $AR_SecFlags & 0xFFFE
    IntOp $AR_SecFlags $AR_RegFlags | $AR_SecFlags
    SectionSetFlags ${${SecName}} $AR_SecFlags
  "default_${SecName}:"
!macroend

!macro FinishSection SecName
    SectionGetFlags ${${SecName}} $AR_SecFlags  ;Reading section flags
    IntOp $AR_SecFlags $AR_SecFlags & 0x0001
    IntCmp $AR_SecFlags 1 "leave_${SecName}"
    !insertmacro "Remove_${${SecName}}"
    WriteRegDWORD HKLM "${REG_UNINSTALL}\Components\${SecName}" "Installed" 0
    Goto "exit_${SecName}"
  "leave_${SecName}:"
    WriteRegDWORD HKLM "${REG_UNINSTALL}\Components\${SecName}" "Installed" 1
  "exit_${SecName}:"
!macroend

!macro RemoveSection SecName
    !insertmacro "Remove_${${SecName}}"
!macroend

!define InstFile "sigen.exe"
OutFile "${InstFile}"

!define REG_UNINSTALL "Software\Microsoft\Windows\CurrentVersion\Uninstall\Sigma Game Engine"

InstallDir "$PROGRAMFILES\Sigma Game Engine"
Name "Sigma Game Engine"
ComponentText "Check the components you want to add and uncheck the components you want to remove:"
ShowInstDetails show
ShowUnInstDetails show

Section "Required Files" required_files
    SectionIn RO
    
    CreateDirectory $INSTDIR
    WriteUninstaller "$INSTDIR\Uninstall.exe"
    
    WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayName" "Sigma Game Engine"
    WriteRegStr HKLM "${REG_UNINSTALL}" "UninstallString" "$INSTDIR\uninstall.exe"
    WriteRegStr HKLM "${REG_UNINSTALL}" "InstallLocation" "$INSTDIR"
    WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayIcon" "$INSTDIR\sigen.png"
    WriteRegStr HKLM "${REG_UNINSTALL}" "DisplayVersion" "0.1.0"
    WriteRegStr HKLM "${REG_UNINSTALL}" "Publisher" "Ben Boeckel"
    WriteRegStr HKLM "${REG_UNINSTALL}" "InstallSource" "$EXEDIR\"
    WriteRegStr HKLM "${REG_UNINSTALL}" "HelpLink" "http://www.sourceforge.net/projects/sigen"
    WriteRegDWORD HKLM "${REG_UNINSTALL}" "NoModify" 0
    WriteRegDWORD HKLM "${REG_UNINSTALL}" "NoRepair" 0
    WriteRegStr HKLM "${REG_UNINSTALL}" "Comments" "The Sigma Game Engine is an RPG/strategy game engine"
    WriteRegStr HKLM "${REG_UNINSTALL}" "ModifyPath" '"$EXEDIR\${InstFile}"'
SectionEnd
!macro Remove_${required_files}
    RMDir /R "$SMPROGRAMS\Sigma Game Engine"
    DeleteRegKey HKLM "${REG_UNINSTALL}"
!macroend

Section /o "Common sources" common_src
    DetailPrint "*** Installing common sources..."
    setOutPath "$INSTDIR\src"
    CreateDirectory "$INSTDIR\src"
    file CMakeLists.txt
    file cross-mingw32.cmake
    file LICENSE
SectionEnd
!macro Remove_${common_src}
    DetailPrint "*** Removing common sources..."
    RMDir /R "$INSTDIR\src"
!macroend

Section /o "Sigcore" sigcore_bin
    DetailPrint "*** Installing Sigcore..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\libsigcore.dll
SectionEnd
!macro Remove_${sigcore_bin}
    DetailPrint "*** Removing Sigcore..."
    Delete "$INSTDIR\bin\libsigcore.dll"
!macroend

Section /o "Sigcore sources" sigcore_src
    DetailPrint "*** Installing Sigcore sources..."
    setOutPath "$INSTDIR\src\sigcore"
    CreateDirectory "$INSTDIR\src\sigcore"
    file sigcore\CMakeLists.txt
    file sigcore\*.h
    file sigcore\*.cpp
SectionEnd
!macro Remove_${sigcore_src}
    DetailPrint "*** Removing Sigcore sources..."
    RMDir /R "$INSTDIR\src\sigcore"
!macroend

Section /o "Sigmod" sigmod_bin
    DetailPrint "*** Installing Sigmod..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\libsigmod.dll
    ; TODO
    setOutPath "$INSTDIR\"
    CreateDirectory "$INSTDIR\"
    file sigmod.knsrc
SectionEnd
!macro Remove_${sigmod_bin}
    DetailPrint "*** Removing Sigmod..."
    Delete "$INSTDIR\bin\libsigmod.dll"
    Delete "$INSTDIR\sigmod.knsrc"
!macroend

Section /o "Sigmod sources" sigmod_src
    DetailPrint "*** Installing Sigmod sources..."
    setOutPath "$INSTDIR\src\sigmod"
    CreateDirectory "$INSTDIR\src\sigmod"
    file sigmod\CMakeLists.txt
    file sigmod\*.h
    file sigmod\*.cpp
SectionEnd
!macro Remove_${sigmod_src}
    DetailPrint "*** Removing Sigmod sources..."
    RMDir /R "$INSTDIR\src\sigmod"
!macroend

Section /o "Sigmodr core widgets" sigmodrcorewidgets_bin
    DetailPrint "*** Installing Sigmodr core widgets..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\libsigmodrcorewidgets.dll
SectionEnd
!macro Remove_${sigmodrcorewidgets_bin}
    DetailPrint "*** Removing Sigmodr core widgets..."
    Delete "$INSTDIR\bin\libsigmodrcorewidgets.dll"
!macroend

Section /o "Sigmodr core widget sources" sigmodrcorewidgets_src
    DetailPrint "*** Installing Sigmodr core widget sources..."
    setOutPath "$INSTDIR\src\sigmodr\corewidgets"
    CreateDirectory "$INSTDIR\src\sigmodr\corewidgets"
    file sigmodr\corewidgets\CMakeLists.txt
    file sigmodr\corewidgets\*.h
    file sigmodr\corewidgets\*.cpp
    file sigmodr\corewidgets\*.ui
    file sigmodr\corewidgets\*.qrc
SectionEnd
!macro Remove_${sigmodrcorewidgets_src}
    DetailPrint "*** Removing Sigmodr core widget sources..."
    RMDir /r "$INSTDIR\src\sigmodr\corewidgets"
!macroend

Section /o "Sigmodr core widgets plugin" sigmodrcorewidgetsplugin_bin
    DetailPrint "*** Installing Sigmodr core widgets plugin..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\sigmodrcorewidgetsplugin.dll
SectionEnd
!macro Remove_${sigmodrcorewidgetsplugin_bin}
    DetailPrint "*** Removing Sigmodr core widgets plugin..."
    Delete "$INSTDIR\bin\sigmodrcorewidgetsplugin.dll"
!macroend

Section /o "Sigmodr core widget plugin sources" sigmodrcorewidgetsplugin_src
    DetailPrint "*** Installing Sigmodr core widget plugin sources..."
    setOutPath "$INSTDIR\src\sigmodr\corewidgets"
    CreateDirectory "$INSTDIR\src\sigmodr\corewidgets"
    file sigmodr\corewidgets\*.widgets
SectionEnd
!macro Remove_${sigmodrcorewidgetsplugin_src}
    DetailPrint "*** Removing Sigmodr core widget plugin sources..."
    RMDir /r "$INSTDIR\src\sigmodr\corewidgets\*.widgets"
!macroend

Section /o "Sigmodr widgets" sigmodrwidgets_bin
    DetailPrint "*** Installing Sigmodr widgets..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\libsigmodrwidgets.dll
SectionEnd
!macro Remove_${sigmodrwidgets_bin}
    DetailPrint "*** Removing Sigmodr widgets..."
    Delete "$INSTDIR\bin\libsigmodrwidgets.dll"
!macroend

Section /o "Sigmodr widget sources" sigmodrwidgets_src
    DetailPrint "*** Installing Sigmodr widget sources..."
    setOutPath "$INSTDIR\src\sigmodr\widgets"
    CreateDirectory "$INSTDIR\src\sigmodr\widgets"
    file sigmodr\widgets\CMakeLists.txt
    file sigmodr\widgets\*.h
    file sigmodr\widgets\*.cpp
    file sigmodr\widgets\gui\*.ui
    file sigmodr\widgets\gui\*.qrc
SectionEnd
!macro Remove_${sigmodrwidgets_src}
    DetailPrint "*** Removing Sigmodr widget sources..."
    RMDir /r "$INSTDIR\src\sigmodr\widgets"
!macroend

Section /o "Sigmodr tree" sigmodrtree_bin
    DetailPrint "*** Installing Sigmodr tree..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\libsigmodrtree.dll
SectionEnd
!macro Remove_${sigmodrtree_bin}
    DetailPrint "*** Removing Sigmodr tree..."
    Delete "$INSTDIR\bin\libsigmodrtree.dll"
!macroend

Section /o "Sigmodr tree sources" sigmodrtree_src
    DetailPrint "*** Installing Sigmodr tree sources..."
    setOutPath "$INSTDIR\src\sigmodr\tree"
    CreateDirectory "$INSTDIR\src\sigmodr\tree"
    file sigmodr\tree\CMakeLists.txt
    file sigmodr\tree\*.h
    file sigmodr\tree\*.cpp
SectionEnd
!macro Remove_${sigmodrtree_src}
    DetailPrint "*** Removing Sigmodr tree sources..."
    RMDir /r "$INSTDIR\src\sigmodr\tree"
!macroend

Section /o "Sigmodr" sigmodr_bin
    DetailPrint "*** Installing Sigmodr..."
    setOutPath "$INSTDIR\bin"
    CreateDirectory "$INSTDIR\bin"
    file bin\sigmodr.exe
    ; TODO
    setOutPath "$INSTDIR\"
    CreateDirectory "$INSTDIR\"
    file sigmodr\sigmodrui.rc
    createShortcut "$SMPROGRAMS\Sigma Game Engine\Sigmodr.lnk" "$INSTDIR\bin\sigmodr.exe"
SectionEnd
!macro Remove_${sigmodr_bin}
    DetailPrint "*** Removing Sigmodr..."
    Delete "$INSTDIR\bin\sigmodr.exe"
    Delete "$INSTDIR\sigmodrui.rc"
    Delete "$SMPROGRAMS\Sigma Game Engine\Sigmodr.lnk"
!macroend

Section /o "Sigmodr sources" sigmodr_src
    DetailPrint "*** Installing Sigmodr sources..."
    setOutPath "$INSTDIR\src\sigmodr"
    CreateDirectory "$INSTDIR\src\sigmodr"
    file sigmodr\CMakeLists.txt
    file sigmodr\Sigmodr.cpp
    file sigmodr\SigmodrPreferencesWidget.*
    file sigmodr\SigmodrUI.*
    file sigmodr\*.kcfg
    file sigmodr\*.ui
SectionEnd
!macro Remove_${sigmodr_src}
    DetailPrint "*** Removing Sigmodr sources..."
    RMDir /r "$INSTDIR\src\sigmodr"
!macroend

!macro SectionList MacroName
    !insertmacro "${MacroName}" "common_src"
    !insertmacro "${MacroName}" "sigcore_bin"
    !insertmacro "${MacroName}" "sigcore_src"
    !insertmacro "${MacroName}" "sigmod_bin"
    !insertmacro "${MacroName}" "sigmod_src"
    !insertmacro "${MacroName}" "sigmodrcorewidgets_bin"
    !insertmacro "${MacroName}" "sigmodrcorewidgets_src"
    !insertmacro "${MacroName}" "sigmodrcorewidgetsplugin_bin"
    !insertmacro "${MacroName}" "sigmodrcorewidgetsplugin_src"
    !insertmacro "${MacroName}" "sigmodrwidgets_bin"
    !insertmacro "${MacroName}" "sigmodrwidgets_src"
    !insertmacro "${MacroName}" "sigmodrtree_bin"
    !insertmacro "${MacroName}" "sigmodrtree_src"
    !insertmacro "${MacroName}" "sigmodr_bin"
    !insertmacro "${MacroName}" "sigmodr_src"
!macroend

!insertmacro SectionList "DeclareStates"

Function .onInit
    !insertmacro SectionList "InitStates"
    !insertmacro SectionList "InitSection"
FunctionEnd

Function deselect_common_src_dependents
    !insertmacro UnselectDependent sigcore_src
FunctionEnd

Function deselect_sigcore_src_dependents
    !insertmacro UnselectDependent sigmod_src
    !insertmacro UnselectDependent sigmodrcorewidgets_src
FunctionEnd

Function deselect_sigmod_src_dependents
    !insertmacro UnselectDependent sigmodrwidgets_src
FunctionEnd

Function deselect_sigmodrcorewidgets_src_dependents
    !insertmacro UnselectDependent sigmodrwidgets_src
    !insertmacro UnselectDependent sigmodrcorewidgetsplugin_src
FunctionEnd

Function deselect_sigmodrwidgets_src_dependents
    !insertmacro UnselectDependent sigmodrtree_src
FunctionEnd

Function deselect_sigmodrtree_src_dependents
    !insertmacro UnselectDependent sigmodr_src
FunctionEnd

Function deselect_sigmodrcorewidgetsplugin_src_dependents
    !insertmacro UnselectDependent sigmodr_src
FunctionEnd

Function deselect_sigmodr_src_dependents
FunctionEnd

Function deselect_sigcore_bin_dependents
    !insertmacro UnselectDependent sigmod_bin
    !insertmacro UnselectDependent sigmodrcorewidgets_bin
FunctionEnd

Function deselect_sigmod_bin_dependents
    !insertmacro UnselectDependent sigmodrwidgets_bin
FunctionEnd

Function deselect_sigmodrcorewidgets_bin_dependents
    !insertmacro UnselectDependent sigmodrwidgets_bin
    !insertmacro UnselectDependent sigmodrcorewidgetsplugin_bin
FunctionEnd

Function deselect_sigmodrwidgets_bin_dependents
    !insertmacro UnselectDependent sigmodrtree_bin
FunctionEnd

Function deselect_sigmodrtree_bin_dependents
    !insertmacro UnselectDependent sigmodr_bin
FunctionEnd

Function deselect_sigmodrcorewidgetsplugin_bin_dependents
    !insertmacro UnselectDependent sigmodr_bin
FunctionEnd

Function deselect_sigmodr_bin_dependents
FunctionEnd

Function select_common_src_dependencies
FunctionEnd

Function select_sigcore_src_dependencies
    !insertmacro SelectDependency common_src
FunctionEnd

Function select_sigmod_src_dependencies
    !insertmacro SelectDependency sigcore_src
FunctionEnd

Function select_sigmodrcorewidgets_src_dependencies
    !insertmacro SelectDependency sigcore_src
FunctionEnd

Function select_sigmodrwidgets_src_dependencies
    !insertmacro SelectDependency sigmod_src
    !insertmacro SelectDependency sigmodrcorewidgets_src
FunctionEnd

Function select_sigmodrtree_src_dependencies
    !insertmacro SelectDependency sigmodrwidgets_src
FunctionEnd

Function select_sigmodrcorewidgetsplugin_src_dependencies
    !insertmacro SelectDependency sigmodrcorewidgets_src
FunctionEnd

Function select_sigmodr_src_dependencies
    !insertmacro SelectDependency sigmodrcorewidgetsplugin_src
    !insertmacro SelectDependency sigmodrtree_src
FunctionEnd

Function select_sigcore_bin_dependencies
FunctionEnd

Function select_sigmod_bin_dependencies
    !insertmacro SelectDependency sigcore_bin
FunctionEnd

Function select_sigmodrcorewidgets_bin_dependencies
    !insertmacro SelectDependency sigcore_bin
FunctionEnd

Function select_sigmodrwidgets_bin_dependencies
    !insertmacro SelectDependency sigmod_bin
    !insertmacro SelectDependency sigmodrcorewidgets_bin
FunctionEnd

Function select_sigmodrtree_bin_dependencies
    !insertmacro SelectDependency sigmodrwidgets_bin
FunctionEnd

Function select_sigmodrcorewidgetsplugin_bin_dependencies
    !insertmacro SelectDependency sigmodrcorewidgets_bin
FunctionEnd

Function select_sigmodr_bin_dependencies
    !insertmacro SelectDependency sigmodrcorewidgetsplugin_bin
    !insertmacro SelectDependency sigmodrtree_bin
FunctionEnd

Function .onSelChange
    Push $R0
    Push $R1
    !insertmacro SectionList CheckDependencies
    Pop $R1
    Pop $R0
FunctionEnd

Section -FinishComponents
    !insertmacro SectionList "FinishSection"
SectionEnd

Section -Post
    ExecShell "open" "$INSTDIR"
SectionEnd

Section Uninstall
    !insertmacro SectionList "RemoveSection"
    
    RMDIR /r $INSTDIR
    DeleteRegKey HKLM "${REG_UNINSTALL}"
SectionEnd
