/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGWORLD_GLOBAL
#define SIGWORLD_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGWORLD_EXPORT
#   ifdef MAKE_SIGWORLD_LIB
#       define SIGWORLD_EXPORT KDE_EXPORT
#   else
#       define SIGWORLD_EXPORT KDE_IMPORT
#   endif
#   define SIGWORLD_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGWORLD_EXPORT_DEPRECATED
#   define SIGWORLD_EXPORT_DEPRECATED KDE_DEPRECATED SIGWORLD_EXPORT
#endif

#endif
