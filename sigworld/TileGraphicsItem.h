/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGWORLD_TILEGRAPHICSITEM
#define SIGWORLD_TILEGRAPHICSITEM

// Sigworld includes
#include "Global.h"

// Qt includes
#include <QtCore/QObject>
#include <QtGui/QGraphicsPixmapItem>

// Forward declarations
namespace Sigscript
{
class MapTileWrapper;
}

namespace Sigworld
{
class MapScene;

class SIGWORLD_EXPORT TileGraphicsItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    
    public:
        TileGraphicsItem(Sigscript::MapTileWrapper* tile, MapScene* scene);
        
        QPainterPath shape() const;
    public slots:
        void moveBy(qreal dx, qreal dy);
        
        void setSprite(const int spriteId);
    signals:
        void playerEntered();
    private:
        Sigscript::MapTileWrapper* m_tile;
};
}

#endif
