/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGWORLD_MAPSCENE
#define SIGWORLD_MAPSCENE

// Sigworld includes
#include "Global.h"

// Qt includes
#include <QtGui/QGraphicsScene>

// Forward declarations
namespace Kross
{
class ActionCollection;
}
namespace Sigscript
{
class MapWrapper;
}

namespace Sigworld
{
class SIGWORLD_EXPORT MapScene : public QGraphicsScene
{
    Q_OBJECT
    
    public:
        MapScene(Sigscript::MapWrapper* map, QObject* parent);
        
        QString makeTileName(const int id) const;
        
        Kross::ActionCollection* collection();
    private:
        Sigscript::MapWrapper* m_map;
        Kross::ActionCollection* m_collection;
};
}

#endif
