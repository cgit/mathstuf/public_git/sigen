/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TileGraphicsItem.h"

// Sigworld includes
#include "MapScene.h"

// Sigscript includes
#include "../sigscript/MapTileWrapper.h"
#include "../sigscript/SigmodWrapper.h"
#include "../sigscript/SpriteWrapper.h"
#include "../sigscript/TileWrapper.h"

// KDE includes
#include <kross/core/action.h>
#include <kross/core/actioncollection.h>

Sigworld::TileGraphicsItem::TileGraphicsItem(Sigscript::MapTileWrapper* tile, MapScene* scene) :
        QObject(scene),
        m_tile(tile)
{
    setPos(m_tile->position());
    Sigcore::Script script = m_tile->tile()->script();
    Kross::Action* action = new Kross::Action(scene->collection(), scene->makeTileName(m_tile->id()));
    action->addObject(this, "tile");
    action->setInterpreter(script.interpreter());
    action->setCode(script.script().toUtf8());
    action->trigger();
}

QPainterPath Sigworld::TileGraphicsItem::shape() const
{
    if (m_tile->tile()->walkable())
        return QPainterPath();
    return QGraphicsPixmapItem::shape();
}

void Sigworld::TileGraphicsItem::moveBy(qreal dx, qreal dy)
{
    QGraphicsPixmapItem::moveBy(dx, dy);
}

void Sigworld::TileGraphicsItem::setSprite(const int spriteId)
{
    setPixmap(m_tile->sigmod()->sprite(spriteId)->sprite());
}
