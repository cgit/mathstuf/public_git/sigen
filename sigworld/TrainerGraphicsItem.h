/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGWORLD_TRAINERGRAPHICSITEM
#define SIGWORLD_TRAINERGRAPHICSITEM

// Sigworld includes
#include "Global.h"

// Qt includes
#include <QtCore/QObject>
#include <QtGui/QGraphicsPixmapItem>

// Forward declarations
namespace Sigscript
{
class MapTrainerWrapper;
}

namespace Sigworld
{
class MapScene;

class SIGWORLD_EXPORT TrainerGraphicsItem : public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
    
    public:
        TrainerGraphicsItem(Sigscript::MapTrainerWrapper* tile, MapScene* scene);
        
        QRect boundingRect() const;
        QPainterPath shape() const;
    public slots:
        
};
}

#endif
