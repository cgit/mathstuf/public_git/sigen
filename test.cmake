set(TESTING OFF CACHE BOOL "Build tests")

if (TESTING)
    ENABLE_TESTING()
endif (TESTING)

macro (make_test testvariable valgrindvariable callgrindvariable libraries class)
    kde4_add_unit_test(test-${class}
        Test${class}.cpp
    )
    target_link_libraries(test-${class}
        ${QT_QTCORE_LIBRARY}
        ${QT_QTTEST_LIBRARY}
        ${${libraries}}
    )
    set(${testvariable}
        ${${testvariable}}
        test-${class}
    )
    if (UNIX)
        add_custom_target(valgrind-${class}
            COMMAND
                valgrind
                    --leak-check=full
                    --show-reachable=yes
                    --log-file=${EXECUTABLE_OUTPUT_PATH}/valgrind.${class}
                    bin/test-${class}
            DEPENDS
                test-${class}
            WORKING_DIRECTORY
                ${CMAKE_BINARY_DIR}
            COMMENT
                "Running valgrind on ${class}"
        )
        set(${valgrindvariable}
            ${${valgrindvariable}}
            valgrind-${class}
        )
        add_custom_target(callgrind-${class}
            COMMAND
                valgrind
                    --tool=callgrind
                    --dump-instr=yes
                    --trace-jump=yes
                    --log-file=${EXECUTABLE_OUTPUT_PATH}/callgrind.log.${class}
                    --callgrind-out-file=${EXECUTABLE_OUTPUT_PATH}/callgrind.out.${class}
                    bin/test-${class}
            DEPENDS
                test-${class}
            WORKING_DIRECTORY
                ${CMAKE_BINARY_DIR}
            COMMENT
                "Running callgrind on ${class}"
        )
        set(${callgrindvariable}
            ${${callgrindvariable}}
            callgrind-${class}
        )
    endif (UNIX)
endmacro (make_test)

macro (make_test_group groupname tests)
    add_custom_target(tests-${groupname}
        DEPENDS
            ${${tests}}
    )
endmacro (make_test_group)

macro (make_valgrind_group groupname tests)
    if (UNIX)
        add_custom_target(valgrind-${groupname}
            DEPENDS
                ${${tests}}
        )
    endif (UNIX)
endmacro (make_valgrind_group)

macro (make_callgrind_group groupname tests)
    if (UNIX)
        add_custom_target(callgrind-${groupname}
            DEPENDS
                ${${tests}}
        )
    endif (UNIX)
endmacro (make_callgrind_group)
