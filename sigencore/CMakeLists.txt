project(sigencore)

set(sigencore_HEADERS
    Arena.h
    Canvas.h
    Client.h
    Containment.h
    Creature.h
    Global.h
    Overworld.h
    Player.h
    RunScript.h
    Team.h
    TeamMember.h
)
set(sigencore_SRCS
    Arena.cpp
    Canvas.cpp
    Client.cpp
    Containment.cpp
    Creature.cpp
    Overworld.cpp
    Player.cpp
    RunScript.cpp
    Team.cpp
    TeamMember.cpp
)

kde4_add_library(sigencore
    SHARED
    ${sigencore_SRCS}
)
set_target_properties(sigencore
    PROPERTIES
        VERSION ${SIGEN_VERSION}
        SOVERSION ${SIGEN_SOVERSION}
)
target_link_libraries(sigencore
    ${QT_QTCORE_LIBRARY}
    ${QT_QTGUI_LIBRARY}
    ${KDE4_KROSSCORE_LIBRARY}
    sigcore
    sigscript
)
target_link_libraries(sigencore LINK_INTERFACE_LIBRARIES
    ${QT_QTCORE_LIBRARY}
    sigcore
    sigscript
)

add_subdirectory(plugins)

install(
    TARGETS
        sigencore
    EXPORT
        sigen_EXPORTS
    DESTINATION
        ${LIB_INSTALL_DIR}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigencore_HEADERS}
    DESTINATION
        ${INCLUDE_INSTALL_DIR}/${CMAKE_PROJECT_NAME}/${PROJECT_NAME}
    COMPONENT
        development
)
