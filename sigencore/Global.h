/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_GLOBAL
#define SIGENCORE_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGENCORE_EXPORT
#   ifdef MAKE_SIGENCORE_LIB
#       define SIGENCORE_EXPORT KDE_EXPORT
#   else
#       define SIGENCORE_EXPORT KDE_IMPORT
#   endif
#   define SIGENCORE_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGENCORE_EXPORT_DEPRECATED
#   define SIGENCORE_EXPORT_DEPRECATED KDE_DEPRECATED SIGENCORE_EXPORT
#endif

#endif
