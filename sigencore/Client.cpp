/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Client.h"

// Sigencore includes
#include "Canvas.h"

using namespace Sigscript;
using namespace Sigencore;

Client::Client(GameWrapper* game, Config* parent) :
        Config(parent),
        m_game(game),
        m_arena(NULL),
        m_world(NULL)
{
}

Client::~Client()
{
}

bool Client::enterArena(Arena* arena)
{
    m_arena = arena;
    return true;
}

void Client::exitArena()
{
    m_arena = NULL;
}

Arena* Client::arena()
{
    return m_arena;
}

bool Client::enterWorld(Overworld* world)
{
    m_world = world;
    return true;
}

void Client::exitWorld()
{
    m_world = NULL;
}

Overworld* Client::world()
{
    return m_world;
}

bool Client::addCanvas(const QString& name, Canvas* canvas)
{
    if (!m_canvases.contains(name))
    {
        m_canvases[name] = canvas;
        return true;
    }
    return false;
}

void Client::removeCanvas(const QString& canvas)
{
    if (m_canvases.contains(canvas))
        m_canvases[canvas]->cleanup();
}

Canvas* Client::canvas(const QString& canvas)
{
    if (m_canvases.contains(canvas))
        return m_canvases[canvas];
    return NULL;
}

void Client::postRequest(const int type, const QVariantList& arguments)
{
}
