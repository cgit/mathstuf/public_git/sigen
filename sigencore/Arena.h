/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_ARENA
#define SIGENCORE_ARENA

// Sigencore includes
#include "Global.h"
#include "TeamMember.h"

// Sigscript includes
#include <sigscript/Config.h>

// Qt includes
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QMetaType>
#include <QtCore/QPair>
#include <QtCore/QUuid>

// Forward declarations
class QGraphicsScene;
namespace Kross
{
class Action;
class ActionCollection;
}
namespace Sigcore
{
class Script;
}
namespace Sigscript
{
class GameWrapper;
class WeatherWrapper;
}

namespace Sigencore
{
class Client;
class Player;
class Team;

class SIGENCORE_EXPORT Arena : public Sigscript::Config
{
    Q_OBJECT
    Q_ENUMS(State)
    
    public:
        enum State
        {
            Setup = 0,
            InProgress = 1,
            Completed = 2
        };
        
        typedef QPair<int, int> Spoil;
        static const int Spectator;
        static const int Fighters;
        static const int AllTeams;
        static const int NoTeam;
        
        Q_SCRIPTABLE Sigscript::GameWrapper* game() const;
        
        Q_SCRIPTABLE QList<TeamMember*> active(const int team) const;
        Q_SCRIPTABLE QList<TeamMember*> active(Client* client) const;
        Q_SCRIPTABLE QList<Client*> teamPlayers(const int team) const;
        Q_SCRIPTABLE Player* player(TeamMember* teamMember) const;
        Q_SCRIPTABLE int team(Client* client) const;
        Q_SCRIPTABLE int team(TeamMember* teamMember) const;
        Q_SCRIPTABLE int numTeams() const;
        Q_SCRIPTABLE int numPlayers() const;
        Q_SCRIPTABLE int numTeamMembers() const;
        
        Q_SCRIPTABLE State state() const;
        
        Q_SCRIPTABLE QList<Sigscript::WeatherWrapper*> weathers() const;
        Q_SCRIPTABLE void setWeather(TeamMember* teamMember, Sigscript::WeatherWrapper* weather);
        Q_SCRIPTABLE bool unsetWeather(Sigscript::WeatherWrapper* weather);
    public slots:
        bool addClient(Client* client, const int team);
        void removeClient(Client* client);
    signals:
        void battleAboutToStart();
        void battleStarted();
        void battleAboutToEnd();
        void battleEnded();
        
        void cleanupArena();
        
        void aboutToClearActions();
        
        void weatherStarted(TeamMember* teamMember, Sigscript::WeatherWrapper* weather);
        void weatherEnded(Sigscript::WeatherWrapper* weather);
        
        void clientAdded(Sigencore::Client* client, const int team);
        void clientRemoved(Sigencore::Client* client, const int team);
    protected:
        Arena(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        virtual ~Arena();
        
        virtual void handleAction(TeamMember* teamMember, TeamMember::Action action);
        
        virtual bool isTeamAllowed(Team* team) = 0;
        
        virtual void setupBattle();
        
        virtual void distributeWinnings();
        virtual void checkForLosers();
        
        TeamMember* findMember(const QUuid& id);
        
        Sigscript::GameWrapper* m_game;
        QMap<Client*, int> m_teams;
        Kross::ActionCollection* m_actions;
        QMap<Sigscript::WeatherWrapper*, Kross::Action*> m_weathers;
        QMap<Player*, Spoil> m_spoils;
        QList<Player*> m_losers;
    protected slots:
        virtual void registerScript(const Sigcore::Script& script);
        
        virtual void cleanUp();
    private:
        State m_state;
        const QUuid m_id;
};

SIGENCORE_EXPORT TeamMember::RequestedAction requestDecision(TeamMember* teamMember);
SIGENCORE_EXPORT TeamMember::Action decision(Player* player, TeamMember* teamMember);

}
Q_DECLARE_METATYPE(Sigencore::Arena*)
Q_DECLARE_METATYPE(Sigencore::Arena::State)

#endif
