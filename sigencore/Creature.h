/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_CREATURE
#define SIGENCORE_CREATURE

// Sigencore includes
#include "Global.h"

// Sigscript includes
#include <sigscript/Config.h>

// Sigmod includes
#include <sigmod/Species.h>
#include <sigmod/Stat.h>

// Qt includes
#include <QtCore/QFuture>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QString>
#include <QtCore/QUuid>

// Forward declarations
namespace Sigcore
{
class Fraction;
}
namespace Sigscript
{
class AbilityWrapper;
class GameWrapper;
class ItemWrapper;
class MoveWrapper;
class NatureWrapper;
class SpeciesWrapper;
class StatusWrapper;
}

namespace Sigencore
{
class Containment;

class SIGENCORE_EXPORT Creature : public Sigscript::Config
{
    Q_OBJECT
    
    public:
        enum Gender
        {
            Undecided = -1,
            Male = 0,
            Female = 1,
            Genderless = 2
        };
        
        Creature(Sigscript::SpeciesWrapper* species, const int level, Containment* containment, const bool suppressInitialization = false);
        virtual ~Creature();
        
        Q_SCRIPTABLE QUuid id() const;
        
        Q_SCRIPTABLE Sigscript::GameWrapper* game() const;
        
        virtual bool setContainment(Containment* containment);
        Q_SCRIPTABLE Containment* containment() const;
        
        Q_SCRIPTABLE Sigscript::SpeciesWrapper* species() const;
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Gender gender() const;
        
        Q_SCRIPTABLE int level() const;
        static int calcLevel(const Sigmod::Species::Style growth, const long long levelExp);
        Q_SCRIPTABLE long long levelExperience() const;
        static long long calcLevelExperience(const Sigmod::Species::Style growth, const int level);
        
        Q_SCRIPTABLE int currentHp() const;
        Q_SCRIPTABLE int dv(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE long long statExperience(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE virtual int statValue(const Sigmod::Stat stat) const;
        static int calcStat(Sigscript::GameWrapper* game, const Sigmod::Stat stat, const int level, const int baseStat, const int dv, const int statExp, const Sigcore::Fraction& multiplier = Sigcore::Fraction(1, 1));
        
        Q_SCRIPTABLE QList<Sigscript::AbilityWrapper*> abilities() const;
        Q_SCRIPTABLE bool hasAbility(Sigscript::AbilityWrapper* ability) const;
        
        Q_SCRIPTABLE QList<Sigscript::ItemWrapper*> items() const;
        Q_SCRIPTABLE int itemWeight() const;
        Q_SCRIPTABLE int itemCount() const;
        Q_SCRIPTABLE int hasItem(Sigscript::ItemWrapper* item) const;
        
        Q_SCRIPTABLE QList<Sigscript::MoveWrapper*> moves() const;
        Q_SCRIPTABLE bool hasMove(Sigscript::MoveWrapper* move) const;
        
        Q_SCRIPTABLE QList<Sigscript::NatureWrapper*> natures() const;
        Q_SCRIPTABLE bool hasNature(Sigscript::NatureWrapper* nature) const;
        
        Q_SCRIPTABLE QList<Sigscript::StatusWrapper*> status() const;
        Q_SCRIPTABLE bool hasStatus(Sigscript::StatusWrapper* status) const;
        
        virtual void completeData();
    public slots:
        void setName(const QString& name);
        bool setGender(const Gender gender);
        
        bool setLevel(const int level);
        bool giveLevels(const int levels);
        bool setLevelExperience(const long long levelExp);
        bool giveLevelExperience(const long long levelExp);
        
        bool setCurrentHp(const int hp);
        bool changeCurrentHp(const int hp);
        
        bool setDv(const Sigmod::Stat stat, const int dv);
        bool setStatExperience(const Sigmod::Stat stat, const long long statExp);
        bool giveStatExperience(const Sigmod::Stat stat, const long long statExp);
        
        virtual bool addAbility(Sigscript::AbilityWrapper* ability);
        virtual bool removeAbility(Sigscript::AbilityWrapper* ability);
        
        virtual bool addItems(Sigscript::ItemWrapper* item, const int count, const bool allOrNothing = false);
        
        virtual bool addMove(Sigscript::MoveWrapper* move);
        virtual bool removeMove(Sigscript::MoveWrapper* move);
        
        virtual bool addNature(Sigscript::NatureWrapper* nature);
        virtual bool removeNature(Sigscript::NatureWrapper* nature);
        
        virtual bool addStatus(Sigscript::StatusWrapper* status);
        virtual bool removeStatus(Sigscript::StatusWrapper* status);
    signals:
        void nameChanged(const QString& newName);
        void genderChanged(const Gender newGender);
        
        void levelChanged(const int newLevel);
        void levelExperienceChanged(const int newExp);
        
        void currentHpChanged(const int newHp);
        void knockedOut();
        void dvChanged(const Sigmod::Stat stat, const int newDv);
        void statExperienceChanged(const Sigmod::Stat stat, const long long newStatExp);
        void statValueChanged(const Sigmod::Stat stat, const int newValue);
        
        void abilityAdded(Sigscript::AbilityWrapper* ability);
        void abilityRemoved(Sigscript::AbilityWrapper* ability);
        
        void itemsAdded(Sigscript::ItemWrapper* item, const int count);
        
        void moveAdded(Sigscript::MoveWrapper* move);
        void moveRemoved(Sigscript::MoveWrapper* move);
        
        void natureAdded(Sigscript::NatureWrapper* nature);
        void natureRemoved(Sigscript::NatureWrapper* nature);
        
        void statusAdded(Sigscript::StatusWrapper* status);
        void statusRemoved(Sigscript::StatusWrapper* status);
        
        void initialized();
    protected:
        virtual void makeConnections();
        
        virtual void completeStats();
        virtual void completeAbilities();
        virtual void completeItems();
        virtual void completeMoves();
        virtual void completeNatures();
        
        Sigscript::GameWrapper* m_game;
        Containment* m_containment;
        
        Sigscript::SpeciesWrapper* m_species;
        
        QString m_name;
        Gender m_gender;
        
        int m_level;
        long long m_levelExp;
        
        int m_currentHp;
        int m_dv[Sigmod::ST_SpecialDefense - Sigmod::ST_HP + 1];
        long long m_statExp[Sigmod::ST_SpecialDefense - Sigmod::ST_HP + 1];
        int m_stages[Sigmod::ST_Evasion - Sigmod::ST_Attack + 1];
        
        QList<Sigscript::AbilityWrapper*> m_abilities;
        QMap<Sigscript::ItemWrapper*, int> m_items;
        QList<Sigscript::MoveWrapper*> m_moves;
        QList<Sigscript::NatureWrapper*> m_natures;
        QList<Sigscript::StatusWrapper*> m_status;
        
        typedef QPair<Sigmod::Species::Style, int> StyleLevel;
        static QMap<StyleLevel, long long> m_expCache;
    protected slots:
        void recalcStats();
        void recalcStat(const Sigmod::Stat stat);
    private:
        const QUuid m_id;
};
}
Q_DECLARE_METATYPE(Sigencore::Creature*)
Q_DECLARE_METATYPE(Sigencore::Creature::Gender)

#endif
