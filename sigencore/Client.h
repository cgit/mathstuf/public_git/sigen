/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_CLIENT
#define SIGENCORE_CLIENT

// Sigencore includes
#include "Global.h"

// Sigscript includes
#include <sigscript/Config.h>

// Forward declarations
namespace Sigscript
{
class GameWrapper;
}

namespace Sigencore
{
class Arena;
class Canvas;
class Overworld;

class SIGENCORE_EXPORT Client : public Sigscript::Config
{
    Q_OBJECT
    Q_ENUMS(ClientRequestType)
    
    public:
        enum ClientRequestType
        {
        };
        
        Q_SCRIPTABLE Arena* arena();
        Q_SCRIPTABLE Overworld* world();
        Q_SCRIPTABLE Canvas* canvas(const QString& canvas);
    public slots:
        virtual bool enterWorld(Overworld* world);
        virtual void exitWorld();
        
        virtual bool enterArena(Arena* arena);
        virtual void exitArena();
        
        virtual bool addCanvas(const QString& name, Canvas* canvas);
        virtual void removeCanvas(const QString& canvas);
        
        virtual void postRequest(const int type, const QVariantList& arguments = QVariantList());
    signals:
        void postReply(const int type, const QVariantList& arguments = QVariantList());
        
        void cleanupClient();
    protected:
        Client(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        virtual ~Client();
        
        Sigscript::GameWrapper* m_game;
        Arena* m_arena;
        Overworld* m_world;
        QMap<QString, Canvas*> m_canvases;
};
}

#endif
