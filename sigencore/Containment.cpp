/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Containment.h"

// Sigencore includes
#include "Creature.h"

// Sigscript includes
#include <sigscript/GameWrapper.h>
#include <sigscript/RulesWrapper.h>

using namespace Sigscript;
using namespace Sigencore;

Containment::Containment(GameWrapper* game, Config* parent) :
        Config(parent),
        m_game(game)
{
}

Containment::~Containment()
{
}

QList<Creature*> Containment::members() const
{
    return m_members;
}

int Containment::numMembers() const
{
    return m_members.size();
}

bool Containment::addMember(Creature* member)
{
    member->setContainment(this);
    m_members.append(member);
    emit(memberAdded(member, m_members.size() - 1));
    return true;
}

bool Containment::removeMember(Creature* member)
{
    // TODO
    return false;
}

Creature* Containment::findMember(const QUuid& id) const
{
    foreach (Creature* member, m_members)
    {
        if (member->id() == id)
            return member;
    }
    return NULL;
}

bool Containment::reorder(const int from, const int to)
{
    if ((from < m_members.size()) && (to < m_members.size()))
    {
        Creature* member = m_members[from];
        m_members.insert(to, m_members.takeAt(from));
        emit(memberMoved(member, from, to));
        return true;
    }
    return false;
}

GameWrapper* Containment::game() const
{
    return m_game;
}
