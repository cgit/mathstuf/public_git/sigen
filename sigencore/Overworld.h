/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_OVERWORLD
#define SIGENCORE_OVERWORLD

// Sigencore includes
#include "Global.h"

// Sigscript includes
#include <sigscript/Config.h>

// Forward declarations
namespace Sigscript
{
class GameWrapper;
}

namespace Sigencore
{
class SIGENCORE_EXPORT Overworld : public Sigscript::Config
{
    public:
        Overworld(Sigscript::GameWrapper* game, Sigscript::Config* parent);
};
}

#endif
