/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "RunScript.h"

// Sigscript includes
#include <sigscript/GameWrapper.h>
#include <sigscript/GlobalScriptWrapper.h>

// Sigcore includes
#include <sigcore/Script.h>

// KDE includes
#include <kross/core/action.h>
#include <kross/core/actioncollection.h>

using namespace Sigcore;
using namespace Sigscript;
using namespace Sigencore;

Kross::Action* Sigencore::runScript(const QString& name, const Script& script, const ObjectMap& objects, Kross::ActionCollection* collection)
{
    Kross::Action* action = new Kross::Action(collection, name);
    action->setInterpreter(script.interpreter());
    action->setCode(script.script().toUtf8());
    QStringList names = objects.keys();
    foreach (const QString& name, names)
        action->addObject(objects[name], name);
    return action;
}

Kross::Action* Sigencore::globalScript(GameWrapper* game, const QString& name, const QString& scriptName, const ObjectMap& objects, Kross::ActionCollection* collection)
{
    GlobalScriptWrapper* script = game->globalScript(name);
    if (script)
        return runScript(scriptName, script->script(), objects, collection);
    return NULL;
}
