/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCOREPLUGINS_ARENAPLUGIN
#define SIGENCOREPLUGINS_ARENAPLUGIN

// Plugin includes
#include "PluginBase.h"

// KDE includes
#include <KPluginFactory>

// Forward declarations
namespace Sigscript
{
class Config;
class GameWrapper;
}

namespace Sigencore
{
class Arena;

namespace Plugins
{
class SIGENCOREPLUGINS_EXPORT ArenaPlugin : public PluginBase
{
    Q_OBJECT
    
    public:
        ArenaPlugin(QObject* parent, const QVariantList& args);
        virtual ~ArenaPlugin();
        
        Arena* getArena(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent);
        
        virtual QStringList classList() const = 0;
        virtual QString description(const QString& name) const = 0;
        virtual QIcon icon(const QString& name) const = 0;
        virtual QStringList extensions(const QString& name) const = 0;
    protected:
        virtual Arena* createArena(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent) = 0;
    protected slots:
        virtual void cleanupArena(Sigencore::Arena* arena) = 0;
    private:
        class Private;
        Private* const d;
};
}
}

#define SIGEN_ARENA_PLUGIN(type, name) \
    K_PLUGIN_FACTORY(ArenaFactory, registerPlugin<type>();) \
    K_EXPORT_PLUGIN(ArenaFactory(name))

#endif
