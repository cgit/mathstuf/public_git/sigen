project(plugins)

set(sigencoreplugins_HEADERS
    ArenaPlugin.h
    CanvasPlugin.h
    ClientPlugin.h
    PluginBase.h
    Global.h
)
set(sigencoreplugins_SRCS
    ArenaPlugin.cpp
    CanvasPlugin.cpp
    ClientPlugin.cpp
    PluginBase.cpp
)
set(sigencoreplugins_SERVICETYPES
    sigen_arena.desktop
    sigen_canvas.desktop
    sigen_client.desktop
)

kde4_add_library(sigencoreplugins
    SHARED
    ${sigencoreplugins_SRCS}
)
set_target_properties(sigencoreplugins
    PROPERTIES
        VERSION ${SIGEN_VERSION}
        SOVERSION ${SIGEN_SOVERSION}
)
target_link_libraries(sigencoreplugins
    sigencore
)
target_link_libraries(sigencoreplugins LINK_INTERFACE_LIBRARIES
    sigencore
)

install(
    TARGETS
        sigencoreplugins
    EXPORT
        sigen_EXPORTS
    DESTINATION
        ${LIB_INSTALL_DIR}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigencoreplugins_HEADERS}
    DESTINATION
        ${INCLUDE_INSTALL_DIR}/${CMAKE_PROJECT_NAME}/sigencore/${PROJECT_NAME}
    COMPONENT
        development
)

install(
    FILES
        ${sigencoreplugins_SERVICETYPES}
    DESTINATION
        ${SERVICETYPES_INSTALL_DIR}
    COMPONENT
        runtime
)
