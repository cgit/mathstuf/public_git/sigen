/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "CanvasPlugin.h"
#include "CanvasPlugin_p.h"

// Sigencore includes
#include <sigencore/Canvas.h>

// Qt includes
#include <QtCore/QSignalMapper>

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

CanvasPlugin::CanvasPlugin(QObject* parent, const QVariantList& args) :
        PluginBase(parent, args),
        d(new Private(this, args))
{
}

CanvasPlugin::~CanvasPlugin()
{
    delete d;
}

Canvas* CanvasPlugin::getCanvas(const QString& name, GameWrapper* game, Config* parent)
{
    Canvas* canvas = createCanvas(name, game, parent);
    if (canvas)
    {
        ++m_count;
        d->addCanvas(canvas);
    }
    return canvas;
}

CanvasPlugin::Private::Private(CanvasPlugin* plugin, const QVariantList& args) :
        q(plugin),
        m_mapper(new QSignalMapper(this))
{
    Q_UNUSED(args)
    connect(m_mapper, SIGNAL(mapped(QObject*)), this, SLOT(cleanupCanvas(QObject*)));
}

CanvasPlugin::Private::~Private()
{
}

void CanvasPlugin::Private::addCanvas(Canvas* canvas)
{
    connect(canvas, SIGNAL(cleanupCanvas()), m_mapper, SLOT(map()));
    m_mapper->setMapping(canvas, canvas);
}

void CanvasPlugin::Private::cleanupCanvas(QObject* object)
{
    Canvas* canvas = qobject_cast<Canvas*>(object);
    if (canvas)
    {
        q->cleanupCanvas(canvas);
        --q->m_count;
    }
}
