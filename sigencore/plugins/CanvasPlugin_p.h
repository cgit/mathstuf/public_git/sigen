/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCOREPLUGINS_CANVASPLUGIN_P
#define SIGENCOREPLUGINS_CANVASPLUGIN_P

// Header include
#include "CanvasPlugin.h"

// Forward declarations
class QSignalMapper;

namespace Sigencore
{
namespace Plugins
{
class SIGENCOREPLUGINS_NO_EXPORT CanvasPlugin::Private : public QObject
{
    Q_OBJECT
    
    public:
        Private(CanvasPlugin* plugin, const QVariantList& args);
        ~Private();
        
        void addCanvas(Canvas* canvas);
    private:
        CanvasPlugin* const q;
        QSignalMapper* m_mapper;
    private slots:
        void cleanupCanvas(QObject* object);
};
}
}

#endif
