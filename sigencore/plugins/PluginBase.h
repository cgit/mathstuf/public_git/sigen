/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCOREPLUGINS_PLUGINBASE
#define SIGENCOREPLUGINS_PLUGINBASE

// Sigencore plugin includes
#include "Global.h"

// Qt includes
#include <QtCore/QStringList>
#include <QtCore/QVariantList>
#include <QtGui/QIcon>

namespace Sigencore
{
namespace Plugins
{
class SIGENCOREPLUGINS_EXPORT PluginBase : public QObject
{
    Q_OBJECT
    
    public:
        PluginBase(QObject* parent, const QVariantList& args);
        virtual ~PluginBase();
        
        virtual QStringList classList() const = 0;
        virtual QString description(const QString& name) const = 0;
        virtual QIcon icon(const QString& name) const = 0;
        virtual QStringList extensions(const QString& name) const = 0;
        
        int classesUsedCount() const;
    protected:
        int m_count;
};
}
}

#endif
