/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ClientPlugin.h"
#include "ClientPlugin_p.h"

// Sigencore includes
#include <sigencore/Client.h>

// Qt includes
#include <QtCore/QSignalMapper>

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

ClientPlugin::ClientPlugin(QObject* parent, const QVariantList& args) :
        PluginBase(parent, args),
        d(new Private(this, args))
{
}

ClientPlugin::~ClientPlugin()
{
    delete d;
}

Client* ClientPlugin::getClient(const QString& name, GameWrapper* game, Config* parent)
{
    Client* client = createClient(name, game, parent);
    if (client)
    {
        ++m_count;
        d->addClient(client);
    }
    return client;
}

ClientPlugin::Private::Private(ClientPlugin* plugin, const QVariantList& args) :
        q(plugin),
        m_mapper(new QSignalMapper(this))
{
    Q_UNUSED(args)
    connect(m_mapper, SIGNAL(mapped(QObject*)), this, SLOT(cleanupClient(QObject*)));
}

ClientPlugin::Private::~Private()
{
}

void ClientPlugin::Private::addClient(Client* client)
{
    connect(client, SIGNAL(cleanupClient()), m_mapper, SLOT(map()));
    m_mapper->setMapping(client, client);
}

void ClientPlugin::Private::cleanupClient(QObject* object)
{
    Client* client = qobject_cast<Client*>(object);
    if (client)
    {
        q->cleanupClient(client);
        --q->m_count;
    }
}
