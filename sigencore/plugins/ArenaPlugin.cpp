/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ArenaPlugin.h"
#include "ArenaPlugin_p.h"

// Sigencore includes
#include <sigencore/Arena.h>

// Qt includes
#include <QtCore/QSignalMapper>

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

ArenaPlugin::ArenaPlugin(QObject* parent, const QVariantList& args) :
        PluginBase(parent, args),
        d(new Private(this, args))
{
}

ArenaPlugin::~ArenaPlugin()
{
    delete d;
}

Arena* ArenaPlugin::getArena(const QString& name, GameWrapper* game, Config* parent)
{
    Arena* arena = createArena(name, game, parent);
    if (arena)
    {
        ++m_count;
        d->addArena(arena);
    }
    return arena;
}

ArenaPlugin::Private::Private(ArenaPlugin* plugin, const QVariantList& args) :
        q(plugin),
        m_mapper(new QSignalMapper(this))
{
    Q_UNUSED(args)
    connect(m_mapper, SIGNAL(mapped(QObject*)), this, SLOT(cleanupArena(QObject*)));
}

ArenaPlugin::Private::~Private()
{
}

void ArenaPlugin::Private::addArena(Arena* arena)
{
    connect(arena, SIGNAL(cleanupArena()), m_mapper, SLOT(map()));
    m_mapper->setMapping(arena, arena);
}

void ArenaPlugin::Private::cleanupArena(QObject* object)
{
    Arena* arena = qobject_cast<Arena*>(object);
    if (arena)
    {
        q->cleanupArena(arena);
        --q->m_count;
    }
}
