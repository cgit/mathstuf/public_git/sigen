/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCOREPLUGINS_ARENAPLUGIN_P
#define SIGENCOREPLUGINS_ARENAPLUGIN_P

// Header include
#include "ArenaPlugin.h"

// Forward declarations
class QSignalMapper;

namespace Sigencore
{
namespace Plugins
{
class SIGENCOREPLUGINS_NO_EXPORT ArenaPlugin::Private : public QObject
{
    Q_OBJECT
    
    public:
        Private(ArenaPlugin* plugin, const QVariantList& args);
        ~Private();
        
        void addArena(Arena* arena);
    private:
        ArenaPlugin* const q;
        QSignalMapper* m_mapper;
    private slots:
        void cleanupArena(QObject* object);
};
}
}

#endif
