/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCOREPLUGINS_CANVASPLUGIN
#define SIGENCOREPLUGINS_CANVASPLUGIN

// Plugin includes
#include "PluginBase.h"

// KDE includes
#include <KPluginFactory>

// Forward declarations
namespace Sigscript
{
class Config;
class GameWrapper;
}

namespace Sigencore
{
class Canvas;

namespace Plugins
{
class SIGENCOREPLUGINS_EXPORT CanvasPlugin : public PluginBase
{
    Q_OBJECT
    
    public:
        CanvasPlugin(QObject* parent, const QVariantList& args);
        virtual ~CanvasPlugin();
        
        Canvas* getCanvas(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent);
        
        virtual QStringList classList() const = 0;
        virtual QString description(const QString& name) const = 0;
        virtual QIcon icon(const QString& name) const = 0;
        virtual QStringList extensions(const QString& name) const = 0;
    protected:
        virtual Canvas* createCanvas(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent) = 0;
    protected slots:
        virtual void cleanupCanvas(Sigencore::Canvas* arena) = 0;
    private:
        class Private;
        Private* const d;
};
}
}

#define SIGEN_CANVAS_PLUGIN(type, name) \
    K_PLUGIN_FACTORY(CanvasFactory, registerPlugin<type>();) \
    K_EXPORT_PLUGIN(CanvasFactory(name))

#endif
