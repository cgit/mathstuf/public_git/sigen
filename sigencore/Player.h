/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_PLAYER
#define SIGENCORE_PLAYER

// Sigencore includes
#include "Client.h"
#include "Global.h"
#include "TeamMember.h"

// Qt includes
#include <QtCore/QMetaType>

// Forward declarations
namespace Sigscript
{
class ItemWrapper;
}

namespace Sigencore
{
class Team;

class SIGENCORE_EXPORT Player : public Client
{
    Q_OBJECT
    Q_ENUMS(PlayerRequestType)
    
    public:
        enum PlayerRequestType
        {
        };
        
        Player(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        virtual ~Player();
        
        Q_SCRIPTABLE Team* team() const;
        Q_SCRIPTABLE TeamMember* findMember(const QUuid& id) const;
        Q_SCRIPTABLE QList<TeamMember*> active() const;
        Q_SCRIPTABLE void switchOut(TeamMember* oldActive, TeamMember* newActive);
        
        Q_SCRIPTABLE bool isKnockedOut() const;
        
        Q_SCRIPTABLE int itemWeightTotal(const bool distinct = true) const;
        Q_SCRIPTABLE int itemWeightType(const int type = -1, const bool distinct = true) const;
        Q_SCRIPTABLE int itemWeight(const int item = -1, const bool distinct = true) const;
        Q_SCRIPTABLE int itemCountTotal(const bool distinct = true) const;
        Q_SCRIPTABLE int itemCountType(const int type = -1, const bool distinct = true) const;
        Q_SCRIPTABLE int itemCount(const int item = -1, const bool distinct = true) const;
        Q_SCRIPTABLE QMap<Sigscript::ItemWrapper*, int> items(const int type = -1) const;
        
        Q_SCRIPTABLE int money() const;
    public slots:
        virtual bool enterArena(Arena* arena);
        virtual void exitArena();
        
        virtual bool enterWorld(Overworld* world);
        virtual void exitWorld();
        
        virtual void postRequest(const int type, const QVariantList& arguments);
        
        int giveItems(Sigscript::ItemWrapper* item, const int count, const bool allOrNothing = true);
        
        bool giveMoney(const int amount, const bool allOrNothing = true);
        
        virtual TeamMember::Action requestAction(const TeamMember* teamMember) const = 0;
    signals:
        void itemsGiven(Sigscript::ItemWrapper* item, const int);
        
        void moneyChanged(const int money);
    protected:
        Team* m_team;
        QList<TeamMember*> m_active;
        QMap<Sigscript::ItemWrapper*, int> m_items;
        int m_money;
};
}
Q_DECLARE_METATYPE(Sigencore::Player*)

#endif
