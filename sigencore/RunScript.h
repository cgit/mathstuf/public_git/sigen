/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_RUNSCRIPT
#define SIGENCORE_RUNSCRIPT

// Sigencore includes
#include "Global.h"

// Qt includes
#include <QtCore/QMap>

// Forward declarations
class QObject;
class QString;
namespace Kross
{
class Action;
class ActionCollection;
}
namespace Sigcore
{
class Script;
}
namespace Sigscript
{
class GameWrapper;
}

namespace Sigencore
{
typedef QMap<QString, QObject*> ObjectMap;

SIGENCORE_EXPORT Kross::Action* runScript(const QString& name, const Sigcore::Script& script, const ObjectMap& objects, Kross::ActionCollection* collection);
SIGENCORE_EXPORT Kross::Action* globalScript(Sigscript::GameWrapper* game, const QString& name, const QString& scriptName, const ObjectMap& objects, Kross::ActionCollection* collection);

}

#endif
