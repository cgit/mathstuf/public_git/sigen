/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_TEAM
#define SIGENCORE_TEAM

// Sigencore includes
#include "Containment.h"

// FIXME: C++0x - delete inherited non-team member methods

// Forward declarations
namespace Sigscript
{
class GameWrapper;
}

namespace Sigencore
{
class Arena;
class Player;
class TeamMember;

class SIGENCORE_EXPORT Team : public Containment
{
    Q_OBJECT
    
    public:
        explicit Team(Sigscript::GameWrapper* game, Player* player = NULL);
        virtual ~Team();
        
        QList<TeamMember*> teamMembers() const;
        int numTeamMembers() const;
        virtual bool addTeamMember(TeamMember* teamMember);
        virtual bool removeTeamMember(TeamMember* teamMember);
        TeamMember* findTeamMember(const QUuid& id) const;
        bool reorder(const int from, const int to);
        
        bool isKnockedOut() const;
        
        void setPlayer(Player* player);
        Player* player() const;
    signals:
        void teamMemberAdded(TeamMember* teamMember, const int position);
        void teamMemberMoved(TeamMember* teamMember, const int from, const int to);
        
        void teamKnockedOut();
    protected:
        Arena* m_arena;
        Player* m_player;
        QList<TeamMember*> m_teamMembers;
};
}
Q_DECLARE_METATYPE(Sigencore::Team*)

#endif
