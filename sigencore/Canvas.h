/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_CANVAS
#define SIGENCORE_CANVAS

// Sigencore includes
#include "Global.h"

// Sigscript includes
#include <sigscript/Config.h>

namespace Sigencore
{
class SIGENCORE_EXPORT Canvas : public Sigscript::Config
{
    Q_OBJECT
    Q_ENUMS(Type)
    
    public:
        enum Type
        {
            QGraphicsSceneCanvas = 0,
            KGLEngineCanvas = 1,
            OpenGLCanvas = 2,
            
            CustomCanvas = 50
        };
        
        Q_SCRIPTABLE virtual void addItem(const QString& name, const QString& context, const QVariantList& parameters) = 0;
        Q_SCRIPTABLE virtual void removeItem(const QString& name) = 0;
        Q_SCRIPTABLE virtual void transform(const QString& transform, const QString& name, const QVariantList& parameters) = 0;
        
        Q_SCRIPTABLE virtual void cleanup();
        
        virtual int type() const = 0;
        
        virtual QWidget* viewport() = 0;
    signals:
        void cleanupCanvas();
    protected:
        Canvas(Sigscript::Config* parent);
        virtual ~Canvas();
};
}

#endif
