/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Team.h"

// Sigencore includes
#include "Player.h"
#include "TeamMember.h"

// Sigscript includes
#include <sigscript/GameWrapper.h>
#include <sigscript/RulesWrapper.h>

using namespace Sigscript;
using namespace Sigencore;

Team::Team(GameWrapper* game, Player* player) :
        Containment(game, player),
        m_arena(NULL),
        m_player(player)
{
}

Team::~Team()
{
}

QList<TeamMember*> Team::teamMembers() const
{
    return m_teamMembers;
}

int Team::numTeamMembers() const
{
    return m_teamMembers.size();
}

bool Team::addTeamMember(TeamMember* teamMember)
{
    if (m_teamMembers.size() < m_game->rules()->maxParty())
    {
        teamMember->setTeam(this);
        m_teamMembers.append(teamMember);
        emit(teamMemberAdded(teamMember, m_teamMembers.size() - 1));
        Containment::addMember(teamMember);
        return true;
    }
    return false;
}

bool Team::removeTeamMember(TeamMember* teamMember)
{
    // TODO
    return false;
}

TeamMember* Team::findTeamMember(const QUuid& id) const
{
    return qobject_cast<TeamMember*>(Containment::findMember(id));
}

bool Team::reorder(const int from, const int to)
{
    if ((from < m_teamMembers.size()) && (to < m_teamMembers.size()))
    {
        TeamMember* teamMember = m_teamMembers[from];
        m_teamMembers.insert(to, m_teamMembers.takeAt(from));
        emit(teamMemberMoved(teamMember, from, to));
        Containment::reorder(from, to);
        return true;
    }
    return false;
}

bool Team::isKnockedOut() const
{
    foreach (TeamMember* teamMember, m_teamMembers)
    {
        if (teamMember->currentHp())
            return false;
    }
    return true;
}

void Team::setPlayer(Player* player)
{
    // TODO: Unlink m_player with this
    m_player = player;
}

Player* Team::player() const
{
    return m_player;
}
