/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_CONTAINMENT
#define SIGENCORE_CONTAINMENT

// Sigencore includes
#include "Global.h"

// Sigscript includes
#include <sigscript/Config.h>

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QList>

// Forward declarations
class QUuid;
namespace Sigscript
{
class GameWrapper;
}

namespace Sigencore
{
class Creature;

class SIGENCORE_EXPORT Containment : public Sigscript::Config
{
    Q_OBJECT
    
    public:
        Containment(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        virtual ~Containment();
        
        QList<Creature*> members() const;
        int numMembers() const;
        virtual bool addMember(Creature* member);
        virtual bool removeMember(Creature* member);
        Creature* findMember(const QUuid& id) const;
        virtual bool reorder(const int from, const int to);
        
        Sigscript::GameWrapper* game() const;
    signals:
        void memberAdded(Creature* member, const int position);
        void memberMoved(Creature* member, const int from, const int to);
    protected:
        Sigscript::GameWrapper* m_game;
        QList<Creature*> m_members;
};
}
Q_DECLARE_METATYPE(Sigencore::Containment*)

#endif
