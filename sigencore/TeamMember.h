/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCORE_TEAMMEMBER
#define SIGENCORE_TEAMMEMBER

// Sigencore includes
#include "Creature.h"
#include "Global.h"

// Qt includes
#include <QtCore/QFuture>
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QString>
#include <QtCore/QUuid>

// Forward declarations
namespace Kross
{
class Action;
class ActionCollection;
}

namespace Sigencore
{
class Arena;
class Team;

class SIGENCORE_EXPORT TeamMember : public Creature
{
    Q_OBJECT
    
    public:
        enum ActionType
        {
            Invalid = -2,
            Timeout = -1,
            Attack = 0,
            Item = 1,
            Switch = 2,
            Run = 3,
            UserAction = 20
        };
        
        typedef QList<QUuid> Targets;
        typedef QPair<QVariant, Targets> ActionData;
        typedef QPair<int, ActionData> Action;
        typedef QPair<TeamMember*, QFuture<Action> > RequestedAction;
        
        TeamMember(Sigscript::SpeciesWrapper* species, const int level, Team* team, const bool suppressInitialization = true);
        virtual ~TeamMember();
        
        virtual bool setContainment(Containment* containment);
        virtual bool setTeam(Team* team);
        Q_SCRIPTABLE Team* team() const;
        
        Q_SCRIPTABLE int statStage(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE int statValue(const Sigmod::Stat stat) const;
    public slots:
        bool setStatStage(const Sigmod::Stat stat, const int stage);
        bool giveStatStages(const Sigmod::Stat stat, const int stages);
        
        bool addAbility(Sigscript::AbilityWrapper* ability);
        bool removeAbility(Sigscript::AbilityWrapper* ability);
        
        bool addItems(Sigscript::ItemWrapper* item, const int count, const bool allOrNothing = false);
        
        bool addMove(Sigscript::MoveWrapper* move);
        bool removeMove(Sigscript::MoveWrapper* move);
        
        bool addNature(Sigscript::NatureWrapper* nature);
        bool removeNature(Sigscript::NatureWrapper* nature);
        
        bool addStatus(Sigscript::StatusWrapper* status);
        bool removeStatus(Sigscript::StatusWrapper* status);
        
        virtual void makeActive(Arena* arena);
        virtual void exitArena();
    signals:
        void statStageChanged(const Sigmod::Stat stat, const int newStage);
    protected:
        virtual void makeConnections();
        
        Team* m_team;
        Arena* m_arena;
        
        int m_stages[Sigmod::ST_Evasion - Sigmod::ST_Attack + 1];
        
        Kross::ActionCollection* m_abilityBattleScripts;
        Kross::ActionCollection* m_abilityWorldScripts;
        Kross::ActionCollection* m_heldItemScripts;
        Kross::ActionCollection* m_moveBattleScripts;
        Kross::ActionCollection* m_moveWorldScripts;
        Kross::ActionCollection* m_statusBattleScripts;
        Kross::ActionCollection* m_statusWorldScripts;
        
        QMap<Sigscript::AbilityWrapper*, Kross::Action*> m_abilityBattleScriptsMap;
        QMap<Sigscript::AbilityWrapper*, Kross::Action*> m_abilityWorldScriptsMap;
        QMap<Sigscript::ItemWrapper*, Kross::Action*> m_heldItemsScriptsMap;
        QMap<Sigscript::MoveWrapper*, Kross::Action*> m_moveBattleScriptsMap;
        QMap<Sigscript::MoveWrapper*, Kross::Action*> m_moveWorldScriptsMap;
        QMap<Sigscript::StatusWrapper*, Kross::Action*> m_statusBattleScriptsMap;
        QMap<Sigscript::StatusWrapper*, Kross::Action*> m_statusWorldScriptsMap;
};

SIGENCORE_EXPORT int actionPriority(TeamMember* teamMember, const TeamMember::Action& action);

}
Q_DECLARE_METATYPE(Sigencore::TeamMember*)
Q_DECLARE_METATYPE(Sigencore::TeamMember::ActionType)
Q_DECLARE_METATYPE(Sigencore::TeamMember::Targets)
Q_DECLARE_METATYPE(Sigencore::TeamMember::ActionData)
Q_DECLARE_METATYPE(Sigencore::TeamMember::Action)
Q_DECLARE_METATYPE(Sigencore::TeamMember::RequestedAction)

#endif
