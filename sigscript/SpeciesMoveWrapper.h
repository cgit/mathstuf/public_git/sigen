/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_SPECIESMOVEWRAPPER
#define SIGSCRIPT_SPECIESMOVEWRAPPER

// Sigscript includes
#include "MoveWrapper.h"
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class SpeciesMove;
}

namespace Sigscript
{
class SpeciesWrapper;

class SIGSCRIPT_EXPORT SpeciesMoveWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static SpeciesMoveWrapper* create(const Sigmod::SpeciesMove* move, SpeciesWrapper* parent);
        
        Q_SCRIPTABLE MoveWrapper* move();
        Q_SCRIPTABLE int level() const;
        Q_SCRIPTABLE int wild() const;
    private:
        SpeciesMoveWrapper(const Sigmod::SpeciesMove* move, SpeciesWrapper* parent);
        SpeciesMoveWrapper& operator=(const SpeciesMoveWrapper& rhs);
        
        const Sigmod::SpeciesMove* m_move;
};
}
Q_DECLARE_METATYPE(Sigscript::SpeciesMoveWrapper*)

#endif
