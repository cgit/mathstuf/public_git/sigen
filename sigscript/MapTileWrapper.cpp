/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTileWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapWrapper.h"

// Sigmod includes
#include <sigmod/MapTile.h>

using namespace Sigmod;
using namespace Sigscript;

MapTileWrapper* MapTileWrapper::create(const MapTile* tile, MapWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(tile->className(), tile->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapTileWrapper(tile, parent);
    return qobject_cast<MapTileWrapper*>(m_instances[sig]);
}

MapTileWrapper::MapTileWrapper(const MapTile* tile, MapWrapper* parent) :
        ObjectWrapper(tile, parent),
        m_tile(tile)
{
}

TileWrapper* MapTileWrapper::tile()
{
    return game()->tile(m_tile->tile());
}

QPoint MapTileWrapper::position() const
{
    ALLOW_OVERRIDE_SO(tile, QPoint, position);
    return m_tile->position();
}

int MapTileWrapper::zIndex() const
{
    ALLOW_OVERRIDE_SO(tile, int, zIndex);
    return m_tile->zIndex();
}
