/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpriteWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Sprite.h>

using namespace Sigmod;
using namespace Sigscript;

SpriteWrapper* SpriteWrapper::create(const Sprite* sprite, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(sprite->className(), sprite->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new SpriteWrapper(sprite, parent);
    return qobject_cast<SpriteWrapper*>(m_instances[sig]);
}

SpriteWrapper::SpriteWrapper(const Sprite* sprite, GameWrapper* parent) :
        ObjectWrapper(sprite, parent),
        m_sprite(sprite)
{
    m_pixmap.loadFromData(m_sprite->sprite());
}

QString SpriteWrapper::name() const
{
    return m_sprite->name();
}

QPixmap SpriteWrapper::sprite() const
{
    return m_pixmap;
}
