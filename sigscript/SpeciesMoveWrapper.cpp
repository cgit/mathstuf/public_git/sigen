/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpeciesMoveWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "SpeciesWrapper.h"

// Sigmod includes
#include <sigmod/SpeciesMove.h>

using namespace Sigmod;
using namespace Sigscript;

SpeciesMoveWrapper* SpeciesMoveWrapper::create(const SpeciesMove* move, SpeciesWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(move->className(), move->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new SpeciesMoveWrapper(move, parent);
    return qobject_cast<SpeciesMoveWrapper*>(m_instances[sig]);
}

SpeciesMoveWrapper::SpeciesMoveWrapper(const SpeciesMove* move, SpeciesWrapper* parent) :
        ObjectWrapper(move, parent),
        m_move(move)
{
}

MoveWrapper* SpeciesMoveWrapper::move()
{
    return game()->move(m_move->move());
}

int SpeciesMoveWrapper::level() const
{
    return m_move->level();
}

int SpeciesMoveWrapper::wild() const
{
    return m_move->wild();
}
