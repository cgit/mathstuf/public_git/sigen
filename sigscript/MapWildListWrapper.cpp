/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWildListWrapper.h"

// Sigscript includes
#include "MapWrapper.h"
#include "MapWildListEncounterWrapper.h"

// Sigmod includes
#include <sigmod/MapWildList.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

MapWildListWrapper* MapWildListWrapper::create(const MapWildList* wildList, MapWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(wildList->className(), wildList->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapWildListWrapper(wildList, parent);
    return qobject_cast<MapWildListWrapper*>(m_instances[sig]);
}

MapWildListWrapper::MapWildListWrapper(const MapWildList* wildList, MapWrapper* parent) :
        ObjectWrapper(wildList, parent),
        m_wildList(wildList)
{
}

Hat<MapWildListEncounterWrapper*> MapWildListWrapper::encounterHat()
{
    Hat<MapWildListEncounterWrapper*> hat;
    for (int i = 0; i < encounterCount(); ++i)
        hat.add(encounter(i), encounter(i)->weight());
    return hat;
}

QString MapWildListWrapper::name() const
{
    return m_wildList->name();
}

MapWildListEncounterWrapper* MapWildListWrapper::encounter(const int index)
{
    return MapWildListEncounterWrapper::create(m_wildList->encounter(index), this);
}

int MapWildListWrapper::encounterCount() const
{
    return m_wildList->encounterCount();
}
