/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPTILEWRAPPER
#define SIGSCRIPT_MAPTILEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class MapTile;
}

namespace Sigscript
{
class MapWrapper;
class TileWrapper;

class SIGSCRIPT_EXPORT MapTileWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapTileWrapper* create(const Sigmod::MapTile* tile, MapWrapper* parent);
    
        Q_SCRIPTABLE TileWrapper* tile();
        Q_SCRIPTABLE QPoint position() const;
        Q_SCRIPTABLE int zIndex() const;
    private:
        MapTileWrapper(const Sigmod::MapTile* tile, MapWrapper* parent);
        MapTileWrapper& operator=(const MapTileWrapper& rhs);
    
        const Sigmod::MapTile* m_tile;
};
}
Q_DECLARE_METATYPE(Sigscript::MapTileWrapper*)

#endif
