/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPWILDLISTWRAPPER
#define SIGSCRIPT_MAPWILDLISTWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Hat.h>

// Forward declarations
namespace Sigmod
{
class MapWildList;
}

namespace Sigscript
{
class MapWrapper;
class MapWildListEncounterWrapper;

class SIGSCRIPT_EXPORT MapWildListWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapWildListWrapper* create(const Sigmod::MapWildList* wildList, MapWrapper* parent);
        
        Sigcore::Hat<MapWildListEncounterWrapper*> encounterHat();
        
        Q_SCRIPTABLE QString name() const;
        
        Q_SCRIPTABLE MapWildListEncounterWrapper* encounter(const int index);
        Q_SCRIPTABLE int encounterCount() const;
    private:
        MapWildListWrapper(const Sigmod::MapWildList* wildList, MapWrapper* parent);
        MapWildListWrapper& operator=(const MapWildListWrapper& rhs);
        
        const Sigmod::MapWildList* m_wildList;
};
}
Q_DECLARE_METATYPE(Sigscript::MapWildListWrapper*)

#endif
