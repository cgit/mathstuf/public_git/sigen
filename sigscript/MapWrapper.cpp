/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapEffectWrapper.h"
#include "MapTileWrapper.h"
#include "MapTrainerWrapper.h"
#include "MapWarpWrapper.h"
#include "MapWildListWrapper.h"

// Sigmod includes
#include <sigmod/MapEffect.h>
#include <sigmod/MapTrainer.h>
#include <sigmod/MapWildList.h>

using namespace Sigmod;
using namespace Sigscript;

MapWrapper* MapWrapper::create(const Map* map, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(map->className(), map->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapWrapper(map, parent);
    return qobject_cast<MapWrapper*>(m_instances[sig]);
}

MapWrapper::MapWrapper(const Map* map, GameWrapper* parent) :
        ObjectWrapper(map, parent),
        m_map(map)
{
}

MapEffectWrapper* MapWrapper::effect(const int id)
{
    return MapEffectWrapper::create(m_map->effectById(id), this);
}

MapTileWrapper* MapWrapper::tile(const int id)
{
    return MapTileWrapper::create(m_map->tileById(id), this);
}

MapTrainerWrapper* MapWrapper::trainer(const int id)
{
    return MapTrainerWrapper::create(m_map->trainerById(id), this);
}

MapWarpWrapper* MapWrapper::warp(const int id)
{
    return MapWarpWrapper::create(m_map->warpById(id), this);
}

MapWildListWrapper* MapWrapper::wildList(const int id)
{
    return MapWildListWrapper::create(m_map->wildListById(id), this);
}

QString MapWrapper::name() const
{
    return m_map->name();
}

int MapWrapper::width() const
{
    return m_map->width();
}

int MapWrapper::height() const
{
    return m_map->height();
}

bool MapWrapper::isWorld() const
{
    return m_map->isWorld();
}

MapEffectWrapper* MapWrapper::effect(const QString& name)
{
    for (int i = 0; i < m_map->effectCount(); ++i)
    {
        if (m_map->effect(i)->name() == name)
            return MapEffectWrapper::create(m_map->effect(i), this);
    }
    return NULL;
}

QList<MapTileWrapper*> MapWrapper::tiles()
{
    QList<MapTileWrapper*> tiles;
    for (int i = 0; i < m_map->tileCount(); ++i)
        tiles.append(MapTileWrapper::create(m_map->tile(i), this));
    return tiles;
}

MapTrainerWrapper* MapWrapper::trainer(const QString& name)
{
    for (int i = 0; i < m_map->trainerCount(); ++i)
    {
        if (m_map->trainer(i)->name() == name)
            return MapTrainerWrapper::create(m_map->trainer(i), this);
    }
    return NULL;
}

MapWarpWrapper* MapWrapper::warp(const QString& name)
{
    for (int i = 0; i < m_map->warpCount(); ++i)
    {
        if (m_map->warp(i)->name() == name)
            return MapWarpWrapper::create(m_map->warp(i), this);
    }
    return NULL;
}

MapWildListWrapper* MapWrapper::wildList(const QString& name)
{
    for (int i = 0; i < m_map->wildListCount(); ++i)
    {
        if (m_map->wildList(i)->name() == name)
            return MapWildListWrapper::create(m_map->wildList(i), this);
    }
    return NULL;
}
