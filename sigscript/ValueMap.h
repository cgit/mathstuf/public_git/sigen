/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_VALUEMAP
#define SIGSCRIPT_VALUEMAP

// Sigscript includes
#include "ConfigOptions.h"
#include "Global.h"

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QPair>
#include <QtCore/QReadWriteLock>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

namespace Sigscript
{
class SIGSCRIPT_NO_EXPORT ValueMap
{
    public:
        bool contains(const QString& name) const;
        
        QStringList keys() const;
        
        QVariant value(const QString& name) const;
        void setValue(const QString& name, const QVariant& value);
        void unsetValue(const QString& name);
        
        ConfigOptions options(const QString& name) const;
        void setOptions(const QString& name, const ConfigOptions options);
        void unsetOptions(const QString& name, const ConfigOptions options);
        
        ConfigOptions overrides(const QString& name) const;
        void setOverrides(const QString& name, const ConfigOptions options);
        void unsetOverrides(const QString& name, const ConfigOptions options);
    private:
        typedef QPair<ConfigOptions, ConfigOptions> Options;
        typedef QPair<QVariant, Options> Value;
        QMap<QString, Value> m_map;
        mutable QReadWriteLock m_mutex;
};
}

#endif
