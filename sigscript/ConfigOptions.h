/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_CONFIGOPTIONS
#define SIGSCRIPT_CONFIGOPTIONS

// Qt includes
#include <QtCore/QMetaType>

namespace Sigscript
{
enum ConfigOption
{
    Temporary = 1,
    Deleted = 2,
    ReadOnly = 4,
    Hidden = 8
};
Q_DECLARE_FLAGS(ConfigOptions, ConfigOption)

}

Q_DECLARE_METATYPE(Sigscript::ConfigOptions)
Q_DECLARE_OPERATORS_FOR_FLAGS(Sigscript::ConfigOptions)

#endif
