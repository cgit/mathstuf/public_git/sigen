/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "BadgeWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Badge.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

BadgeWrapper* BadgeWrapper::create(const Badge* badge, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(badge->className(), badge->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new BadgeWrapper(badge, parent);
    return qobject_cast<BadgeWrapper*>(m_instances[sig]);
}

BadgeWrapper::BadgeWrapper(const Badge* badge, GameWrapper* parent) :
        ObjectWrapper(badge, parent),
        m_badge(badge)
{
}

QString BadgeWrapper::name() const
{
    return m_badge->name();
}

SpriteWrapper* BadgeWrapper::face()
{
    return game()->sprite(m_badge->face());
}

SpriteWrapper* BadgeWrapper::badge()
{
    return game()->sprite(m_badge->badge());
}

int BadgeWrapper::obey() const
{
    return m_badge->obey();
}

Fraction BadgeWrapper::stat(const Stat stat) const
{
    return m_badge->stat(stat);
}
