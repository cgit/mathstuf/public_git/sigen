/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_TRAINERWRAPPER
#define SIGSCRIPT_TRAINERWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/Trainer.h>

namespace Sigscript
{
// Forward declarations
class SkinWrapper;

class SIGSCRIPT_EXPORT TrainerWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static TrainerWrapper* create(const Sigmod::Trainer* trainer, GameWrapper* parent);
        
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence intelligence(const QString& name) const;
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int moneyFactor() const;
        Q_SCRIPTABLE SkinWrapper* skin();
        Q_SCRIPTABLE int depth() const;
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence teamIntel() const;
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence moveIntel() const;
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence itemIntel() const;
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence abilityIntel() const;
        Q_SCRIPTABLE Sigmod::Trainer::Intelligence statIntel() const;
    private:
        TrainerWrapper(const Sigmod::Trainer* trainer, GameWrapper* parent);
        TrainerWrapper& operator=(const TrainerWrapper& rhs);
        
        const Sigmod::Trainer* m_trainer;
};
}
Q_DECLARE_METATYPE(Sigscript::TrainerWrapper*)

#endif
