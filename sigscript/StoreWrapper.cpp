/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "StoreWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Store.h>

using namespace Sigmod;
using namespace Sigscript;

StoreWrapper* StoreWrapper::create(const Store* store, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(store->className(), store->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new StoreWrapper(store, parent);
    return qobject_cast<StoreWrapper*>(m_instances[sig]);
}

StoreWrapper::StoreWrapper(const Store* store, GameWrapper* parent) :
        ObjectWrapper(store, parent),
        m_store(store)
{
}

QString StoreWrapper::name() const
{
    return m_store->name();
}

QList<ItemWrapper*> StoreWrapper::items()
{
    QList<int> itemIds = m_store->item();
    QList<ItemWrapper*> items;
    foreach (int id, itemIds)
        items.append(game()->item(id));
    return items;
}
