/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_OBJECTWRAPPER
#define SIGSCRIPT_OBJECTWRAPPER

// Sigscript includes
#include "Config.h"

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QObject>
#include <QtCore/QPair>

// Forward declarations
namespace Sigmod
{
class Object;
}

namespace Sigscript
{
class GameWrapper;

class SIGSCRIPT_EXPORT ObjectWrapper : public Config
{
    Q_OBJECT
    Q_PROPERTY(int id READ id)
    
    public:
        typedef QPair<QString, int> Subsignature;
        typedef QPair<ObjectWrapper*, Subsignature> Signature;
        
        ObjectWrapper(const Sigmod::Object* object, ObjectWrapper* parent);
        
        int id() const;
        
        const ObjectWrapper* parent() const;
        ObjectWrapper* parent();
        
        const GameWrapper* game() const;
        GameWrapper* game();
    protected:
        static QMap<Signature, ObjectWrapper*> m_instances;
    private:
        ObjectWrapper* m_parent;
        const Sigmod::Object* m_object;
};
}

#endif
