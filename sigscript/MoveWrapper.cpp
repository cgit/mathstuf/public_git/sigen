/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MoveWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Move.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

MoveWrapper* MoveWrapper::create(const Move* move, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(move->className(), move->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MoveWrapper(move, parent);
    return qobject_cast<MoveWrapper*>(m_instances[sig]);
}

MoveWrapper::MoveWrapper(const Move* move, GameWrapper* parent) :
        ObjectWrapper(move, parent),
        m_move(move)
{
}

QString MoveWrapper::name() const
{
    return m_move->name();
}

Fraction MoveWrapper::accuracy() const
{
    ALLOW_OVERRIDE_SO(move, Fraction, accuracy);
    return m_move->accuracy();
}

int MoveWrapper::power() const
{
    ALLOW_OVERRIDE_SO(move, int, power);
    return m_move->power();
}

TypeWrapper* MoveWrapper::type()
{
    return game()->type(m_move->type());
}

bool MoveWrapper::special() const
{
    return m_move->special();
}

int MoveWrapper::powerPoints() const
{
    return m_move->powerPoints();
}

int MoveWrapper::priority() const
{
    ALLOW_OVERRIDE_SO(move, int, priority);
    return m_move->priority();
}

QString MoveWrapper::description() const
{
    return m_move->description();
}

Script MoveWrapper::battleScript() const
{
    return m_move->battleScript();
}

Script MoveWrapper::worldScript() const
{
    return m_move->worldScript();
}

Script MoveWrapper::priorityScript() const
{
    return m_move->priorityScript();
}
