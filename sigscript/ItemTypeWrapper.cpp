/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ItemTypeWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

using namespace Sigmod;
using namespace Sigscript;

ItemTypeWrapper* ItemTypeWrapper::create(const ItemType* itemType, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(itemType->className(), itemType->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new ItemTypeWrapper(itemType, parent);
    return qobject_cast<ItemTypeWrapper*>(m_instances[sig]);
}

ItemTypeWrapper::ItemTypeWrapper(const ItemType* itemType, GameWrapper* parent) :
        ObjectWrapper(itemType, parent),
        m_itemType(itemType)
{
}

ItemType::Count ItemTypeWrapper::count(const QString& name) const
{
    if (name == "Distinct")
        return ItemType::Distinct;
    else if (name == "Total")
        return ItemType::Total;
    return QVariant(-1).value<ItemType::Count>();
}

QString ItemTypeWrapper::name() const
{
    return m_itemType->name();
}

int ItemTypeWrapper::computer() const
{
    return m_itemType->computer();
}

int ItemTypeWrapper::player() const
{
    return m_itemType->player();
}

int ItemTypeWrapper::maxWeight() const
{
    ALLOW_OVERRIDE_SO(itemType, int, maxWeight);
    return m_itemType->maxWeight();
}

int ItemTypeWrapper::count() const
{
    return m_itemType->count();
}
