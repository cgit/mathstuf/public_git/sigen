/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "RulesWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Rules.h>

using namespace Sigmod;
using namespace Sigscript;

RulesWrapper* RulesWrapper::create(const Rules* rules, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(rules->className(), rules->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new RulesWrapper(rules, parent);
    return qobject_cast<RulesWrapper*>(m_instances[sig]);
}

RulesWrapper::RulesWrapper(const Rules* rules, GameWrapper* parent) :
        ObjectWrapper(rules, parent),
        m_rules(rules)
{
}

bool RulesWrapper::genderAllowed() const
{
    return m_rules->genderAllowed();
}

bool RulesWrapper::breedingAllowed() const
{
    return m_rules->breedingAllowed();
}

bool RulesWrapper::criticalDomains() const
{
    return m_rules->criticalDomains();
}

int RulesWrapper::numBoxes() const
{
    ALLOW_OVERRIDE_SO(rules, int, numBoxes);
    return m_rules->numBoxes();
}

int RulesWrapper::boxSize() const
{
    ALLOW_OVERRIDE_SO(rules, int, boxSize);
    return m_rules->boxSize();
}

int RulesWrapper::maxParty() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxParty);
    return m_rules->maxParty();
}

int RulesWrapper::maxFight() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxFight);
    return m_rules->maxFight();
}

int RulesWrapper::maxPlayers() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxFight);
    return m_rules->maxPlayers();
}

int RulesWrapper::maxHeldItems() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxHeldItems);
    return m_rules->maxHeldItems();
}

int RulesWrapper::maxAbilities() const
{
    return m_rules->maxAbilities();
}

int RulesWrapper::maxNatures() const
{
    return m_rules->maxNatures();
}

int RulesWrapper::maxMoves() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxMoves);
    return m_rules->maxMoves();
}

int RulesWrapper::maxLevel() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxLevel);
    return m_rules->maxLevel();
}

int RulesWrapper::maxStages() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxStages);
    return m_rules->maxStages();
}

int RulesWrapper::maxMoney() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxMoney);
    return m_rules->maxMoney();
}

int RulesWrapper::maxTotalWeight() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxTotalWeight);
    return m_rules->maxTotalWeight();
}

bool RulesWrapper::specialSplit() const
{
    return m_rules->specialSplit();
}

bool RulesWrapper::specialDVSplit() const
{
    return m_rules->specialDVSplit();
}

int RulesWrapper::maxTotalEV() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxTotalEV);
    return m_rules->maxTotalEV();
}

int RulesWrapper::maxEVPerStat() const
{
    ALLOW_OVERRIDE_SO(rules, int, maxEVPerStat);
    return m_rules->maxEVPerStat();
}
