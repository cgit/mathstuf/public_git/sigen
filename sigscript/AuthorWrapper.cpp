/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "AuthorWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Author.h>

using namespace Sigmod;
using namespace Sigscript;

AuthorWrapper* AuthorWrapper::create(const Author* author, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(author->className(), author->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new AuthorWrapper(author, parent);
    return qobject_cast<AuthorWrapper*>(m_instances[sig]);
}

AuthorWrapper::AuthorWrapper(const Author* author, GameWrapper* parent) :
        ObjectWrapper(author, parent),
        m_author(author)
{
}

QString AuthorWrapper::name() const
{
    return m_author->name();
}

QString AuthorWrapper::email() const
{
    return m_author->email();
}

QString AuthorWrapper::role() const
{
    return m_author->role();
}
