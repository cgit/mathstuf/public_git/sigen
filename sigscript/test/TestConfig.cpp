/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestConfig.h"

// Sigscript includes
#include <sigscript/Config.h>

// Sigcore includes
#include <sigcore/Fraction.h>

// Qt includes
#include <QtTest/QSignalSpy>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigscript;

void TestConfig::initTestCase()
{
    qRegisterMetaType<QVariant>("QVariant");
    qRegisterMetaType<ConfigOptions>("Sigscript::ConfigOptions");
    m_config = new Config(NULL);
}

void TestConfig::cleanupTestCase()
{
    delete m_config;
}

//     void valueAdded(const QString& name, const QVariant& value);
//     void valueChanged(const QString& name, const QVariant& newValue);
//     void valueRemoved(const QString& name);
//     void optionsChanged(const QString& name, const Sigscript::Config::Options newOptions);

void TestConfig::testAddValue()
{
    QSignalSpy addSpy(m_config, SIGNAL(valueAdded(QString, QVariant)));
    QSignalSpy changeSpy(m_config, SIGNAL(valueChanged(QString, QVariant)));
    
    QCOMPARE(m_config->addValue("foo", 10), true);
    QCOMPARE(m_config->addValue("bar", 20), true);
    QCOMPARE(m_config->addValue("bar", 25), false);
    
    QCOMPARE(addSpy.count(), 2);
    QCOMPARE(changeSpy.count(), 2);
}

void TestConfig::testSetValue()
{
    QSignalSpy addSpy(m_config, SIGNAL(valueAdded(QString, QVariant)));
    QSignalSpy changeSpy(m_config, SIGNAL(valueChanged(QString, QVariant)));
    
    QCOMPARE(m_config->setValue("foo", 5), true);
    QCOMPARE(m_config->setValue("baz", 30), true);
    
    QCOMPARE(addSpy.count(), 1);
    QCOMPARE(changeSpy.count(), 2);
    
    QCOMPARE(m_config->setValue("op1", 1), true);
    QCOMPARE(m_config->setValue("op2", 2), true);
    QCOMPARE(m_config->setValue("op3", 3), true);
    QCOMPARE(m_config->setValue("op4", 4), true);
}

void TestConfig::testRemoveValue()
{
    Config* subConfig = new Config(m_config);
    
    QSignalSpy removeSpy(m_config, SIGNAL(valueRemoved(QString)));
    QSignalSpy removeSpySub(subConfig, SIGNAL(valueRemoved(QString)));
    
    QCOMPARE(m_config->removeValue("foo"), true);
    QCOMPARE(m_config->removeValue("quux", true), true);
    QCOMPARE(m_config->removeValue("blag", false), false);
    QCOMPARE(subConfig->removeValue("bar", true), true);
    QCOMPARE(subConfig->removeValue("baz", false), false);
    
    QCOMPARE(removeSpy.count(), 2);
    QCOMPARE(removeSpySub.count(), 1);
    
    delete subConfig;
}

void TestConfig::testSetOptions()
{
    QSignalSpy changeSpy(m_config, SIGNAL(optionsChanged(QString, Sigscript::ConfigOptions)));
    
    QCOMPARE(m_config->setOptions("op1", Deleted), true);
    QCOMPARE(m_config->setOptions("op2", Hidden), true);
    QCOMPARE(m_config->setOptions("op3", ReadOnly), true);
    QCOMPARE(m_config->setOptions("op4", Temporary), true);
    
    QCOMPARE(m_config->setOptions("op3", ReadOnly), false);
    
    QCOMPARE(changeSpy.count(), 4);
}

void TestConfig::testOptions()
{
    QSignalSpy changeSpy(m_config, SIGNAL(valueChanged(QString, QVariant)));
    QSignalSpy optChangeSpy(m_config, SIGNAL(optionsChanged(QString, Sigscript::ConfigOptions)));
    
    QCOMPARE(m_config->hasValue("op1"), false);
    QCOMPARE(m_config->hasValue("op2"), false);
    QCOMPARE(m_config->hasValue("op3"), true);
    QCOMPARE(m_config->hasValue("op4"), true);
    
    QCOMPARE(m_config->addValue("op1", 10), true);
    QCOMPARE(m_config->addValue("op2", 20), true);
    QCOMPARE(m_config->setValue("op3", 30), false);
    
    QCOMPARE(m_config->value("op3").toInt(), 3);
    
    QCOMPARE(changeSpy.count(), 2);
    QCOMPARE(optChangeSpy.count(), 1);
}

void TestConfig::testClean()
{
    QSignalSpy removeSpy(m_config, SIGNAL(valueRemoved(QString)));
    QSignalSpy changeSpy(m_config, SIGNAL(optionsChanged(QString, Sigscript::ConfigOptions)));
    
    m_config->clean();
    
    QCOMPARE(removeSpy.count(), 1);
    QCOMPARE(changeSpy.count(), 1);
}

void TestConfig::testUnsetOptions()
{
    QSignalSpy changeSpy(m_config, SIGNAL(valueChanged(QString, QVariant)));
    QSignalSpy optChangeSpy(m_config, SIGNAL(optionsChanged(QString, Sigscript::ConfigOptions)));
    
    QCOMPARE(m_config->unsetOptions("blag", Hidden), false);
    QCOMPARE(m_config->unsetOptions("op3", ReadOnly), true);
    
    QCOMPARE(m_config->setValue("op3", 30), true);
    QCOMPARE(m_config->value("op3").toInt(), 30);
    
    QCOMPARE(changeSpy.count(), 1);
    QCOMPARE(optChangeSpy.count(), 1);
}

void TestConfig::testValueList()
{
    m_config->setOptions("asdf", Hidden);
    
    QStringList values = m_config->values(false);
    qSort(values);
    QStringList list;
    list << "bar" << "baz" << "op1" << "op2" << "op3";
    
    QCOMPARE(values, list);
    
    Config* subConfig = new Config(m_config);
    
    subConfig->addValue("blag", 20);
    subConfig->removeValue("baz", true);
    
    values = subConfig->values(false);
    qSort(values);
    list.clear();
    list << "blag";
    
    QCOMPARE(values, list);
    
    values = subConfig->values(true);
    qSort(values);
    list.clear();
    list << "bar" << "blag" << "op1" << "op2" << "op3";
    qSort(list);
    
    QCOMPARE(values, list);
    
    delete subConfig;
}

void TestConfig::testOptionsRead()
{
    QCOMPARE(m_config->options("asdf"), Hidden);
}

void TestConfig::testValueRead()
{
    int intValue;
    QString strValue;
    Fraction fracValue(4, 5);
    
    QCOMPARE(m_config->valueOfType("op3", &intValue), true);
    QCOMPARE(m_config->valueOfType("op3", &strValue), true);
    QCOMPARE(m_config->valueOfType("op3", &fracValue), false);
    
    QCOMPARE(intValue, 30);
    QCOMPARE(strValue, QString("30"));
    QCOMPARE(fracValue, Fraction(4, 5));
    
    Config* subConfig = new Config(m_config);
    
    QCOMPARE(subConfig->setValue("op3", "foobar"), true);
    
    QCOMPARE(subConfig->valueOfType("op3", &strValue), true);
    
    QCOMPARE(strValue, QString("foobar"));
    
    delete subConfig;
}

QTEST_APPLESS_MAIN(TestConfig)
