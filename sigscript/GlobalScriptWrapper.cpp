/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "GlobalScriptWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/GlobalScript.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

GlobalScriptWrapper* GlobalScriptWrapper::create(const GlobalScript* globalScript, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(globalScript->className(), globalScript->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new GlobalScriptWrapper(globalScript, parent);
    return qobject_cast<GlobalScriptWrapper*>(m_instances[sig]);
}

GlobalScriptWrapper::GlobalScriptWrapper(const GlobalScript* globalScript, GameWrapper* parent) :
        ObjectWrapper(globalScript, parent),
        m_globalScript(globalScript)
{
}

QString GlobalScriptWrapper::name() const
{
    return m_globalScript->name();
}

Script GlobalScriptWrapper::script() const
{
    return m_globalScript->script();
}
