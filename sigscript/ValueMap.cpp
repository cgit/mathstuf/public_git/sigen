/*
* Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// Header include
#include "ValueMap.h"

// Qt includes
#include <QtCore/QReadLocker>
#include <QtCore/QWriteLocker>

using namespace Sigscript;

bool ValueMap::contains(const QString& name) const
{
    QReadLocker locker(&m_mutex);
    return m_map.contains(name);
}

QStringList ValueMap::keys() const
{
    QReadLocker locker(&m_mutex);
    return m_map.keys();
}

QVariant ValueMap::value(const QString& name) const
{
    QReadLocker locker(&m_mutex);
    if (m_map.contains(name))
        return m_map[name].first;
    return QVariant();
}

void ValueMap::setValue(const QString& name, const QVariant& value)
{
    QWriteLocker locker(&m_mutex);
    m_map[name].first = value;
}

void ValueMap::unsetValue(const QString& name)
{
    QWriteLocker locker(&m_mutex);
    m_map.remove(name);
}

ConfigOptions ValueMap::options(const QString& name) const
{
    QReadLocker locker(&m_mutex);
    if (m_map.contains(name))
        return m_map[name].second.first;
    return ConfigOptions();
}

void ValueMap::setOptions(const QString& name, const ConfigOptions options)
{
    QWriteLocker locker(&m_mutex);
    m_map[name].second.first |= options;
}

void ValueMap::unsetOptions(const QString& name, const ConfigOptions options)
{
    QWriteLocker locker(&m_mutex);
    m_map[name].second.first &= ~options;
}

ConfigOptions ValueMap::overrides(const QString& name) const
{
    QReadLocker locker(&m_mutex);
    if (m_map.contains(name))
        return m_map[name].second.second;
    return ConfigOptions();
}

void ValueMap::setOverrides(const QString& name, const ConfigOptions options)
{
    QWriteLocker locker(&m_mutex);
    m_map[name].second.second |= options;
}

void ValueMap::unsetOverrides(const QString& name, const ConfigOptions options)
{
    QWriteLocker locker(&m_mutex);
    m_map[name].second.second &= ~options;
}
