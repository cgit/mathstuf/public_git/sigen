/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TileWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Tile.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

TileWrapper* TileWrapper::create(const Tile* tile, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(tile->className(), tile->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new TileWrapper(tile, parent);
    return qobject_cast<TileWrapper*>(m_instances[sig]);
}

TileWrapper::TileWrapper(const Tile* tile, GameWrapper* parent) :
        ObjectWrapper(tile, parent),
        m_tile(tile)
{
}

QString TileWrapper::name() const
{
    return m_tile->name();
}

bool TileWrapper::walkable() const
{
    ALLOW_OVERRIDE_SO(tile, bool, walkable);
    return m_tile->walkable();
}

Script TileWrapper::script() const
{
    return m_tile->script();
}
