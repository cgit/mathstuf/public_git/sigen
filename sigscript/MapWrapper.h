/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPWRAPPER
#define SIGSCRIPT_MAPWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/Map.h>

namespace Sigscript
{
// Forward declarations
class MapEffectWrapper;
class MapTileWrapper;
class MapTrainerWrapper;
class MapWarpWrapper;
class MapWildListWrapper;
class TileWrapper;

class SIGSCRIPT_EXPORT MapWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapWrapper* create(const Sigmod::Map* map, GameWrapper* parent);
        
        MapEffectWrapper* effect(const int id);
        MapTileWrapper* tile(const int id);
        MapTrainerWrapper* trainer(const int id);
        MapWarpWrapper* warp(const int id);
        MapWildListWrapper* wildList(const int id);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int width() const;
        Q_SCRIPTABLE int height() const;
        Q_SCRIPTABLE bool isWorld() const;
        
        Q_SCRIPTABLE MapEffectWrapper* effect(const QString& name);
        Q_SCRIPTABLE QList<MapTileWrapper*> tiles();
        Q_SCRIPTABLE MapTrainerWrapper* trainer(const QString& name);
        Q_SCRIPTABLE MapWarpWrapper* warp(const QString& name);
        Q_SCRIPTABLE MapWildListWrapper* wildList(const QString& name);
    private:
        MapWrapper(const Sigmod::Map* map, GameWrapper* parent);
        MapWrapper& operator=(const MapWrapper& rhs);
        
        const Sigmod::Map* m_map;
};
}
Q_DECLARE_METATYPE(Sigscript::MapWrapper*)

#endif
