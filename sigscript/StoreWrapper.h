/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_STOREWRAPPER
#define SIGSCRIPT_STOREWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class Store;
}

namespace Sigscript
{
class ItemWrapper;

class SIGSCRIPT_EXPORT StoreWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static StoreWrapper* create(const Sigmod::Store* store, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE QList<ItemWrapper*> items();
    private:
        StoreWrapper(const Sigmod::Store* store, GameWrapper* parent);
        StoreWrapper& operator=(const StoreWrapper& rhs);
        
        const Sigmod::Store* m_store;
};
}
Q_DECLARE_METATYPE(Sigscript::StoreWrapper*)

#endif
