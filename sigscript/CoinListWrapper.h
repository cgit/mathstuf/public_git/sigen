/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_COINLISTWRAPPER
#define SIGSCRIPT_COINLISTWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class CoinList;
}

namespace Sigscript
{
class CoinListItemWrapper;
class GameWrapper;

class SIGSCRIPT_EXPORT CoinListWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static CoinListWrapper* create(const Sigmod::CoinList* coinList, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
        
        Q_SCRIPTABLE CoinListItemWrapper* item(const int index);
        Q_SCRIPTABLE int itemCount() const;
    private:
        CoinListWrapper(const Sigmod::CoinList* coinList, GameWrapper* parent);
        CoinListWrapper& operator=(const CoinListWrapper& rhs);
        
        const Sigmod::CoinList* m_coinList;
};
}
Q_DECLARE_METATYPE(Sigscript::CoinListWrapper*)

#endif
