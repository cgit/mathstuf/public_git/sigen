/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_BADGEWRAPPER
#define SIGSCRIPT_BADGEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/Stat.h>

// Sigcore includes
#include <sigcore/Fraction.h>

// Forward declarations
namespace Sigmod
{
class Badge;
}

namespace Sigscript
{
// Forward declarations
class SpriteWrapper;

class SIGSCRIPT_EXPORT BadgeWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static BadgeWrapper* create(const Sigmod::Badge* badge, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE SpriteWrapper* face();
        Q_SCRIPTABLE SpriteWrapper* badge();
        Q_SCRIPTABLE int obey() const;
        Q_SCRIPTABLE Sigcore::Fraction stat(const Sigmod::Stat stat) const;
    private:
        BadgeWrapper(const Sigmod::Badge* badge, GameWrapper* parent);
        BadgeWrapper& operator=(const BadgeWrapper& rhs);
        
        const Sigmod::Badge* m_badge;
};
}
Q_DECLARE_METATYPE(Sigscript::BadgeWrapper*)

#endif
