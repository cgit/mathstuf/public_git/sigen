/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "StatusWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Status.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

StatusWrapper* StatusWrapper::create(const Status* status, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(status->className(), status->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new StatusWrapper(status, parent);
    return qobject_cast<StatusWrapper*>(m_instances[sig]);
}

StatusWrapper::StatusWrapper(const Status* status, GameWrapper* parent) :
        ObjectWrapper(status, parent),
        m_status(status)
{
}

QString StatusWrapper::name() const
{
    return m_status->name();
}

Script StatusWrapper::battleScript() const
{
    return m_status->battleScript();
}

Script StatusWrapper::worldScript() const
{
    return m_status->worldScript();
}
