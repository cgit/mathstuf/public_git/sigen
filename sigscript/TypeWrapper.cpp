/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TypeWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Type.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

TypeWrapper* TypeWrapper::create(const Type* type, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(type->className(), type->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new TypeWrapper(type, parent);
    return qobject_cast<TypeWrapper*>(m_instances[sig]);
}

TypeWrapper::TypeWrapper(const Type* type, GameWrapper* parent) :
        ObjectWrapper(type, parent),
        m_type(type)
{
}

QString TypeWrapper::TypeWrapper::name() const
{
    return m_type->name();
}

Fraction TypeWrapper::TypeWrapper::stab() const
{
    return m_type->stab();
}
