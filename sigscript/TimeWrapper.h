/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_TIMEWRAPPER
#define SIGSCRIPT_TIMEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Time;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT TimeWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static TimeWrapper* create(const Sigmod::Time* time, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int hour() const;
        Q_SCRIPTABLE int minute() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        TimeWrapper(const Sigmod::Time* time, GameWrapper* parent);
        TimeWrapper& operator=(const TimeWrapper& rhs);
        
        const Sigmod::Time* m_time;
};
}
Q_DECLARE_METATYPE(Sigscript::TimeWrapper*)

#endif
