/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "WeatherWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Weather.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

WeatherWrapper* WeatherWrapper::create(const Weather* weather, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(weather->className(), weather->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new WeatherWrapper(weather, parent);
    return qobject_cast<WeatherWrapper*>(m_instances[sig]);
}

WeatherWrapper::WeatherWrapper(const Weather* weather, GameWrapper* parent) :
        ObjectWrapper(weather, parent),
        m_weather(weather)
{
}

QString WeatherWrapper::name() const
{
    return m_weather->name();
}

Script WeatherWrapper::script() const
{
    return m_weather->script();
}
