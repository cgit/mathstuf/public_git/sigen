/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "CoinListWrapper.h"

// Sigscript includes
#include "CoinListItemWrapper.h"
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/CoinList.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

CoinListWrapper* CoinListWrapper::create(const CoinList* coinList, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(coinList->className(), coinList->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new CoinListWrapper(coinList, parent);
    return qobject_cast<CoinListWrapper*>(m_instances[sig]);
}

CoinListWrapper::CoinListWrapper(const CoinList* coinList, GameWrapper* parent) :
        ObjectWrapper(coinList, parent),
        m_coinList(coinList)
{
}

QString CoinListWrapper::name() const
{
    return m_coinList->name();
}

Script CoinListWrapper::script() const
{
    return m_coinList->script();
}

CoinListItemWrapper* CoinListWrapper::item(const int index)
{
    return CoinListItemWrapper::create(m_coinList->item(index), this);
}

int CoinListWrapper::itemCount() const
{
    return m_coinList->itemCount();
}
