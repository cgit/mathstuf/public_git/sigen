/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "NatureWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Nature.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

NatureWrapper* NatureWrapper::create(const Nature* nature, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(nature->className(), nature->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new NatureWrapper(nature, parent);
    return qobject_cast<NatureWrapper*>(m_instances[sig]);
}

NatureWrapper::NatureWrapper(const Nature* nature, GameWrapper* parent) :
        ObjectWrapper(nature, parent),
        m_nature(nature)
{
}

QString NatureWrapper::name() const
{
    return m_nature->name();
}

Fraction NatureWrapper::stat(const Stat stat) const
{
    return m_nature->stat(stat);
}

int NatureWrapper::weight() const
{
    return m_nature->weight();
}
