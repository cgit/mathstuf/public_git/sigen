/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "GameWrapper.h"

// Sigscript includes
#include "AbilityWrapper.h"
#include "AuthorWrapper.h"
#include "BadgeWrapper.h"
#include "CoinListWrapper.h"
#include "EggGroupWrapper.h"
#include "GlobalScriptWrapper.h"
#include "ItemWrapper.h"
#include "ItemTypeWrapper.h"
#include "MapWrapper.h"
#include "MapWarpWrapper.h"
#include "MoveWrapper.h"
#include "NatureWrapper.h"
#include "RulesWrapper.h"
#include "SkinWrapper.h"
#include "SoundWrapper.h"
#include "SpeciesWrapper.h"
#include "SpriteWrapper.h"
#include "StatusWrapper.h"
#include "StoreWrapper.h"
#include "TileWrapper.h"
#include "TimeWrapper.h"
#include "TrainerWrapper.h"
#include "TypeWrapper.h"
#include "WeatherWrapper.h"

// Sigmod includes
#include <sigmod/Ability.h>
#include <sigmod/Author.h>
#include <sigmod/Badge.h>
#include <sigmod/CoinList.h>
#include <sigmod/EggGroup.h>
#include <sigmod/Game.h>
#include <sigmod/GlobalScript.h>
#include <sigmod/Item.h>
#include <sigmod/Move.h>
#include <sigmod/Nature.h>
#include <sigmod/Store.h>
#include <sigmod/Skin.h>
#include <sigmod/Sprite.h>
#include <sigmod/Status.h>
#include <sigmod/Tile.h>
#include <sigmod/Time.h>
#include <sigmod/Type.h>
#include <sigmod/Weather.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

GameWrapper::GameWrapper(const Game* game) :
        ObjectWrapper(game, NULL),
        m_game(game)
{
}

Hat<NatureWrapper*> GameWrapper::natureHat()
{
    Hat<NatureWrapper*> hat;
    for (int i = 0; i < m_game->natureCount(); ++i)
    {
        NatureWrapper* nature = NatureWrapper::create(m_game->nature(i), this);
        hat.add(nature, nature->weight());
    }
    return hat;
}

AbilityWrapper* GameWrapper::ability(const int id)
{
    return AbilityWrapper::create(m_game->abilityById(id), this);
}

AuthorWrapper* GameWrapper::author(const int id)
{
    return AuthorWrapper::create(m_game->authorById(id), this);
}

BadgeWrapper* GameWrapper::badge(const int id)
{
    return BadgeWrapper::create(m_game->badgeById(id), this);
}

CoinListWrapper* GameWrapper::coinList(const int id)
{
    return CoinListWrapper::create(m_game->coinListById(id), this);
}

EggGroupWrapper* GameWrapper::eggGroup(const int id)
{
    return EggGroupWrapper::create(m_game->eggGroupById(id), this);
}

GlobalScriptWrapper* GameWrapper::globalScript(const int id)
{
    return GlobalScriptWrapper::create(m_game->globalScriptById(id), this);
}

ItemWrapper* GameWrapper::item(const int id)
{
    return ItemWrapper::create(m_game->itemById(id), this);
}

ItemTypeWrapper* GameWrapper::itemType(const int id)
{
    return ItemTypeWrapper::create(m_game->itemTypeById(id), this);
}

MapWrapper* GameWrapper::map(const int id)
{
    return MapWrapper::create(m_game->mapById(id), this);
}

MoveWrapper* GameWrapper::move(const int id)
{
    return MoveWrapper::create(m_game->moveById(id), this);
}

NatureWrapper* GameWrapper::nature(const int id)
{
    return NatureWrapper::create(m_game->natureById(id), this);
}

SkinWrapper* GameWrapper::skin(const int id)
{
    return SkinWrapper::create(m_game->skinById(id), this);
}

SoundWrapper* GameWrapper::sound(const int id)
{
    return SoundWrapper::create(m_game->soundById(id), this);
}

SpeciesWrapper* GameWrapper::species(const int id)
{
    return SpeciesWrapper::create(m_game->speciesById(id), this);
}

SpriteWrapper* GameWrapper::sprite(const int id)
{
    return SpriteWrapper::create(m_game->spriteById(id), this);
}

StatusWrapper* GameWrapper::status(const int id)
{
    return StatusWrapper::create(m_game->statusById(id), this);
}

StoreWrapper* GameWrapper::store(const int id)
{
    return StoreWrapper::create(m_game->storeById(id), this);
}

TileWrapper* GameWrapper::tile(const int id)
{
    return TileWrapper::create(m_game->tileById(id), this);
}

TimeWrapper* GameWrapper::time(const int id)
{
    return TimeWrapper::create(m_game->timeById(id), this);
}

TrainerWrapper* GameWrapper::trainer(const int id)
{
    return TrainerWrapper::create(m_game->trainerById(id), this);
}

TypeWrapper* GameWrapper::type(const int id)
{
    return TypeWrapper::create(m_game->typeById(id), this);
}

WeatherWrapper* GameWrapper::weather(const int id)
{
    return WeatherWrapper::create(m_game->weatherById(id), this);
}

Stat GameWrapper::stat(const QString& name) const
{
    if (name == "HP")
        return ST_HP;
    else if (name == "Attack")
        return ST_Attack;
    else if (name == "Defense")
        return ST_Defense;
    else if (name == "Speed")
        return ST_Speed;
    else if (name == "Special")
        return ST_Special;
    else if (name == "Special Attack")
        return ST_SpecialAttack;
    else if (name == "Special Defense")
        return ST_SpecialAttack;
    else if (name == "Accuracy")
        return ST_Accuracy;
    else if (name == "Evasion")
        return ST_Evasion;
    return QVariant(-1).value<Stat>();
}

Direction GameWrapper::direction(const QString& name) const
{
    if (name == "Up")
        return D_Up;
    else if (name == "Down")
        return D_Down;
    else if (name == "Left")
        return D_Left;
    else if (name == "Right")
        return D_Right;
    else if (name == "None")
        return D_None;
    return QVariant(-1).value<Direction>();
}

QString GameWrapper::title() const
{
    return m_game->title();
}

QString GameWrapper::version() const
{
    return m_game->version();
}

QString GameWrapper::description() const
{
    return m_game->description();
}

bool GameWrapper::singlePlayer() const
{
    return m_game->singlePlayer();
}

Script GameWrapper::startScript()
{
    return m_game->startScript();
}

Fraction GameWrapper::effectiveness(const TypeWrapper* attacker, const TypeWrapper* defender) const
{
    return m_game->typechart(m_game->typeIndex(attacker->id()), m_game->typeIndex(defender->id()));
}

QPoint GameWrapper::mapPosition(const MapWrapper* map) const
{
    return m_game->mapPosition(map->id());
}

RulesWrapper* GameWrapper::rules()
{
    return RulesWrapper::create(m_game->rules(), this);
}

AbilityWrapper* GameWrapper::ability(const QString& name)
{
    for (int i = 0; i < m_game->abilityCount(); ++i)
    {
        if (m_game->ability(i)->name() == name)
            return AbilityWrapper::create(m_game->ability(i), this);
    }
    return NULL;
}

AuthorWrapper* GameWrapper::author(const QString& name)
{
    for (int i = 0; i < m_game->authorCount(); ++i)
    {
        if (m_game->author(i)->name() == name)
            return AuthorWrapper::create(m_game->author(i), this);
    }
    return NULL;
}

BadgeWrapper* GameWrapper::badge(const QString& name)
{
    for (int i = 0; i < m_game->badgeCount(); ++i)
    {
        if (m_game->badge(i)->name() == name)
            return BadgeWrapper::create(m_game->badge(i), this);
    }
    return NULL;
}

CoinListWrapper* GameWrapper::coinList(const QString& name)
{
    for (int i = 0; i < m_game->coinListCount(); ++i)
    {
        if (m_game->coinList(i)->name() == name)
            return CoinListWrapper::create(m_game->coinList(i), this);
    }
    return NULL;
}

EggGroupWrapper* GameWrapper::eggGroup(const QString& name)
{
    for (int i = 0; i < m_game->eggGroupCount(); ++i)
    {
        if (m_game->eggGroup(i)->name() == name)
            return EggGroupWrapper::create(m_game->eggGroup(i), this);
    }
    return NULL;
}

GlobalScriptWrapper* GameWrapper::globalScript(const QString& name)
{
    for (int i = 0; i < m_game->globalScriptCount(); ++i)
    {
        if (m_game->globalScript(i)->name() == name)
            return GlobalScriptWrapper::create(m_game->globalScript(i), this);
    }
    return NULL;
}

ItemWrapper* GameWrapper::item(const QString& name)
{
    for (int i = 0; i < m_game->itemCount(); ++i)
    {
        if (m_game->item(i)->name() == name)
            return ItemWrapper::create(m_game->item(i), this);
    }
    return NULL;
}

ItemTypeWrapper* GameWrapper::itemType(const QString& name)
{
    for (int i = 0; i < m_game->itemTypeCount(); ++i)
    {
        if (m_game->itemType(i)->name() == name)
            return ItemTypeWrapper::create(m_game->itemType(i), this);
    }
    return NULL;
}

MapWrapper* GameWrapper::map(const QString& name)
{
    for (int i = 0; i < m_game->mapCount(); ++i)
    {
        if (m_game->map(i)->name() == name)
            return MapWrapper::create(m_game->map(i), this);
    }
    return NULL;
}

MoveWrapper* GameWrapper::move(const QString& name)
{
    for (int i = 0; i < m_game->moveCount(); ++i)
    {
        if (m_game->move(i)->name() == name)
            return MoveWrapper::create(m_game->move(i), this);
    }
    return NULL;
}

NatureWrapper* GameWrapper::nature(const QString& name)
{
    for (int i = 0; i < m_game->natureCount(); ++i)
    {
        if (m_game->nature(i)->name() == name)
            return NatureWrapper::create(m_game->nature(i), this);
    }
    return NULL;
}

SkinWrapper* GameWrapper::skin(const QString& name)
{
    for (int i = 0; i < m_game->skinCount(); ++i)
    {
        if (m_game->skin(i)->name() == name)
            return SkinWrapper::create(m_game->skin(i), this);
    }
    return NULL;
}

SoundWrapper* GameWrapper::sound(const QString& name)
{
    for (int i = 0; i < m_game->soundCount(); ++i)
    {
        if (m_game->sound(i)->name() == name)
            return SoundWrapper::create(m_game->sound(i), this);
    }
    return NULL;
}

SpeciesWrapper* GameWrapper::species(const QString& name)
{
    for (int i = 0; i < m_game->speciesCount(); ++i)
    {
        if (m_game->species(i)->name() == name)
            return SpeciesWrapper::create(m_game->species(i), this);
    }
    return NULL;
}

SpriteWrapper* GameWrapper::sprite(const QString& name)
{
    for (int i = 0; i < m_game->spriteCount(); ++i)
    {
        if (m_game->sprite(i)->name() == name)
            return SpriteWrapper::create(m_game->sprite(i), this);
    }
    return NULL;
}

StatusWrapper* GameWrapper::status(const QString& name)
{
    for (int i = 0; i < m_game->statusCount(); ++i)
    {
        if (m_game->status(i)->name() == name)
            return StatusWrapper::create(m_game->status(i), this);
    }
    return NULL;
}

StoreWrapper* GameWrapper::store(const QString& name)
{
    for (int i = 0; i < m_game->storeCount(); ++i)
    {
        if (m_game->store(i)->name() == name)
            return StoreWrapper::create(m_game->store(i), this);
    }
    return NULL;
}

TileWrapper* GameWrapper::tile(const QString& name)
{
    for (int i = 0; i < m_game->tileCount(); ++i)
    {
        if (m_game->tile(i)->name() == name)
            return TileWrapper::create(m_game->tile(i), this);
    }
    return NULL;
}

TimeWrapper* GameWrapper::time(const QString& name)
{
    for (int i = 0; i < m_game->timeCount(); ++i)
    {
        if (m_game->time(i)->name() == name)
            return TimeWrapper::create(m_game->time(i), this);
    }
    return NULL;
}

TrainerWrapper* GameWrapper::trainer(const QString& name)
{
    for (int i = 0; i < m_game->trainerCount(); ++i)
    {
        if (m_game->trainer(i)->name() == name)
            return TrainerWrapper::create(m_game->trainer(i), this);
    }
    return NULL;
}

TypeWrapper* GameWrapper::type(const QString& name)
{
    for (int i = 0; i < m_game->typeCount(); ++i)
    {
        if (m_game->type(i)->name() == name)
            return TypeWrapper::create(m_game->type(i), this);
    }
    return NULL;
}

WeatherWrapper* GameWrapper::weather(const QString& name)
{
    for (int i = 0; i < m_game->weatherCount(); ++i)
    {
        if (m_game->weather(i)->name() == name)
            return WeatherWrapper::create(m_game->weather(i), this);
    }
    return NULL;
}
