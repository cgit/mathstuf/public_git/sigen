/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPTRAINERWRAPPER
#define SIGSCRIPT_MAPTRAINERWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class MapTrainer;
}

namespace Sigscript
{
class MapWrapper;
class MapTrainerTeamMemberWrapper;
class TrainerWrapper;

class SIGSCRIPT_EXPORT MapTrainerWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapTrainerWrapper* create(const Sigmod::MapTrainer* trainer, MapWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE TrainerWrapper* trainerClass();
        Q_SCRIPTABLE QPoint position() const;
        Q_SCRIPTABLE int numberFight() const;
        Q_SCRIPTABLE QList<MapTrainerTeamMemberWrapper*> leadTeamMember();
        Q_SCRIPTABLE Sigcore::Script script() const;
        
        Q_SCRIPTABLE MapTrainerTeamMemberWrapper* teamMember(const int index);
        Q_SCRIPTABLE int teamMemberCount() const;
    private:
        MapTrainerWrapper(const Sigmod::MapTrainer* trainer, MapWrapper* parent);
        MapTrainerWrapper& operator=(const MapTrainerWrapper& rhs);
        
        const Sigmod::MapTrainer* m_trainer;
};
}
Q_DECLARE_METATYPE(Sigscript::MapTrainerWrapper*)

#endif
