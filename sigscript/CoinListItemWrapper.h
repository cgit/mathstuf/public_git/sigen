/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_COINLISTITEMWRAPPER
#define SIGSCRIPT_COINLISTITEMWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/CoinListItem.h>

namespace Sigscript
{
// Forward declarations
class CoinListWrapper;
class ItemWrapper;
class SpeciesWrapper;

class SIGSCRIPT_EXPORT CoinListItemWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static CoinListItemWrapper* create(const Sigmod::CoinListItem* object, CoinListWrapper* parent);
        
        Q_SCRIPTABLE Sigmod::CoinListItem::Type type(const QString& name) const;
        
        Q_SCRIPTABLE Sigmod::CoinListItem::Type type() const;
        Q_SCRIPTABLE ItemWrapper* itemObject();
        Q_SCRIPTABLE SpeciesWrapper* speciesObject();
        Q_SCRIPTABLE int cost() const;
    private:
        CoinListItemWrapper(const Sigmod::CoinListItem* object, CoinListWrapper* parent);
        CoinListItemWrapper& operator=(const CoinListItemWrapper& rhs);
        
        const Sigmod::CoinListItem* m_object;
};
}
Q_DECLARE_METATYPE(Sigscript::CoinListItemWrapper*)

#endif
