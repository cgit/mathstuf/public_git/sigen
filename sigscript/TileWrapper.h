/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_TILEWRAPPER
#define SIGSCRIPT_TILEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Tile;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT TileWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static TileWrapper* create(const Sigmod::Tile* tile, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE bool walkable() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        TileWrapper(const Sigmod::Tile* tile, GameWrapper* parent);
        TileWrapper& operator=(const TileWrapper& rhs);
        
        const Sigmod::Tile* m_tile;
};
}
Q_DECLARE_METATYPE(Sigscript::TileWrapper*)

#endif
