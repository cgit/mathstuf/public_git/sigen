/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_WEATHERWRAPPER
#define SIGSCRIPT_WEATHERWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Weather;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT WeatherWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static WeatherWrapper* create(const Sigmod::Weather* weather, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        WeatherWrapper(const Sigmod::Weather* weather, GameWrapper* parent);
        WeatherWrapper& operator=(const WeatherWrapper& rhs);
        
        const Sigmod::Weather* m_weather;
};
}
Q_DECLARE_METATYPE(Sigscript::WeatherWrapper*)

#endif
