/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWarpWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapWrapper.h"

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

MapWarpWrapper* MapWarpWrapper::create(const MapWarp* warp, MapWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(warp->className(), warp->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapWarpWrapper(warp, parent);
    return qobject_cast<MapWarpWrapper*>(m_instances[sig]);
}

MapWarpWrapper::MapWarpWrapper(const MapWarp* warp, MapWrapper* parent) :
        ObjectWrapper(warp, parent),
        m_warp(warp)
{
}

QString MapWarpWrapper::name() const
{
    return m_warp->name();
}

QPoint MapWarpWrapper::position() const
{
    return m_warp->position();
}

QPainterPath MapWarpWrapper::area() const
{
    return m_warp->area();
}

MapWarpWrapper* MapWarpWrapper::toWarp()
{
    return game()->map(m_warp->toMap())->warp(m_warp->toWarp());
}

Script MapWarpWrapper::script() const
{
    return m_warp->script();
}
