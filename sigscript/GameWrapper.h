/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_GAMEWRAPPER
#define SIGSCRIPT_GAMEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Hat.h>
#include <sigcore/Script.h>

// Sigmod includes
#include <sigmod/Direction.h>
#include <sigmod/Stat.h>

// Qt includes
#include <QtCore/QPoint>

// Forward declarations
namespace Sigmod
{
class Game;
}

namespace Sigscript
{
class AbilityWrapper;
class AuthorWrapper;
class BadgeWrapper;
class CoinListWrapper;
class EggGroupWrapper;
class GlobalScriptWrapper;
class ItemWrapper;
class ItemTypeWrapper;
class MapWrapper;
class MapWarpWrapper;
class MoveWrapper;
class NatureWrapper;
class RulesWrapper;
class SkinWrapper;
class SoundWrapper;
class SpeciesWrapper;
class SpriteWrapper;
class StatusWrapper;
class StoreWrapper;
class TileWrapper;
class TimeWrapper;
class TrainerWrapper;
class TypeWrapper;
class WeatherWrapper;

class SIGSCRIPT_EXPORT GameWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        GameWrapper(const Sigmod::Game* game);
        
        Sigcore::Hat<NatureWrapper*> natureHat();
        
        AbilityWrapper* ability(const int id);
        AuthorWrapper* author(const int id);
        BadgeWrapper* badge(const int id);
        CoinListWrapper* coinList(const int id);
        EggGroupWrapper* eggGroup(const int id);
        GlobalScriptWrapper* globalScript(const int id);
        ItemWrapper* item(const int id);
        ItemTypeWrapper* itemType(const int id);
        MapWrapper* map(const int id);
        MoveWrapper* move(const int id);
        NatureWrapper* nature(const int id);
        SkinWrapper* skin(const int id);
        SoundWrapper* sound(const int id);
        SpeciesWrapper* species(const int id);
        SpriteWrapper* sprite(const int id);
        StatusWrapper* status(const int id);
        StoreWrapper* store(const int id);
        TileWrapper* tile(const int id);
        TimeWrapper* time(const int id);
        TrainerWrapper* trainer(const int id);
        TypeWrapper* type(const int id);
        WeatherWrapper* weather(const int id);
        
        Q_SCRIPTABLE Sigmod::Stat stat(const QString& name) const;
        Q_SCRIPTABLE Sigmod::Direction direction(const QString& direction) const;
        
        Q_SCRIPTABLE QString title() const;
        Q_SCRIPTABLE QString version() const;
        Q_SCRIPTABLE QString description() const;
        Q_SCRIPTABLE bool singlePlayer() const;
        Q_SCRIPTABLE Sigcore::Script startScript();
        Q_SCRIPTABLE Sigcore::Fraction effectiveness(const TypeWrapper* attacker, const TypeWrapper* defender) const;
        Q_SCRIPTABLE QPoint mapPosition(const MapWrapper* map) const;
        Q_SCRIPTABLE RulesWrapper* rules();
        
        Q_SCRIPTABLE AbilityWrapper* ability(const QString& name);
        Q_SCRIPTABLE AuthorWrapper* author(const QString& name);
        Q_SCRIPTABLE BadgeWrapper* badge(const QString& name);
        Q_SCRIPTABLE CoinListWrapper* coinList(const QString& name);
        Q_SCRIPTABLE EggGroupWrapper* eggGroup(const QString& name);
        Q_SCRIPTABLE GlobalScriptWrapper* globalScript(const QString& name);
        Q_SCRIPTABLE ItemWrapper* item(const QString& name);
        Q_SCRIPTABLE ItemTypeWrapper* itemType(const QString& name);
        Q_SCRIPTABLE MapWrapper* map(const QString& name);
        Q_SCRIPTABLE MoveWrapper* move(const QString& name);
        Q_SCRIPTABLE NatureWrapper* nature(const QString& name);
        Q_SCRIPTABLE SkinWrapper* skin(const QString& name);
        Q_SCRIPTABLE SoundWrapper* sound(const QString& name);
        Q_SCRIPTABLE SpeciesWrapper* species(const QString& name);
        Q_SCRIPTABLE SpriteWrapper* sprite(const QString& name);
        Q_SCRIPTABLE StatusWrapper* status(const QString& name);
        Q_SCRIPTABLE StoreWrapper* store(const QString& name);
        Q_SCRIPTABLE TileWrapper* tile(const QString& name);
        Q_SCRIPTABLE TimeWrapper* time(const QString& name);
        Q_SCRIPTABLE TrainerWrapper* trainer(const QString& name);
        Q_SCRIPTABLE TypeWrapper* type(const QString& name);
        Q_SCRIPTABLE WeatherWrapper* weather(const QString& name);
    private:
        GameWrapper& operator=(const GameWrapper& rhs);
        
        const Sigmod::Game* m_game;
};
}
Q_DECLARE_METATYPE(Sigscript::GameWrapper*)

#endif
