/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_ITEMTYPEWRAPPER
#define SIGSCRIPT_ITEMTYPEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/ItemType.h>

namespace Sigscript
{
class SIGSCRIPT_EXPORT ItemTypeWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static ItemTypeWrapper* create(const Sigmod::ItemType* itemType, GameWrapper* parent);
        
        Q_SCRIPTABLE Sigmod::ItemType::Count count(const QString& name) const;
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int computer() const;
        Q_SCRIPTABLE int player() const;
        Q_SCRIPTABLE int maxWeight() const;
        Q_SCRIPTABLE int count() const;
    private:
        ItemTypeWrapper(const Sigmod::ItemType* itemType, GameWrapper* parent);
        ItemTypeWrapper& operator=(const ItemTypeWrapper& rhs);
        
        const Sigmod::ItemType* m_itemType;
};
}
Q_DECLARE_METATYPE(Sigscript::ItemTypeWrapper*)

#endif
