/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_RULESWRAPPER
#define SIGSCRIPT_RULESWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class Rules;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT RulesWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static RulesWrapper* create(const Sigmod::Rules* rules, GameWrapper* parent);
        
        Q_SCRIPTABLE bool genderAllowed() const;
        Q_SCRIPTABLE bool breedingAllowed() const;
        Q_SCRIPTABLE bool criticalDomains() const;
        Q_SCRIPTABLE int numBoxes() const;
        Q_SCRIPTABLE int boxSize() const;
        Q_SCRIPTABLE int maxParty() const;
        Q_SCRIPTABLE int maxFight() const;
        Q_SCRIPTABLE int maxPlayers() const;
        Q_SCRIPTABLE int maxHeldItems() const;
        Q_SCRIPTABLE int maxAbilities() const;
        Q_SCRIPTABLE int maxNatures() const;
        Q_SCRIPTABLE int maxMoves() const;
        Q_SCRIPTABLE int maxLevel() const;
        Q_SCRIPTABLE int maxStages() const;
        Q_SCRIPTABLE int maxMoney() const;
        Q_SCRIPTABLE int maxTotalWeight() const;
        Q_SCRIPTABLE bool specialSplit() const;
        Q_SCRIPTABLE bool specialDVSplit() const;
        Q_SCRIPTABLE int maxTotalEV() const;
        Q_SCRIPTABLE int maxEVPerStat() const;
    private:
        RulesWrapper(const Sigmod::Rules* rules, GameWrapper* parent);
        RulesWrapper& operator=(const RulesWrapper& rhs);
        
        const Sigmod::Rules* m_rules;
};
}
Q_DECLARE_METATYPE(Sigscript::RulesWrapper*)

#endif
