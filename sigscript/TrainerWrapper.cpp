/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TrainerWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

using namespace Sigmod;
using namespace Sigscript;

TrainerWrapper* TrainerWrapper::create(const Trainer* trainer, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(trainer->className(), trainer->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new TrainerWrapper(trainer, parent);
    return qobject_cast<TrainerWrapper*>(m_instances[sig]);
}

TrainerWrapper::TrainerWrapper(const Trainer* trainer, GameWrapper* parent) :
        ObjectWrapper(trainer, parent),
        m_trainer(trainer)
{
}

Trainer::Intelligence TrainerWrapper::intelligence(const QString& name) const
{
    if (name == "Ignorant")
        return Trainer::Ignorant;
    else if (name == "Determine")
        return Trainer::Determine;
    else if (name == "Remember")
        return Trainer::Remember;
    else if (name == "Cheating")
        return Trainer::Cheating;
    return QVariant(-1).value<Trainer::Intelligence>();
}

QString TrainerWrapper::TrainerWrapper::name() const
{
    return m_trainer->name();
}

int TrainerWrapper::TrainerWrapper::moneyFactor() const
{
    ALLOW_OVERRIDE_SO(trainer, int, moneyFactor);
    return m_trainer->moneyFactor();
}

SkinWrapper* TrainerWrapper::TrainerWrapper::skin()
{
    return game()->skin(m_trainer->skin());
}

int TrainerWrapper::TrainerWrapper::depth() const
{
    ALLOW_OVERRIDE_SO(trainer, int, depth);
    return m_trainer->depth();
}

Trainer::Intelligence TrainerWrapper::TrainerWrapper::teamIntel() const
{
    return m_trainer->teamIntel();
}

Trainer::Intelligence TrainerWrapper::TrainerWrapper::moveIntel() const
{
    return m_trainer->moveIntel();
}

Trainer::Intelligence TrainerWrapper::TrainerWrapper::itemIntel() const
{
    return m_trainer->itemIntel();
}

Trainer::Intelligence TrainerWrapper::TrainerWrapper::abilityIntel() const
{
    return m_trainer->abilityIntel();
}

Trainer::Intelligence TrainerWrapper::TrainerWrapper::statIntel() const
{
    return m_trainer->statIntel();
}
