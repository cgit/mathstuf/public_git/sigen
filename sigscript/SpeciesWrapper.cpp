/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpeciesWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "SpeciesMoveWrapper.h"

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

SpeciesWrapper* SpeciesWrapper::create(const Species* species, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(species->className(), species->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new SpeciesWrapper(species, parent);
    return qobject_cast<SpeciesWrapper*>(m_instances[sig]);
}

SpeciesWrapper::SpeciesWrapper(const Species* species, GameWrapper* parent) :
        ObjectWrapper(species, parent),
        m_species(species)
{
}

Hat<AbilityWrapper*> SpeciesWrapper::abilityHat()
{
    QMap<AbilityWrapper*, int> abilityMap = abilities();
    const QList<AbilityWrapper*> abilities = abilityMap.keys();
    Hat<AbilityWrapper*> hat;
    foreach (AbilityWrapper* ability, abilities)
        hat.add(ability, abilityMap[ability]);
    return hat;
}

Hat<ItemWrapper*> SpeciesWrapper::itemHat()
{
    QMap<ItemWrapper*, int> itemMap = items();
    const QList<ItemWrapper*> items = itemMap.keys();
    Hat<ItemWrapper*> hat;
    foreach (ItemWrapper* item, items)
        hat.add(item, itemMap[item]);
    return hat;
}

Species::Style SpeciesWrapper::growth(const QString& name) const
{
    if (name == "Fluctuating")
        return Species::Fluctuating;
    else if (name == "Fading")
        return Species::Fading;
    else if (name == "Slow")
        return Species::Slow;
    else if (name == "Normal")
        return Species::Normal;
    else if (name == "Fast")
        return Species::Fast;
    else if (name == "Erratic")
        return Species::Erratic;
    return QVariant(-1).value<Species::Style>();
}

QString SpeciesWrapper::name() const
{
    return m_species->name();
}

int SpeciesWrapper::baseStat(const Stat stat) const
{
    return m_species->baseStat(stat);
}

int SpeciesWrapper::effortValue(const Stat stat) const
{
    return m_species->effortValue(stat);
}

Species::Style SpeciesWrapper::growth() const
{
    return m_species->growth();
}

int SpeciesWrapper::experienceValue() const
{
    return m_species->experienceValue();
}

int SpeciesWrapper::catchValue() const
{
    return m_species->catchValue();
}

int SpeciesWrapper::maxHoldWeight() const
{
    return m_species->maxHoldWeight();
}

Fraction SpeciesWrapper::runChance() const
{
    ALLOW_OVERRIDE_SO(species, Fraction, runChance);
    return m_species->runChance();
}

Fraction SpeciesWrapper::fleeChance() const
{
    ALLOW_OVERRIDE_SO(species, Fraction, fleeChance);
    return m_species->fleeChance();
}

Fraction SpeciesWrapper::itemChance() const
{
    ALLOW_OVERRIDE_SO(species, Fraction, itemChance);
    return m_species->itemChance();
}

int SpeciesWrapper::encyclopediaNumber() const
{
    return m_species->encyclopediaNumber();
}

int SpeciesWrapper::weight() const
{
    return m_species->weight();
}

int SpeciesWrapper::height() const
{
    return m_species->height();
}

QString SpeciesWrapper::encyclopediaEntry() const
{
    return m_species->encyclopediaEntry();
}

SpriteWrapper* SpeciesWrapper::frontMaleSprite()
{
    return game()->sprite(m_species->frontMaleSprite());
}

SpriteWrapper* SpeciesWrapper::backMaleSprite()
{
    return game()->sprite(m_species->backMaleSprite());
}

SpriteWrapper* SpeciesWrapper::frontFemaleSprite()
{
    return game()->sprite(m_species->frontFemaleSprite());
}

SpriteWrapper* SpeciesWrapper::backFemaleSprite()
{
    return game()->sprite(m_species->backFemaleSprite());
}

SkinWrapper* SpeciesWrapper::skin()
{
    return game()->skin(m_species->skin());
}

Fraction SpeciesWrapper::genderFactor() const
{
    return m_species->genderFactor();
}

int SpeciesWrapper::eggSpecies() const
{
    ALLOW_OVERRIDE_SO(species, int, eggSpecies);
    return m_species->eggSpecies();
}

int SpeciesWrapper::eggSteps() const
{
    return m_species->eggSteps();
}

QList<TypeWrapper*> SpeciesWrapper::types()
{
    QList<int> typeIds = m_species->type();
    QList<TypeWrapper*> types;
    foreach (int id, typeIds)
        types.append(game()->type(id));
    return types;
}

QList<EggGroupWrapper*> SpeciesWrapper::eggGroups()
{
    QList<int> eggGroupIds = m_species->eggGroup();
    QList<EggGroupWrapper*> eggGroups;
    foreach (int id, eggGroupIds)
        eggGroups.append(game()->eggGroup(id));
    return eggGroups;
}

Script SpeciesWrapper::evolution() const
{
    return m_species->evolution();
}

QMap<AbilityWrapper*, int> SpeciesWrapper::abilities()
{
    const QMap<int, int>& abilityMap = m_species->ability();
    QList<int> abilityIds = abilityMap.keys();
    QMap<AbilityWrapper*, int> abilities;
    foreach (int id, abilityIds)
        abilities[game()->ability(id)] = abilityMap[id];
    return abilities;
}

QMap<ItemWrapper*, int> SpeciesWrapper::items()
{
    const QMap<int, int>& itemMap = m_species->item();
    QList<int> itemIds = itemMap.keys();
    QMap<ItemWrapper*, int> items;
    foreach (int id, itemIds)
        items[game()->item(id)] = itemMap[id];
    return items;
}

SpeciesMoveWrapper* SpeciesWrapper::move(const int index)
{
    return SpeciesMoveWrapper::create(m_species->move(index), this);
}

int SpeciesWrapper::moveCount() const
{
    return m_species->moveCount();
}
