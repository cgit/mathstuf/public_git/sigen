/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_GLOBALSCRIPTWRAPPER
#define SIGSCRIPT_GLOBALSCRIPTWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class GlobalScript;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT GlobalScriptWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static GlobalScriptWrapper* create(const Sigmod::GlobalScript* globalScript, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        GlobalScriptWrapper(const Sigmod::GlobalScript* globalScript, GameWrapper* parent);
        GlobalScriptWrapper& operator=(const GlobalScriptWrapper& rhs);
        
        const Sigmod::GlobalScript* m_globalScript;
};
}
Q_DECLARE_METATYPE(Sigscript::GlobalScriptWrapper*)

#endif
