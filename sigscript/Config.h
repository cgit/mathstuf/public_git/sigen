/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_CONFIG
#define SIGSCRIPT_CONFIG

// Sigscript includes
#include "ConfigOptions.h"
#include "Global.h"
#include "ValueMap.h"

// Qt includes
#include <QtCore/QObject>

namespace Sigscript
{
class SIGSCRIPT_EXPORT Config : public QObject
{
    Q_OBJECT
    
    public:
        Config(Config* parent);
        
        Q_SCRIPTABLE QVariant value(const QString& name, const bool recursive = true) const;
        template<typename T> bool valueOfType(const QString& name, T* value, const bool recursive = true) const;
        Q_SCRIPTABLE bool hasValue(const QString& name, const bool recursive = true) const;
        template<typename T> bool hasValueOfType(const QString& name, const bool recursive = true) const;
        Q_SCRIPTABLE QStringList values(const bool recursive = false) const;
        
        Q_SCRIPTABLE ConfigOptions options(const QString& name, const bool recursive = true) const;
        Q_SCRIPTABLE ConfigOptions overrides(const QString& name) const;
    signals:
        void valueAdded(const QString& name, const QVariant& value);
        void valueChanged(const QString& name, const QVariant& newValue);
        void valueRemoved(const QString& name);
        
        void optionsChanged(const QString& name, const Sigscript::ConfigOptions newOptions);
        void overridesChanged(const QString& name, const Sigscript::ConfigOptions newOverrides);
    public slots:
        bool addValue(const QString& name, const QVariant& value);
        bool setValue(const QString& name, const QVariant& value);
        bool removeValue(const QString& name, const bool shadow = false);
        
        bool setOptions(const QString& name, const Sigscript::ConfigOptions options);
        bool unsetOptions(const QString& name, const Sigscript::ConfigOptions options);
        
        bool setOverrides(const QString& name, const Sigscript::ConfigOptions options);
        bool unsetOverrides(const QString& name, const Sigscript::ConfigOptions options);
        
        void clean();
        
        virtual void writeBack();
    private:
        Config* m_parent;
        ValueMap m_values;
};

template<typename T> bool Config::valueOfType(const QString& name, T* dest, const bool recursive) const
{
    if (hasValueOfType<T>(name))
    {
        *dest = value(name, recursive).value<T>();
        return true;
    }
    return false;
}

template<typename T> bool Config::hasValueOfType(const QString& name, const bool recursive) const
{
    if (hasValue(name) && value(name, recursive).canConvert<T>())
        return true;
    return false;
}

}
Q_DECLARE_METATYPE(Sigscript::Config*)

#endif
