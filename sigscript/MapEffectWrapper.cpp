/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapEffectWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapWrapper.h"

// Sigmod includes
#include <sigmod/MapEffect.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

MapEffectWrapper* MapEffectWrapper::create(const MapEffect* effect, MapWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(effect->className(), effect->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapEffectWrapper(effect, parent);
    return qobject_cast<MapEffectWrapper*>(m_instances[sig]);
}

MapEffectWrapper::MapEffectWrapper(const MapEffect* effect, MapWrapper* parent) :
        ObjectWrapper(effect, parent),
        m_effect(effect)
{
}

QString MapEffectWrapper::name() const
{
    return m_effect->name();
}

QPoint MapEffectWrapper::position() const
{
    return m_effect->position();
}

QPainterPath MapEffectWrapper::area() const
{
    return m_effect->area();
}

SkinWrapper* MapEffectWrapper::skin()
{
    return game()->skin(m_effect->skin());
}

bool MapEffectWrapper::isGhost() const
{
    ALLOW_OVERRIDE_SO(effect, bool, isGhost);
    return m_effect->isGhost();
}

Script MapEffectWrapper::script() const
{
    return m_effect->script();
}
