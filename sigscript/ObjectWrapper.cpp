/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ObjectWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Object.h>

using namespace Sigmod;
using namespace Sigscript;

QMap<ObjectWrapper::Signature, ObjectWrapper*> ObjectWrapper::m_instances;

ObjectWrapper::ObjectWrapper(const Object* object, ObjectWrapper* parent) :
        Config(parent),
        m_object(object)
{
}

int ObjectWrapper::id() const
{
    return m_object->id();
}

const ObjectWrapper* ObjectWrapper::parent() const
{
    return m_parent;
}

ObjectWrapper* ObjectWrapper::parent()
{
    return m_parent;
}

const GameWrapper* ObjectWrapper::game() const
{
    if (m_parent)
        return m_parent->game();
    return qobject_cast<const GameWrapper*>(m_parent);
}

GameWrapper* ObjectWrapper::game()
{
    if (m_parent)
        return m_parent->game();
    return qobject_cast<GameWrapper*>(m_parent);
}
