/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SkinWrapper.h"

// Sigmod scripting
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Skin.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

SkinWrapper* SkinWrapper::create(const Skin* skin, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(skin->className(), skin->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new SkinWrapper(skin, parent);
    return qobject_cast<SkinWrapper*>(m_instances[sig]);
}

SkinWrapper::SkinWrapper(const Skin* skin, GameWrapper* parent) :
        ObjectWrapper(skin, parent),
        m_skin(skin)
{
}

QString SkinWrapper::name() const
{
    return m_skin->name();
}

QSize SkinWrapper::size() const
{
    return m_skin->size();
}

Script SkinWrapper::script() const
{
    return m_skin->script();
}
