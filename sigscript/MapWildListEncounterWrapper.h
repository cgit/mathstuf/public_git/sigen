/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPWILDLISTENCOUNTERWRAPPER
#define SIGSCRIPT_MAPWILDLISTENCOUNTERWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class MapWildListEncounter;
}

namespace Sigscript
{
class MapWildListWrapper;
class SpeciesWrapper;

class SIGSCRIPT_EXPORT MapWildListEncounterWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapWildListEncounterWrapper* create(const Sigmod::MapWildListEncounter* encounter, MapWildListWrapper* parent);
        
        Q_SCRIPTABLE SpeciesWrapper* species();
        Q_SCRIPTABLE int level() const;
        Q_SCRIPTABLE int weight() const;
    private:
        MapWildListEncounterWrapper(const Sigmod::MapWildListEncounter* encounter, MapWildListWrapper* parent);
        MapWildListEncounterWrapper& operator=(const MapWildListEncounterWrapper& rhs);
        
        const Sigmod::MapWildListEncounter* m_encounter;
};
}
Q_DECLARE_METATYPE(Sigscript::MapWildListEncounterWrapper*)

#endif
