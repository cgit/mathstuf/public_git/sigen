/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MOVEWRAPPER
#define SIGSCRIPT_MOVEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Move;
}

namespace Sigscript
{
class TypeWrapper;

class SIGSCRIPT_EXPORT MoveWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MoveWrapper* create(const Sigmod::Move* move, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigcore::Fraction accuracy() const;
        Q_SCRIPTABLE int power() const;
        Q_SCRIPTABLE TypeWrapper* type();
        Q_SCRIPTABLE bool special() const;
        Q_SCRIPTABLE int powerPoints() const;
        Q_SCRIPTABLE int priority() const;
        Q_SCRIPTABLE QString description() const;
        Q_SCRIPTABLE Sigcore::Script battleScript() const;
        Q_SCRIPTABLE Sigcore::Script worldScript() const;
        Q_SCRIPTABLE Sigcore::Script priorityScript() const;
    private:
        MoveWrapper(const Sigmod::Move* move, GameWrapper* parent);
        MoveWrapper& operator=(const MoveWrapper& rhs);
        
        const Sigmod::Move* m_move;
};
}
Q_DECLARE_METATYPE(Sigscript::MoveWrapper*)

#endif
