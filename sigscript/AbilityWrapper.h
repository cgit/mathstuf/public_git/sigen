/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_ABILITYWRAPPER
#define SIGSCRIPT_ABILITYWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Ability;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT AbilityWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static AbilityWrapper* create(const Sigmod::Ability* ability, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int priority() const;
        Q_SCRIPTABLE QString description() const;
        Q_SCRIPTABLE Sigcore::Script battleScript() const;
        Q_SCRIPTABLE Sigcore::Script worldScript() const;
        Q_SCRIPTABLE Sigcore::Script priorityScript() const;
    private:
        AbilityWrapper(const Sigmod::Ability* ability, GameWrapper* parent);
        AbilityWrapper& operator=(const AbilityWrapper& rhs);
        
        const Sigmod::Ability* m_ability;
};
}
Q_DECLARE_METATYPE(Sigscript::AbilityWrapper*)

#endif
