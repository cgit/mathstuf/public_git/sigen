/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Config.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigscript;

Config::Config(Config* parent) :
        QObject(parent),
        m_parent(parent)
{
}

bool Config::addValue(const QString& name, const QVariant& value)
{
    if (!hasValue(name))
        return setValue(name, value);
    return false;
}

bool Config::setValue(const QString& name, const QVariant& value)
{
    if (hasValue(name) && (options(name) & ReadOnly))
        return false;
    if (!m_values.contains(name))
        emit(valueAdded(name, value));
    if (options(name) & Deleted)
        unsetOptions(name, Deleted);
    m_values.setValue(name, value);
    emit(valueChanged(name, value));
    return true;
}

bool Config::removeValue(const QString& name, const bool shadow)
{
    if (shadow)
        setOptions(name, Deleted);
    else if (m_values.contains(name))
        m_values.unsetValue(name);
    else
        return false;
    emit(valueRemoved(name));
    return true;
}

QVariant Config::value(const QString& name, const bool recursive) const
{
    if (m_values.contains(name))
    {
        if (options(name) & (Deleted | Hidden))
            return QVariant();
        return m_values.value(name);
    }
    if (recursive && m_parent)
        return m_parent->value(name, true);
    return QVariant();
}

bool Config::hasValue(const QString& name, const bool recursive) const
{
    if (m_values.contains(name))
        return !(options(name) & (Deleted | Hidden));
    if (recursive && m_parent)
        return m_parent->hasValue(name, true);
    return false;
}

QStringList Config::values(const bool recursive) const
{
    QStringList values = m_values.keys();
    if (recursive && m_parent)
        values += m_parent->values(true);
    const QStringList keys = m_values.keys();
    foreach (const QString& key, keys)
    {
        if (options(key) & (Deleted | Hidden))
            values.removeAll(key);
    }
    return values.toSet().toList();
}

bool Config::setOptions(const QString& name, const ConfigOptions newOptions)
{
    if ((~options(name) & newOptions) || (overrides(name) & newOptions) || addValue(name, QVariant()))
    {
        m_values.setOptions(name, newOptions);
        emit(optionsChanged(name, options(name)));
        return true;
    }
    return false;
}

bool Config::unsetOptions(const QString& name, const ConfigOptions oldOptions)
{
    if ((options(name) & oldOptions) || (overrides(name) & oldOptions))
    {
        m_values.unsetOptions(name, oldOptions);
        emit(optionsChanged(name, options(name)));
        return true;
    }
    return false;
}

ConfigOptions Config::options(const QString& name, const bool recursive) const
{
    ConfigOptions options = 0;
    if (m_values.contains(name))
        options = m_values.options(name);
    if (recursive && m_parent)
        options |= m_parent->options(name, recursive) & ~overrides(name);
    return options;
}

bool Config::setOverrides(const QString& name, const ConfigOptions newOverrides)
{
    if ((~overrides(name) & newOverrides) || addValue(name, QVariant()))
    {
        m_values.setOptions(name, options(name) & newOverrides);
        m_values.setOverrides(name, newOverrides);
        emit(overridesChanged(name, overrides(name)));
        return true;
    }
    return false;
}

bool Config::unsetOverrides(const QString& name, const ConfigOptions oldOverrides)
{
    if (overrides(name) & oldOverrides)
    {
        m_values.unsetOverrides(name, oldOverrides);
        emit(overridesChanged(name, overrides(name)));
        return true;
    }
    return false;
}

ConfigOptions Config::overrides(const QString& name) const
{
    if (m_values.contains(name))
        return m_values.overrides(name);
    return 0;
}

void Config::clean()
{
    QStringList values = m_values.keys();
    foreach (const QString& value, values)
    {
        unsetOptions(value, Hidden);
        if (options(value) & Temporary)
            removeValue(value, false);
    }
}

void Config::writeBack()
{
}
