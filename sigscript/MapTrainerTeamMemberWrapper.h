/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPTRAINERTEAMMEMBERWRAPPER
#define SIGSCRIPT_MAPTRAINERTEAMMEMBERWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class MapTrainerTeamMember;
}

namespace Sigscript
{
class AbilityWrapper;
class ItemWrapper;
class MapTrainerWrapper;
class MoveWrapper;
class NatureWrapper;
class SpeciesWrapper;

class SIGSCRIPT_EXPORT MapTrainerTeamMemberWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapTrainerTeamMemberWrapper* create(const Sigmod::MapTrainerTeamMember* teamMember, MapTrainerWrapper* parent);
        
        Q_SCRIPTABLE SpeciesWrapper* species();
        Q_SCRIPTABLE int level() const;
        Q_SCRIPTABLE QList<AbilityWrapper*> abilities();
        Q_SCRIPTABLE QMap<ItemWrapper*, int> items();
        Q_SCRIPTABLE QList<MoveWrapper*> moves();
        Q_SCRIPTABLE QList<NatureWrapper*> natures();
    private:
        MapTrainerTeamMemberWrapper(const Sigmod::MapTrainerTeamMember* teamMember, MapTrainerWrapper* parent);
        MapTrainerTeamMemberWrapper& operator=(const MapTrainerTeamMemberWrapper& rhs);
        
        const Sigmod::MapTrainerTeamMember* m_teamMember;
};
}
Q_DECLARE_METATYPE(Sigscript::MapTrainerTeamMemberWrapper*)

#endif
