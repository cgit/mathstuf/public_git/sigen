/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_SPRITEWRAPPER
#define SIGSCRIPT_SPRITEWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Qt includes
#include <QtGui/QPixmap>

// Forward declarations
namespace Sigmod
{
class Sprite;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT SpriteWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static SpriteWrapper* create(const Sigmod::Sprite* sprite, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE QPixmap sprite() const;
    private:
        SpriteWrapper(const Sigmod::Sprite* sprite, GameWrapper* parent);
        SpriteWrapper& operator=(const SpriteWrapper& rhs);
        
        QPixmap m_pixmap;
        const Sigmod::Sprite* m_sprite;
};
}
Q_DECLARE_METATYPE(Sigscript::SpriteWrapper*)

#endif
