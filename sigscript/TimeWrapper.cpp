/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TimeWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Time.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

TimeWrapper* TimeWrapper::create(const Time* time, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(time->className(), time->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new TimeWrapper(time, parent);
    return qobject_cast<TimeWrapper*>(m_instances[sig]);
}

TimeWrapper::TimeWrapper(const Time* time, GameWrapper* parent) :
        ObjectWrapper(time, parent),
        m_time(time)
{
}

QString TimeWrapper::TimeWrapper::name() const
{
    return m_time->name();
}

int TimeWrapper::TimeWrapper::hour() const
{
    ALLOW_OVERRIDE(time, bool, hour);
    return m_time->hour();
}

int TimeWrapper::TimeWrapper::minute() const
{
    ALLOW_OVERRIDE(time, bool, minute);
    return m_time->minute();
}

Script TimeWrapper::script() const
{
    return m_time->script();
}
