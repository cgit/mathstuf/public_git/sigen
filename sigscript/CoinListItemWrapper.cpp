/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "CoinListItemWrapper.h"

// Sigscript includes
#include "CoinListWrapper.h"
#include "GameWrapper.h"

using namespace Sigmod;
using namespace Sigscript;

CoinListItemWrapper* CoinListItemWrapper::create(const CoinListItem* object, CoinListWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(object->className(), object->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new CoinListItemWrapper(object, parent);
    return qobject_cast<CoinListItemWrapper*>(m_instances[sig]);
}

CoinListItemWrapper::CoinListItemWrapper(const CoinListItem* object, CoinListWrapper* parent) :
        ObjectWrapper(object, parent),
        m_object(object)
{
}

CoinListItem::Type CoinListItemWrapper::type(const QString& name) const
{
    if (name == "Item")
        return CoinListItem::Item;
    else if (name == "Species")
        return CoinListItem::Species;
    return QVariant(-1).value<CoinListItem::Type>();
}

CoinListItem::Type CoinListItemWrapper::type() const
{
    return m_object->type();
}

ItemWrapper* CoinListItemWrapper::itemObject()
{
    if (m_object->type() == CoinListItem::Item)
        return game()->item(m_object->object());
    return NULL;
}

SpeciesWrapper* CoinListItemWrapper::speciesObject()
{
    if (m_object->type() == CoinListItem::Species)
        return game()->species(m_object->object());
    return NULL;
}

int CoinListItemWrapper::cost() const
{
    ALLOW_OVERRIDE_SO(object, int, cost);
    return m_object->cost();
}
