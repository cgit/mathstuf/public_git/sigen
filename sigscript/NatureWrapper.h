/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_NATUREWRAPPER
#define SIGSCRIPT_NATUREWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Fraction.h>

// Sigmod includes
#include <sigmod/Stat.h>

// Forward declarations
namespace Sigmod
{
class Nature;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT NatureWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static NatureWrapper* create(const Sigmod::Nature* nature, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigcore::Fraction stat(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE int weight() const;
    private:
        NatureWrapper(const Sigmod::Nature* nature, GameWrapper* parent);
        NatureWrapper& operator=(const NatureWrapper& rhs);
        
        const Sigmod::Nature* m_nature;
};
}
Q_DECLARE_METATYPE(Sigscript::NatureWrapper*)

#endif
