/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_ITEMWRAPPER
#define SIGSCRIPT_ITEMWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Forward declarations
namespace Sigmod
{
class Item;
}

namespace Sigscript
{
class ItemTypeWrapper;

class SIGSCRIPT_EXPORT ItemWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static ItemWrapper* create(const Sigmod::Item* item, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE ItemTypeWrapper* type();
        Q_SCRIPTABLE int price() const;
        Q_SCRIPTABLE int sellPrice() const;
        Q_SCRIPTABLE int weight() const;
        Q_SCRIPTABLE QString description() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        ItemWrapper(const Sigmod::Item* item, GameWrapper* parent);
        ItemWrapper& operator=(const ItemWrapper& rhs);
        
        const Sigmod::Item* m_item;
};
}
Q_DECLARE_METATYPE(Sigscript::ItemWrapper*)

#endif
