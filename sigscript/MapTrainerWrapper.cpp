/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTrainerWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapWrapper.h"
#include "MapTrainerTeamMemberWrapper.h"

// Sigmod includes
#include <sigmod/MapTrainer.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

MapTrainerWrapper* MapTrainerWrapper::create(const MapTrainer* trainer, MapWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(trainer->className(), trainer->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapTrainerWrapper(trainer, parent);
    return qobject_cast<MapTrainerWrapper*>(m_instances[sig]);
}

MapTrainerWrapper::MapTrainerWrapper(const MapTrainer* trainer, MapWrapper* parent) :
        ObjectWrapper(trainer, parent),
        m_trainer(trainer)
{
}

QString MapTrainerWrapper::name() const
{
    return m_trainer->name();
}

TrainerWrapper* MapTrainerWrapper::trainerClass()
{
    return game()->trainer(m_trainer->trainerClass());
}

QPoint MapTrainerWrapper::position() const
{
    ALLOW_OVERRIDE_SO(trainer, QPoint, position);
    return m_trainer->position();
}

int MapTrainerWrapper::numberFight() const
{
    return m_trainer->numberFight();
}

QList<MapTrainerTeamMemberWrapper*> MapTrainerWrapper::leadTeamMember()
{
    QList<MapTrainerTeamMemberWrapper*> teamMembers;
    foreach (int member, m_trainer->leadTeamMember())
        teamMembers.append(teamMember(member));
    return teamMembers;
}

Script MapTrainerWrapper::script() const
{
    return m_trainer->script();
}

MapTrainerTeamMemberWrapper* MapTrainerWrapper::teamMember(const int index)
{
    return MapTrainerTeamMemberWrapper::create(m_trainer->teamMember(index), this);
}

int MapTrainerWrapper::teamMemberCount() const
{
    return m_trainer->teamMemberCount();
}
