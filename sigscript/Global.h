/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_GLOBAL
#define SIGSCRIPT_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGSCRIPT_EXPORT
#   ifdef MAKE_SIGSCRIPT_LIB
#       define SIGSCRIPT_EXPORT KDE_EXPORT
#   else
#       define SIGSCRIPT_EXPORT KDE_IMPORT
#   endif
#   define SIGSCRIPT_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGSCRIPT_EXPORT_DEPRECATED
#   define SIGSCRIPT_EXPORT_DEPRECATED KDE_DEPRECATED SIGSCRIPT_EXPORT
#endif

#ifdef MAKE_SIGSCRIPT_LIB

#define ALLOW_OVERRIDE_SO(class, type, variable) \
    if (game()->singlePlayer()) \
    { \
        ALLOW_OVERRIDE(class, type, variable); \
    }
#define ALLOW_OVERRIDE(class, type, variable) \
    type value = type(); \
    if (valueOfType(#variable, &value) && m_##class->variable##Check(value)) \
        return value

#endif

#endif
