/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTrainerTeamMemberWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapTrainerWrapper.h"

// Sigmod includes
#include <sigmod/MapTrainerTeamMember.h>

using namespace Sigmod;
using namespace Sigscript;

MapTrainerTeamMemberWrapper* MapTrainerTeamMemberWrapper::create(const MapTrainerTeamMember* teamMember, MapTrainerWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(teamMember->className(), teamMember->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapTrainerTeamMemberWrapper(teamMember, parent);
    return qobject_cast<MapTrainerTeamMemberWrapper*>(m_instances[sig]);
}

MapTrainerTeamMemberWrapper::MapTrainerTeamMemberWrapper(const MapTrainerTeamMember* teamMember, MapTrainerWrapper* parent) :
        ObjectWrapper(teamMember, parent),
        m_teamMember(teamMember)
{
}

SpeciesWrapper* MapTrainerTeamMemberWrapper::species()
{
    return game()->species(m_teamMember->species());
}

int MapTrainerTeamMemberWrapper::level() const
{
    ALLOW_OVERRIDE_SO(teamMember, int, level);
    return m_teamMember->level();
}

QList<AbilityWrapper*> MapTrainerTeamMemberWrapper::abilities()
{
    QList<int> abilityIds = m_teamMember->ability();
    QList<AbilityWrapper*> abilities;
    foreach (int id, abilityIds)
        abilities.append(game()->ability(id));
    return abilities;
}

QMap<ItemWrapper*, int> MapTrainerTeamMemberWrapper::items()
{
    const QMap<int, int>& itemMap = m_teamMember->item();
    QList<int> itemIds = itemMap.keys();
    QMap<ItemWrapper*, int> items;
    foreach (int id, itemIds)
        items[game()->item(id)] = itemMap[id];
    return items;
}

QList<MoveWrapper*> MapTrainerTeamMemberWrapper::moves()
{
    QList<int> moveIds = m_teamMember->move();
    QList<MoveWrapper*> moves;
    foreach (int id, moveIds)
        moves.append(game()->move(id));
    return moves;
}

QList<NatureWrapper*> MapTrainerTeamMemberWrapper::natures()
{
    QList<int> natureIds = m_teamMember->nature();
    QList<NatureWrapper*> natures;
    foreach (int id, natureIds)
        natures.append(game()->nature(id));
    return natures;
}
