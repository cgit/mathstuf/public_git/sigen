/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_EGGGROUPWRAPPER
#define SIGSCRIPT_EGGGROUPWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Forward declarations
namespace Sigmod
{
class EggGroup;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT EggGroupWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static EggGroupWrapper* create(const Sigmod::EggGroup* eggGroup, GameWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
    private:
        EggGroupWrapper(const Sigmod::EggGroup* eggGroup, GameWrapper* parent);
        EggGroupWrapper& operator=(const EggGroupWrapper& rhs);
        
        const Sigmod::EggGroup* m_eggGroup;
};
}
Q_DECLARE_METATYPE(Sigscript::EggGroupWrapper*)

#endif
