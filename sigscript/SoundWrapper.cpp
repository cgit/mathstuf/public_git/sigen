/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SoundWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Qt includes
#include <QtCore/QBuffer>

// Phonon includes
#include <Phonon/MediaObject>
#include <Phonon/MediaSource>

using namespace Sigmod;
using namespace Sigscript;

SoundWrapper* SoundWrapper::create(const Sound* sound, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(sound->className(), sound->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new SoundWrapper(sound, parent);
    return qobject_cast<SoundWrapper*>(m_instances[sig]);
}

SoundWrapper::SoundWrapper(const Sound* sound, GameWrapper* parent) :
        ObjectWrapper(sound, parent),
        m_sound(sound)
{
}

Sound::Type SoundWrapper::type(const QString& name) const
{
    if (name == "Sound Effect")
        return Sound::SoundEffect;
    else if (name == "Music")
        return Sound::Music;
    return QVariant(-1).value<Sound::Type>();
}

QString SoundWrapper::name() const
{
    return m_sound->name();
}

Sound::Type SoundWrapper::type() const
{
    return m_sound->type();
}

Phonon::MediaObject* SoundWrapper::audio()
{
    Phonon::MediaObject* media = new Phonon::MediaObject(this);
    QBuffer* buffer = new QBuffer(media);
    buffer->setData(m_sound->data());
    media->setCurrentSource(buffer);
    return media;
}
