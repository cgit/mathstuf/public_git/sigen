/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWildListEncounterWrapper.h"

// Sigscript includes
#include "GameWrapper.h"
#include "MapWildListWrapper.h"

// Sigmod includes
#include <sigmod/MapWildListEncounter.h>

using namespace Sigmod;
using namespace Sigscript;

MapWildListEncounterWrapper* MapWildListEncounterWrapper::create(const MapWildListEncounter* encounter, MapWildListWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(encounter->className(), encounter->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new MapWildListEncounterWrapper(encounter, parent);
    return qobject_cast<MapWildListEncounterWrapper*>(m_instances[sig]);
}

MapWildListEncounterWrapper::MapWildListEncounterWrapper(const MapWildListEncounter* encounter, MapWildListWrapper* parent) :
        ObjectWrapper(encounter, parent),
        m_encounter(encounter)
{
}

SpeciesWrapper* MapWildListEncounterWrapper::species()
{
    return game()->species(m_encounter->species());
}

int MapWildListEncounterWrapper::level() const
{
    ALLOW_OVERRIDE_SO(encounter, int, level);
    return m_encounter->level();
}

int MapWildListEncounterWrapper::weight() const
{
    ALLOW_OVERRIDE_SO(encounter, int, weight);
    return m_encounter->weight();
}
