/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPWARPWRAPPER
#define SIGSCRIPT_MAPWARPWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/MapWarp.h>

namespace Sigscript
{
// Forward declarations
class MapWrapper;

class SIGSCRIPT_EXPORT MapWarpWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapWarpWrapper* create(const Sigmod::MapWarp* warp, MapWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE QPoint position() const;
        Q_SCRIPTABLE QPainterPath area() const;
        Q_SCRIPTABLE MapWarpWrapper* toWarp();
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        MapWarpWrapper(const Sigmod::MapWarp* warp, MapWrapper* parent);
        MapWarpWrapper& operator=(const MapWarpWrapper& rhs);
        
        const Sigmod::MapWarp* m_warp;
};
}
Q_DECLARE_METATYPE(Sigscript::MapWarpWrapper*)

#endif
