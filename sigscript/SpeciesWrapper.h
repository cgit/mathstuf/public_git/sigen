/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_SPECIESWRAPPER
#define SIGSCRIPT_SPECIESWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Hat.h>

// Sigmod includes
#include <sigmod/Species.h>

namespace Sigscript
{
// Forward declarations
class AbilityWrapper;
class EggGroupWrapper;
class ItemWrapper;
class SkinWrapper;
class SpeciesMoveWrapper;
class SpriteWrapper;
class TypeWrapper;

class SIGSCRIPT_EXPORT SpeciesWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static SpeciesWrapper* create(const Sigmod::Species* species, GameWrapper* parent);
        
        Sigcore::Hat<AbilityWrapper*> abilityHat();
        Sigcore::Hat<ItemWrapper*> itemHat();
        
        Q_SCRIPTABLE Sigmod::Species::Style growth(const QString& name) const;
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE int baseStat(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE int effortValue(const Sigmod::Stat stat) const;
        Q_SCRIPTABLE Sigmod::Species::Style growth() const;
        Q_SCRIPTABLE int experienceValue() const;
        Q_SCRIPTABLE int catchValue() const;
        Q_SCRIPTABLE int maxHoldWeight() const;
        Q_SCRIPTABLE Sigcore::Fraction runChance() const;
        Q_SCRIPTABLE Sigcore::Fraction fleeChance() const;
        Q_SCRIPTABLE Sigcore::Fraction itemChance() const;
        Q_SCRIPTABLE int encyclopediaNumber() const;
        Q_SCRIPTABLE int weight() const;
        Q_SCRIPTABLE int height() const;
        Q_SCRIPTABLE QString encyclopediaEntry() const;
        Q_SCRIPTABLE SpriteWrapper* frontMaleSprite();
        Q_SCRIPTABLE SpriteWrapper* backMaleSprite();
        Q_SCRIPTABLE SpriteWrapper* frontFemaleSprite();
        Q_SCRIPTABLE SpriteWrapper* backFemaleSprite();
        Q_SCRIPTABLE SkinWrapper* skin();
        Q_SCRIPTABLE Sigcore::Fraction genderFactor() const;
        Q_SCRIPTABLE int eggSpecies() const;
        Q_SCRIPTABLE int eggSteps() const;
        Q_SCRIPTABLE QList<TypeWrapper*> types();
        Q_SCRIPTABLE QList<EggGroupWrapper*> eggGroups();
        Q_SCRIPTABLE Sigcore::Script evolution() const;
        Q_SCRIPTABLE QMap<AbilityWrapper*, int> abilities();
        Q_SCRIPTABLE QMap<ItemWrapper*, int> items();
        
        Q_SCRIPTABLE SpeciesMoveWrapper* move(const int index);
        Q_SCRIPTABLE int moveCount() const;
    private:
        SpeciesWrapper(const Sigmod::Species* species, GameWrapper* parent);
        SpeciesWrapper& operator=(const SpeciesWrapper& rhs);
        
        const Sigmod::Species* m_species;
};
}
Q_DECLARE_METATYPE(Sigscript::SpeciesWrapper*)

#endif
