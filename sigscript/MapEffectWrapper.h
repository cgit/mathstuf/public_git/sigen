/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_MAPEFFECTWRAPPER
#define SIGSCRIPT_MAPEFFECTWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigcore includes
#include <sigcore/Script.h>

// Qt includes
#include <QtGui/QPainterPath>

// Forward declarations
namespace Sigmod
{
class MapEffect;
}

namespace Sigscript
{
class MapWrapper;
class SkinWrapper;

class SIGSCRIPT_EXPORT MapEffectWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static MapEffectWrapper* create(const Sigmod::MapEffect* effect, MapWrapper* parent);
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE QPoint position() const;
        Q_SCRIPTABLE QPainterPath area() const;
        Q_SCRIPTABLE SkinWrapper* skin();
        Q_SCRIPTABLE bool isGhost() const;
        Q_SCRIPTABLE Sigcore::Script script() const;
    private:
        MapEffectWrapper(const Sigmod::MapEffect* effect, MapWrapper* parent);
        MapEffectWrapper& operator=(const MapEffectWrapper& rhs);
        
        const Sigmod::MapEffect* m_effect;
};
}
Q_DECLARE_METATYPE(Sigscript::MapEffectWrapper*)

#endif
