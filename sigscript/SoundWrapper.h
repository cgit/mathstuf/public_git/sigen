/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGSCRIPT_SOUNDWRAPPER
#define SIGSCRIPT_SOUNDWRAPPER

// Sigscript includes
#include "ObjectWrapper.h"

// Sigmod includes
#include <sigmod/Sound.h>

// Forward declarations
namespace Phonon
{
class MediaObject;
}

namespace Sigscript
{
class SIGSCRIPT_EXPORT SoundWrapper : public ObjectWrapper
{
    Q_OBJECT
    
    public:
        static SoundWrapper* create(const Sigmod::Sound* sound, GameWrapper* parent);
        
        Q_SCRIPTABLE Sigmod::Sound::Type type(const QString& name) const;
        
        Q_SCRIPTABLE QString name() const;
        Q_SCRIPTABLE Sigmod::Sound::Type type() const;
        // TODO: Use KALEngine?
        Q_SCRIPTABLE Phonon::MediaObject* audio();
    private:
        SoundWrapper(const Sigmod::Sound* sound, GameWrapper* parent);
        SoundWrapper& operator=(const SoundWrapper& rhs);
        
        const Sigmod::Sound* m_sound;
};
}
Q_DECLARE_METATYPE(Sigscript::SoundWrapper*)

#endif
