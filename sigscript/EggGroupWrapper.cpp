/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "EggGroupWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/EggGroup.h>

using namespace Sigmod;
using namespace Sigscript;

EggGroupWrapper* EggGroupWrapper::create(const EggGroup* eggGroup, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(eggGroup->className(), eggGroup->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new EggGroupWrapper(eggGroup, parent);
    return qobject_cast<EggGroupWrapper*>(m_instances[sig]);
}

EggGroupWrapper::EggGroupWrapper(const EggGroup* eggGroup, GameWrapper* parent) :
        ObjectWrapper(eggGroup, parent),
        m_eggGroup(eggGroup)
{
}

QString EggGroupWrapper::name() const
{
    return m_eggGroup->name();
}
