/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ItemWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Item.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

ItemWrapper* ItemWrapper::create(const Item* item, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(item->className(), item->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new ItemWrapper(item, parent);
    return qobject_cast<ItemWrapper*>(m_instances[sig]);
}

ItemWrapper::ItemWrapper(const Item* item, GameWrapper* parent) :
        ObjectWrapper(item, parent),
        m_item(item)
{
}

QString ItemWrapper::name() const
{
    return m_item->name();
}

ItemTypeWrapper* ItemWrapper::type()
{
    return game()->itemType(m_item->type());
}

int ItemWrapper::price() const
{
    ALLOW_OVERRIDE_SO(item, int, price);
    return m_item->price();
}

int ItemWrapper::sellPrice() const
{
    ALLOW_OVERRIDE_SO(item, int, sellPrice);
    return m_item->sellPrice();
}

int ItemWrapper::weight() const
{
    return m_item->weight();
}

QString ItemWrapper::description() const
{
    return m_item->description();
}

Script ItemWrapper::script() const
{
    return m_item->script();
}
