/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// header include
#include "AbilityWrapper.h"

// Sigscript includes
#include "GameWrapper.h"

// Sigmod includes
#include <sigmod/Ability.h>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigscript;

AbilityWrapper* AbilityWrapper::create(const Ability* ability, GameWrapper* parent)
{
    Signature sig = Signature(parent, Subsignature(ability->className(), ability->id()));
    if (!m_instances.contains(sig))
        m_instances[sig] = new AbilityWrapper(ability, parent);
    return qobject_cast<AbilityWrapper*>(m_instances[sig]);
}

AbilityWrapper::AbilityWrapper(const Ability* ability, GameWrapper* parent) :
        ObjectWrapper(ability, parent),
        m_ability(ability)
{
}

QString AbilityWrapper::name() const
{
    return m_ability->name();
}

int AbilityWrapper::priority() const
{
    ALLOW_OVERRIDE_SO(ability, int, priority);
    return m_ability->priority();
}

QString AbilityWrapper::description() const
{
    return m_ability->description();
}

Script AbilityWrapper::battleScript() const
{
    return m_ability->battleScript();
}

Script AbilityWrapper::worldScript() const
{
    return m_ability->worldScript();
}

Script AbilityWrapper::priorityScript() const
{
    return m_ability->priorityScript();
}
