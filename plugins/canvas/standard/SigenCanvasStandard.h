/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARENAPLUGIN_SIGENCANVASSTANDARD
#define ARENAPLUGIN_SIGENCANVASSTANDARD

// Sigencore plugin includes
#include <sigencore/plugins/CanvasPlugin.h>

class SigenCanvasStandard : public Sigencore::Plugins::CanvasPlugin
{
    Q_OBJECT
    
    public:
        SigenCanvasStandard(QObject* parent, const QVariantList& args);
        ~SigenCanvasStandard();
        
        QStringList classList() const;
        QString description(const QString& name) const;
        QIcon icon(const QString& name) const;
        QStringList extensions(const QString& name) const;
    protected:
        Sigencore::Canvas* createCanvas(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent);
    protected slots:
        void cleanupCanvas(Sigencore::Canvas* canvas);
};

#endif
