/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "QGSCanvas.h"

// Sigencore includes
#include <sigencore/RunScript.h>

// Sigscript includes
#include <sigscript/GameWrapper.h>
#include <sigscript/SpriteWrapper.h>

// KDE includes
#include <KIcon>
#include <kross/core/action.h>
#include <kross/core/actioncollection.h>
#include <kross/core/manager.h>

// Qt includes
#include <QtCore/QUuid>
#include <QtGui/QGraphicsItem>
#include <QtGui/QGraphicsScene>
#include <QtGui/QGraphicsView>

using namespace Sigscript;
using namespace Sigencore;

QGSCanvas::QGSCanvas(GameWrapper* game, Config* parent) :
        Canvas(parent),
        m_game(game),
        m_scene(new QGraphicsScene),
        m_collection(new Kross::ActionCollection(QString("canvas-%1").arg(QUuid::createUuid().toString()), Kross::Manager::self().actionCollection()))
{
}

QGSCanvas::~QGSCanvas()
{
    delete m_scene;
    delete m_collection;
}

QString QGSCanvas::name()
{
    return "QGraphicsScene canvas";
}

QString QGSCanvas::description()
{
    return "A canvas using QGraphicsScene";
}

QIcon QGSCanvas::icon()
{
    return KIcon();
}

QStringList QGSCanvas::extensions()
{
    return QStringList();
}

void QGSCanvas::addItem(const QString& name, const QString& context, const QVariantList& parameters)
{
    // TODO: NOOP or refresh?
    if (m_items.contains(name))
        return;
}

void QGSCanvas::removeItem(const QString& name)
{
    if (m_items.contains(name))
    {
        delete m_items[name];
        m_items.remove(name);
    }
}

void QGSCanvas::transform(const QString& transform, const QString& name, const QVariantList& parameters)
{
}

int QGSCanvas::type() const
{
    return QGraphicsSceneCanvas;
}

QWidget* QGSCanvas::viewport()
{
    // TODO: Only one view allowed? or as many as wanted?
    QGraphicsView* view = new QGraphicsView;
    view->setScene(m_scene);
    return view;
}
