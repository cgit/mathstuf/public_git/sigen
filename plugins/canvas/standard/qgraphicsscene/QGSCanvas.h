/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENCANVASES_QGSCANVAS
#define SIGENCANVASES_QGSCANVAS

// Sigencore includes
#include <sigencore/Canvas.h>

// Forward declarations
namespace Kross
{
class ActionCollection;
}
class QGraphicsItem;
class QGraphicsScene;
namespace Sigscript
{
class GameWrapper;
}

class QGSCanvas : public Sigencore::Canvas
{
    Q_OBJECT
    
    public:
        QGSCanvas(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        ~QGSCanvas();
        
        static QString name();
        static QString description();
        static QIcon icon();
        static QStringList extensions();
        
        Q_SCRIPTABLE void addItem(const QString& name, const QString& context, const QVariantList& parameters);
        Q_SCRIPTABLE void removeItem(const QString& name);
        Q_SCRIPTABLE void transform(const QString& transform, const QString& name, const QVariantList& parameters);
        
        int type() const;
        
        QWidget* viewport();
    private:
        Sigscript::GameWrapper* m_game;
        QGraphicsScene* m_scene;
        QMap<QString, QGraphicsItem*> m_items;
        Kross::ActionCollection* m_collection;
};

#endif
