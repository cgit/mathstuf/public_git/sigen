/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SigenCanvasStandard.h"

// Canvas includes
#include "qgraphicsscene/QGSCanvas.h"

// Sigencore includes
#include <sigencore/Canvas.h>

// KDE includes
#include <KIcon>

SIGEN_CANVAS_PLUGIN(SigenCanvasStandard, "sigen_canvas_standard")

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

SigenCanvasStandard::SigenCanvasStandard(QObject* parent, const QVariantList& args) :
        CanvasPlugin(parent, args)
{
}

SigenCanvasStandard::~SigenCanvasStandard()
{
}

QStringList SigenCanvasStandard::classList() const
{
    return QStringList() << QGSCanvas::name();
}

QString SigenCanvasStandard::description(const QString& name) const
{
    if (name == QGSCanvas::name())
        return QGSCanvas::description();
    return "(Unknown canvas)";
}

QIcon SigenCanvasStandard::icon(const QString& name) const
{
    if (name == QGSCanvas::name())
        return QGSCanvas::icon();
    return KIcon();
}

QStringList SigenCanvasStandard::extensions(const QString& name) const
{
    if (name == QGSCanvas::name())
        return QGSCanvas::extensions();
    return QStringList();
}

Canvas* SigenCanvasStandard::createCanvas(const QString& name, GameWrapper* game, Config* parent)
{
    if (name == QGSCanvas::name())
        return new QGSCanvas(game, parent);
    return NULL;
}

void SigenCanvasStandard::cleanupCanvas(Canvas* canvas)
{
    QGSCanvas* qgsCanvas = qobject_cast<QGSCanvas*>(canvas);
    if (qgsCanvas)
    {
        delete qgsCanvas;
        return;
    }
}
