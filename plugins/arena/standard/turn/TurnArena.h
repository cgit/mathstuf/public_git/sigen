/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENARENAS_TURNARENA
#define SIGENARENAS_TURNARENA

// Sigencore includes
#include <sigencore/Arena.h>

class TurnArena : public Sigencore::Arena
{
    Q_OBJECT
    
    public:
        TurnArena(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        
        static QString name();
        static QString description();
        static QIcon icon();
        static QStringList extensions();
    signals:
        void roundAboutToStart();
        void roundStarted();
        void roundEnded();
        void roundAboutToEnd();
    protected:
        virtual void handleAction(Sigencore::TeamMember* teamMember, Sigencore::TeamMember::Action action);
        
        virtual bool isTeamAllowed(Sigencore::Team* team);
        
        virtual void setupBattle();
        
        virtual void distributeWinnings();
        virtual void checkForLosers();
        
        Kross::ActionCollection* m_priorityScripts;
    protected slots:
        void processRound();
        
        virtual void registerScript(const Sigcore::Script& script);
        
        virtual void cleanUp();
};

bool sortActions(const Sigencore::TeamMember::RequestedAction& reqAction1, const Sigencore::TeamMember::RequestedAction& reqAction2);

#endif
