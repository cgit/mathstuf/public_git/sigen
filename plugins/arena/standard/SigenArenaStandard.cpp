/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SigenArenaStandard.h"

// Arena includes
#include "atb/ATBArena.h"
#include "turn/TurnArena.h"

// Sigencore includes
#include <sigencore/Arena.h>

// KDE includes
#include <KIcon>

SIGEN_ARENA_PLUGIN(SigenArenaStandard, "sigen_arena_standard")

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

SigenArenaStandard::SigenArenaStandard(QObject* parent, const QVariantList& args) :
        ArenaPlugin(parent, args)
{
}

SigenArenaStandard::~SigenArenaStandard()
{
}

QStringList SigenArenaStandard::classList() const
{
    return QStringList() << TurnArena::name() << ATBArena::name();
}

QString SigenArenaStandard::description(const QString& name) const
{
    if (name == TurnArena::name())
        return TurnArena::description();
    if (name == ATBArena::name())
        return ATBArena::description();
    return "(Unknown arena)";
}

QIcon SigenArenaStandard::icon(const QString& name) const
{
    if (name == TurnArena::name())
        return TurnArena::icon();
    if (name == ATBArena::name())
        return ATBArena::icon();
    return KIcon();
}

QStringList SigenArenaStandard::extensions(const QString& name) const
{
    if (name == TurnArena::name())
        return TurnArena::extensions();
    if (name == ATBArena::name())
        return ATBArena::extensions();
    return QStringList();
}

Arena* SigenArenaStandard::createArena(const QString& name, GameWrapper* game, Config* parent)
{
    if (name == TurnArena::name())
        return new TurnArena(game, parent);
    if (name == ATBArena::name())
        return new ATBArena(game, parent);
    return NULL;
}

void SigenArenaStandard::cleanupArena(Arena* arena)
{
    TurnArena* turnArena = qobject_cast<TurnArena*>(arena);
    if (turnArena)
    {
        delete turnArena;
        return;
    }
    ATBArena* atbArena = qobject_cast<ATBArena*>(arena);
    if (atbArena)
    {
        delete atbArena;
        return;
    }
}
