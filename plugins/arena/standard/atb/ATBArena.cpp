/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header includes
#include "ATBArena.h"

// Sigbattle includes
#include "ATBTimer.h"

// KDE includes
#include <KIcon>

// Qt includes
#include <QtCore/QTimer>
#include <QtGui/QIcon>

using namespace Sigscript;
using namespace Sigencore;

ATBArena::ATBArena(GameWrapper* sigmod, Config* parent) :
        Arena(sigmod, parent),
        m_atbTimer(new ATBTimer(this, m_decisions))
{
    connect(m_atbTimer, SIGNAL(actionReady(Sigencore::TeamMember*)), this, SLOT(processAction(Sigencore::TeamMember*)));
    setupBattle();
}

ATBArena::~ATBArena()
{
}

QString ATBArena::name()
{
    return "Sigen ATB Arena";
}

QString ATBArena::description()
{
    return "An arena where creatures have an action bar which when filled, can act";
}

QIcon ATBArena::icon()
{
    // TODO
    return KIcon();
}

QStringList ATBArena::extensions()
{
    return QStringList();
}

void ATBArena::handleAction(TeamMember* teamMember, TeamMember::Action action)
{
    if ((action.first == TeamMember::Invalid) || (action.first == TeamMember::Timeout))
        return;
    Arena::handleAction(teamMember, action);
}

bool ATBArena::isTeamAllowed(Sigencore::Team* team)
{
    Q_UNUSED(team)
    // TODO: Use rules format here?
    return true;
}

void ATBArena::setupBattle()
{
    Arena::setupBattle();
}

void ATBArena::processAction(TeamMember* teamMember)
{
    handleAction(teamMember, m_decisions[teamMember].second);
    m_decisions.remove(teamMember);
}

void ATBArena::cleanUp()
{
    Arena::cleanUp();
}
