/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ActionMap.h"

// Qt includes
#include <QtCore/QReadLocker>
#include <QtCore/QWriteLocker>

using namespace Sigencore;

bool ActionMap::contains(TeamMember* teamMember) const
{
    QReadLocker locker(&m_mutex);
    return m_map.contains(teamMember);
}

int ActionMap::remove(TeamMember* teamMember)
{
    QWriteLocker locker(&m_mutex);
    return m_map.remove(teamMember);
}

bool ActionMap::isEmpty() const
{
    QReadLocker locker(&m_mutex);
    return m_map.isEmpty();
}

void ActionMap::set(TeamMember* teamMember, const TeamMember::RequestedAction& action)
{
    QWriteLocker locker(&m_mutex);
    m_map[teamMember] = action;
}

const TeamMember::RequestedAction ActionMap::operator[](TeamMember* teamMember) const
{
    QReadLocker locker(&m_mutex);
    return m_map[teamMember];
}
