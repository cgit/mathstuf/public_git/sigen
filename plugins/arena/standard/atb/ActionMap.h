/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENARENAS_ACTIONMAP
#define SIGENARENAS_ACTIONMAP

// Sigencore includes
#include <sigencore/TeamMember.h>

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QReadWriteLock>

class ActionMap
{
    public:
        bool contains(Sigencore::TeamMember* teamMember) const;
        int remove(Sigencore::TeamMember* teamMember);
        
        bool isEmpty() const;
        
        void set(Sigencore::TeamMember* teamMember, const Sigencore::TeamMember::RequestedAction& action);
        
        const Sigencore::TeamMember::RequestedAction operator[](Sigencore::TeamMember* teamMember) const;
    private:
        QMap<Sigencore::TeamMember*, Sigencore::TeamMember::RequestedAction> m_map;
        mutable QReadWriteLock m_mutex;
};

#endif
