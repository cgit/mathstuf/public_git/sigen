/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ATBTimer.h"

// ATB includes
#include "ActionMap.h"
#include "ATBArena.h"

// Sigscript includes
#include <sigscript/GameWrapper.h>
#include <sigscript/RulesWrapper.h>

// Qt includes
#include <QtCore/QtConcurrentFilter>
#include <QtCore/QFuture>
#include <QtCore/QSignalMapper>
#include <QtCore/QTimerEvent>

using namespace Sigencore;

struct ATBTimer::TimerIncrementFunctor
{
    TimerIncrementFunctor(ATBTimer* timer) :
            m_timer(timer)
    {
    }
    
    typedef bool result_type;
    
    bool operator()(TeamMember* teamMember)
    {
        double factor = .1;
        m_timer->m_arena->valueOfType("meter-factor", &factor);
        if (!m_timer->m_actions.contains(teamMember))
        {
            m_timer->m_timers[teamMember] += teamMember->statValue(Sigmod::ST_Speed) * factor;
            int threshold = 1000;
            m_timer->m_arena->valueOfType("meter-threshold", &threshold);
            if (threshold <= m_timer->m_timers[teamMember])
            {
                QFutureWatcher<TeamMember::Action>* watcher = new QFutureWatcher<TeamMember::Action>(m_timer);
                TeamMember::RequestedAction action = requestDecision(teamMember);
                watcher->setFuture(action.second);
                m_timer->connect(watcher, SIGNAL(finished()), m_timer->m_actionMapper, SLOT(map()));
                m_timer->connect(watcher, SIGNAL(finished()), SLOT(deleteLater()));
                m_timer->m_actionMapper->setMapping(watcher, teamMember);
                m_timer->m_actions.set(teamMember, action);
            }
        }
        return false;
    }
    
    ATBTimer* m_timer;
};

ATBTimer::ATBTimer(ATBArena* arena, ActionMap& actions) :
        QThread(arena),
        m_arena(arena),
        m_actionMapper(new QSignalMapper(this)),
        m_actions(actions)
{
    connect(m_actionMapper, SIGNAL(mapped(QObject*)), this, SLOT(actionReceived(QObject*)));
}

ATBTimer::~ATBTimer()
{
}

void ATBTimer::run()
{
    startTimer(50);
    exec();
}

void ATBTimer::timerEvent(QTimerEvent* event)
{
    Q_UNUSED(event)
    const QList<TeamMember*> active = m_arena->active(Arena::Fighters);
    QtConcurrent::blockingFiltered(active, TimerIncrementFunctor(this));
}

void ATBTimer::actionReceived(QObject* object)
{
    TeamMember* teamMember = qobject_cast<TeamMember*>(object);
    if (teamMember)
        emit(actionReady(teamMember));
}
