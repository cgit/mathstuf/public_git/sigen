/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGENARENAS_ATBARENA
#define SIGENARENAS_ATBARENA

// ATB includes
#include "ActionMap.h"

// Sigencore includes
#include <sigencore/Arena.h>

// Forward declaraions
class QTimer;
class ATBTimer;

class ATBArena : public Sigencore::Arena
{
    Q_OBJECT
    
    public:
        ATBArena(Sigscript::GameWrapper* game, Sigscript::Config* parent);
        ~ATBArena();
        
        static QString name();
        static QString description();
        static QIcon icon();
        static QStringList extensions();
    protected:
        virtual void handleAction(Sigencore::TeamMember* teamMember, Sigencore::TeamMember::Action action);
        
        virtual bool isTeamAllowed(Sigencore::Team* team);
        
        virtual void setupBattle();
        
        Kross::ActionCollection* m_priorityScripts;
    protected slots:
        void processAction(Sigencore::TeamMember* teamMember);
        
        virtual void cleanUp();
    private:
        ActionMap m_decisions;
        ATBTimer* m_atbTimer;
};

#endif
