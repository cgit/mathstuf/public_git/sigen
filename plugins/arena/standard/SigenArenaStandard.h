/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARENAPLUGIN_SIGENARENASTANDARD
#define ARENAPLUGIN_SIGENARENASTANDARD

// Sigencore plugin includes
#include <sigencore/plugins/ArenaPlugin.h>

class SigenArenaStandard : public Sigencore::Plugins::ArenaPlugin
{
    Q_OBJECT
    
    public:
        SigenArenaStandard(QObject* parent, const QVariantList& args);
        ~SigenArenaStandard();
        
        QStringList classList() const;
        QString description(const QString& name) const;
        QIcon icon(const QString& name) const;
        QStringList extensions(const QString& name) const;
    protected:
        Sigencore::Arena* createArena(const QString& name, Sigscript::GameWrapper* game, Sigscript::Config* parent);
    protected slots:
        void cleanupArena(Sigencore::Arena* arena);
};

#endif
