/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SigenArenaKross.h"

// Sigencore includes
#include <sigencore/Arena.h>

// KDE includes
#include <KIcon>
#include <KServiceTypeTrader>
#include <kross/core/manager.h>

SIGEN_ARENA_PLUGIN(SigenArenaKross, "sigen_arena_kross")

using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;

SigenArenaKross::SigenArenaKross(QObject* parent, const QVariantList& args) :
        ArenaPlugin(parent, args)
{
    KService::List services;
    QStringList interpreterQueries;
    const QStringList interpreters = Kross::Manager::self().interpreters();
    foreach (const QString& interpreter, interpreters)
        interpreterQueries.append(QString("([X-Sigen-Kross-Interpreter] == %1)").arg(interpreter));
    if (interpreterQueries.size())
        services = KServiceTypeTrader::self()->query("Sigen/Arena/Kross", QString("([X-Sigen-Kross-ApiVersion] == 0) and (%1)").arg(interpreterQueries.join(" or ")));
    foreach (KService::Ptr service, services)
        m_scripts[service->name()] = service;
}

SigenArenaKross::~SigenArenaKross()
{
}

QStringList SigenArenaKross::classList() const
{
    return m_scripts.keys();
}

QString SigenArenaKross::description(const QString& name) const
{
    if (m_scripts.contains(name))
        return m_scripts[name]->comment();
    return "(Unknown arena)";
}

QIcon SigenArenaKross::icon(const QString& name) const
{
    if (m_scripts.contains(name))
        return KIcon(m_scripts[name]->icon());
    return KIcon();
}

QStringList SigenArenaKross::extensions(const QString& name) const
{
    if (m_scripts.contains(name))
        return m_scripts[name]->property("X-Sigen-Kross-Extensions", QVariant::StringList).toStringList();
    return QStringList();
}

Arena* SigenArenaKross::createArena(const QString& name, GameWrapper* game, Config* parent)
{
    return NULL;
}

void SigenArenaKross::cleanupArena(Arena* arena)
{
}
