/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "PluginTreeModel.h"

// Sigtools includes
#include "PluginTree.h"
#include "PluginLoader.h"

// Sigencore plugin includes
#include <sigencore/plugins/ArenaPlugin.h>
#include <sigencore/plugins/CanvasPlugin.h>

// KDE includes
#include <KCategorizedSortFilterProxyModel>
#include <KIcon>

// Qt includes
#include <QtGui/QIcon>

using namespace Sigencore::Plugins;
using namespace Sigtools;

PluginTreeModel::PluginTreeModel(PluginTree* tree) :
        QAbstractListModel(tree)
{
}

void PluginTreeModel::setTypes(const QStringList& types)
{
    foreach (const QString& type, types)
    {
        const QStringList names = PluginLoader::availablePlugins(type);
        foreach (const QString& name, names)
        {
            ClassData data;
            data.m_type = type;
            data.m_name = name;
            if (type == "Sigmod")
            {
                KService::Ptr service = PluginLoader::service(type, name);
                const QString category = service->property("X-Sigen-Sigmod-Category", QVariant::String).toString();
                if (!category.isEmpty())
                    data.m_type.append(QString("/%1").arg(category));
                data.m_name.replace('/', " - ");
                data.m_description = service->property("X-Sigen-Sigmod-Description", QVariant::String).toString();
                data.m_icon = KIcon(service->icon());
            }
            else
            {
                data.m_description = PluginLoader::description(type, name);
                data.m_icon = PluginLoader::icon(type, name);
            }
            beginInsertRows(QModelIndex(), m_entries.size(), m_entries.size());
            m_entries.append(data);
            endInsertRows();
        }
    }
}

QModelIndex PluginTreeModel::index(const int row, const int column, const QModelIndex& parent) const
{
    if (parent.isValid() || column || (row <= -1) || (m_entries.size() <= row))
        return QModelIndex();
    return createIndex(row, 0, (void*)&m_entries[row]);
}

QVariant PluginTreeModel::data(const QModelIndex& index, const int role) const
{
    if (!index.isValid() || !index.internalPointer())
        return QVariant();
    ClassData* data = static_cast<ClassData*>(index.internalPointer());
    switch (role)
    {
        case Qt::DisplayRole:
            return data->m_name;
        case Qt::UserRole:
            return data->m_description;
        case Qt::DecorationRole:
            return data->m_icon;
        case KCategorizedSortFilterProxyModel::CategoryDisplayRole:
        case KCategorizedSortFilterProxyModel::CategorySortRole:
            return data->m_type;
        default:
            return QVariant();
    }
}

int PluginTreeModel::rowCount(const QModelIndex& parent) const
{
    if (parent.isValid())
        return 0;
    return m_entries.size();
}
