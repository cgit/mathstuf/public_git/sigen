/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/*
 * Drawing code adapted from KPluginSelector (2009-03-26)
 * 
 * Copyright (C) 2007, 2006 Rafael Fernández López <ereslibre@kde.org>
 * Copyright (C) 2002-2003 Matthias Kretz <kretz@kde.org>
 */

// Header include
#include "PluginTreeDelegate.h"

// Sigtools includes
#include "PluginLoader.h"
#include "PluginTree.h"

// KDE includes
#include <KApplication>
#include <KAboutApplicationDialog>
#include <KCategorizedView>
#include <KCategorizedSortFilterProxyModel>
#include <KPluginInfo>
#include <KPushButton>
#include <KService>

// Qt includes
#include <QtGui/QPainter>

static const int margins = 3;

using namespace Sigtools;

PluginTreeDelegate::PluginTreeDelegate(KCategorizedView* view, PluginTree* tree) :
        KWidgetItemDelegate(view, tree),
        m_about(new KPushButton)
{
    m_about->setIcon(KIcon("dialog-information"));
}

PluginTreeDelegate::~PluginTreeDelegate()
{
    delete m_about;
}

void PluginTreeDelegate::paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    if (!index.isValid())
        return;
    
    KApplication::style()->drawPrimitive(QStyle::PE_PanelItemViewItem, &option, painter, 0);
    
    int iconSize = option.rect.height() - 2 * margins;
    painter->drawPixmap(QRect(margins + option.rect.left(), margins + option.rect.top(), iconSize, iconSize), KIconLoader::global()->loadIcon(index.model()->data(index, Qt::DecorationRole).toString(), KIconLoader::Desktop, iconSize));
    
    QRect contentsRect(2 * margins + iconSize + option.rect.left(), margins + option.rect.top(), option.rect.width() - 5 * margins - iconSize - m_about->sizeHint().width(), option.rect.height() - 2 * margins);
    
    painter->save();
    
    if (option.state & QStyle::State_Selected)
        painter->setPen(option.palette.highlightedText().color());
    
    QFont font = option.font;
    font.setBold(true);
    QFontMetrics fmTitle(font);
    painter->setFont(font);
    painter->drawText(contentsRect, Qt::AlignLeft | Qt::AlignTop, fmTitle.elidedText(index.model()->data(index, Qt::DisplayRole).toString(), Qt::ElideRight, contentsRect.width()));
    
    painter->setFont(option.font);
    painter->drawText(contentsRect, Qt::AlignLeft | Qt::AlignBottom, option.fontMetrics.elidedText(index.model()->data(index, Qt::UserRole).toString(), Qt::ElideRight, contentsRect.width()));
    
    painter->restore();
}

QSize PluginTreeDelegate::sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const
{
    QFont font = option.font;
    font.setBold(true);
    QFontMetrics fmTitle(font);
    const QSize nameSize = fmTitle.size(0, index.model()->data(index, Qt::DisplayRole).toString());
    const QSize descSize = option.fontMetrics.size(0, index.model()->data(index, Qt::UserRole).toString());
    
    return QSize(qMax(nameSize.width(), descSize.width()) + KIconLoader::SizeMedium, qMax(int(KIconLoader::SizeMedium), nameSize.height() + descSize.height()) + 2 * margins);
}

QList<QWidget*> PluginTreeDelegate::createItemWidgets() const
{
    QList<QWidget*> widgetList;
    KPushButton* aboutPushButton = new KPushButton;
    aboutPushButton->setIcon(KIcon("dialog-information"));
    connect(aboutPushButton, SIGNAL(clicked(bool)), this, SLOT(aboutClicked()));
    
    setBlockedEventTypes(aboutPushButton, QList<QEvent::Type>() << QEvent::MouseButtonPress << QEvent::MouseButtonRelease << QEvent::MouseButtonDblClick);
    
    widgetList << aboutPushButton;
    return widgetList;
}

void PluginTreeDelegate::updateItemWidgets(const QList<QWidget*> widgets, const QStyleOptionViewItem& option, const QPersistentModelIndex& index) const
{
    Q_UNUSED(index)
    QSize aboutSize = widgets[0]->sizeHint();
    widgets[0]->resize(aboutSize);
    widgets[0]->move(option.rect.width() - margins - aboutSize.width(), (option.rect.height() - aboutSize.height()) / 2);
}

void PluginTreeDelegate::aboutClicked()
{
    const QModelIndex index = focusedIndex();
    const QAbstractItemModel* model = index.model();
    const QString type = model->data(index, KCategorizedSortFilterProxyModel::CategoryDisplayRole).toString();
    const QString cname = model->data(index, Qt::DisplayRole).toString();
    KService::Ptr service = PluginLoader::service(type, cname);
    KPluginInfo info(service);
    const QString name = info.name();
    const QStringList authors = info.author().split(',');
    const QStringList emails = info.email().split(',');
    KAboutData aboutData(name.toUtf8(), name.toUtf8(), ki18n(name.toUtf8()), info.version().toUtf8(), ki18n(info.comment().toUtf8()), KAboutLicense::byKeyword(info.license()).key(), ki18n(QByteArray()), ki18n(QByteArray()), info.website().toLatin1());
    aboutData.setProgramIconName(info.icon());
    if (authors.count() == emails.count())
    {
        for (int i = 0; i < authors.count(); ++i)
        {
            if (!authors[i].isEmpty())
                aboutData.addAuthor(ki18n(authors[i].toUtf8()), ki18n(QByteArray()), emails[i].toUtf8(), 0);
        }
    }
    KAboutApplicationDialog aboutPlugin(&aboutData, itemView());
    aboutPlugin.exec();
}
