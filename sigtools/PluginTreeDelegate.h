/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINTREEDELEGATE
#define SIGTOOLS_PLUGINTREEDELEGATE

// Sigtools includes
#include "Global.h"

// KDE includes
#include <kwidgetitemdelegate.h>

// Forward declarations
class QPushButton;
class KCategorizedView;

namespace Sigtools
{
class PluginTree;

class SIGTOOLS_NO_EXPORT PluginTreeDelegate : public KWidgetItemDelegate
{
    Q_OBJECT
    
    public:
        PluginTreeDelegate(KCategorizedView* view, PluginTree* tree);
        ~PluginTreeDelegate();
        
        void paint(QPainter* painter, const QStyleOptionViewItem& option, const QModelIndex& index) const;
        QSize sizeHint(const QStyleOptionViewItem& option, const QModelIndex& index) const;
    protected:
        QList<QWidget*> createItemWidgets() const;
        void updateItemWidgets(const QList<QWidget*> widgets, const QStyleOptionViewItem& option, const QPersistentModelIndex& index) const;
    private slots:
        void aboutClicked();
    private:
        QPushButton* m_about;
};
}

#endif
