/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "PluginTreeProxyModel.h"

// Sigtools includes
#include "PluginTree.h"
#include "PluginTreeModel.h"

// KDE includes
#include <KLineEdit>
#include <KStringHandler>

using namespace Sigtools;

PluginTreeProxyModel::PluginTreeProxyModel(KLineEdit* filter, PluginTreeModel* model) :
        KCategorizedSortFilterProxyModel(model),
        m_filter(filter)
{
    sort(0);
    setCategorizedModel(true);
    setSourceModel(model);
}

bool PluginTreeProxyModel::filterAcceptsRow(const int sourceRow, const QModelIndex& sourceParent) const
{
    Q_UNUSED(sourceParent)
    const QString filter = m_filter->text();
    if (!filter.isEmpty())
    {
        const PluginTreeModel::ClassData* classInfo = static_cast<PluginTreeModel::ClassData*>(sourceModel()->index(sourceRow, 0).internalPointer());
        return classInfo->m_name.contains(filter, Qt::CaseInsensitive) || classInfo->m_description.contains(filter, Qt::CaseInsensitive);
    }
    return true;
}

bool PluginTreeProxyModel::subSortLessThan(const QModelIndex& left, const QModelIndex& right) const
{
    return KStringHandler::naturalCompare(static_cast<PluginTreeModel::ClassData*>(left.internalPointer())->m_name, static_cast<PluginTreeModel::ClassData*>(right.internalPointer())->m_name, Qt::CaseInsensitive) < 0;
}
