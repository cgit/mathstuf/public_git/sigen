/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINTREEPROXYMODEL
#define SIGTOOLS_PLUGINTREEPROXYMODEL

// Sigtools includes
#include "Global.h"

// KDE includes
#include <KCategorizedSortFilterProxyModel>

// Forward declarations
class KLineEdit;

namespace Sigtools
{
class PluginTreeModel;

class PluginTreeProxyModel : public KCategorizedSortFilterProxyModel
{
    public:
        PluginTreeProxyModel(KLineEdit* filter, PluginTreeModel* model);
    protected:
        bool filterAcceptsRow(const int sourceRow, const QModelIndex& sourceParent) const;
        bool subSortLessThan(const QModelIndex& left, const QModelIndex& right) const;
    private:
        const KLineEdit* const m_filter;
};
}

#endif
