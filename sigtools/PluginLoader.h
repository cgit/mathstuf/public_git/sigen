/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINLOADER
#define SIGTOOLS_PLUGINLOADER

// Sigtools includes
#include "Global.h"

// KDE includes
#include <KService>

// Qt includes
#include <QtCore/QSharedPointer>

// Forward declarations
class KPluginFactory;
namespace Sigencore
{
class Arena;
class Canvas;
class Client;
}
namespace Sigscript
{
class Config;
class GameWrapper;
}
namespace Sigmod
{
class Game;
}

namespace Sigtools
{
namespace PluginLoader
{
    SIGTOOLS_EXPORT QStringList availablePlugins(const QString& type, const bool forceLookup = false);
    SIGTOOLS_EXPORT QStringList services(const QString& type, const QStringList& extensions);
    SIGTOOLS_EXPORT KService::Ptr service(const QString& type, const QString& name);
    
    SIGTOOLS_EXPORT QString description(const QString& type, const QString& name);
    SIGTOOLS_EXPORT QIcon icon(const QString& type, const QString& name);
    SIGTOOLS_EXPORT QStringList extensions(const QString& type, const QString& name);
    
    SIGTOOLS_EXPORT QSharedPointer<Sigmod::Game> game(const QString& game);
    SIGTOOLS_EXPORT Sigencore::Arena* arena(const QString& arena, Sigscript::GameWrapper* game, Sigscript::Config* parent);
    SIGTOOLS_EXPORT Sigencore::Canvas* canvas(const QString& canvas, Sigscript::GameWrapper* game, Sigscript::Config* parent);
    SIGTOOLS_EXPORT Sigencore::Client* client(const QString& client, Sigscript::GameWrapper* game, Sigscript::Config* parent);
}
}

#endif
