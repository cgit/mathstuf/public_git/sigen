project(sigtools)

set(sigtools_HEADERS
    Global.h
    PluginLoader.h
    PluginTree.h
)
set(sigtools_SRCS
    PluginLoader.cpp
    PluginTree.cpp
    PluginTreeDelegate.cpp
    PluginTreeModel.cpp
    PluginTreeProxyModel.cpp
)

kde4_add_library(sigtools
    SHARED
    ${sigtools_SRCS}
)
set_target_properties(sigtools
    PROPERTIES
        VERSION ${SIGEN_VERSION}
        SOVERSION ${SIGEN_SOVERSION}
)
target_link_libraries(sigtools
    ${QT_QTCORE_LIBRARY}
    ${QT_QTGUI_LIBRARY}
    ${KDE4_KDEUI_LIBRARY}
    sigmod
    sigencoreplugins
)
target_link_libraries(sigtools LINK_INTERFACE_LIBRARIES
    ${QT_QTCORE_LIBRARY}
    ${QT_QTGUI_LIBRARY}
    sigmod
)

install(
    TARGETS
        sigtools
    EXPORT
        sigen_EXPORTS
    DESTINATION
        ${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigtools_HEADERS}
    DESTINATION
        ${CMAKE_INSTALL_PREFIX}/include/${CMAKE_PROJECT_NAME}/${PROJECT_NAME}
    COMPONENT
        development
)
