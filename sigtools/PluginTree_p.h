/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINTREE_P
#define SIGTOOLS_PLUGINTREE_P

// Header include
#include "PluginTree.h"

// Forward declarations
class KCategorizedView;
class KCategoryDrawer;
class KLineEdit;

namespace Sigtools
{
class PluginTreeModel;
class PluginTreeProxyModel;

class SIGTOOLS_NO_EXPORT PluginTree::Private
{
    public:
        Private(const QStringList& types, PluginTree* tree);
        ~Private();
        
        KLineEdit* m_filter;
        KCategorizedView* m_view;
        KCategoryDrawer* m_drawer;
        PluginTreeModel* m_model;
        PluginTreeProxyModel* m_proxy;
};
}

#endif
