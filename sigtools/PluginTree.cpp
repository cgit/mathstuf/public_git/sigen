/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "PluginTree.h"
#include "PluginTree_p.h"

// Sigtools includes
#include "PluginTreeDelegate.h"
#include "PluginTreeModel.h"
#include "PluginTreeProxyModel.h"

// KDE includes
#include <KCategorizedView>
#include <KCategoryDrawer>
#include <KLineEdit>
#include <KPluginInfo>
#include <KServiceTypeTrader>

// Qt includes
#include <QtGui/QGridLayout>

using namespace Sigtools;

PluginTree::PluginTree(const QStringList& types, QWidget* parent) :
        QWidget(parent),
        d(new Private(types, this))
{
    QGridLayout* layout = new QGridLayout;
    setLayout(layout);
    layout->addWidget(d->m_filter, 0, 0);
    layout->addWidget(d->m_view, 1, 0);
    setTabOrder(d->m_view, d->m_filter);
}

QString PluginTree::currentType() const
{
    QModelIndex index = d->m_view->currentIndex();
    if (!index.isValid())
        return QString();
    return static_cast<PluginTreeModel::ClassData*>(index.internalPointer())->m_type;
}

QString PluginTree::currentName() const
{
    QModelIndex index = d->m_view->currentIndex();
    if (!index.isValid())
        return QString();
    return static_cast<PluginTreeModel::ClassData*>(index.internalPointer())->m_name;
}

PluginTree::Private::Private(const QStringList& types, PluginTree* tree) :
        m_filter(new KLineEdit(tree)),
        m_view(new KCategorizedView(tree)),
        m_drawer(new KCategoryDrawer),
        m_model(new PluginTreeModel(tree)),
        m_proxy(new PluginTreeProxyModel(m_filter, m_model))
{
    m_filter->setClearButtonShown(true);
    m_filter->setClickMessage("Search Plugins");
    
    m_view->setCategoryDrawer(m_drawer);
    m_view->setModel(m_proxy);
    m_view->setAlternatingRowColors(true);
    m_view->setItemDelegate(new PluginTreeDelegate(m_view, tree));
    
    m_model->setTypes(types);
    
    m_filter->connect(m_filter, SIGNAL(textChanged(QString)), m_proxy, SLOT(invalidate()));
}

PluginTree::Private::~Private()
{
    delete m_drawer;
}
