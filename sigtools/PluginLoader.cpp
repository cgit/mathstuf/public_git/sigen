/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "PluginLoader.h"
#include "PluginLoader_p.h"

// Sigencore plugin includes
#include <sigencore/plugins/ArenaPlugin.h>
#include <sigencore/plugins/CanvasPlugin.h>
#include <sigencore/plugins/ClientPlugin.h>
#include <sigencore/plugins/PluginBase.h>

// Sigmod includes
#include <sigmod/Game.h>

// KDE includes
#include <KMessageBox>
#include <KServiceTypeTrader>
#include <KStandardDirs>

// Qt includes
#include <QtCore/QFile>
#include <QtCore/QSet>

using namespace Sigmod;
using namespace Sigscript;
using namespace Sigencore;
using namespace Sigencore::Plugins;
using namespace Sigtools;

K_GLOBAL_STATIC(PluginLoader::Private, loader)

QStringList PluginLoader::availablePlugins(const QString& type, const bool forceLookup)
{
    if (forceLookup)
        loader->refresh(type);
    return loader->services(type);
}

QStringList PluginLoader::services(const QString& type, const QStringList& extensions)
{
    const QSet<QString> query = extensions.toSet();
    const QStringList names = loader->services(type);
    QStringList list;
    foreach (const QString& name, names)
    {
        PluginBase* plugin = loader->factory(type, name);
        if (plugin && (plugin->extensions(name).toSet().intersect(query).size() == query.size()))
            list.append(name);
    }
    return list;
}

KService::Ptr PluginLoader::service(const QString& type, const QString& name)
{
    return loader->service(type, name);
}

QString PluginLoader::description(const QString& type, const QString& name)
{
    PluginBase* plugin = loader->factory(type, name);
    if (plugin)
        return plugin->description(name);
    return QString();
}

QIcon PluginLoader::icon(const QString& type, const QString& name)
{
    PluginBase* plugin = loader->factory(type, name);
    if (plugin)
        return plugin->icon(name);
    return QIcon();
}

QStringList PluginLoader::extensions(const QString& type, const QString& name)
{
    PluginBase* plugin = loader->factory(type, name);
    if (plugin)
        return plugin->extensions(name);
    return QStringList();
}

QSharedPointer<Game> PluginLoader::game(const QString& game)
{
    return loader->game(game);
}

Arena* PluginLoader::arena(const QString& arena, GameWrapper* game, Config* parent)
{
    ArenaPlugin* plugin = qobject_cast<ArenaPlugin*>(loader->factory("Arena", arena));
    if (plugin)
        return plugin->getArena(arena, game, parent);
    return NULL;
}

Canvas* PluginLoader::canvas(const QString& canvas, GameWrapper* game, Config* parent)
{
    CanvasPlugin* plugin = qobject_cast<CanvasPlugin*>(loader->factory("Canvas", canvas));
    if (plugin)
        return plugin->getCanvas(canvas, game, parent);
    return NULL;
}

Client* PluginLoader::client(const QString& client, GameWrapper* game, Config* parent)
{
    ClientPlugin* plugin = qobject_cast<ClientPlugin*>(loader->factory("Player", client));
    if (plugin)
        return plugin->getClient(client, game, parent);
    return NULL;
}

PluginLoader::Private::Private()
{
    if (!KGlobal::dirs()->resourceDirs("sigmod").size())
        KGlobal::dirs()->addResourceType("sigmod", "data", "sigmod/");
}

PluginLoader::Private::~Private()
{
    QStringList types = m_plugins.keys();
    foreach (const QString& type, types)
        clean(type, true);
}

void PluginLoader::Private::refresh(const QString& type)
{
    if (type == "Sigmod")
        m_games.clear();
    // TODO: Progress dialog?
    m_available[type].clear();
    clean(type);
    KService::List services = KServiceTypeTrader::self()->query(QString("Sigen/%1").arg(type), "[X-Sigen-PluginApiVersion] == 0");
    foreach (KService::Ptr service, services)
    {
        if (type == "Sigmod")
        {
            const QString name = service->property("X-Sigen-Sigmod-Name", QVariant::String).toString();
            const QStringList version = service->property("X-Sigen-Sigmod-Version", QVariant::StringList).toStringList();
            const QString variant = service->property("X-Sigen-Sigmod-Variant", QVariant::String).toString();
            const QStringList variantVersion = service->property("X-Sigen-Sigmod-VariantVersion", QVariant::StringList).toStringList();
            QString fullName;
            QString varPath;
            if (!variant.isEmpty())
                varPath = QString("/%1/%2").arg(variant).arg(variantVersion.join("-"));
            fullName = QString("%1/%2%3").arg(name).arg(version.join("-")).arg(varPath);
            QString fileName = KStandardDirs::locate("sigmod", QString("%1.smod").arg(fullName));
            if (!fileName.isEmpty())
            {
                QFile file(fileName);
                QSharedPointer<Game> game;
                if (file.open(QIODevice::ReadOnly))
                {
                    QDomDocument xml;
                    QString error;
                    int line;
                    int column;
                    if (xml.setContent(&file, &error, &line, &column))
                    {
                        if (xml.doctype().name() == "Sigmod")
                            game = QSharedPointer<Game>(new Game(xml.documentElement()));
                        else
                            KMessageBox::error(NULL, "The file is not a Sigmod.", "Invalid Sigmod");
                    }
                    else
                        KMessageBox::error(NULL, QString("%1 at line %2, column %3").arg(error).arg(line).arg(column), "XML Error");
                }
                file.close();
                // TODO: validate
                if (game)
                {
                    m_available[type][fullName] = Service(service, NULL);
                    m_games[fullName] = game;
                }
            }
        }
        else
        {
            KPluginLoader loader(service->library());
            KPluginFactory* factory = loader.factory();
            if (factory)
            {
                PluginBase* plugin = NULL;
                if (type == "Arena")
                    plugin = factory->create<ArenaPlugin>(this);
                else if (type == "Canvas")
                    plugin = factory->create<CanvasPlugin>(this);
                else if (type == "Client")
                    plugin = factory->create<ClientPlugin>(this);
                else
                    KMessageBox::information(NULL, QString("The plugin type \"Sigen/%1\" is not supported.").arg(type), "Unsupported plugin type");
                if (plugin)
                {
                    m_plugins[type].append(plugin);
                    QStringList classes = plugin->classList();
                    foreach (const QString& className, classes)
                        m_available[type][className] = Service(service, plugin);
                }
            }
            else
                KMessageBox::error(NULL, QString("The plugin of type \"Sigen/%1\" with name \"%2\" is not a valid Sigen plugin. The error was:\n%3").arg(type).arg(service->name()).arg(loader.errorString()), "Plugin loading error");
        }
    }
}

QStringList PluginLoader::Private::services(const QString& type)
{
    if (!m_available.contains(type))
        refresh(type);
    if (m_available.contains(type))
        return m_available[type].keys();
    return QStringList();
}

KService::Ptr PluginLoader::Private::service(const QString& type, const QString& name)
{
    if (!m_available.contains(type))
        refresh(type);
    if (m_available.contains(type) && m_available[type].contains(name))
        return m_available[type][name].first;
    return KService::Ptr();
}

PluginBase* PluginLoader::Private::factory(const QString& type, const QString& name)
{
    if (!m_available.contains(type))
        refresh(type);
    if (m_available.contains(type) && m_available[type].contains(name))
        return m_available[type][name].second;
    return NULL;
}

QSharedPointer<Game> PluginLoader::Private::game(const QString& name)
{
    if (!m_games.contains(name))
        refresh("Sigmod");
    if (m_games.contains(name))
        return m_games[name];
    return QSharedPointer<Game>();
}

void PluginLoader::Private::clean(const QString& type, const bool forceDeletion)
{
    QMutableListIterator<PluginBase*> i(m_plugins[type]);
    while (i.hasNext())
    {
        i.next();
        if (forceDeletion || !i.value()->classesUsedCount())
        {
            delete i.value();
            i.remove();
        }
    }
}
