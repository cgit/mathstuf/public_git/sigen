/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINTREEMODEL
#define SIGTOOLS_PLUGINTREEMODEL

// Sigtools includes
#include "Global.h"

// Qt includes
#include <QtCore/QAbstractListModel>
#include <QtGui/QIcon>

namespace Sigtools
{
class PluginTree;

class SIGTOOLS_NO_EXPORT PluginTreeModel : public QAbstractListModel
{
    Q_OBJECT
    
    public:
        struct ClassData
        {
            QString m_type;
            QString m_name;
            QString m_description;
            QIcon m_icon;
        };
        
        PluginTreeModel(PluginTree* tree);
        
        void setTypes(const QStringList& types);
        
        QModelIndex index(const int row, const int column = 0, const QModelIndex& parent = QModelIndex()) const;
        
        QVariant data(const QModelIndex& index, const int role = Qt::DisplayRole) const;
        
        int rowCount(const QModelIndex& parent = QModelIndex()) const;
    private:
        QList<ClassData> m_entries;
};
}

#endif
