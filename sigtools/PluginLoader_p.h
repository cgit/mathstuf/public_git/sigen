/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_PLUGINLOADER_P
#define SIGTOOLS_PLUGINLOADER_P

// Sigtools includes
#include "Global.h"

// KDE includes
#include <KService>

// Qt includes
#include <QtCore/QObject>
#include <QtCore/QPair>
#include <QtCore/QSharedPointer>

// Forward declarations
namespace Sigencore
{
namespace Plugins
{
class PluginBase;
}
}
namespace Sigmod
{
class Game;
}

namespace Sigtools
{
namespace PluginLoader
{
class SIGTOOLS_NO_EXPORT Private : public QObject
{
    Q_OBJECT
    
    public:
        Private();
        ~Private();
        
        void refresh(const QString& type);
        QStringList services(const QString& type);
        KService::Ptr service(const QString& type, const QString& name);
        Sigencore::Plugins::PluginBase* factory(const QString& type, const QString& name);
        QSharedPointer<Sigmod::Game> game(const QString& name);
    protected:
        void clean(const QString& type, const bool forceDeletion = false);
        
        typedef QPair<KService::Ptr, Sigencore::Plugins::PluginBase*> Service;
        typedef QMap<QString, Service> PluginList;
        QMap<QString, QList<Sigencore::Plugins::PluginBase*> > m_plugins;
        QMap<QString, PluginList> m_available;
        QMap<QString, QSharedPointer<Sigmod::Game> > m_games;
};
}
}

#endif
