/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGTOOLS_GLOBAL
#define SIGTOOLS_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGTOOLS_EXPORT
#   ifdef MAKE_SIGTOOLS_LIB
#       define SIGTOOLS_EXPORT KDE_EXPORT
#   else
#       define SIGTOOLS_EXPORT KDE_IMPORT
#   endif
#   define SIGTOOLS_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGTOOLS_EXPORT_DEPRECATED
#   define SIGTOOLS_EXPORT_DEPRECATED KDE_DEPRECATED SIGTOOLS_EXPORT
#endif

#endif
