/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SigbattlePreferencesWidget.h"

// Sigbattle includes
#include "SigbattlePreferences.h"

// Qt includes
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>

using namespace Sigbattle;

SigbattlePreferencesWidget::SigbattlePreferencesWidget(QWidget* parent) :
        QWidget(parent)
{
//     QLabel* label = new QLabel(":");
//     label->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
//     kcfg_ = new ();
//     kcfg_->;
//     label->setBuddy(kcfg_);
//     
//     QLayout* layout = new QHBoxLayout;
//     layout->addWidget(label);
//     layout->addWidget(kcfg_);
//     
//     QWidget* widget = new QWidget;
//     widget->setLayout(layout);
//     
//     layout = new QVBoxLayout;
//     layout->addWidget(widget);
//     
//     setLayout(layout);
}

void SigbattlePreferencesWidget::save()
{
//     SigbattlePreferences::;
}
