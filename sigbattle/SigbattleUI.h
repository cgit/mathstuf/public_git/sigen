/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGBATTLE_SIGBATTLEUI
#define SIGBATTLE_SIGBATTLEUI

// Form include
#include "ui_sigbattle.h"

// KDE includes
#include <KXmlGuiWindow>

// Qt includes
#include <QtXml/QDomDocument>

namespace Sigbattle
{
class SigbattleUI : public KXmlGuiWindow, private Ui::formSigbattle
{
    Q_OBJECT
    
    public:
        SigbattleUI(QWidget* parent = 0);
        ~SigbattleUI();
    protected slots:
        void preferences();
    private:
        void setupActions();
};
}

#endif
