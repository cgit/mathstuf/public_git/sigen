/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Ghost.h"

// Ghost(const PokeMod::Sigmod& par, const unsigned s, const unsigned l, const unsigned t) :
//         Pokemon(par, s, l),
//         teamSize(t),
//         unknownMoves(par->GetMaxMoves())
// {
//     for (unsigned i = 0; i < PokeMod::ST_End_GSC; ++i)
//     {
//         dv[i] = 0;
//         statExp[i] = 0;
//         minStats[i] = GetStat(i);
//         dv[i] = sigmod->GetMaxDV();
//         statExp[i] = 65535;
//         maxStats[i] = GetStat(i);
//     }
//     Species* pkmn = sigmod->GetSpeciesByID(s);
//     if (pkmn->GetNumAbilities() == 1)
//         FeedAbility(pkmn->GetAbility(0));
// }
// 
// void FeedAttack(const unsigned actualDamage, const unsigned stat, const unsigned otherLevel, const unsigned power, const bool isAttacker)
// {
//     
// }
// 
// void FeedItem(const unsigned i)
// {
//     items.append(i);
// }
// 
// void FeedMove(const unsigned m)
// {
//     if (!moves.contains(m))
//     {
//         moves.insert(m);
//         --unknownMoves;
//         moveChances[m] = UINT_MAX;
//         for (QMutableMapIterator<unsigned, unsigned> i(moveChances); i.hasNext(); i.next())
//         {
//             if (i.value() != UINT_MAX)
//             {
//                 i.value() *= unknownMoves;
//                 i.value() /= (unknownMoves + 1);
//                 if (!i.value())
//                     moveChances.erase(i);
//             }
//         }
//         for (QListIterator<MoveCombo> i(moveCombos); i.hasNext(); i.next())
//         {
//             if (i.prereqs.intersect(moves).size() && !moves.contains(i.move))
//                 moves[i.move] *= i.chance;
//         }
//     }
// }
// 
// void FeedMoveChance(const unsigned m, const unsigned weight)
// {
//     moveChances[m] = weight;
// }
// 
// void FeedAbility(const unsigned a)
// {
//     ability = a;
// }
// 
// void UpdateHP()
// {
//     curHP = hpPercent * maxStats[PokeMod::ST_HP];
// }
// 
// void SetHP(const Frac p)
// {
//     hpPercent = p;
//     UpdateHP();
// }
