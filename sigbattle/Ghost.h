/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGBATTLE_GHOST
#define SIGBATTLE_GHOST

// Sigbattle includes
#include "TeamMember.h"

namespace Sigbattle
{
class SIGBATTLE_EXPORT Ghost : public TeamMember
{
    Q_OBJECT
    
//     public:
//         Ghost(const PokeMod::Sigmod& par, const unsigned s, const unsigned l);
//         
//         void FeedAttack(const unsigned actualDamage, const unsigned stat, const unsigned otherLevel, const unsigned power, const bool isAttacker);
//         void FeedItem(const unsigned i);
//         void FeedMove(const unsigned m);
//         void FeedMoveChance(const unsigned m, const unsigned weight);
//         void FeedMoveCombo(const MoveCombo m);
//         void FeedAbility(const unsigned a);
//         
//         void UpdateHP();
//         void SetHP(const Frac p);
//     private:
//         struct MoveCombo
//         {
//             public:
//                 MoveCombo(const QSet<unsigned> p, const unsigned m, const unsigned c) :
//                         prereqs(p),
//                         move(m),
//                         chance(c)
//                 {
//                 }
//                 QSet<unsigned> prereqs;
//                 unsigned move;
//                 const unsigned chance;
//         };
//         
//         unsigned minStats[PokeMod::ST_End_GSC];
//         unsigned maxStats[PokeMod::ST_End_GSC];
//         QMap<unsigned, unsigned> moveChances;
//         QList<MoveCombo> moveCombos;
//         unsigned unknownMoves;
};
}

#endif
