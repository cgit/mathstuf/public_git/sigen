/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGBATTLE_BOT
#define SIGBATTLE_BOT

// Sigbattle includes
#include "Player.h"

// Standard library includes
#include <climits>

// Forward declarations
namespace Sigmod
{
class MapTrainer;
}

namespace Sigbattle
{
class Arena;
class GhostBot;

class SIGBATTLE_EXPORT Bot : public Player
{
    Q_OBJECT
    
    public:
        Bot(const Sigmod::MapTrainer& trainer);
    public slots:
    signals:
    protected:
    protected slots:
    private:
        long alphaBeta(const Arena& arena, const int trainerClass, const long alpha = LONG_MIN, const long beta = LONG_MAX);
        
        QList<GhostBot*> m_enemies;
};
}

#endif
