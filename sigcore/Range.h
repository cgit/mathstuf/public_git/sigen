/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Range.h
 */

#ifndef SIGCORE_RANGE
#define SIGCORE_RANGE

// Sigcore includes
#include "Global.h"

// Qt includes
#include <QtCore/QMetaType>

namespace Sigcore
{
/**
 * \class Sigcore::Range Range.h sigcore/Range.h
 * \brief Class that represents a range of values.
 */
class SIGCORE_EXPORT Range
{
    public:
        /**
         * Default constructor.
         */
        Range();
        
        /**
         * Conversion constructor.
         * 
         * \param value Value to set the range to.
         */
        Range(const double value);
        
        /**
         * Constructor.
         * 
         * \param minimum The minimum value of the range.
         * \param maximum The maximum value of the range.
         */
        Range(const double minimum, const double maximum);
        
        /**
         * Sets the values for the range.
         * 
         * \param minimum The minimum value of the range.
         * \param maximum The maximum value of the range.
         */
        void set(const double minimum, const double maximum);
        /**
         * Set the minimum value for the range.
         * 
         * \param minimum The minimum value of the range.
         */
        void setMinimum(const double minimum);
        /**
         * Set the maximum value for the range.
         * 
         * \param maximum The maximum value of the range.
         */
        void setMaximum(const double maximum);
        
        /**
         * \sa setMinimum
         * 
         * \return The minimum value of the range.
         */
        double minimum() const;
        /**
         * \sa setMaximum
         * 
         * \return The maximum value of the range.
         */
        double maximum() const;
        
        /**
         * \return \p true if the range is valid, \p false otherwise.
         */
        bool valid() const;
        
        /**
         * \param value The value to check for.
         * 
         * \return \p true if the range contains \p value, \p false otherwise.
         */
        bool in(const double value) const;
        /**
         * \param range The range to check for.
         * 
         * \return \p true if \p range is completely within the range, \p false otherwise.
         */
        bool in(const Range& range) const;
        
        /**
         * Intersects two ranges.
         * 
         * \param rhs The range to intersect with.
         * \return The common range.
         */
        Range operator&(const Range& rhs) const;
        Range& operator&=(const Range& rhs);
        
        /**
         * Merges two ranges.
         * 
         * \param rhs The range to merge with.
         * \return The merged range.
         */
        Range operator|(const Range& rhs) const;
        Range& operator|=(const Range& rhs);
        
        Range& operator=(const Range& rhs);
        Range operator-() const;
        Range operator+(const Range& rhs) const;
        Range operator-(const Range& rhs) const;
        Range operator*(const Range& rhs) const;
        Range operator/(const Range& rhs) const;
        Range& operator+=(const Range& rhs);
        Range& operator-=(const Range& rhs);
        Range& operator*=(const Range& rhs);
        Range& operator/=(const Range& rhs);
        bool operator==(const Range& rhs) const;
        bool operator!=(const Range& rhs) const;
    private:
        double m_minimum;
        double m_maximum;
};
}
Q_DECLARE_METATYPE(Sigcore::Range)

#endif
