/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Range.cpp
 */

// Header include
#include "Range.h"

using namespace Sigcore;

Range::Range()
{
    set(1, -1);
}

Range::Range(const double value)
{
    set(value, value);
}

Range::Range(const double minimum, const double maximum)
{
    set(minimum, maximum);
}

void Range::set(const double minimum, const double maximum)
{
    m_minimum = minimum;
    m_maximum = maximum;
}

void Range::setMinimum(const double minimum)
{
    if (minimum <= m_maximum)
        m_minimum = minimum;
    else
        m_minimum = m_maximum = minimum;
}

void Range::setMaximum(const double maximum)
{
    if (m_minimum <= maximum)
        m_maximum = maximum;
    else
        m_minimum = m_maximum = maximum;
}

double Range::minimum() const
{
    return m_minimum;
}

double Range::maximum() const
{
    return m_maximum;
}

bool Range::valid() const
{
    return (m_minimum <= m_maximum);
}

bool Range::in(const double value) const
{
    return ((m_minimum <= value) && (value <= m_maximum));
}

bool Range::in(const Range& range) const
{
    return (range.valid() && (m_minimum <= range.m_minimum) && (range.m_maximum <= m_maximum));
}

Range Range::operator&(const Range& rhs) const
{
    return Range(qMax(m_minimum, rhs.m_minimum), qMin(m_maximum, rhs.m_maximum));
}

Range& Range::operator&=(const Range& rhs)
{
    return *this = *this & rhs;
}

Range Range::operator|(const Range& rhs) const
{
    return Range(qMin(m_minimum, rhs.m_minimum), qMax(m_maximum, rhs.m_maximum));
}

Range& Range::operator|=(const Range& rhs)
{
    return *this = *this | rhs;
}

Range& Range::operator=(const Range& rhs)
{
    if (this == &rhs)
        return *this;
    m_minimum = rhs.m_minimum;
    m_maximum = rhs.m_maximum;
    return *this;
}

Range Range::operator-() const
{
    return Range(-m_maximum, -m_minimum);
}

Range Range::operator+(const Range& rhs) const
{
    return Range(m_minimum + rhs.m_minimum, m_maximum + rhs.m_maximum);
}

Range Range::operator-(const Range& rhs) const
{
    return Range(m_minimum - rhs.m_maximum, m_maximum - rhs.m_minimum);
}

Range Range::operator*(const Range& rhs) const
{
    const double minMin = m_minimum * rhs.m_minimum;
    const double minMax = m_minimum * rhs.m_maximum;
    const double maxMin = m_maximum * rhs.m_minimum;
    const double maxMax = m_maximum * rhs.m_maximum;
    return Range(qMin(qMin(minMin, minMax), qMin(maxMin, maxMax)), qMax(qMax(minMin, minMax), qMax(maxMin, maxMax)));
}

Range Range::operator/(const Range& rhs) const
{
    if (rhs.in(0))
        return Range(1, -1);
    const double minMin = m_minimum / rhs.m_minimum;
    const double minMax = m_minimum / rhs.m_maximum;
    const double maxMin = m_maximum / rhs.m_minimum;
    const double maxMax = m_maximum / rhs.m_maximum;
    return Range(qMin(qMin(minMin, minMax), qMin(maxMin, maxMax)), qMax(qMax(minMin, minMax), qMax(maxMin, maxMax)));
}

Range& Range::operator+=(const Range& rhs)
{
    return *this = *this + rhs;
}

Range& Range::operator-=(const Range& rhs)
{
    return *this = *this - rhs;
}

Range& Range::operator*=(const Range& rhs)
{
    return *this = *this * rhs;
}

Range& Range::operator/=(const Range& rhs)
{
    return *this = *this / rhs;
}

bool Range::operator==(const Range& rhs) const
{
    return ((m_minimum == rhs.m_minimum) && (m_maximum == rhs.m_maximum));
}

bool Range::operator!=(const Range& rhs) const
{
    return !(*this == rhs);
}
