/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Fraction.cpp
 */

// Header include
#include "Fraction.h"

// Standard includes
#include <cstdlib>

using namespace Sigcore;

Fraction::Fraction(const int numerator, const int denominator)
{
    set(numerator, denominator);
}

Fraction::Fraction(const Fraction& fraction)
{
    set(fraction.numerator(), fraction.denominator());
}

void Fraction::set(const int numerator, const int denominator)
{
    m_numerator = numerator;
    m_denominator = denominator;
    normalize();
}

void Fraction::setNumerator(const int numerator)
{
    set(numerator, m_denominator);
}

void Fraction::setDenominator(const int denominator)
{
    set(m_numerator, denominator);
}

int Fraction::numerator() const
{
    return m_numerator;
}

int Fraction::denominator() const
{
    return m_denominator;
}

void Fraction::reduce()
{
    if (!m_numerator || !m_denominator)
        return;
    int i = m_numerator;
    int j = m_denominator;
    if (i < 0)
        i = -i;
    while (i != j)
        (i > j) ? (i -= j) : (j -= i);
    m_numerator /= i;
    m_denominator /= i;
}

bool Fraction::poll() const
{
    return (drand48() * m_denominator) < m_numerator;
}

bool Fraction::poll(const int numerator, const int denominator)
{
    return (drand48() * denominator) < numerator;
}

Fraction& Fraction::operator=(const Fraction& rhs)
{
    if (this == &rhs)
        return *this;
    m_numerator = rhs.m_numerator;
    m_denominator = rhs.m_denominator;
    return *this;
}

Fraction::operator double() const
{
    return (double(m_numerator) / m_denominator);
}

Fraction Fraction::operator+(const Fraction& rhs) const
{
    return Fraction((m_numerator * rhs.m_denominator) + (m_denominator * rhs.m_numerator), m_denominator * rhs.m_denominator);
}

Fraction Fraction::operator-(const Fraction& rhs) const
{
    return Fraction((m_numerator * rhs.m_denominator) - (m_denominator * rhs.m_numerator), m_denominator * rhs.m_denominator);
}

Fraction Fraction::operator*(const Fraction& rhs) const
{
    return Fraction(m_numerator * rhs.m_numerator, m_denominator * rhs.m_denominator);
}

Fraction Fraction::operator/(const Fraction& rhs) const
{
    return Fraction(m_numerator * rhs.m_denominator, m_denominator * rhs.m_numerator);
}

Fraction Fraction::operator%(const Fraction& rhs) const
{
    // Formula from Kevin Kofler
    return Fraction((m_numerator * rhs.m_denominator) % (m_denominator * rhs.m_numerator), m_denominator * rhs.m_denominator);
}

Fraction& Fraction::operator+=(const Fraction& rhs)
{
    return *this = *this + rhs;
}

Fraction& Fraction::operator-=(const Fraction& rhs)
{
    return *this = *this - rhs;
}

Fraction& Fraction::operator*=(const Fraction& rhs)
{
    return *this = *this * rhs;
}

Fraction& Fraction::operator/=(const Fraction& rhs)
{
    return *this = *this / rhs;
}

Fraction& Fraction::operator%=(const Fraction& rhs)
{
    return *this = *this % rhs;
}

bool Fraction::operator==(const Fraction& rhs) const
{
    return ((m_numerator * rhs.m_denominator) == (m_denominator * rhs.m_numerator));
}

bool Fraction::operator!=(const Fraction& rhs) const
{
    return !(*this == rhs);
}

bool Fraction::operator<(const Fraction& rhs) const
{
    return ((m_numerator * rhs.m_denominator) < (m_denominator * rhs.m_numerator));
}

bool Fraction::operator<=(const Fraction& rhs) const
{
    return ((m_numerator * rhs.m_denominator) <= (m_denominator * rhs.m_numerator));
}

bool Fraction::operator>(const Fraction& rhs) const
{
    return !(*this <= rhs);
}

bool Fraction::operator>=(const Fraction& rhs) const
{
    return !(*this < rhs);
}

void Fraction::normalize()
{
    if (m_denominator < 0)
    {
        m_denominator = -m_denominator;
        m_numerator = -m_numerator;
    }
}
