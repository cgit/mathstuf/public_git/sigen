/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Script.h
 */

#ifndef SIGCORE_SCRIPT
#define SIGCORE_SCRIPT

// Sigcore includes
#include "Global.h"

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QString>

namespace Sigcore
{
/**
 * \class Sigcore::Script Script.h sigcore/Script.h
 * \brief Class that describes a script for the game engine.
 */
class SIGCORE_EXPORT Script
{
    public:
        /**
         * Constructor.
         * 
         * \param interpreter The language of the script.
         * \param script The code for the script.
         */
        explicit Script(const QString& interpreter = "", const QString& script = "");
        
        /**
         * Set the language of the script. The default game engine uses Kross <http://kross.dipe.org/> to
         * run the scripts. The following values are valid for Kross:
         * 
         *  - \b mono -- C# <http://www.mono-project.com/Main_Page>
         *  - \b falcon -- Falcon <http://www.falconpl.org/>
         *  - \b javascript -- KDE JavaScript <http://api.kde.org/4.x-api/kdelibs-apidocs/kjs/html/index.html>
         *  - \b qtscript -- QtScript <http://doc.trolltech.com/latest/qtscript.html>
         *  - \b java -- Java <http://www.java.com/en/>
         *  - \b lua -- Lua <http://www.lua.org/>
         *  - \b php -- PHP <http://www.php.net/>
         *  - \b python -- Python <http://www.python.org/>
         *  - \b ruby -- Ruby <http://www.ruby-lang.org/en/>
         * 
         * Other languages may be added in the future.
         * 
         * \param interpreter The language of the script.
         */
        void setInterpreter(const QString& interpreter);
        /**
         * 
         * \param script The code for the script.
         */
        void setScript(const QString& script);
        
        /**
         * \sa setInterpreter
         * 
         * \return The language of the script.
         */
        QString interpreter() const;
        /**
         * \sa setScript
         * 
         * \return The code for the script.
         */
        QString script() const;
        
        Script& operator=(const Script& rhs);
        bool operator==(const Script& rhs) const;
        bool operator!=(const Script& rhs) const;
    private:
        QString m_interpreter;
        QString m_script;
};
}
Q_DECLARE_METATYPE(Sigcore::Script)

#endif
