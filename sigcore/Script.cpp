/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Script.cpp
 */

// Header include
#include "Script.h"

using namespace Sigcore;

Script::Script(const QString& interpreter, const QString& script) :
        m_interpreter(interpreter),
        m_script(script)
{
}

void Script::setInterpreter(const QString& interpreter)
{
    m_interpreter = interpreter;
}

void Script::setScript(const QString& script)
{
    m_script = script;
}

QString Script::interpreter() const
{
    return m_interpreter;
}

QString Script::script() const
{
    return m_script;
}

Script& Script::operator=(const Script& rhs)
{
    if (this == &rhs)
        return *this;
    m_interpreter = rhs.m_interpreter;
    m_script = rhs.m_script;
    return *this;
}

bool Script::operator==(const Script& rhs) const
{
    return ((m_interpreter == rhs.m_interpreter) && (m_script == rhs.m_script));
}

bool Script::operator!=(const Script& rhs) const
{
    return !(*this == rhs);
}
