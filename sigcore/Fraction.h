/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Fraction.h
 */

#ifndef SIGCORE_FRACTION
#define SIGCORE_FRACTION

// Sigcore includes
#include "Global.h"

// Qt includes
#include <QtCore/QMetaType>

namespace Sigcore
{
/**
 * \class Sigcore::Fraction Fraction.h sigcore/Fraction.h
 * \brief Class that represents a fractional quantity.
 * 
 * This class should be used in place of \p double because of accuracy.
 */
class SIGCORE_EXPORT Fraction
{
    public:
        /**
         * Constructor.
         * 
         * \param numerator The numerator of the fraction.
         * \param denominator The denominator of the fraction.
         */
        explicit Fraction(const int numerator = 1, const int denominator = 1);
        /**
         * Copy constructor.
         * 
         * \param fraction The value to copy.
         */
        Fraction(const Fraction& fraction);
        
        /**
         * Set the value of the fraction.
         * 
         * \param numerator The numerator of the fraction.
         * \param denominator The denominator of the fraction.
         */
        void set(const int numerator, const int denominator);
        /**
         * Sets the numerator of the fraction.
         * 
         * \param numerator The numerator of the fraction.
         */
        void setNumerator(const int numerator);
        /**
         * Set the denominator of the fraciton.
         * 
         * \param denominator The denominator of the fraction.
         */
        void setDenominator(const int denominator);
        
        /**
         * \return The numerator of the fraction.
         */
        int numerator() const;
        /**
         * \return The denominator of the fraction.
         */
        int denominator() const;
        
        /**
         * Reduces the fraction.
         */
        void reduce();
        /**
         * Uses the value of the fraction to return a weighted boolean.
         * 
         * \return \p true with a weight of the fraction, \p false otherwise.
         */
        bool poll() const;
        /**
         * Convenience function to get a weighted boolean value.
         * 
         * \param numerator The numerator of the chance that the function returns \p true.
         * \param demoninator The denominator of the chance that the function returns \p true.
         * \return \p true with a weight of the (\p numerator / \p denominator), \p false otherwise.
         */
        static bool poll(const int numerator, const int demoninator);
        
        Fraction& operator=(const Fraction& rhs);
        operator double() const;
        Fraction operator+(const Fraction& rhs) const;
        Fraction operator-(const Fraction& rhs) const;
        Fraction operator*(const Fraction& rhs) const;
        Fraction operator/(const Fraction& rhs) const;
        Fraction operator%(const Fraction& rhs) const;
        Fraction& operator+=(const Fraction& rhs);
        Fraction& operator-=(const Fraction& rhs);
        Fraction& operator*=(const Fraction& rhs);
        Fraction& operator/=(const Fraction& rhs);
        Fraction& operator%=(const Fraction& rhs);
        bool operator==(const Fraction& rhs) const;
        bool operator!=(const Fraction& rhs) const;
        bool operator<(const Fraction& rhs) const;
        bool operator<=(const Fraction& rhs) const;
        bool operator>(const Fraction& rhs) const;
        bool operator>=(const Fraction& rhs) const;
    protected:
        void normalize();
    private:
        int m_numerator;
        int m_denominator;
};
}
Q_DECLARE_METATYPE(Sigcore::Fraction)

#endif
