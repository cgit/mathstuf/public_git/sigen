/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigcore/Global.h
 */

#ifndef SIGCORE_GLOBAL
#define SIGCORE_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGCORE_EXPORT
#   ifdef MAKE_SIGCORE_LIB
#       define SIGCORE_EXPORT KDE_EXPORT /// Export the symbol if building the library.
#   else
#       define SIGCORE_EXPORT KDE_IMPORT /// Import the symbol if including the library.
#   endif
#   define SIGCORE_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGCORE_EXPORT_DEPRECATED
#   define SIGCORE_EXPORT_DEPRECATED KDE_DEPRECATED SIGCORE_EXPORT /// Mark as deprecated
#endif

#endif
