/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestScript.h"

// Sigcore includes
#include "../Script.h"

// KDE includes
#include <kross/core/action.h>

// Qt includes
#include <QtTest/QTest>

using namespace Sigcore;

QString TestScript::value() const
{
    return m_value;
}

void TestScript::setValue(const QString& value)
{
    m_value = value;
}

void TestScript::slot1()
{
    m_foo = true;
}

void TestScript::slot2()
{
    m_bar = true;
}

void TestScript::init()
{
    m_foo = false;
    m_bar = false;
    m_value = "";
    m_action = NULL;
}

void TestScript::cleanup()
{
    delete m_action;
}

void TestScript::setInterpreter()
{
    Script script;
    
    QCOMPARE(script.interpreter(), QString(""));
    
    script.setInterpreter("python");
    
    QCOMPARE(script.interpreter(), QString("python"));
}

void TestScript::setScript()
{
    Script script;
    
    QCOMPARE(script.script(), QString(""));
    
    script.setScript("import os");
    
    QCOMPARE(script.script(), QString("import os"));
}

void TestScript::assignment()
{
    Script script1;
    Script script2("python", "import os");
    
    QCOMPARE(script1.interpreter(), QString(""));
    QCOMPARE(script1.script(), QString(""));
    
    script1 = script2;
    
    QCOMPARE(script1.interpreter(), QString("python"));
    QCOMPARE(script1.script(), QString("import os"));
}

void TestScript::equality()
{
    Script script1;
    Script script2("python", "import os");
    
    QVERIFY(script1 != script2);
    
    script1 = script2;
    
    QVERIFY(script1 == script2);
}

void TestScript::python()
{
    Script script("python", "import obj\n"
                            "\n"
                            "obj.value = \"set\"\n"
                            "def slot():\n"
                            "    obj.slot1()\n"
                            "obj.connect(\"signal1()\", slot)\n"
                            "obj.connect(\"signal2()\", \"slot2()\")\n"
                            "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}

/*
void TestScript::falcon()
{
    Script script("falcon", "obj.value = \"set\"\n"
                            "function slot()\n"
                            "    obj.slot1()\n"
                            "end\n"
                            "obj.connect(\"signal1()\", slot)\n"
                            "obj.connect(\"signal2()\", \"slot2()\")\n"
                            "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}
*/

void TestScript::javascript()
{
    Script script("javascript", "obj.value = \"set\"\n"
                                "function slot()\n"
                                "{\n"
                                "    obj.slot1()\n"
                                "}\n"
                                "connect(obj, \"signal1()\", this, \"slot()\")\n"
                                "connect(obj, \"signal2()\", obj, \"slot2()\")\n"
                                "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}

void TestScript::qtscript()
{
    Script script("qtscript", "obj.value = \"set\"\n"
                              "function slot()\n"
                              "{\n"
                              "    obj.slot1()\n"
                              "}\n"
                              "obj.signal1.connect(slot)\n"
                              "obj.signal2.connect(obj.slot2)\n"
                              "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}

/*
void java()
{
    Script script("java", "import obj\n"
                          "\n"
                          "obj.value = \"set\"\n"
                          "def slot():\n"
                          "    object.slot1()\n"
                          "obj.connect(\"signal1()\", slot)\n"
                          "obj.connect(\"signal2()\", \"slot2()\")\n"
                          "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}

void lua()
{
    Script script("lua", "require\"obj\"\n"
                         "\n"
                         "obj.value = \"set\"\n"
                         "function slot ()\n"
                         "    obj.slot1()\n"
                         "end\n"
                         "obj.connect(\"signal1()\", slot)\n"
                         "obj.connect(\"signal2()\", \"slot2()\")\n"
                         "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}
*/

void TestScript::ruby()
{
    Script script("ruby", "require \'obj\'\n"
                          "\n"
                          "Obj.value = \"set\"\n"
                          "def slot()\n"
                          "    Obj.slot1()\n"
                          "end\n"
                          "Obj.connect(\"signal1()\", slot)\n"
                          // FIXME: krossruby does not support connect fully yet :(
//                           "Obj.connect(\"signal2()\", \"slot2()\")\n"
                          "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    // FIXME: Enable when krossruby is fixed
//     QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}

/*
void mono()
{
    Script script("mono", "import obj\n"
                          "\n"
                          "obj.value = \"set\"\n"
                          "def slot():\n"
                          "    obj.slot1()\n"
                          "obj.connect(\"signal1()\", slot)\n"
                          "obj.connect(\"signal2()\", \"slot2()\")\n"
                          "");
    
    m_action = new Kross::Action(this, "test-script");
    m_action->setInterpreter(script.interpreter());
    m_action->setCode(script.script().toUtf8());
    m_action->addObject(this, "obj");
    m_action->trigger();
    
    emit(signal1());
    emit(signal2());
    
    QCOMPARE(m_foo, true);
    QCOMPARE(m_bar, true);
    QCOMPARE(m_value, QString("set"));
}
*/

QTEST_MAIN(TestScript)
