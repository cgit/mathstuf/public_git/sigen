/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMatrix.h"

// Sigcore includes
#include "../Matrix.h"

// Qt includes
#include <QtTest/QTest>

using namespace Sigcore;

void TestMatrix::addRow()
{
    Matrix<int> mat;
    
    QCOMPARE(mat.width(), 0);
    QCOMPARE(mat.height(), 0);
    
    mat.addRow();
    
    QCOMPARE(mat.width(), 1);
    QCOMPARE(mat.height(), 1);
    
    mat.addRow(4);
    
    QCOMPARE(mat.height(), 2);
    QCOMPARE(mat.width(), 1);
    QCOMPARE(mat(1, 0), 4);
}

void TestMatrix::addColumn()
{
    Matrix<int> mat;
    
    QCOMPARE(mat.width(), 0);
    QCOMPARE(mat.height(), 0);
    
    mat.addColumn();
    
    QCOMPARE(mat.width(), 1);
    QCOMPARE(mat.height(), 1);
    
    mat.addColumn(4);
    
    QCOMPARE(mat.height(), 1);
    QCOMPARE(mat.width(), 2);
    QCOMPARE(mat(0, 1), 4);
}

void TestMatrix::insertRow()
{
    Matrix<int> mat;
    
    mat.addRow();
    mat.addRow(4);
    
    mat.insertRow(1, 3);
    
    QCOMPARE(mat.height(), 3);
    QCOMPARE(mat.width(), 1);
    QCOMPARE(mat(1, 0), 3);
}

void TestMatrix::insertColumn()
{
    Matrix<int> mat;
    
    mat.addColumn();
    mat.addColumn(4);
    
    mat.insertColumn(1, 3);
    
    QCOMPARE(mat.height(), 1);
    QCOMPARE(mat.width(), 3);
    QCOMPARE(mat(0, 1), 3);
}

void TestMatrix::deleteRow()
{
    Matrix<int> mat;
    
    mat.addRow();
    mat.addRow(4);
    mat.addRow(3);
    
    mat.deleteRow(1);
    
    QCOMPARE(mat.height(), 2);
    QCOMPARE(mat.width(), 1);
    QCOMPARE(mat(1, 0), 3);
    
    mat.deleteRow(0);
    mat.deleteRow(0);
    
    QCOMPARE(mat.height(), 0);
    QCOMPARE(mat.width(), 0);
}

void TestMatrix::deleteColumn()
{
    Matrix<int> mat;
    
    mat.addColumn();
    mat.addColumn(4);
    mat.addColumn(3);
    
    mat.deleteColumn(1);
    
    QCOMPARE(mat.height(), 1);
    QCOMPARE(mat.width(), 2);
    QCOMPARE(mat(0, 1), 3);
    
    mat.deleteColumn(0);
    mat.deleteColumn(0);
    
    QCOMPARE(mat.height(), 0);
    QCOMPARE(mat.width(), 0);
}

void TestMatrix::clear()
{
    Matrix<int> mat(3, 3);
    
    mat.clear();
    
    QCOMPARE(mat.height(), 0);
    QCOMPARE(mat.width(), 0);
}

void TestMatrix::resize()
{
    Matrix<int> mat(3, 3);
    
    mat.resize(4, 4, 9);
    
    QCOMPARE(mat.height(), 4);
    QCOMPARE(mat.width(), 4);
    QCOMPARE(mat(3, 3), 9);
    
    mat.resize(2, 2, 9);
    
    QCOMPARE(mat.height(), 2);
    QCOMPARE(mat.width(), 2);
}

void TestMatrix::row()
{
    Matrix<int> mat(3, 3);
    
    mat(0, 0) = 0;
    mat(0, 1) = 1;
    mat(0, 2) = 2;
    
    mat(1, 0) = 3;
    mat(1, 1) = 4;
    mat(1, 2) = 5;
    
    mat(2, 0) = 6;
    mat(2, 1) = 7;
    mat(2, 2) = 8;
    
    QVector<int> row = mat.row(1);
    
    QCOMPARE(row[0], 3);
    QCOMPARE(row[1], 4);
    QCOMPARE(row[2], 5);
}

void TestMatrix::column()
{
    Matrix<int> mat(3, 3);
    
    mat(0, 0) = 0;
    mat(0, 1) = 1;
    mat(0, 2) = 2;
    
    mat(1, 0) = 3;
    mat(1, 1) = 4;
    mat(1, 2) = 5;
    
    mat(2, 0) = 6;
    mat(2, 1) = 7;
    mat(2, 2) = 8;
    
    QVector<int> column = mat.column(1);
    
    QCOMPARE(column[0], 1);
    QCOMPARE(column[1], 4);
    QCOMPARE(column[2], 7);
}

void TestMatrix::access()
{
    Matrix<int> mat(3, 3);
    
    mat(0, 0) = 0;
    mat(0, 1) = 1;
    mat(0, 2) = 2;
    
    mat(1, 0) = 3;
    mat(1, 1) = 4;
    mat(1, 2) = 5;
    
    mat(2, 0) = 6;
    mat(2, 1) = 7;
    mat(2, 2) = 8;
    
    QCOMPARE(mat(0, 0), 0);
    QCOMPARE(mat(0, 1), 1);
    QCOMPARE(mat(0, 2), 2);
    
    QCOMPARE(mat(1, 0), 3);
    QCOMPARE(mat(1, 1), 4);
    QCOMPARE(mat(1, 2), 5);
    
    QCOMPARE(mat(2, 0), 6);
    QCOMPARE(mat(2, 1), 7);
    QCOMPARE(mat(2, 2), 8);
}

void TestMatrix::assignment()
{
    Matrix<int> mat1(3, 3);
    Matrix<int> mat2;
    
    mat1(0, 0) = 0;
    mat1(0, 1) = 1;
    mat1(0, 2) = 2;
    
    mat1(1, 0) = 3;
    mat1(1, 1) = 4;
    mat1(1, 2) = 5;
    
    mat1(2, 0) = 6;
    mat1(2, 1) = 7;
    mat1(2, 2) = 8;
    
    mat2 = mat1;
    
    QCOMPARE(mat2(0, 0), 0);
    QCOMPARE(mat2(0, 1), 1);
    QCOMPARE(mat2(0, 2), 2);
    
    QCOMPARE(mat2(1, 0), 3);
    QCOMPARE(mat2(1, 1), 4);
    QCOMPARE(mat2(1, 2), 5);
    
    QCOMPARE(mat2(2, 0), 6);
    QCOMPARE(mat2(2, 1), 7);
    QCOMPARE(mat2(2, 2), 8);
}

void TestMatrix::equal()
{
    Matrix<int> mat1(3, 3);
    Matrix<int> mat2;
    
    mat1(0, 0) = 0;
    mat1(0, 1) = 1;
    mat1(0, 2) = 2;
    
    mat1(1, 0) = 3;
    mat1(1, 1) = 4;
    mat1(1, 2) = 5;
    
    mat1(2, 0) = 6;
    mat1(2, 1) = 7;
    mat1(2, 2) = 8;
    
    QVERIFY(mat1 != mat2);
    
    mat2 = mat1;
    
    QCOMPARE(mat2(0, 0), 0);
    QCOMPARE(mat2(0, 1), 1);
    QCOMPARE(mat2(0, 2), 2);
    
    QCOMPARE(mat2(1, 0), 3);
    QCOMPARE(mat2(1, 1), 4);
    QCOMPARE(mat2(1, 2), 5);
    
    QCOMPARE(mat2(2, 0), 6);
    QCOMPARE(mat2(2, 1), 7);
    QCOMPARE(mat2(2, 2), 8);
    
    QVERIFY(mat1 == mat2);
}

QTEST_APPLESS_MAIN(TestMatrix)
