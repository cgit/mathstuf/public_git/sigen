/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestHat.h"

// Sigcore includes
#include "../Hat.h"

// Qt includes
#include <QtTest/QTest>

using namespace Sigcore;

void TestHat::initTestCase()
{
    qsrand(QDateTime::currentDateTime().toTime_t());
}

void TestHat::pick()
{
    Hat<int> hat;
    
    QCOMPARE(hat.pick(), int());
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    hat.pick();
    
    QCOMPARE(hat.count(), 15);
    QCOMPARE(hat.distinctCount(), 5);
}

void TestHat::take()
{
    Hat<int> hat;
    
    QCOMPARE(hat.take(), int());
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    int picked = hat.take();
    
    QCOMPARE(hat.count(), 14);
    QCOMPARE(hat.count(picked), 4 - picked);
    
    while (hat.count())
        hat.take();
    
    QCOMPARE(hat.take(), int());
}

void TestHat::takeAndClear()
{
    Hat<int> hat;
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    int picked = hat.takeAndClear();
    
    QCOMPARE(hat.count(), 15 - 5 + picked);
    QCOMPARE(hat.count(picked), 0);
}

void TestHat::setCount()
{
    Hat<int> hat;
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    hat.setCount(4, 3);
    
    QCOMPARE(hat.count(), 17);
    QCOMPARE(hat.distinctCount(), 5);
    
    hat.setCount(4, 0);
    
    QCOMPARE(hat.count(), 14);
    QCOMPARE(hat.distinctCount(), 4);
    
    hat.setCount(4, 1);
    
    QCOMPARE(hat.count(), 15);
    QCOMPARE(hat.distinctCount(), 5);
}

void TestHat::add()
{
    Hat<int> hat;
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    hat.add(4, 3);
    
    QCOMPARE(hat.count(), 18);
    QCOMPARE(hat.distinctCount(), 5);
    
    hat.add(4, -3);
    
    QCOMPARE(hat.count(), 15);
    QCOMPARE(hat.distinctCount(), 5);
    
    hat.add(4, -2);
    
    QCOMPARE(hat.count(), 14);
    QCOMPARE(hat.distinctCount(), 4);
}

void TestHat::chance()
{
    Hat<int> hat;
    
    hat.add(0, 5);
    hat.add(1, 4);
    hat.add(2, 3);
    hat.add(3, 2);
    hat.add(4, 1);
    
    int picked = hat.pick();
    
    QCOMPARE(double(hat.chance(picked)), (5 - picked) / 15.);
}

QTEST_APPLESS_MAIN(TestHat)
