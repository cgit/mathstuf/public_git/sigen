/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTMATRIX
#define TESTMATRIX

// Qt includes
#include <QtCore/QObject>

class TestMatrix : public QObject
{
    Q_OBJECT
    
    private slots:
        void addRow();
        void addColumn();
        void insertRow();
        void insertColumn();
        void deleteRow();
        void deleteColumn();
        
        void clear();
        
        void resize();
        
        void row();
        void column();
        
        void access();
        
        void assignment();
        void equal();
};

#endif
