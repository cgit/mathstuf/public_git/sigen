/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestFraction.h"

// Sigcore includes
#include "../Fraction.h"

// Qt includes
#include <QtTest/QTest>

using namespace Sigcore;

void TestFraction::set()
{
    Fraction frac(1, 1);
    
    QCOMPARE(frac.numerator(), 1);
    QCOMPARE(frac.denominator(), 1);
    
    frac.set(3, 4);
    
    QCOMPARE(frac.numerator(), 3);
    QCOMPARE(frac.denominator(), 4);
}

void TestFraction::setNumerator()
{
    Fraction frac(5, 8);
    
    frac.setNumerator(4);
    
    QCOMPARE(frac.numerator(), 4);
}

void TestFraction::setDenominator()
{
    Fraction frac(5, 8);
    
    frac.setDenominator(4);
    
    QCOMPARE(frac.denominator(), 4);
}

void TestFraction::reduce()
{
    Fraction frac(5, 8);
    
    frac.reduce();
    
    QCOMPARE(frac.numerator(), 5);
    QCOMPARE(frac.denominator(), 8);
    
    frac.set(2, 4);
    frac.reduce();
    
    QCOMPARE(frac.numerator(), 1);
    QCOMPARE(frac.denominator(), 2);
    
    frac.set(-2, 4);
    frac.reduce();
    
    QCOMPARE(frac.numerator(), -1);
    QCOMPARE(frac.denominator(), 2);
}

void TestFraction::assignment()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    frac1 = frac2;
    
    QCOMPARE(frac1.numerator(), 3);
    QCOMPARE(frac1.denominator(), 4);
}

void TestFraction::conversion()
{
    Fraction frac(5, 8);
    
    QVERIFY(frac == .625);
}

void TestFraction::addition()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    frac1 = frac1 + frac2;
    
    QCOMPARE(frac1.numerator(), 44);
    QCOMPARE(frac1.denominator(), 32);
}

void TestFraction::subtraction()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    frac1 = frac1 - frac2;
    
    QCOMPARE(frac1.numerator(), -4);
    QCOMPARE(frac1.denominator(), 32);
}

void TestFraction::multiplication()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    frac1 = frac1 * frac2;
    
    QCOMPARE(frac1.numerator(), 15);
    QCOMPARE(frac1.denominator(), 32);
}

void TestFraction::division()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    frac1 = frac1 / frac2;
    
    QCOMPARE(frac1.numerator(), 20);
    QCOMPARE(frac1.denominator(), 24);
}

void TestFraction::modulo()
{
    Fraction frac1(7, 8);
    Fraction frac2(1, 2);
    
    frac1 = frac1 % frac2;
    frac1.reduce();
    
    QCOMPARE(frac1.numerator(), 3);
    QCOMPARE(frac1.denominator(), 8);
}

void TestFraction::equal()
{
    Fraction frac1(6, 8);
    Fraction frac2(3, 4);
    
    QCOMPARE(frac1, frac2);
}

void TestFraction::less()
{
    Fraction frac1(5, 8);
    Fraction frac2(3, 4);
    
    QVERIFY(frac1 < frac2);
}

void TestFraction::greater()
{
    Fraction frac1(7, 8);
    Fraction frac2(3, 4);
    
    QVERIFY(frac1 > frac2);
}

void TestFraction::normalize()
{
    Fraction frac(-2, -1);
    
    QCOMPARE(frac.numerator(), 2);
    QCOMPARE(frac.denominator(), 1);
    
    frac.set(2, -1);
    
    QCOMPARE(frac.numerator(), -2);
    QCOMPARE(frac.denominator(), 1);
}

QTEST_APPLESS_MAIN(TestFraction)
