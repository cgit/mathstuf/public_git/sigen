/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTFRACTION
#define TESTFRACTION

// Qt includes
#include <QtCore/QObject>

class TestFraction : public QObject
{
    Q_OBJECT
    
    private slots:
        void set();
        void setNumerator();
        void setDenominator();
        
        void reduce();
        
        void assignment();
        void conversion();
        void addition();
        void subtraction();
        void multiplication();
        void division();
        void modulo();
        
        void equal();
        void less();
        void greater();
        
        void normalize();
};

#endif
