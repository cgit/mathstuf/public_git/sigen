/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TESTSCRIPT
#define TESTSCRIPT

// Qt includes
#include <QtCore/QObject>

// Forward declarations
namespace Kross
{
class Action;
}

class TestScript : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString value READ value WRITE setValue)
    
    public:
        QString value() const;
        void setValue(const QString& value);
    public slots:
        void slot1();
        void slot2();
    signals:
        void signal1();
        void signal2();
    private:
        bool m_foo;
        bool m_bar;
        QString m_value;
        Kross::Action* m_action;
    private slots:
        void init();
        void cleanup();
        
        void setInterpreter();
        void setScript();
        void assignment();
        void equality();
        
        void python();
//         void falcon();
        void javascript();
        void qtscript();
//         void java();
//         void lua();
        void ruby();
//         void mono();
};

#endif
