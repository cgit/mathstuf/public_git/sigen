/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestRange.h"

// Sigcore includes
#include "../Range.h"

// Qt includes
#include <QtTest/QTest>

using namespace Sigcore;

void TestRange::set()
{
    Range range(4., 5.);
    
    QCOMPARE(range.minimum(), 4.);
    QCOMPARE(range.maximum(), 5.);
    
    range.set(2., 9.);
    
    QCOMPARE(range.minimum(), 2.);
    QCOMPARE(range.maximum(), 9.);
}

void TestRange::setMinimum()
{
    Range range(0., 5.);
    
    range.setMinimum(2.);
    
    QCOMPARE(range.minimum(), 2.);
    
    range.setMinimum(7.);
    
    QCOMPARE(range.minimum(), 7.);
    QCOMPARE(range.maximum(), 7.);
}

void TestRange::setMaximum()
{
    Range range(3., 8.);
    
    range.setMaximum(5.);
    
    QCOMPARE(range.maximum(), 5.);
    
    range.setMaximum(1.);
    
    QCOMPARE(range.maximum(), 1.);
    QCOMPARE(range.maximum(), 1.);
}

void TestRange::valid()
{
    Range range(0., 5.);
    
    QCOMPARE(range.valid(), true);
    
    range.set(7., 5.);
    
    QCOMPARE(range.valid(), false);
}

void TestRange::in()
{
    Range range(0., 5.);
    
    QCOMPARE(range.in(3.), true);
    QCOMPARE(range.in(6.), false);
    QCOMPARE(range.in(Range(1., 2.)), true);
    QCOMPARE(range.in(Range(4., 6.)), false);
}

void TestRange::intersection()
{
    Range range(0., 5.);
    
    QCOMPARE(range & Range(1., 3.), Range(1., 3.));
    QCOMPARE(range & Range(3., 6.), Range(3., 5.));
}

void TestRange::setUnion()
{
    Range range(0., 5.);
    
    QCOMPARE(range | Range(1., 3.), Range(0., 5.));
    QCOMPARE(range | Range(3., 6.), Range(0., 6.));
}

void TestRange::negation()
{
    Range range(1., 3.);
    
    QCOMPARE(-range, Range(-3., -1.));
}

void TestRange::addition()
{
    Range range(1., 3.);
    
    QCOMPARE(range + Range(2., 4.),  Range(3., 7.));
    QCOMPARE(range + 2.,  Range(3., 5.));
}

void TestRange::subtraction()
{
    Range range(1., 3.);
    
    QCOMPARE(range - Range(2., 4.),  Range(-3., 1.));
    QCOMPARE(range - 2.,  Range(-1., 1.));
}

void TestRange::multiplication()
{
    Range range(1., 3.);
    
    QCOMPARE(range * Range(2., 4.),  Range(2., 12.));
    QCOMPARE(range * Range(-2., 4.),  Range(-6., 12.));
    QCOMPARE(range * 2.,  Range(2., 6.));
    QCOMPARE(range * -2.,  Range(-6., -2.));
}

void TestRange::division()
{
    Range range(1., 3.);
    
    QCOMPARE(range / Range(2., 4.),  Range(1./4., 3./2.));
    QCOMPARE((range / Range(-2., 4.)).valid(),  false);
    QCOMPARE(range / 2.,  Range(.5, 1.5));
    QCOMPARE(range / -2.,  Range(-1.5, -.5));
}

void TestRange::equality()
{
    Range range(1., 3.);
    
    QVERIFY(range == Range(1., 3.));
    QVERIFY(range != Range(1., 4.));
}

QTEST_APPLESS_MAIN(TestRange)
