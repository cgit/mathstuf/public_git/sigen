/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Audio.h"

void Audio::playSFX(const QString& url)
{
    Phonon::MediaObject* sfx = new Phonon::MediaObject;
    sfx->setCurrentSource(url);
    if (sfx->state() != Phonon::ErrorState)
    {
        sfx->play();
        m_curPlay.append(sfx);
    }
}

void Audio::playMusic(const QString& url)
{
    if (!m_started)
        start();
    m_musicUrl = url;
    if (m_music.state() == Phonon::PlayingState)
        m_music.seek(m_music.totalTime() - 1000);
    else
    {
        m_music.setCurrentSource(url);
        if (m_music.state() != Phonon::ErrorState)
            m_music.play();
    }
}

void Audio::prune()
{
    for (QMutableListIterator<Phonon::MediaObject*> i(m_curPlay); i.hasNext(); i.next())
    {
        if (i.value()->state() == Phonon::StoppedState)
        {
            delete i.value();
            i.remove();
        }
    }
}

void Audio::clear()
{
    for (QMutableListIterator<Phonon::MediaObject*> i(m_curPlay); i.hasNext(); i.next())
    {
        if (i.value()->state() == Phonon::PlayingState)
            i.value()->stop();
        delete i.value();
        i.remove();
    }
}
