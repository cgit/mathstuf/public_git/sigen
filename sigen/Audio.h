/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGEN_AUDIO
#define SIGEN_AUDIO

// Qt includes
#include <QList>
#include <QObject>

// Phonon includes
#include <Phonon/AudioOutput>
#include <Phonon/MediaObject>

class Audio : public QObject
{
    Q_OBJECT
    
    public:
        inline Audio() :
                m_output(Phonon::MusicCategory),
                m_started(false)
        {
        }
        inline ~Audio()
        {
            clear();
        }
        
        void playSFX(const QString& url);
        void playMusic(const QString& url);
        
        void prune();
        void clear();
        
        inline double volume()
        {
            return m_output.volume();
        }
        inline void setVolume(const double volume)
        {
            m_output.setVolume(volume);
        }
    private slots:
        inline void loopMusic()
        {
            m_music.enqueue(m_musicUrl);
        }
    private:
        inline void start()
        {
            if (!m_started)
                connect(&m_music, SIGNAL(aboutToFinish()), this, SLOT(loopMusic()));
        }
        
        QList<Phonon::MediaObject*> m_curPlay;
        Phonon::MediaObject m_music;
        QString m_musicUrl;
        Phonon::AudioOutput m_output;
        bool m_started;
};

#endif
