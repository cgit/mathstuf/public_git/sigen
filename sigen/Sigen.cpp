/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// KDE includes
#include <KApplication>
#include <KCmdLineArgs>

int main(int argc, char* argv[])
{
    KApplication app;
    // TODO
    KAboutData about("sigen", "", ki18n("Sigen"), "0.0.2", ki18n(""), License_GPL_V3, ki18n("©2007-2008 Ben Boeckel and Nerdy Productions"), ki18n(""), "http://sourceforge.net/projects/sigen");
    about.setLicenseTextFile(ki18n("LICENSE"));
    // TODO
    about.setProgramName(ki18n("Sigen"));
    about.addAuthor("Ben Boeckel", ki18n("Lead Programmer"), "MathStuf@gmail.com", "http://nerdyproductions.sobertillnoon.com");
    about.addCredit("Peter Fernandes", ki18n("Ideas"), "supersonicandtails@gmail.com", "http://www.hypersonicsoft.org");
    about.addCredit("Kevin Kofler", ki18n("Qt and KDE help"), "kevin.kofler@chello.at", "http://www.tigen.org/kevin.kofler");
}
