/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODR_SIGMODRUI
#define SIGMODR_SIGMODRUI

// Form include
#include "ui_sigmodr.h"

// KDE includes
#include <KXmlGuiWindow>

// Qt includes
#include <QtXml/QDomDocument>

// Forward declarations
class KUrl;
class QDragEnterEvent;
class QDropEvent;
namespace Sigmod
{
class Object;
class Game;
}

namespace Sigmodr
{
namespace Tree
{
class SigmodrTree;
}

class SigmodrUI : public KXmlGuiWindow, private Ui::formSigmodr
{
    Q_OBJECT
    
    public:
        SigmodrUI(QWidget* parent = 0);
        ~SigmodrUI();
    protected:
        void dragEnterEvent(QDragEnterEvent* event);
        void dropEvent(QDropEvent* event);
    protected slots:
        void update();
        
        void closeEvent(QCloseEvent* event);
        void resizeEvent(QResizeEvent* event);
        
        void setChangedTitle(const bool changed);
        
        void setDirty(const bool dirty = true);
        void setDirty(const bool dirty, const QModelIndex& index);
        
        void newGame();
        void openGame();
        bool openGame(const KUrl& url);
        bool openGame(const QString& path, const bool isRemote = false);
        void saveGame();
        void saveGame(const Sigmod::Game* game);
        bool saveGame(const Sigmod::Game* game, const KUrl& url);
        void saveAsGame();
        void saveAsGame(const Sigmod::Game* game);
        void downloadGame();
        void uploadGame();
        void uploadGame(const Sigmod::Game* game);
        bool closeWidget();
        void closeGame();
        bool closeGame(const Sigmod::Game* game, const bool force = false);
        bool closeAllGames(const bool force = false);
        void copyObject();
        void pasteObject();
        void preferences();
        void toggleMenubar();
        void on_splitter_splitterMoved();
        void on_treeSigmod_clicked(const QModelIndex& index);
    private:
        void setupActions();
        
        QModelIndex m_editedIndex;
        QSplitter* ui_splitter;
        QDomDocument m_clipboard;
};
}

#endif
