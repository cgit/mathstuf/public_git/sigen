/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SigmodrPreferencesWidget.h"

// Sigmodr includes
#include "SigmodrPreferences.h"

// Qt includes
#include <QtGui/QCheckBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLabel>
#include <QtGui/QLayout>
#include <QtGui/QVBoxLayout>

using namespace Sigmodr;

SigmodrPreferencesWidget::SigmodrPreferencesWidget(QWidget* parent) :
        QWidget(parent)
{
    QLabel* label = new QLabel("Reload files on startup:");
    label->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    kcfg_ReloadOnOpen = new QCheckBox("Enabled");
    kcfg_ReloadOnOpen->setChecked(SigmodrPreferences::reloadOnStart() ? Qt::Checked : Qt::Unchecked);
    label->setBuddy(kcfg_ReloadOnOpen);
    
    QLayout* layout = new QHBoxLayout;
    layout->addWidget(label);
    layout->addWidget(kcfg_ReloadOnOpen);
    
    QWidget* widget = new QWidget;
    widget->setLayout(layout);
    
    layout = new QVBoxLayout;
    layout->addWidget(widget);
    
    setLayout(layout);
}

void SigmodrPreferencesWidget::save()
{
    SigmodrPreferences::setReloadOnStart(kcfg_ReloadOnOpen->isChecked());
}
