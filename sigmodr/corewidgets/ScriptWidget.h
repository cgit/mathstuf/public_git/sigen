/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRCOREWIDGETS_SCRIPTWIDGET
#define SIGMODRCOREWIDGETS_SCRIPTWIDGET

// Sigmodr core includes
#include "Global.h"

// Sigcore includes
#include <sigcore/Script.h>

// Qt includes
#include <QtGui/QWidget>

namespace Sigmodr
{
namespace CoreWidgets
{
class SIGMODRCOREWIDGETS_EXPORT ScriptWidget : public QWidget
{
    Q_OBJECT
    
    public:
        explicit ScriptWidget(QWidget* parent, const Sigcore::Script& value = Sigcore::Script("", ""));
        
        Sigcore::Script value() const;
    public slots:
        void setValue(const Sigcore::Script& value);
    signals:
        void valueChanged(const Sigcore::Script& value);
    private:
        class Private;
        Private* const d;
};
}
}

#endif
