/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ScriptWidget.h"
#include "ScriptWidget_p.h"

// KDE includes
#include <KActionCollection>
#include <KApplication>
#include <KComboBox>
#include <KMenu>
#include <KMessageBox>
#include <KStandardAction>
#include <KTextEdit>
#include <KXMLGUIFactory>
#include <KXmlGuiWindow>
#include <KTextEditor/Document>
#include <KTextEditor/Editor>
#include <KTextEditor/EditorChooser>
#include <KTextEditor/View>

// Qt includes
#include <QtCore/QFile>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QVBoxLayout>
#include <QtUiTools/QUiLoader>

using namespace Sigcore;
using namespace Sigmodr::CoreWidgets;

ScriptWidget::ScriptWidget(QWidget* parent, const Script& value) :
        QWidget(parent),
        d(new Private(this, value))
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(d->makeWidgets(this));
    setLayout(layout);
}

Script ScriptWidget::value() const
{
    return d->m_value;
}

void ScriptWidget::setValue(const Script& value)
{
    if (d->m_value != value)
    {
        d->m_value = value;
        emit(valueChanged(d->m_value));
    }
}

QMap<QString, ScriptWidget::Private::LanguagePair> ScriptWidget::Private::m_languages;
QList<QString> ScriptWidget::Private::m_interpreters;

ScriptWidget::Private::Private(QObject* parent, const Script& value) :
        QObject(parent),
        m_value(value),
        ui_simpleEdit(NULL),
        m_editor(KTextEditor::EditorChooser::editor()),
        m_document(NULL)
{
    if (!m_languages.size())
    {
//         m_languages["C#"] = LanguagePair("mono", "C#");
//         m_languages["Falcon"] = LanguagePair("falcon", "Falcon");
//         m_languages["Java"] = LanguagePair("java", "Java");
        m_languages["JavaScript"] = LanguagePair("javascript", "JavaScript");
        m_languages["QtScript"] = LanguagePair("qtscript", "JavaScript");
//         m_languages["Lua"] = LanguagePair("lua", "Lua");
//         m_languages["PHP"] = LanguagePair("php", "PHP");
        m_languages["Python"] = LanguagePair("python", "Python");
        m_languages["Ruby"] = LanguagePair("ruby", "Ruby");
        QList<LanguagePair> langs = m_languages.values();
        foreach (const LanguagePair& lang, langs)
            m_interpreters.append(lang.first);
    }
}

ScriptWidget::Private::~Private()
{
    if (m_editor)
        m_editor->writeConfig(NULL);
}

QWidget* ScriptWidget::Private::makeWidgets(ScriptWidget* widget)
{
    QFile file(":/gui/script.ui");
    file.open(QFile::ReadOnly);
    QWidget *formWidget = QUiLoader().load(&file, widget);
    file.close();
    ui_interpreter = formWidget->findChild<KComboBox*>("varInterpreter");
    connect(ui_interpreter, SIGNAL(currentIndexChanged(QString)), this, SLOT(interpreterChanged(QString)));
    ui_interpreter->addItems(m_languages.keys());
    QLabel* labelScript = formWidget->findChild<QLabel*>("labelScript");
    QGridLayout* gridLayout = formWidget->findChild<QGridLayout*>("gridLayout");
    QWidget* editor;
    if (m_editor)
    {
        m_editor->readConfig();
        m_document = m_editor->createDocument(formWidget);
        ui_kteEdit = m_document->createView(formWidget);
        editor = ui_kteEdit;
        KActionCollection* collection = ui_kteEdit->actionCollection();
        collection->action("file_save")->setVisible(false);
        collection->action("file_save")->setEnabled(false);
        collection->action("file_save_as")->setVisible(false);
        collection->action("file_save_as")->setEnabled(false);
        collection->action("edit_undo")->setVisible(false);
        collection->action("edit_undo")->setEnabled(false);
        collection->action("edit_redo")->setVisible(false);
        collection->action("edit_redo")->setEnabled(false);
        collection->action("edit_cut")->setVisible(false);
        collection->action("edit_copy")->setVisible(false);
        collection->action("edit_paste")->setVisible(false);
        KXmlGuiWindow* topLevel = qobject_cast<KXmlGuiWindow*>(KApplication::kApplication()->activeWindow());
        if (topLevel)
        {
            KMenu *menu = qobject_cast<KMenu*>(topLevel->factory()->container("ktexteditor_popup", topLevel));
            if (menu)
                ui_kteEdit->setContextMenu(menu);
        }
        connect(m_document, SIGNAL(textChanged(KTextEditor::Document*)), this, SLOT(scriptChanged()));
        connect(ui_kteEdit, SIGNAL(focusIn(KTextEditor::View*)), this, SLOT(focused(KTextEditor::View*)));
        connect(ui_kteEdit, SIGNAL(focusOut(KTextEditor::View*)), this, SLOT(unfocused(KTextEditor::View*)));
    }
    else
    {
        KMessageBox::information(widget, "A KDE text-editor component could not be found.\nPlease check your KDE installation.\n\nEnhanced editing will not be used.", "KDE editor component not found", "kte-component");
        ui_simpleEdit = new KTextEdit(m_value.script(), widget);
        editor = ui_simpleEdit;
        connect(ui_simpleEdit, SIGNAL(textChanged()), this, SLOT(scriptChanged()));
    }
    editor->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    editor->setMinimumHeight(300);
    labelScript->setBuddy(editor);
    setTabOrder(ui_interpreter, editor);
    gridLayout->addWidget(editor, 1, 1);
    connect(this, SIGNAL(valueChanged(Sigcore::Script)), widget, SIGNAL(valueChanged(Sigcore::Script)));
    connect(this, SIGNAL(valueChanged(Sigcore::Script)), SLOT(setGui()));
    setGui();
    return formWidget;
}

void ScriptWidget::Private::interpreterChanged(const QString& interpreter)
{
    m_value.setInterpreter(m_languages[interpreter].first);
    if (m_document)
        m_document->setHighlightingMode(m_languages[interpreter].second);
    emit(valueChanged(m_value));
}

void ScriptWidget::Private::scriptChanged()
{
    if (m_document)
    {
        if (m_value.script() != m_document->text())
        {
            m_value.setScript(m_document->text());
            emit(valueChanged(m_value));
        }
    }
    else
    {
        if (m_value.script() != ui_simpleEdit->toPlainText())
        {
            m_value.setScript(ui_simpleEdit->toPlainText());
            emit(valueChanged(m_value));
        }
    }
}

void ScriptWidget::Private::focused(KTextEditor::View* view)
{
    KXmlGuiWindow* topLevel = qobject_cast<KXmlGuiWindow*>(KApplication::kApplication()->activeWindow());
    if (topLevel)
        topLevel->guiFactory()->addClient(view);
}

void ScriptWidget::Private::unfocused(KTextEditor::View* view)
{
    KXmlGuiWindow* topLevel = qobject_cast<KXmlGuiWindow*>(KApplication::kApplication()->activeWindow());
    QWidget* focused = KApplication::focusWidget();
    // FIXME: Does this fail with non-Kate KTextEditor implementations?
    if (topLevel && focused && ((focused->metaObject()->className() != QString("KateViewInternal")) || ((focused != view) && (QString(focused->metaObject()->className()) == view->metaObject()->className()))))
        topLevel->guiFactory()->removeClient(view);
}

void ScriptWidget::Private::setGui()
{
    ui_interpreter->setCurrentIndex(m_interpreters.indexOf(m_value.interpreter()));
    if (m_document)
    {
        m_document->setHighlightingMode(ui_interpreter->currentText());
        KTextEditor::Cursor cursor = ui_kteEdit->cursorPosition();
        m_document->setText(m_value.script());
        ui_kteEdit->setCursorPosition(cursor);
    }
    else
    {
        QTextCursor cursor = ui_simpleEdit->textCursor();
        ui_simpleEdit->setPlainText(m_value.script());
        ui_simpleEdit->setTextCursor(cursor);
    }
}
