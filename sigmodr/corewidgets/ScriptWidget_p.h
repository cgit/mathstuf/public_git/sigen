/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRCOREWIDGETS_SCRIPTWIDGET_P
#define SIGMODRCOREWIDGETS_SCRIPTWIDGET_P

// Header include
#include "ScriptWidget.h"

// Sigcore includes
#include <sigcore/Script.h>

// Qt includes
#include <QtCore/QMap>
#include <QtCore/QObject>

// Forward declarations
class KComboBox;
class KTextEdit;
namespace KTextEditor
{
class Document;
class Editor;
class View;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class SIGMODRCOREWIDGETS_NO_EXPORT ScriptWidget::Private : public QObject
{
    Q_OBJECT
    
    public:
        Private(QObject* parent, const Sigcore::Script& value);
        ~Private();
        
        QWidget* makeWidgets(ScriptWidget* widget);
        
        Sigcore::Script m_value;
    signals:
        void valueChanged(const Sigcore::Script& value);
    protected slots:
        void interpreterChanged(const QString& interpreter);
        void scriptChanged();
        
        void focused(KTextEditor::View* view);
        void unfocused(KTextEditor::View* view);
        void setGui();
    private:
        typedef QPair<QString, QString> LanguagePair;
        static QMap<QString, LanguagePair> m_languages;
        static QList<QString> m_interpreters;
        
        KComboBox* ui_interpreter;
        KTextEdit* ui_simpleEdit;
        KTextEditor::View* ui_kteEdit;
        KTextEditor::Editor* m_editor;
        KTextEditor::Document* m_document;
};
}
}

#endif
