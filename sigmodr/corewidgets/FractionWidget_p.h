/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRCOREWIDGETS_FRACTIONWIDGET_P
#define SIGMODRCOREWIDGETS_FRACTIONWIDGET_P

// Header include
#include "FractionWidget.h"

// Sigcore includes
#include <sigcore/Fraction.h>

// Qt includes
#include <QtCore/QObject>

// Forward declarations
class KIntNumInput;
class KLineEdit;

namespace Sigmodr
{
namespace CoreWidgets
{
class SIGMODRCOREWIDGETS_NO_EXPORT FractionWidget::Private : public QObject
{
    Q_OBJECT
    
    public:
        Private(QObject* parent, const Sigcore::Fraction& value);
        
        QWidget* makeWidgets(FractionWidget* widget);
        
        Sigcore::Fraction m_value;
        FractionWidget::Behavior m_behavior;
    signals:
        void valueChanged(const Sigcore::Fraction& value);
    protected slots:
        void numeratorChanged(const int numerator);
        void denominatorChanged(const int denominator);
        
        void setGui();
        void updateValue();
        void resetRanges();
    private:
        KIntNumInput* ui_numerator;
        KIntNumInput* ui_denominator;
        KLineEdit* ui_value;
};
}
}

#endif
