<chapter id="editing-sigmod">

<chapterinfo>

<authorgroup>
<author>
&Ben.Boeckel;
&Ben.Boeckel.mail;
</author>
<!-- TRANS:ROLES_OF_TRANSLATORS -->
</authorgroup>

<date>2009-08-08</date>

</chapterinfo>

<title>Sigmod</title>

<sect1 id="sigmod-description">

<title>Description</title>

<para>The top-level data structure keeps information about the &sigmod; for the
engine as well as kendling interconnections between the other data structures
that do not (or cannot) be handled elsewhere. The <link
linkend="sigmod-typechart">type effectiveness chart</link> controls how
effective types are against one another in battle and the <link
linkend="sigmod-worldmap">world map editor</link> describes how maps are
positioned in the world relative to each other.</para>

</sect1>

<sect1 id="sigmod-general">

<title>General Information</title>

<screenshot id="screen-sigmod-general">
<screeninfo>General tab in &sigmod; editing</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="editing-sigmod-general.png" format="PNG" />
</imageobject>
<textobject>
<phrase>General tab in &sigmod; editing</phrase>
</textobject>
</mediaobject>
</screenshot>

<para>In the <guilabel>General</guilabel> tab, there are the following
values:</para>

<variablelist>

<varlistentry>
<term>Title</term>
<listitem>
<para>The title of the &sigmod;. Anything after a <quote>:</quote> is understood
to be a subtitle, so that a single &sigmod; may have different flavors, &eg;
<userinput>Ice Cream: Vanilla Flavor</userinput> or <userinput>Ice Cream:
Chocolate Flavor</userinput>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Version</term>
<listitem>
<para>The version number of the &sigmod;. This is used to check whether the
&sigmod; is newer than another of the same <guilabel>Title</guilabel>.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Description</term>
<listitem>
<para>Short text describing the &sigmod;. A sentence or two. Don&apos;t stress
yourself out.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Single player</term>
<listitem>
<para>If checked, the &sigmod; will not be allowed to serve as a multi-user
dungeon game.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Start Script</term>
<listitem>
<para>This is a script that is executed upon the game loading. It should handle
a main menu as well as loading saved games and connecting to servers for online
games. Scripting is described in
<!-- TODO: Add available objects -->
<!-- TODO: Uncomment when link works
<link linkend="scripting">its Chapter</link>.</para>
 -->
its Chapter.</para>
</listitem>
</varlistentry>

</variablelist>

</sect1>

<sect1 id="sigmod-typechart">

<title>Typechart</title>

<screenshot id="screen-sigmod-typechart">
<screeninfo>Typechart editing</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="editing-sigmod-typechart.png" format="PNG" />
</imageobject>
<textobject>
<phrase>Typechart editing</phrase>
</textobject>
</mediaobject>
</screenshot>

<para>The typechart is a matrix of multipliers related to how effective one type
is against another. To interpret the typechart, the type that is on the offense
is on the left. Across the top is the defending type for each defense type. By
multiplying the factors together, the overall type effectiveness multipler is
found.</para>

<para>In the typechar, the cells are colored to help find particularly effective
or ineffective type matches. Above 2, the cell is colored
<quote>positively</quote> according to the color scheme and below &frac12;, the cell is
colored <quote>negatively</quote>.</para>

<para>In order to edit a multiplier, use the &LMB; to activate the appropriate
cell. The fraction editor at the bottom will then start editing that cell. The
fraction editor also displays the type matchup that is currently being edited
above it.</para>

</sect1>

<sect1 id="sigmod-worldmap">

<title>World Map</title>

<important>
<para>The world map editor is incomplete. This will become a higher priority
when the <acronym>RPG</acronym> elements of the game engine are further along.
</para>
</important>

<warning>
<para>Validation for world map placement is not in place yet. This means that
maps may have gaps between them and players can reach an edge which does not go
anywhere. All that is currently done is making sure that maps do not overlap
each other (though this is not 100&percnt; working either).</para>
</warning>

<screenshot id="screen-sigmod-worldmap">
<screeninfo>World map editing</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="editing-sigmod-worldmap.png" format="PNG" />
</imageobject>
<textobject>
<phrase>World map editing</phrase>
</textobject>
</mediaobject>
</screenshot>

<para>Editing the world map involves moving the maps that are marked as being on
the world map in positions relative to each other. There are a few things shown
in the screenshot above. The smaller boxes are maps (without any visible tiles).
Gray bordered maps may be moved around and red bordered maps are locked. Maps
can be locked and unlocked with <keycap>Space</keycap>. When hovered or focused,
maps will be tinted.</para>

<screenshot id="screen-sigmod-worldmap-grid">
<screeninfo>Grid viewing in world map editor</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="editing-sigmod-worldmap-grid.png" format="PNG" />
</imageobject>
<textobject>
<phrase>Grid viewing in world map editor</phrase>
</textobject>
</mediaobject>
</screenshot>

<para>In this screenshot, there are a couple of things displayed:</para>

<variablelist>

<varlistentry>
<term>Grid</term>
<listitem>
<para>The light gray lines are a grid for alignment. There is no snapping to the
grid lines.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Zoom</term>
<listitem>
<para>There is also a zoom slider for the world map so that a wider view of the
entire world can be seen.</para>
</listitem>
</varlistentry>

<varlistentry>
<term>Show walkable area</term>
<listitem>
<para>This turns the map into a monochrome image. Black areas are opaque to
players on the map. Other areas my have effects limiting player access, but
there is no map geometry obstructing the player.</para>
</listitem>
</varlistentry>

</variablelist>

<para>When moving maps around, they will collide with any maps that have been
locked into position. While dragging over an area that is completely locked
down, the editor will try to determine the closest available position to the
cursor. It currently is buggy and incomplete.</para>

</sect1>

</chapter>
