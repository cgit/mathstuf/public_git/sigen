<chapter id="editing-beginning">

<chapterinfo>

<authorgroup>
<author>
&Ben.Boeckel;
&Ben.Boeckel.mail;
</author>
<!-- TRANS:ROLES_OF_TRANSLATORS -->
</authorgroup>

<date>2009-08-08</date>

</chapterinfo>

<title>Beginning editing</title>

<caution>
<para>When editing, keep in mind that internal references are kept according to
internal ID numbers and that ID numbers are assigned to new objects as the
lowest available number. ID numbers are unique within each group of children of
a specific object. When adding and deleting objects, references may be changed
from what was originally intended. Using the <link linkend="validation">
validator</link> after each deletion can help to reduce errors made from such
automatic assignments.</para>
</caution>

<para>Editing in &sigmodr; is done in the
<link linkend="editor-panel">editor panel</link>. In order to edit an object,
click the <guibutton>Edit</guibutton> button in the <link linkend="sigmod-tree">
tree</link>.</para>

<sect1 id="editing-basics">

<title>Editing Basics</title>

<sect2 id="boundary-dialogs">

<title>Boundary Dialogs</title>

<para>The fields in the widget control the values for the object. Boundaries are
set so that invalid values may not be entered or selected. If the boundaries are
violated, a small dialog box will pop up describing the error. These should not
happen and indicate that the boundaries are incorrectly set. Please <link
linkend="reporting-bugs">submit a bug</link> if this occurs. Here is an example
of such a dialog box:

<screenshot id="screen-boundary">
<screeninfo>Invalid input dialog</screeninfo>
<mediaobject>
<imageobject>
<imagedata fileref="invalid-input-dialog.png" format="PNG" />
</imageobject>
<textobject>
<phrase>Invalid input dialog</phrase>
</textobject>
</mediaobject>
</screenshot>

This dialog shows that the expected range of values of <guilabel>sellPrice
</guilabel> is from &minus;1 to 0, but &sigmodr; attempted to set the value to
1. &sigmodr; will catch the error and the value will be reverted to the last
valid value.</para>

</sect2>

<sect2 id="data-integrity">

<title>Data Integrity</title>

<para>&sigmodr; does not currently feature <guibutton>Undo</guibutton> and
<guibutton>Redo</guibutton> support. It currently can only save the current
edited state (data that is shown in the <link linkend="editor-panel">editing
panel</link>), the last applied state of the object (restored with <guibutton>
Discard</guibutton>), and the on-disk file (restored when reloading the file and
written when saving). As such, data can easily be lost with an <guibutton>Apply
</guibutton> or a <guibutton>Discard</guibutton>.</para>

</sect2>

</sect1>

</chapter>
