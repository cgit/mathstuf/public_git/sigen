/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TreeWidget.h"

// Sigmodr tree includes
#include "TreeModel.h"

// Sigmodr widget includes
#include <sigmodr/widgets/ObjectUI.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Object.h>

// Qt includes
#include <QtGui/QHeaderView>

using namespace Sigmod;
using namespace Sigmodr::Widgets;
using namespace Sigmodr::Tree;

TreeWidget::TreeWidget(QWidget* parent) :
        QTreeView(parent)
{
    TreeModel* model = new TreeModel(this);
    setModel(model);
    setRootIndex(model->index(-1, -1, QModelIndex()));
    header()->setStretchLastSection(false);
    header()->setResizeMode(QHeaderView::ResizeToContents);
    header()->setResizeMode(0, QHeaderView::Stretch);
    header()->hide();
    setSelectionMode(SingleSelection);
    setSelectionBehavior(SelectRows);
}

QString TreeWidget::description(const QModelIndex& index) const
{
    return model()->data(model()->index(index.row(), 0, index.parent()), Qt::DisplayRole).toString();
}

QStringList TreeWidget::dropTypes(const QModelIndex& index) const
{
    return model()->data(index, TreeModel::AcceptedMimeTypesRole).toStringList();
}

ObjectUI* TreeWidget::editorWidget(const QModelIndex& index)
{
    ObjectUI* widget = qobject_cast<ObjectUI*>(model()->data(index, Qt::EditRole).value<QWidget*>());
    return widget;
}

// QDomDocument TreeWidget::copy(const QModelIndex& index)
// {
//     QDomDocument xml;
//     xml.setContent(model()->data(index, BaseModel::XmlRole).toString());
//     return xml;
// }
// 
// void TreeWidget::paste(const QModelIndex& index, const QDomDocument& data)
// {
//     model()->setData(index, data.toString(), BaseModel::XmlRole);
// }
// 
const Game* TreeWidget::game(const QModelIndex& index) const
{
    return qobject_cast<TreeModel*>(model())->findGame(index);
}

QList<const Game*> TreeWidget::openedGames() const
{
    return m_games.keys();
}

void TreeWidget::addGame(Game* game, const KUrl& url)
{
    qobject_cast<TreeModel*>(model())->addGame(game);
    m_games[game] = UrlDirty(url, false);
}

void TreeWidget::deleteGame(const Game* game)
{
    if (m_games.contains(game))
    {
        qobject_cast<TreeModel*>(model())->deleteGame(game);
        m_games.remove(game);
    }
}

void TreeWidget::deleteAllGames()
{
    const QList<const Game*> games = m_games.keys();
    foreach (const Game* game, games)
        deleteGame(game);
}

bool TreeWidget::isOpen(const KUrl& url) const
{
    if (url.isEmpty())
        return false;
    foreach (const UrlDirty& pair, m_games.values())
    {
        if (url == pair.first)
            return true;
    }
    return false;
}

KUrl TreeWidget::url(const Game* game) const
{
    if (m_games.contains(game))
        return m_games[game].first;
    return KUrl();
}

QStringList TreeWidget::urls() const
{
    QStringList urls;
    foreach (const UrlDirty& pair, m_games.values())
        urls << pair.first.prettyUrl();
    return urls;
}

void TreeWidget::setUrl(const Game* game, const KUrl& url)
{
    if (m_games.contains(game))
        m_games[game].first = url;
}

void TreeWidget::setDirty(const Game* game, const bool dirty)
{
    if (m_games.contains(game))
        m_games[game].second = dirty;
}

bool TreeWidget::dirty(const Game* game) const
{
    if (m_games.contains(game))
        return m_games[game].second;
    return false;
}
