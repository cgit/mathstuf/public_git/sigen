/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRTREE_TREEWIDGET
#define SIGMODRTREE_TREEWIDGET

// Sigmodr tree includes
#include "Global.h"

// KDE includes
#include <KUrl>

// Qt includes
#include <QtCore/QMap>
#include <QtGui/QTreeView>
#include <QtXml/QDomDocument>

// Forward declarations
namespace Sigmod
{
class Game;
}

namespace Sigmodr
{
namespace Widgets
{
class ObjectUI;
}

namespace Tree
{
class SIGMODRTREE_EXPORT TreeWidget : public QTreeView
{
    Q_OBJECT
    
    public:
        TreeWidget(QWidget* parent = 0);
        
        QString description(const QModelIndex& index) const;
        QStringList dropTypes(const QModelIndex& index) const;
        Widgets::ObjectUI* editorWidget(const QModelIndex& index);
        
//         QDomDocument copy(const QModelIndex& index);
//         void paste(const QModelIndex& index, const QDomDocument& data);
//         
        const Sigmod::Game* game(const QModelIndex& index) const;
        QList<const Sigmod::Game*> openedGames() const;
        void addGame(Sigmod::Game* game, const KUrl& url = KUrl());
        void deleteGame(const Sigmod::Game* game);
        void deleteAllGames();
        
        bool isOpen(const KUrl& url) const;
        
        KUrl url(const Sigmod::Game* game) const;
        QStringList urls() const;
        void setUrl(const Sigmod::Game* game, const KUrl& url);
        
        void setDirty(const Sigmod::Game* game, const bool dirty);
        bool dirty(const Sigmod::Game* game = NULL) const;
    private:
        typedef QPair<KUrl, bool> UrlDirty;
        
        QMap<const Sigmod::Game*, UrlDirty> m_games;
};
}
}

#endif
