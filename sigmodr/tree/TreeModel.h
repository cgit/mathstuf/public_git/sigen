/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRTREE_TREEMODEL
#define SIGMODRTREE_TREEMODEL

// Sigmodr tree includes
#include "Global.h"

// Qt includes
#include <QtCore/QAbstractItemModel>

// Forward declarations
namespace Sigmod
{
class Game;
}

namespace Sigmodr
{
namespace Tree
{
class RootTreeItem;

class SIGMODRTREE_NO_EXPORT TreeModel : public QAbstractItemModel
{
    Q_OBJECT
    
    public:
        enum
        {
            AcceptedMimeTypesRole = Qt::UserRole
        };
        
        TreeModel(QObject* parent);
        ~TreeModel();
        
        QVariant data(const QModelIndex& index, const int role) const;
        
        QModelIndex index(const int row, const int column, const QModelIndex& parent) const;
        QModelIndex parent(const QModelIndex& index) const;
        
        bool hasChildren(const QModelIndex& parent) const;
        
        bool insertRows(const int row, const int count, const QModelIndex& parent);
        bool removeRows(const int row, const int count, const QModelIndex& parent);
        
        int rowCount(const QModelIndex& parent = QModelIndex()) const;
        int columnCount(const QModelIndex& parent = QModelIndex()) const;
        
        Qt::ItemFlags flags(const QModelIndex& index) const;
        
        QStringList mimeTypes() const;
        QMimeData* mimeData(const QModelIndexList& indexes) const;
        bool dropMimeData(const QMimeData* data, Qt::DropAction action, const int row, const int column, const QModelIndex& parent);
        Qt::DropActions supportedDropActions() const;
        
        void addGame(Sigmod::Game* game);
        void deleteGame(const Sigmod::Game* game);
        const Sigmod::Game* findGame(const QModelIndex& index) const;
    private:
        RootTreeItem* m_root;
};
}
}

#endif
