/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRTREE_TREEITEM
#define SIGMODRTREE_TREEITEM

// Sigmodr tree includes
#include "Global.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtXml/QDomElement>

// Forward declarations
class QMimeData;
class KMenu;
namespace Sigmod
{
class Game;
class Object;
}

namespace Sigmodr
{
namespace Tree
{
class SIGMODRTREE_NO_EXPORT TreeItem : public QObject
{
    Q_OBJECT
    
    public:
        int row();
        TreeItem* parent();
        
        QString text() const;
        Qt::ItemFlags flags() const;
        
        bool isDirty() const;
        bool canEdit() const;
        bool canAddTo() const;
        bool canRemove() const;
        
        QWidget* editWidget(QWidget* parent = NULL);
        
        int childCount() const;
        TreeItem* addChild();
        TreeItem* childAt(const int child);
        void removeChild(const int child);
        
        const Sigmod::Game* game() const;
        
        QStringList acceptedMimeTypes() const;
        QString mimeType() const;
        QDomElement copyData() const;
        bool dropData(const QDomElement& element);
    signals:
        void update();
    protected:
        enum TreeItemType
        {
            GroupGame = 0,
            GroupAbility = 1,
            GroupAuthor = 2,
            GroupBadge = 3,
            GroupCoinList = 4,
            GroupEggGroup = 5,
            GroupGlobalScript = 6,
            GroupItem = 7,
            GroupItemType = 8,
            GroupMap = 9,
            GroupMapEffect = 10,
            GroupMapTrainer = 11,
            GroupMapWarp = 12,
            GroupMapWildList = 13,
            GroupMove = 14,
            GroupNature = 15,
            GroupSkin = 16,
            GroupSound = 17,
            GroupSpecies = 18,
            GroupSprite = 19,
            GroupStatus = 20,
            GroupStore = 21,
            GroupTile = 22,
            GroupTime = 23,
            GroupTrainer = 24,
            GroupType = 25,
            GroupWeather = 26,
            
            EntryGame = 100,
            EntryRules = 101,
            EntryAbility = 102,
            EntryAuthor = 103,
            EntryBadge = 104,
            EntryCoinList = 105,
            EntryCoinListItem = 106,
            EntryEggGroup = 107,
            EntryGlobalScript = 108,
            EntryItem = 109,
            EntryItemType = 110,
            EntryMap = 111,
            EntryMapEffect = 112,
            EntryMapTrainer = 113,
            EntryMapTrainerTeamMember = 114,
            EntryMapWarp = 115,
            EntryMapWildList = 116,
            EntryMapWildListEncounter = 117,
            EntryMove = 118,
            EntryNature = 119,
            EntrySkin = 120,
            EntrySound = 121,
            EntrySpecies = 122,
            EntrySpeciesMove = 123,
            EntrySprite = 124,
            EntryStatus = 125,
            EntryStore = 126,
            EntryTile = 127,
            EntryTime = 128,
            EntryTrainer = 129,
            EntryType = 130,
            EntryWeather = 131
        };
        
        TreeItem(const TreeItemType type, Sigmod::Object* object, TreeItem* parent);
        virtual ~TreeItem();
        
        TreeItem* newTreeItem(const TreeItemType type, Sigmod::Object* object, TreeItem* parent);
        
        const TreeItemType m_type;
        bool m_dirty;
        Sigmod::Object* m_object;
        TreeItem* m_parent;
        QList<TreeItem*> m_children;
    protected slots:
        void setDirty(const bool dirty);
};
}
}

#endif
