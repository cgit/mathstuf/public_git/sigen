/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TreeItem.h"

// Sigmodr widget includes
#include <sigmodr/widgets/AbilityUI.h>
#include <sigmodr/widgets/AuthorUI.h>
#include <sigmodr/widgets/BadgeUI.h>
#include <sigmodr/widgets/CoinListUI.h>
#include <sigmodr/widgets/CoinListItemUI.h>
#include <sigmodr/widgets/EggGroupUI.h>
#include <sigmodr/widgets/GameUI.h>
#include <sigmodr/widgets/GlobalScriptUI.h>
#include <sigmodr/widgets/ItemUI.h>
#include <sigmodr/widgets/ItemTypeUI.h>
#include <sigmodr/widgets/MapUI.h>
#include <sigmodr/widgets/MapEffectUI.h>
#include <sigmodr/widgets/MapTrainerUI.h>
#include <sigmodr/widgets/MapTrainerTeamMemberUI.h>
#include <sigmodr/widgets/MapWarpUI.h>
#include <sigmodr/widgets/MapWildListUI.h>
#include <sigmodr/widgets/MapWildListEncounterUI.h>
#include <sigmodr/widgets/MoveUI.h>
#include <sigmodr/widgets/NatureUI.h>
#include <sigmodr/widgets/RulesUI.h>
#include <sigmodr/widgets/SkinUI.h>
#include <sigmodr/widgets/SoundUI.h>
#include <sigmodr/widgets/SpeciesUI.h>
#include <sigmodr/widgets/SpeciesMoveUI.h>
#include <sigmodr/widgets/SpriteUI.h>
#include <sigmodr/widgets/StatusUI.h>
#include <sigmodr/widgets/StoreUI.h>
#include <sigmodr/widgets/TileUI.h>
#include <sigmodr/widgets/TimeUI.h>
#include <sigmodr/widgets/TrainerUI.h>
#include <sigmodr/widgets/TypeUI.h>
#include <sigmodr/widgets/WeatherUI.h>

// Sigmod includes
#include <sigmod/Ability.h>
#include <sigmod/Author.h>
#include <sigmod/Badge.h>
#include <sigmod/CoinList.h>
#include <sigmod/CoinListItem.h>
#include <sigmod/EggGroup.h>
#include <sigmod/Game.h>
#include <sigmod/GlobalScript.h>
#include <sigmod/Item.h>
#include <sigmod/ItemType.h>
#include <sigmod/Map.h>
#include <sigmod/MapEffect.h>
#include <sigmod/MapTrainer.h>
#include <sigmod/MapTrainerTeamMember.h>
#include <sigmod/MapWarp.h>
#include <sigmod/MapWildList.h>
#include <sigmod/MapWildListEncounter.h>
#include <sigmod/Move.h>
#include <sigmod/Nature.h>
#include <sigmod/Rules.h>
#include <sigmod/Skin.h>
#include <sigmod/Sound.h>
#include <sigmod/Species.h>
#include <sigmod/SpeciesMove.h>
#include <sigmod/Sprite.h>
#include <sigmod/Status.h>
#include <sigmod/Store.h>
#include <sigmod/Tile.h>
#include <sigmod/Time.h>
#include <sigmod/Trainer.h>
#include <sigmod/Type.h>
#include <sigmod/Weather.h>

using namespace Sigmod;
using namespace Sigmodr::Widgets;
using namespace Sigmodr::Tree;

TreeItem::TreeItem(const TreeItemType type, Object* object, TreeItem* parent) :
        QObject(parent),
        m_type(type),
        m_dirty(false),
        m_object(object),
        m_parent(parent)
{
    Game* game = qobject_cast<Game*>(m_object);
    CoinList* coinList = qobject_cast<CoinList*>(m_object);
    Map* map = qobject_cast<Map*>(m_object);
    MapTrainer* trainer = qobject_cast<MapTrainer*>(m_object);
    MapWildList* wildList = qobject_cast<MapWildList*>(m_object);
    Species* species = qobject_cast<Species*>(m_object);
    if (m_type == GroupAbility)
    {
        for (int i = 0; i < game->abilityCount(); ++i)
            m_children.append(new TreeItem(EntryAbility, game->ability(i), this));
    }
    if (m_type == GroupAuthor)
    {
        for (int i = 0; i < game->authorCount(); ++i)
            m_children.append(new TreeItem(EntryAuthor, game->author(i), this));
    }
    if (m_type == GroupBadge)
    {
        for (int i = 0; i < game->badgeCount(); ++i)
            m_children.append(new TreeItem(EntryBadge, game->badge(i), this));
    }
    if (m_type == GroupCoinList)
    {
        for (int i = 0; i < game->coinListCount(); ++i)
            m_children.append(new TreeItem(EntryCoinList, game->coinList(i), this));
    }
    if (m_type == GroupEggGroup)
    {
        for (int i = 0; i < game->eggGroupCount(); ++i)
            m_children.append(new TreeItem(EntryEggGroup, game->eggGroup(i), this));
    }
    if (m_type == GroupGlobalScript)
    {
        for (int i = 0; i < game->globalScriptCount(); ++i)
            m_children.append(new TreeItem(EntryGlobalScript, game->globalScript(i), this));
    }
    if (m_type == GroupItem)
    {
        for (int i = 0; i < game->itemCount(); ++i)
            m_children.append(new TreeItem(EntryItem, game->item(i), this));
    }
    if (m_type == GroupItemType)
    {
        for (int i = 0; i < game->itemTypeCount(); ++i)
            m_children.append(new TreeItem(EntryItemType, game->itemType(i), this));
    }
    if (m_type == GroupMap)
    {
        for (int i = 0; i < game->mapCount(); ++i)
            m_children.append(new TreeItem(EntryMap, game->map(i), this));
    }
    if (m_type == GroupMapEffect)
    {
        for (int i = 0; i < map->effectCount(); ++i)
            m_children.append(new TreeItem(EntryMapEffect, map->effect(i), this));
    }
    if (m_type == GroupMapTrainer)
    {
        for (int i = 0; i < map->trainerCount(); ++i)
            m_children.append(new TreeItem(EntryMapTrainer, map->trainer(i), this));
    }
    if (m_type == GroupMapWarp)
    {
        for (int i = 0; i < map->warpCount(); ++i)
            m_children.append(new TreeItem(EntryMapWarp, map->warp(i), this));
    }
    if (m_type == GroupMapWildList)
    {
        for (int i = 0; i < map->wildListCount(); ++i)
            m_children.append(new TreeItem(EntryMapWildList, map->wildList(i), this));
    }
    if (m_type == GroupMove)
    {
        for (int i = 0; i < game->moveCount(); ++i)
            m_children.append(new TreeItem(EntryMove, game->move(i), this));
    }
    if (m_type == GroupNature)
    {
        for (int i = 0; i < game->natureCount(); ++i)
            m_children.append(new TreeItem(EntryNature, game->nature(i), this));
    }
    if (m_type == GroupSkin)
    {
        for (int i = 0; i < game->skinCount(); ++i)
            m_children.append(new TreeItem(EntrySkin, game->skin(i), this));
    }
    if (m_type == GroupSound)
    {
        for (int i = 0; i < game->soundCount(); ++i)
            m_children.append(new TreeItem(EntrySound, game->sound(i), this));
    }
    if (m_type == GroupSpecies)
    {
        for (int i = 0; i < game->speciesCount(); ++i)
            m_children.append(new TreeItem(EntrySpecies, game->species(i), this));
    }
    if (m_type == GroupSprite)
    {
        for (int i = 0; i < game->spriteCount(); ++i)
            m_children.append(new TreeItem(EntrySprite, game->sprite(i), this));
    }
    if (m_type == GroupStatus)
    {
        for (int i = 0; i < game->statusCount(); ++i)
            m_children.append(new TreeItem(EntryStatus, game->status(i), this));
    }
    if (m_type == GroupStore)
    {
        for (int i = 0; i < game->storeCount(); ++i)
            m_children.append(new TreeItem(EntryStore, game->store(i), this));
    }
    if (m_type == GroupTile)
    {
        for (int i = 0; i < game->tileCount(); ++i)
            m_children.append(new TreeItem(EntryTile, game->tile(i), this));
    }
    if (m_type == GroupTime)
    {
        for (int i = 0; i < game->timeCount(); ++i)
            m_children.append(new TreeItem(EntryTime, game->time(i), this));
    }
    if (m_type == GroupTrainer)
    {
        for (int i = 0; i < game->trainerCount(); ++i)
            m_children.append(new TreeItem(EntryTrainer, game->trainer(i), this));
    }
    if (m_type == GroupType)
    {
        for (int i = 0; i < game->typeCount(); ++i)
            m_children.append(new TreeItem(EntryType, game->type(i), this));
    }
    if (m_type == GroupWeather)
    {
        for (int i = 0; i < game->weatherCount(); ++i)
            m_children.append(new TreeItem(EntryWeather, game->weather(i), this));
    }
    if (m_type == EntryGame)
    {
        m_children.append(new TreeItem(EntryRules, game->rules(), this));
        m_children.append(new TreeItem(GroupAbility, game, this));
        m_children.append(new TreeItem(GroupAuthor, game, this));
        m_children.append(new TreeItem(GroupBadge, game, this));
        m_children.append(new TreeItem(GroupCoinList, game, this));
        m_children.append(new TreeItem(GroupEggGroup, game, this));
        m_children.append(new TreeItem(GroupGlobalScript, game, this));
        m_children.append(new TreeItem(GroupItem, game, this));
        m_children.append(new TreeItem(GroupItemType, game, this));
        m_children.append(new TreeItem(GroupMap, game, this));
        m_children.append(new TreeItem(GroupMove, game, this));
        m_children.append(new TreeItem(GroupNature, game, this));
        m_children.append(new TreeItem(GroupSkin, game, this));
        m_children.append(new TreeItem(GroupSound, game, this));
        m_children.append(new TreeItem(GroupSpecies, game, this));
        m_children.append(new TreeItem(GroupSprite, game, this));
        m_children.append(new TreeItem(GroupStatus, game, this));
        m_children.append(new TreeItem(GroupStore, game, this));
        m_children.append(new TreeItem(GroupTile, game, this));
        m_children.append(new TreeItem(GroupTime, game, this));
        m_children.append(new TreeItem(GroupTrainer, game, this));
        m_children.append(new TreeItem(GroupType, game, this));
        m_children.append(new TreeItem(GroupWeather, game, this));
    }
    if (m_type == EntryCoinList)
    {
        for (int i = 0; i < coinList->itemCount(); ++i)
            m_children.append(new TreeItem(EntryCoinListItem, coinList->item(i), this));
    }
    if (m_type == EntryMap)
    {
        m_children.append(new TreeItem(GroupMapEffect, map, this));
        m_children.append(new TreeItem(GroupMapTrainer, map, this));
        m_children.append(new TreeItem(GroupMapWarp, map, this));
        m_children.append(new TreeItem(GroupMapWildList, map, this));
    }
    if (m_type == EntryMapTrainer)
    {
        for (int i = 0; i < trainer->teamMemberCount(); ++i)
            m_children.append(new TreeItem(EntryMapTrainerTeamMember, trainer->teamMember(i), this));
    }
    if (m_type == EntryMapWildList)
    {
        for (int i = 0; i < wildList->encounterCount(); ++i)
            m_children.append(new TreeItem(EntryMapWildListEncounter, wildList->encounter(i), this));
    }
    if (m_type == EntrySpecies)
    {
        for (int i = 0; i < species->moveCount(); ++i)
            m_children.append(new TreeItem(EntrySpeciesMove, species->move(i), this));
    }
}

TreeItem::~TreeItem()
{
    foreach (TreeItem* item, m_children)
        delete item;
}

TreeItem* TreeItem::newTreeItem(const TreeItemType type, Sigmod::Object* object, TreeItem* parent)
{
    return new TreeItem(type, object, parent);
}

int TreeItem::row()
{
    if (m_parent)
        return m_parent->m_children.indexOf(this);
    return 0;
}

TreeItem* TreeItem::parent()
{
    return m_parent;
}

QString TreeItem::text() const
{
    QString name;
    switch (m_type)
    {
        case GroupGame:
            name = "Games";
            break;
        case GroupAbility:
            name = "Abilities";
            break;
        case GroupAuthor:
            name = "Authors";
            break;
        case GroupBadge:
            name = "Badges";
            break;
        case GroupCoinList:
            name = "Coin Lists";
            break;
        case GroupEggGroup:
            name = "Egg Groups";
            break;
        case GroupGlobalScript:
            name = "Global Scripts";
            break;
        case GroupItem:
            name = "Items";
            break;
        case GroupItemType:
            name = "Item Types";
            break;
        case GroupMap:
            name = "Maps";
            break;
        case GroupMapEffect:
            name = "Effects";
            break;
        case GroupMapTrainer:
            name = "Trainers";
            break;
        case GroupMapWarp:
            name = "Warps";
            break;
        case GroupMapWildList:
            name = "Wild Lists";
            break;
        case GroupMove:
            name = "Moves";
            break;
        case GroupNature:
            name = "Natures";
            break;
        case GroupSkin:
            name = "Skins";
            break;
        case GroupSound:
            name = "Sounds";
            break;
        case GroupSpecies:
            name = "Species";
            break;
        case GroupSprite:
            name = "Sprites";
            break;
        case GroupStatus:
            name = "Status Effects";
            break;
        case GroupStore:
            name = "Stores";
            break;
        case GroupTile:
            name = "Tiles";
            break;
        case GroupTime:
            name = "Times";
            break;
        case GroupTrainer:
            name = "Trainers";
            break;
        case GroupType:
            name = "Types";
            break;
        case GroupWeather:
            name = "Weathers";
            break;
        case EntryGame:
            name = qobject_cast<Game*>(m_object)->title();
            break;
        case EntryRules:
            name = "Rules";
            break;
        case EntryAbility:
            name = qobject_cast<Ability*>(m_object)->name();
            break;
        case EntryAuthor:
            name = qobject_cast<Author*>(m_object)->name();
            break;
        case EntryBadge:
            name = qobject_cast<Badge*>(m_object)->name();
            break;
        case EntryCoinList:
            name = qobject_cast<CoinList*>(m_object)->name();
            break;
        case EntryCoinListItem:
        {
            CoinListItem* coinListItem = qobject_cast<CoinListItem*>(m_object);
            if (coinListItem->type() == CoinListItem::Item)
            {
                const Item* item = m_object->game()->itemById(coinListItem->object());
                name = item ? item->name() : QString::fromUtf8("«unknown»");
            }
            else if (coinListItem->type() == CoinListItem::Species)
            {
                const Species* species = m_object->game()->speciesById(coinListItem->object());
                name = species ? species->name() : QString::fromUtf8("«unknown»");
            }
            else
                name = QString::fromUtf8("«unknown»");
            break;
        }
        case EntryEggGroup:
            name = qobject_cast<EggGroup*>(m_object)->name();
            break;
        case EntryGlobalScript:
            name = qobject_cast<GlobalScript*>(m_object)->name();
            break;
        case EntryItem:
            name = qobject_cast<Item*>(m_object)->name();
            break;
        case EntryItemType:
            name = qobject_cast<ItemType*>(m_object)->name();
            break;
        case EntryMap:
            name = qobject_cast<Map*>(m_object)->name();
            break;
        case EntryMapEffect:
            name = qobject_cast<MapEffect*>(m_object)->name();
            break;
        case EntryMapTrainer:
        {
            const Trainer* trainer = m_object->game()->trainerById(qobject_cast<MapTrainer*>(m_object)->trainerClass());
            name = trainer ? trainer->name() : QString::fromUtf8("«unknown»");
            break;
        }
        case EntryMapTrainerTeamMember:
        {
            MapTrainerTeamMember* member = qobject_cast<MapTrainerTeamMember*>(m_object);
            const Species* species = m_object->game()->speciesById(member->species());
            name = QString("%1 L%2%3").arg(species ? species->name() : QString::fromUtf8("«unknown»")).arg(member->level());
            if (static_cast<const MapTrainer*>(member->parent())->leadTeamMember().contains(member->id()))
                name.append(" (leading)");
            break;
        }
        case EntryMapWarp:
            name = qobject_cast<MapWarp*>(m_object)->name();
            break;
        case EntryMapWildList:
            name = qobject_cast<MapWildList*>(m_object)->name();
            break;
        case EntryMapWildListEncounter:
        {
            MapWildListEncounter* encounter = qobject_cast<MapWildListEncounter*>(m_object);
            const Species* species = m_object->game()->speciesById(encounter->species());
            name = QString("%1 L%2 (weight: %3)").arg(species ? species->name() : QString::fromUtf8("«unknown»")).arg(encounter->level()).arg(encounter->weight());
            break;
        }
        case EntryMove:
            name = qobject_cast<Move*>(m_object)->name();
            break;
        case EntryNature:
            name = qobject_cast<Nature*>(m_object)->name();
            break;
        case EntrySkin:
            name = qobject_cast<Skin*>(m_object)->name();
            break;
        case EntrySound:
            name = qobject_cast<Sound*>(m_object)->name();
            break;
        case EntrySpecies:
            name = qobject_cast<Species*>(m_object)->name();
            break;
        case EntrySpeciesMove:
        {
            SpeciesMove* speciesMove = qobject_cast<SpeciesMove*>(m_object);
            const Move* move = m_object->game()->moveById(speciesMove->move());
            name = QString("%1 L%2").arg(move ? move->name() : QString::fromUtf8("«unknown»")).arg(speciesMove->level());
            if (speciesMove->wild() != speciesMove->level())
                name.append(QString(" (wild: L%1)").arg(speciesMove->wild()));
            break;
        }
        case EntrySprite:
            name = qobject_cast<Sprite*>(m_object)->name();
            break;
        case EntryStatus:
            name = qobject_cast<Status*>(m_object)->name();
            break;
        case EntryStore:
            name = qobject_cast<Store*>(m_object)->name();
            break;
        case EntryTile:
            name = qobject_cast<Tile*>(m_object)->name();
            break;
        case EntryTime:
            name = qobject_cast<Time*>(m_object)->name();
            break;
        case EntryTrainer:
            name = qobject_cast<Trainer*>(m_object)->name();
            break;
        case EntryType:
            name = qobject_cast<Type*>(m_object)->name();
            break;
        case EntryWeather:
            name = qobject_cast<Weather*>(m_object)->name();
            break;
    }
    if (name.isEmpty())
        name = QString::fromUtf8("«unnamed»");
    else if (isDirty())
        name.append('*');
    return name;
}

Qt::ItemFlags TreeItem::flags() const
{
    Qt::ItemFlags f = Qt::ItemIsDropEnabled | Qt::ItemIsEnabled | Qt::ItemIsSelectable;
    if (canEdit())
        f |= Qt::ItemIsDragEnabled;
    return f;
}

bool TreeItem::isDirty() const
{
    return m_dirty;
}

bool TreeItem::canEdit() const
{
    return (EntryGame <= m_type);
}

bool TreeItem::canAddTo() const
{
    return ((m_type < EntryGame) || (m_type == EntryCoinList) || (m_type == EntryMapTrainer) || (m_type == EntryMapWildList) || (m_type == EntrySpecies));
}

bool TreeItem::canRemove() const
{
    return (EntryRules < m_type);
}

QWidget* TreeItem::editWidget(QWidget* parent)
{
    ObjectUI* editorWidget = NULL;
    switch (m_type)
    {
        case EntryGame:
            editorWidget = new GameUI(qobject_cast<Game*>(m_object), parent);
            break;
        case EntryRules:
            editorWidget = new RulesUI(qobject_cast<Rules*>(m_object), parent);
            break;
        case EntryAbility:
            editorWidget = new AbilityUI(qobject_cast<Ability*>(m_object), parent);
            break;
        case EntryAuthor:
            editorWidget = new AuthorUI(qobject_cast<Author*>(m_object), parent);
            break;
        case EntryBadge:
            editorWidget = new BadgeUI(qobject_cast<Badge*>(m_object), parent);
            break;
        case EntryCoinList:
            editorWidget = new CoinListUI(qobject_cast<CoinList*>(m_object), parent);
            break;
        case EntryCoinListItem:
            editorWidget = new CoinListItemUI(qobject_cast<CoinListItem*>(m_object), parent);
            break;
        case EntryEggGroup:
            editorWidget = new EggGroupUI(qobject_cast<EggGroup*>(m_object), parent);
            break;
        case EntryGlobalScript:
            editorWidget = new GlobalScriptUI(qobject_cast<GlobalScript*>(m_object), parent);
            break;
        case EntryItem:
            editorWidget = new ItemUI(qobject_cast<Item*>(m_object), parent);
            break;
        case EntryItemType:
            editorWidget = new ItemTypeUI(qobject_cast<ItemType*>(m_object), parent);
            break;
        case EntryMap:
            editorWidget = new MapUI(qobject_cast<Map*>(m_object), parent);
            break;
        case EntryMapEffect:
            editorWidget = new MapEffectUI(qobject_cast<MapEffect*>(m_object), parent);
            break;
        case EntryMapTrainer:
            editorWidget = new MapTrainerUI(qobject_cast<MapTrainer*>(m_object), parent);
            break;
        case EntryMapTrainerTeamMember:
            editorWidget = new MapTrainerTeamMemberUI(qobject_cast<MapTrainerTeamMember*>(m_object), parent);
            break;
        case EntryMapWarp:
            editorWidget = new MapWarpUI(qobject_cast<MapWarp*>(m_object), parent);
            break;
        case EntryMapWildList:
            editorWidget = new MapWildListUI(qobject_cast<MapWildList*>(m_object), parent);
            break;
        case EntryMapWildListEncounter:
            editorWidget = new MapWildListEncounterUI(qobject_cast<MapWildListEncounter*>(m_object), parent);
            break;
        case EntryMove:
            editorWidget = new MoveUI(qobject_cast<Move*>(m_object), parent);
            break;
        case EntryNature:
            editorWidget = new NatureUI(qobject_cast<Nature*>(m_object), parent);
            break;
        case EntrySkin:
            editorWidget = new SkinUI(qobject_cast<Skin*>(m_object), parent);
            break;
        case EntrySound:
            editorWidget = new SoundUI(qobject_cast<Sound*>(m_object), parent);
            break;
        case EntrySpecies:
            editorWidget = new SpeciesUI(qobject_cast<Species*>(m_object), parent);
            break;
        case EntrySpeciesMove:
            editorWidget = new SpeciesMoveUI(qobject_cast<SpeciesMove*>(m_object), parent);
            break;
        case EntrySprite:
            editorWidget = new SpriteUI(qobject_cast<Sprite*>(m_object), parent);
            break;
        case EntryStatus:
            editorWidget = new StatusUI(qobject_cast<Status*>(m_object), parent);
            break;
        case EntryStore:
            editorWidget = new StoreUI(qobject_cast<Store*>(m_object), parent);
            break;
        case EntryTile:
            editorWidget = new TileUI(qobject_cast<Tile*>(m_object), parent);
            break;
        case EntryTime:
            editorWidget = new TimeUI(qobject_cast<Time*>(m_object), parent);
            break;
        case EntryTrainer:
            editorWidget = new TrainerUI(qobject_cast<Trainer*>(m_object), parent);
            break;
        case EntryType:
            editorWidget = new TypeUI(qobject_cast<Type*>(m_object), parent);
            break;
        case EntryWeather:
            editorWidget = new WeatherUI(qobject_cast<Weather*>(m_object), parent);
            break;
        default:
            break;
    }
    if (editorWidget)
    {
        connect(editorWidget, SIGNAL(changed(bool)), this, SLOT(setDirty(bool)));
        return editorWidget;
    }
    return NULL;
}

int TreeItem::childCount() const
{
    return m_children.size();
}

TreeItem* TreeItem::addChild()
{
    TreeItem* item = NULL;
    switch (m_type)
    {
        case GroupGame:
            item = new TreeItem(EntryGame, new Game, this);
            break;
        case GroupAbility:
            item = new TreeItem(EntryAbility, qobject_cast<Game*>(m_object)->newAbility(), this);
            break;
        case GroupAuthor:
            item = new TreeItem(EntryAuthor, qobject_cast<Game*>(m_object)->newAuthor(), this);
            break;
        case GroupBadge:
            item = new TreeItem(EntryBadge, qobject_cast<Game*>(m_object)->newBadge(), this);
            break;
        case GroupCoinList:
            item = new TreeItem(EntryCoinList, qobject_cast<Game*>(m_object)->newCoinList(), this);
            break;
        case GroupEggGroup:
            item = new TreeItem(EntryEggGroup, qobject_cast<Game*>(m_object)->newEggGroup(), this);
            break;
        case GroupGlobalScript:
            item = new TreeItem(EntryGlobalScript, qobject_cast<Game*>(m_object)->newGlobalScript(), this);
            break;
        case GroupItem:
            item = new TreeItem(EntryItem, qobject_cast<Game*>(m_object)->newItem(), this);
            break;
        case GroupItemType:
            item = new TreeItem(EntryItemType, qobject_cast<Game*>(m_object)->newItemType(), this);
            break;
        case GroupMap:
            item = new TreeItem(EntryMap, qobject_cast<Game*>(m_object)->newMap(), this);
            break;
        case GroupMapEffect:
            item = new TreeItem(EntryMapEffect, qobject_cast<Map*>(m_object)->newEffect(), this);
            break;
        case GroupMapTrainer:
            item = new TreeItem(EntryMapTrainer, qobject_cast<Map*>(m_object)->newTrainer(), this);
            break;
        case GroupMapWarp:
            item = new TreeItem(EntryMapWarp, qobject_cast<Map*>(m_object)->newWarp(), this);
            break;
        case GroupMapWildList:
            item = new TreeItem(EntryMapWildList, qobject_cast<Map*>(m_object)->newWildList(), this);
            break;
        case GroupMove:
            item = new TreeItem(EntryMove, qobject_cast<Game*>(m_object)->newMove(), this);
            break;
        case GroupNature:
            item = new TreeItem(EntryNature, qobject_cast<Game*>(m_object)->newNature(), this);
            break;
        case GroupSkin:
            item = new TreeItem(EntrySkin, qobject_cast<Game*>(m_object)->newSkin(), this);
            break;
        case GroupSound:
            item = new TreeItem(EntrySound, qobject_cast<Game*>(m_object)->newSound(), this);
            break;
        case GroupSpecies:
            item = new TreeItem(EntrySpecies, qobject_cast<Game*>(m_object)->newSpecies(), this);
            break;
        case GroupSprite:
            item = new TreeItem(EntrySprite, qobject_cast<Game*>(m_object)->newSprite(), this);
            break;
        case GroupStatus:
            item = new TreeItem(EntryStatus, qobject_cast<Game*>(m_object)->newStatus(), this);
            break;
        case GroupStore:
            item = new TreeItem(EntryStore, qobject_cast<Game*>(m_object)->newStore(), this);
            break;
        case GroupTile:
            item = new TreeItem(EntryTile, qobject_cast<Game*>(m_object)->newTile(), this);
            break;
        case GroupTime:
            item = new TreeItem(EntryTime, qobject_cast<Game*>(m_object)->newTime(), this);
            break;
        case GroupTrainer:
            item = new TreeItem(EntryTrainer, qobject_cast<Game*>(m_object)->newTrainer(), this);
            break;
        case GroupType:
            item = new TreeItem(EntryType, qobject_cast<Game*>(m_object)->newType(), this);
            break;
        case GroupWeather:
            item = new TreeItem(EntryWeather, qobject_cast<Game*>(m_object)->newWeather(), this);
            break;
        case EntryCoinList:
            item = new TreeItem(EntryCoinListItem, qobject_cast<CoinList*>(m_object)->newItem(), this);
            break;
        case EntryMapTrainer:
            item = new TreeItem(EntryMapTrainerTeamMember, qobject_cast<MapTrainer*>(m_object)->newTeamMember(), this);
            break;
        case EntryMapWildList:
            item = new TreeItem(EntryMapWildListEncounter, qobject_cast<MapWildList*>(m_object)->newEncounter(), this);
            break;
        case EntrySpecies:
            item = new TreeItem(EntrySpeciesMove, qobject_cast<Species*>(m_object)->newMove(), this);
            break;
        default:
            break;
    }
    if (item)
        m_children.append(item);
    return item;
}

TreeItem* TreeItem::childAt(const int child)
{
    if ((child < 0) || (m_children.size() <= child))
        return NULL;
    return m_children[child];
}

void TreeItem::removeChild(const int child)
{
    if ((child < 0) || (m_children.size() <= child))
        return;
    switch (m_type)
    {
        case GroupAbility:
            qobject_cast<Game*>(m_object)->deleteAbility(child);
            break;
        case GroupAuthor:
            qobject_cast<Game*>(m_object)->deleteAuthor(child);
            break;
        case GroupBadge:
            qobject_cast<Game*>(m_object)->deleteBadge(child);
            break;
        case GroupCoinList:
            qobject_cast<Game*>(m_object)->deleteCoinList(child);
            break;
        case GroupEggGroup:
            qobject_cast<Game*>(m_object)->deleteEggGroup(child);
            break;
        case GroupGlobalScript:
            qobject_cast<Game*>(m_object)->deleteGlobalScript(child);
            break;
        case GroupItem:
            qobject_cast<Game*>(m_object)->deleteItem(child);
            break;
        case GroupItemType:
            qobject_cast<Game*>(m_object)->deleteItemType(child);
            break;
        case GroupMap:
            qobject_cast<Game*>(m_object)->deleteMap(child);
            break;
        case GroupMapEffect:
            qobject_cast<Map*>(m_object)->deleteEffect(child);
            break;
        case GroupMapTrainer:
            qobject_cast<Map*>(m_object)->deleteTrainer(child);
            break;
        case GroupMapWarp:
            qobject_cast<Map*>(m_object)->deleteWarp(child);
            break;
        case GroupMapWildList:
            qobject_cast<Map*>(m_object)->deleteWildList(child);
            break;
        case GroupMove:
            qobject_cast<Game*>(m_object)->deleteMove(child);
            break;
        case GroupNature:
            qobject_cast<Game*>(m_object)->deleteNature(child);
            break;
        case GroupSkin:
            qobject_cast<Game*>(m_object)->deleteSkin(child);
            break;
        case GroupSound:
            qobject_cast<Game*>(m_object)->deleteSound(child);
            break;
        case GroupSpecies:
            qobject_cast<Game*>(m_object)->deleteSpecies(child);
            break;
        case GroupSprite:
            qobject_cast<Game*>(m_object)->deleteSprite(child);
            break;
        case GroupStatus:
            qobject_cast<Game*>(m_object)->deleteStatus(child);
            break;
        case GroupStore:
            qobject_cast<Game*>(m_object)->deleteStore(child);
            break;
        case GroupTile:
            qobject_cast<Game*>(m_object)->deleteTile(child);
            break;
        case GroupTime:
            qobject_cast<Game*>(m_object)->deleteTime(child);
            break;
        case GroupTrainer:
            qobject_cast<Game*>(m_object)->deleteTrainer(child);
            break;
        case GroupType:
            qobject_cast<Game*>(m_object)->deleteType(child);
            break;
        case GroupWeather:
            qobject_cast<Game*>(m_object)->deleteWeather(child);
            break;
        case EntryCoinList:
            qobject_cast<CoinList*>(m_object)->deleteItem(child);
            break;
        case EntryMapTrainer:
            qobject_cast<MapTrainer*>(m_object)->deleteTeamMember(child);
            break;
        case EntryMapWildList:
            qobject_cast<MapWildList*>(m_object)->deleteEncounter(child);
            break;
        case EntrySpecies:
            qobject_cast<Species*>(m_object)->deleteMove(child);
            break;
        default:
            break;
    }
    delete m_children[child];
    m_children.removeAt(child);
}

const Game* TreeItem::game() const
{
    if (m_object)
        return m_object->game();
    return NULL;
}

QStringList TreeItem::acceptedMimeTypes() const
{
    // TODO
    return QStringList();
}

QString TreeItem::mimeType() const
{
    // TODO
    return QString();
}

QDomElement TreeItem::copyData() const
{
    if (canEdit())
        return m_object->save();
    return QDomElement();
}

bool TreeItem::dropData(const QDomElement& element)
{
    // TODO
    return false;
}

void TreeItem::setDirty(const bool dirty)
{
    if (m_dirty != dirty)
    {
        emit(update());
        m_dirty = dirty;
    }
}
