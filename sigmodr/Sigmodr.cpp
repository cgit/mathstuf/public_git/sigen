/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Sigmodr includes
#include "SigmodrUI.h"

// KDE includes
#include <KAboutData>
#include <KApplication>
#include <KCmdLineArgs>

// Standard includes
#include <cstdio>
#include <cstdlib>

using namespace std;
using namespace Sigmodr;

static void messageHandler(QtMsgType type, const char* message)
{
    switch (type)
    {
        case QtDebugMsg:
            fprintf(stderr, "Debug: %s\n", message);
            break;
        case QtWarningMsg:
            fprintf(stderr, "Warning: %s\n", message);
            break;
        case QtCriticalMsg:
            fprintf(stderr, "Critical: %s\n", message);
            break;
        case QtFatalMsg:
            fprintf(stderr, "Fatal: %s\n", message);
            abort();
    }
}

int main(int argc, char* argv[])
{
    qInstallMsgHandler(messageHandler);
    srand48(time(NULL));
    
    // TODO
    KAboutData about("sigmodr", "sigmodr", ki18n("Sigmodr"), "0.1.1", ki18n(""), KAboutData::License_GPL_V3, ki18n("©2007-2009 Ben Boeckel"), ki18n("This program offers an easy interface so that Sigmods can be easily created."), "");
    about.setProgramName(ki18n("Sigmodr"));
    about.addAuthor(ki18n("Ben Boeckel"), ki18n("Lead Programmer"), "MathStuf@gmail.com", "http://benboeckel.net/blog");
    about.addCredit(ki18n("Peter Fernandes"), ki18n("Ideas"), "supersonicandtails@gmail.com", "http://www.hypersonicsoft.org");
    about.addCredit(ki18n("Kevin Kofler"), ki18n("Qt, KDE, debugging help"), "kevin.kofler@chello.at", "http://www.tigen.org/kevin.kofler");
    about.addCredit(ki18n("Luke Greco"), ki18n("Ideas, Tester"), "sirlewk@gmail.com", "");
    // TODO
    about.setBugAddress("");
    about.setCopyrightStatement(ki18n("©2007-2009, Ben Boeckel"));
    // TODO
    about.setOrganizationDomain("");
    // TODO: Need one of these...
//     about.setProgramLogo();
    // TODO: And some of these...
    about.setTranslator(ki18nc("NAME OF TRANSLATORS", "Your names"), ki18nc("EMAIL OF TRANSLATORS", "Your emails"));
    
    KCmdLineArgs::init(argc, argv, &about);
    KCmdLineOptions options;
    options.add("+[files]", ki18n("Files to open"));
    KCmdLineArgs::addCmdLineOptions(options);
    
    KApplication* app = new KApplication;
    
    SigmodrUI* mainWindow = new SigmodrUI;
    mainWindow->show();
    int result = app->exec();
    
    delete app;
    return result;
}
