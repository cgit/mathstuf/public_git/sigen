/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpeciesMoveUI.h"
#include "SpeciesMoveUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Move.h>
#include <sigmod/Rules.h>
#include <sigmod/SpeciesMove.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

SpeciesMoveUI::SpeciesMoveUI(SpeciesMove* move, QWidget* parent) :
        ObjectUI(move, parent),
        d(new Private(new SpeciesMove(*move)))
{
    setPrivate(d);
}

void SpeciesMoveUI::apply()
{
    *qobject_cast<SpeciesMove*>(m_object) = *d->m_move;
    ObjectUI::apply();
}

void SpeciesMoveUI::discard()
{
    *d->m_move = *qobject_cast<SpeciesMove*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

SpeciesMoveUI::Private::Private(SpeciesMove* move) :
        ObjectUIPrivate(move),
        m_move(move)
{
}

SpeciesMoveUI::Private::~Private()
{
    delete m_move;
}

QWidget* SpeciesMoveUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/speciesmove.ui", widget);
    ui_move = form->findChild<KComboBox*>("varMove");
    ui_level = form->findChild<KIntNumInput*>("varLevel");
    ui_wildLevel = form->findChild<KIntNumInput*>("varWildLevel");
    connect(ui_move, SIGNAL(currentIndexChanged(int)), this, SLOT(moveChanged(int)));
    connect(ui_level, SIGNAL(valueChanged(int)), this, SLOT(levelChanged(int)));
    connect(ui_wildLevel, SIGNAL(valueChanged(int)), this, SLOT(wildLevelChanged(int)));
    return form;
}

void SpeciesMoveUI::Private::refreshGui()
{
    const bool blocked = ui_move->blockSignals(true);
    ui_move->clear();
    for (int i = 0; i < m_move->game()->moveCount(); ++i)
        ui_move->addItem(m_move->game()->move(i)->name());
    ui_move->blockSignals(blocked);
    ui_level->setMaximum(m_move->game()->rules()->maxLevel());
    ui_wildLevel->setMaximum(m_move->game()->rules()->maxLevel());
    ObjectUIPrivate::refreshGui();
}

void SpeciesMoveUI::Private::resetGui()
{
    ui_move->setCurrentIndex(m_move->game()->moveIndex(m_move->move()));
    ui_level->setValue(m_move->level());
    ui_wildLevel->setValue(m_move->wild());
}

void SpeciesMoveUI::Private::moveChanged(const int move)
{
    if (0 <= move)
        m_move->setMove(m_move->game()->move(move)->id());
}

void SpeciesMoveUI::Private::levelChanged(const int level)
{
    m_move->setLevel(level);
}

void SpeciesMoveUI::Private::wildLevelChanged(const int wildLevel)
{
    m_move->setWild(wildLevel);
}
