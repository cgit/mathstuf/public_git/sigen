/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TileUI.h"
#include "TileUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Tile.h>
#include <sigmod/Sprite.h>

// KDE includes
#include <KComboBox>
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

TileUI::TileUI(Tile* tile, QWidget* parent) :
        ObjectUI(tile, parent),
        d(new Private(new Tile(*tile)))
{
    setPrivate(d);
}

void TileUI::apply()
{
    *qobject_cast<Tile*>(m_object) = *d->m_tile;
    ObjectUI::apply();
}

void TileUI::discard()
{
    *d->m_tile = *qobject_cast<Tile*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

TileUI::Private::Private(Tile* tile) :
        ObjectUIPrivate(tile),
        m_tile(tile)
{
}

TileUI::Private::~Private()
{
    delete m_tile;
}

QWidget* TileUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/tile.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_walkable = form->findChild<QCheckBox*>("varWalkable");
    ui_preview = form->findChild<KComboBox*>("varPreview");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_walkable, SIGNAL(toggled(bool)), this, SLOT(walkableChanged(bool)));
    connect(ui_preview, SIGNAL(currentIndexChanged(int)), this, SLOT(previewChanged(int)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void TileUI::Private::refreshGui()
{
    int maxHeight = 0;
    int maxWidth = 0;
    const bool blockedPreview = ui_preview->blockSignals(true);
    ui_preview->clear();
    for (int i = 0; i < m_tile->game()->spriteCount(); ++i)
    {
        const Sprite* sprite = m_tile->game()->sprite(i);
        QPixmap icon;
        icon.loadFromData(sprite->sprite());
        maxHeight = qMax(maxHeight, icon.height());
        maxWidth = qMax(maxWidth, icon.width());
        ui_preview->addItem(icon, sprite->name());
    }
    ui_preview->blockSignals(blockedPreview);
    const QSize maxSize(maxWidth, maxHeight);
    ui_preview->setIconSize(maxSize);
    ObjectUIPrivate::refreshGui();
}

void TileUI::Private::resetGui()
{
    ui_name->setText(m_tile->name());
    ui_walkable->setChecked(m_tile->walkable());
    ui_preview->setCurrentIndex(m_tile->preview());
    ui_script->setValue(m_tile->script());
}

void TileUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_tile->setName(name);
    ui_name->setCursorPosition(cursor);
}

void TileUI::Private::walkableChanged(const bool walkable)
{
    m_tile->setWalkable(walkable);
}

void TileUI::Private::previewChanged(const int preview)
{
    m_tile->setPreview(preview);
}

void TileUI::Private::scriptChanged(const Script& script)
{
    m_tile->setScript(script);
}
