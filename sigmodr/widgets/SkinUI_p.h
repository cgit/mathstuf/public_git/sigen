/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_SKINUI_P
#define SIGMODRWIDGETS_SKINUI_P

// Header include
#include "SkinUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KIntNumInput;
class KLineEdit;
namespace Sigcore
{
class Script;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class ScriptWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT SkinUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Skin* skin);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Skin* m_skin;
    public slots:
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void widthChanged(const int width);
        void heightChanged(const int height);
        void scriptChanged(const Sigcore::Script& script);
    private:
        KLineEdit* ui_name;
        KIntNumInput* ui_width;
        KIntNumInput* ui_height;
        CoreWidgets::ScriptWidget* ui_script;
};
}
}

#endif
