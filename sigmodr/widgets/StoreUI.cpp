/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "StoreUI.h"
#include "StoreUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/Store.h>

// KDE includes
#include <KActionSelector>
#include <KLineEdit>

// Qt includes
#include <QtGui/QListWidgetItem>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

StoreUI::StoreUI(Store* store, QWidget* parent) :
        ObjectUI(store, parent),
        d(new Private(new Store(*store)))
{
    setPrivate(d);
}

void StoreUI::apply()
{
    *qobject_cast<Store*>(m_object) = *d->m_store;
    ObjectUI::apply();
}

void StoreUI::discard()
{
    *d->m_store = *qobject_cast<Store*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

StoreUI::Private::Private(Store* store) :
        ObjectUIPrivate(store),
        m_store(store)
{
}

StoreUI::Private::~Private()
{
    delete m_store;
}

QWidget* StoreUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/store.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_items = form->findChild<KActionSelector*>("varItems");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_items, SIGNAL(added(QListWidgetItem*)), this, SLOT(itemAdded(QListWidgetItem*)));
    connect(ui_items, SIGNAL(removed(QListWidgetItem*)), this, SLOT(itemRemoved(QListWidgetItem*)));
    return form;
}

void StoreUI::Private::refreshGui()
{
    const bool blockedItems = ui_items->blockSignals(true);
    ui_items->availableListWidget()->clear();
    ui_items->selectedListWidget()->clear();
    for (int i = 0; i < m_store->game()->itemCount(); ++i)
    {
        const Item* item = m_store->game()->item(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(item->name(), ui_items->availableListWidget());
        widgetItem->setData(Qt::UserRole, item->id());
    }
    ui_items->blockSignals(blockedItems);
    ui_items->setButtonsEnabled();
    ObjectUIPrivate::refreshGui();
}

void StoreUI::Private::resetGui()
{
    ui_name->setText(m_store->name());
    for (int i = 0; i < ui_items->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->availableListWidget()->item(i);
        if (m_store->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->selectedListWidget()->addItem(ui_items->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_items->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->selectedListWidget()->item(i);
        if (!m_store->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->availableListWidget()->addItem(ui_items->selectedListWidget()->takeItem(i--));
    }
    ui_items->setButtonsEnabled();
}

void StoreUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_store->setName(name);
    ui_name->setCursorPosition(cursor);
}

void StoreUI::Private::itemAdded(QListWidgetItem* item)
{
    m_store->setItem(item->data(Qt::UserRole).toInt(), true);
}

void StoreUI::Private::itemRemoved(QListWidgetItem* item)
{
    m_store->setItem(item->data(Qt::UserRole).toInt(), false);
}
