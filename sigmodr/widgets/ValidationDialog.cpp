/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ValidationDialog.h"

// Sigmod includes
#include <sigmod/Object.h>

// KDE includes
#include <KColorScheme>
#include <KMessageBox>
#include <KProgressDialog>

// Qt includes
#include <QtGui/QLabel>
#include <QtGui/QTreeWidget>
#include <QtGui/QVBoxLayout>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

ValidationDialog::ValidationDialog(Object* object, QWidget* parent) :
        QWidget(parent),
        m_processed(false),
        m_object(object),
        m_valTree(NULL)
{
    connect(m_object, SIGNAL(changed()), this, SLOT(objectChanged()));
}

int ValidationDialog::errors() const
{
    return m_errors;
}

int ValidationDialog::warnings() const
{
    return m_warnings;
}

void ValidationDialog::process()
{
    if (!m_processed)
    {
        m_errors = 0;
        m_warnings = 0;
        m_valTree = new QTreeWidget;
        m_valTree->setHeaderHidden(true);
        m_parents.clear();
        m_parents.push(ObjectErrorCount(m_valTree->invisibleRootItem(), 0));
        KProgressDialog* progress = new KProgressDialog(this, "Validating", "Please wait");
        progress->progressBar()->setRange(0, 0);
        progress->setAllowCancel(false);
        progress->setModal(true);
        progress->show();
        connect(m_object, SIGNAL(valMessage(QString)), this, SLOT(addMessage(QString)));
        connect(m_object, SIGNAL(valWarning(QString)), this, SLOT(addWarning(QString)));
        connect(m_object, SIGNAL(valError(QString)), this, SLOT(addError(QString)));
        m_object->validate();
        disconnect(m_object, SIGNAL(valMessage(QString)), this, SLOT(addMessage(QString)));
        disconnect(m_object, SIGNAL(valWarning(QString)), this, SLOT(addWarning(QString)));
        disconnect(m_object, SIGNAL(valError(QString)), this, SLOT(addError(QString)));
        delete progress;
        m_valTree->addTopLevelItem(m_parents.top().first);
    }
    m_processed = true;
}

void ValidationDialog::show()
{
    if (!m_processed)
        process();
    if (m_parents.top().second)
    {
        KDialog* dialog = new KDialog(this);
        dialog->setCaption("Validation Messages");
        dialog->setButtons(KDialog::Ok);
        QWidget* widget = new QWidget;
        QVBoxLayout* layout = new QVBoxLayout;
        layout->addWidget(new QLabel(QString("Warnings: %1\nErrors: %2").arg(m_warnings).arg(m_errors)));
        layout->addWidget(m_valTree);
        widget->setLayout(layout);
        dialog->setMainWidget(widget);
        dialog->exec();
        delete dialog;
        m_processed = false;
    }
    else
        KMessageBox::information(this, "No messages", "Validation");
}

void ValidationDialog::insertMessage(const QString& msg, const QBrush& brush)
{
    ++m_parents.top().second;
    QTreeWidgetItem* item = new QTreeWidgetItem(m_parents.top().first, QStringList(msg));
    item->setBackground(0, brush);
}

void ValidationDialog::addMessage(const QString& msg)
{
    if (msg.startsWith(QString("++")))
        m_parents.push(ObjectErrorCount(new QTreeWidgetItem(QStringList(msg.mid(2))), 0));
    else if (msg.startsWith(QString("--")))
    {
        ObjectErrorCount count = m_parents.pop();
        if (count.second)
        {
            m_parents.top().first->addChild(count.first);
            ++m_parents.top().second;
        }
    }
    else
        insertMessage(msg, KStatefulBrush(KColorScheme::View, KColorScheme::PositiveBackground).brush(QPalette::Normal));
}

void ValidationDialog::addError(const QString& msg)
{
    insertMessage(msg, KStatefulBrush(KColorScheme::View, KColorScheme::NegativeBackground).brush(QPalette::Normal));
    ++m_errors;
}

void ValidationDialog::addWarning(const QString& msg)
{
    insertMessage(msg, KStatefulBrush(KColorScheme::View, KColorScheme::NeutralBackground).brush(QPalette::Normal));
    ++m_warnings;
}

void ValidationDialog::objectChanged()
{
    m_processed = false;
}
