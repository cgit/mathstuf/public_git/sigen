/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapEffectUI.h"
#include "MapEffectUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/MapEffect.h>
#include <sigmod/Skin.h>

// KDE includes
#include <KComboBox>
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

MapEffectUI::MapEffectUI(MapEffect* effect, QWidget* parent) :
        ObjectUI(effect, parent),
        d(new Private(new MapEffect(*effect)))
{
    setPrivate(d);
}

void MapEffectUI::apply()
{
    *qobject_cast<MapEffect*>(m_object) = *d->m_effect;
    ObjectUI::apply();
}

void MapEffectUI::discard()
{
    *d->m_effect = *qobject_cast<MapEffect*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapEffectUI::Private::Private(MapEffect* effect) :
        ObjectUIPrivate(effect),
        m_effect(effect)
{
}

MapEffectUI::Private::~Private()
{
    delete m_effect;
}

QWidget* MapEffectUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/mapeffect.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_hasSkin = form->findChild<QCheckBox*>("varHasSkin");
    ui_skin = form->findChild<KComboBox*>("varSkin");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    ui_isGhost = form->findChild<QCheckBox*>("varIsGhost");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_hasSkin, SIGNAL(toggled(bool)), this, SLOT(hasSkinChanged(bool)));
    connect(ui_skin, SIGNAL(currentIndexChanged(int)), this, SLOT(skinChanged(int)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    connect(ui_isGhost, SIGNAL(toggled(bool)), this, SLOT(isGhostChanged(bool)));
    ui_skin->setEnabled(0 <= m_effect->skin());
    return form;
}

void MapEffectUI::Private::refreshGui()
{
    const bool blocked = ui_skin->blockSignals(true);
    ui_skin->clear();
    for (int i = 0; i < m_effect->game()->skinCount(); ++i)
        ui_skin->addItem(m_effect->game()->skin(i)->name());
    ui_skin->blockSignals(blocked);
    ObjectUIPrivate::refreshGui();
}

void MapEffectUI::Private::resetGui()
{
    ui_name->setText(m_effect->name());
    ui_hasSkin->setChecked(m_effect->skin() == -1);
    ui_skin->setEnabled(m_effect->skin() != -1);
    ui_skin->setCurrentIndex(m_effect->game()->skinIndex(m_effect->skin()));
    ui_script->setValue(m_effect->script());
    ui_isGhost->setCheckState(m_effect->isGhost() ? Qt::Checked : Qt::Unchecked);
}

void MapEffectUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_effect->setName(name);
    ui_name->setCursorPosition(cursor);
}

void MapEffectUI::Private::hasSkinChanged(const bool hasSkin)
{
    ui_skin->setEnabled(hasSkin);
    if (!hasSkin)
        m_effect->setSkin(-1);
}

void MapEffectUI::Private::skinChanged(const int skin)
{
    if (0 <= skin)
        m_effect->setSkin(m_effect->game()->skin(skin)->id());
}

void MapEffectUI::Private::scriptChanged(const Script& script)
{
    m_effect->setScript(script);
}

void MapEffectUI::Private::isGhostChanged(const bool isGhost)
{
    m_effect->setIsGhost(isGhost);
}
