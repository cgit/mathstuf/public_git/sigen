/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ObjectUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"
#include "ValidationDialog.h"

// Sigmod includes
#include <sigmod/Object.h>

// KDE includes
#include <KAction>
#include <KMenu>

// KDE includes
#include <QtGui/QVBoxLayout>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

ObjectUI::ObjectUI(Object* object, QWidget* parent) :
        QWidget(parent),
        m_object(object),
        m_validator(NULL)
{
    setContextMenuPolicy(Qt::CustomContextMenu);
    connect(this, SIGNAL(changed(bool)), SLOT(setChanged(bool)));
    emit(changed(false));
}

ObjectUI::~ObjectUI()
{
}

const Object* ObjectUI::object() const
{
    return m_object;
}

Object* ObjectUI::object()
{
    return m_object;
}

void ObjectUI::apply()
{
    emit(changed(false));
    emit(saved());
}

void ObjectUI::discard()
{
    emit(changed(false));
}

void ObjectUI::validate()
{
    if (m_changed)
        apply();
    if (m_validator)
        delete m_validator;
    m_validator = new ValidationDialog(m_object, this);
    m_validator->show();
}

void ObjectUI::setPrivate(ObjectUIPrivate* priv)
{
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(priv->makeWidgets(this));
    setLayout(layout);
    priv->refreshGui();
}

void ObjectUI::setChanged(const bool changed)
{
    m_changed = changed;
}
