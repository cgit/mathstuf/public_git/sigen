/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_SKINUI
#define SIGMODRWIDGETS_SKINUI

// Sigmodr widget includes
#include "ObjectUI.h"

// Forward declarations
namespace Sigmod
{
class Skin;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_EXPORT SkinUI : public ObjectUI
{
    Q_OBJECT
    
    public:
        SkinUI(Sigmod::Skin* skin, QWidget* parent);
    public slots:
        void apply();
        void discard();
    private:
        class Private;
        Private* const d;
};
}
}

#endif
