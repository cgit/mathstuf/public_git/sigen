/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPUI_P
#define SIGMODRWIDGETS_MAPUI_P

// Header include
#include "MapUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class QCheckBox;
class KComboBox;
class KLineEdit;

namespace Sigmodr
{
namespace Widgets
{
class MapEditor;

class SIGMODRWIDGETS_NO_EXPORT MapUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Map* map);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Map* m_map;
    public slots:
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void isWorldChanged(const bool isWorld);
    private:
        KLineEdit* ui_name;
        QCheckBox* ui_isWorld;
        MapEditor* ui_editor;
};
}
}

#endif
