/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "RulesUI.h"
#include "RulesUI_p.h"

// Sigmod includes
#include <sigmod/Rules.h>

// KDE includes
#include <KIntNumInput>
#include <KLocalizedString>

// Qt includes
#include <QtGui/QCheckBox>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

RulesUI::RulesUI(Rules* rules, QWidget* parent) :
        ObjectUI(rules, parent),
        d(new Private(new Rules(*rules)))
{
    setPrivate(d);
}

void RulesUI::apply()
{
    *qobject_cast<Rules*>(m_object) = *d->m_rules;
    ObjectUI::apply();
}

void RulesUI::discard()
{
    *d->m_rules = *qobject_cast<Rules*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

RulesUI::Private::Private(Rules* rules) :
        ObjectUIPrivate(rules),
        m_rules(rules)
{
}

RulesUI::Private::~Private()
{
    delete m_rules;
}

QWidget* RulesUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/rules.ui", widget);
    ui_genders = form->findChild<QCheckBox*>("varGenders");
    ui_breeding = form->findChild<QCheckBox*>("varBreeding");
    ui_criticalDomains = form->findChild<QCheckBox*>("varCriticalDomains");
    ui_splitSpecial = form->findChild<QCheckBox*>("varSplitSpecial");
    ui_splitSpecialDV = form->findChild<QCheckBox*>("varSplitSpecialDV");
    ui_maxEV = form->findChild<KIntNumInput*>("varMaxEV");
    ui_maxEVPerStat = form->findChild<KIntNumInput*>("varMaxEVPerStat");
    ui_boxes = form->findChild<KIntNumInput*>("varBoxes");
    ui_boxSize = form->findChild<KIntNumInput*>("varBoxSize");
    ui_maxParty = form->findChild<KIntNumInput*>("varMaxParty");
    ui_maxFight = form->findChild<KIntNumInput*>("varMaxFight");
    ui_maxPlayers = form->findChild<KIntNumInput*>("varMaxPlayers");
    ui_maxMoves = form->findChild<KIntNumInput*>("varMaxMoves");
    ui_maxLevel = form->findChild<KIntNumInput*>("varMaxLevel");
    ui_maxHeldItems = form->findChild<KIntNumInput*>("varMaxHeldItems");
    ui_maxNatures = form->findChild<KIntNumInput*>("varMaxNatures");
    ui_maxAbilities = form->findChild<KIntNumInput*>("varMaxAbilities");
    ui_maxStages = form->findChild<KIntNumInput*>("varMaxStages");
    ui_maxMoney = form->findChild<KIntNumInput*>("varMaxMoney");
    ui_maxTotalWeight = form->findChild<KIntNumInput*>("varMaxTotalWeight");
    connect(ui_genders, SIGNAL(toggled(bool)), this, SLOT(gendersChanged(bool)));
    connect(ui_breeding, SIGNAL(toggled(bool)), this, SLOT(breedingChanged(bool)));
    connect(ui_criticalDomains, SIGNAL(toggled(bool)), this, SLOT(criticalDomainsChanged(bool)));
    connect(ui_splitSpecial, SIGNAL(toggled(bool)), this, SLOT(splitSpecialChanged(bool)));
    connect(ui_splitSpecialDV, SIGNAL(toggled(bool)), this, SLOT(splitSpecialDVChanged(bool)));
    connect(ui_maxEV, SIGNAL(valueChanged(int)), this, SLOT(maxEVChanged(int)));
    connect(ui_maxEVPerStat, SIGNAL(valueChanged(int)), this, SLOT(maxEVPerStatChanged(int)));
    connect(ui_boxes, SIGNAL(valueChanged(int)), this, SLOT(boxesChanged(int)));
    connect(ui_boxSize, SIGNAL(valueChanged(int)), this, SLOT(boxSizeChanged(int)));
    connect(ui_maxParty, SIGNAL(valueChanged(int)), this, SLOT(maxPartyChanged(int)));
    connect(ui_maxFight, SIGNAL(valueChanged(int)), this, SLOT(maxFightChanged(int)));
    connect(ui_maxPlayers, SIGNAL(valueChanged(int)), this, SLOT(maxPlayersChanged(int)));
    connect(ui_maxMoves, SIGNAL(valueChanged(int)), this, SLOT(maxMovesChanged(int)));
    connect(ui_maxLevel, SIGNAL(valueChanged(int)), this, SLOT(maxLevelChanged(int)));
    connect(ui_maxHeldItems, SIGNAL(valueChanged(int)), this, SLOT(maxHeldItemsChanged(int)));
    connect(ui_maxNatures, SIGNAL(valueChanged(int)), this, SLOT(maxNaturesChanged(int)));
    connect(ui_maxAbilities, SIGNAL(valueChanged(int)), this, SLOT(maxAbilitiesChanged(int)));
    connect(ui_maxStages, SIGNAL(valueChanged(int)), this, SLOT(maxStagesChanged(int)));
    connect(ui_maxMoney, SIGNAL(valueChanged(int)), this, SLOT(maxMoneyChanged(int)));
    connect(ui_maxTotalWeight, SIGNAL(valueChanged(int)), this, SLOT(maxTotalWeightChanged(int)));
    ui_breeding->setEnabled(m_rules->genderAllowed());
    ui_splitSpecialDV->setEnabled(m_rules->specialSplit());
    ui_maxEVPerStat->setMaximum(m_rules->maxTotalEV());
    ui_maxEVPerStat->setEnabled(m_rules->maxTotalEV());
    ui_boxSize->setEnabled(m_rules->numBoxes());
    ui_maxFight->setMaximum(m_rules->maxParty());
    return form;
}

void RulesUI::Private::resetGui()
{
    ui_genders->setCheckState(m_rules->genderAllowed() ? Qt::Checked : Qt::Unchecked);
    ui_breeding->setCheckState(m_rules->breedingAllowed() ? Qt::Checked : Qt::Unchecked);
    ui_criticalDomains->setCheckState(m_rules->criticalDomains() ? Qt::Checked : Qt::Unchecked);
    ui_splitSpecial->setCheckState(m_rules->specialSplit() ? Qt::Checked : Qt::Unchecked);
    ui_splitSpecialDV->setCheckState(m_rules->specialDVSplit() ? Qt::Checked : Qt::Unchecked);
    ui_maxEV->setValue(m_rules->maxTotalEV());
    ui_maxEVPerStat->setValue(m_rules->maxEVPerStat());
    ui_boxes->setValue(m_rules->numBoxes());
    ui_boxSize->setValue(m_rules->boxSize());
    ui_maxParty->setValue(m_rules->maxParty());
    ui_maxFight->setValue(m_rules->maxFight());
    ui_maxPlayers->setValue(m_rules->maxPlayers());
    ui_maxMoves->setValue(m_rules->maxMoves());
    ui_maxLevel->setValue(m_rules->maxLevel());
    ui_maxHeldItems->setValue(m_rules->maxHeldItems());
    ui_maxNatures->setValue(m_rules->maxNatures());
    ui_maxAbilities->setValue(m_rules->maxAbilities());
    ui_maxStages->setValue(m_rules->maxStages());
    ui_maxMoney->setValue(m_rules->maxMoney());
    ui_maxTotalWeight->setValue(m_rules->maxTotalWeight());
}

void RulesUI::Private::gendersChanged(const bool genders)
{
    m_rules->setGenderAllowed(genders);
    ui_breeding->setEnabled(genders);
}

void RulesUI::Private::breedingChanged(const bool breeding)
{
    m_rules->setBreedingAllowed(breeding);
}

void RulesUI::Private::criticalDomainsChanged(const bool criticalDomains)
{
    m_rules->setCriticalDomains(criticalDomains);
}

void RulesUI::Private::splitSpecialChanged(const bool splitSpecial)
{
    m_rules->setSpecialSplit(splitSpecial);
    ui_splitSpecialDV->setEnabled(splitSpecial);
}

void RulesUI::Private::splitSpecialDVChanged(const bool splitSpecialDV)
{
    m_rules->setSpecialDVSplit(splitSpecialDV);
}

void RulesUI::Private::maxEVChanged(const int maxEV)
{
    m_rules->setMaxTotalEV(maxEV);
    ui_maxEVPerStat->setMaximum(maxEV);
    ui_maxEVPerStat->setEnabled(maxEV);
    if (maxEV < 0)
    {
        ui_maxEVPerStat->setMinimum(0);
        ui_maxEVPerStat->setSpecialValueText(i18n("No limit"));
    }
    else
    {
        ui_maxEVPerStat->setMinimum(1);
        ui_maxEVPerStat->setSpecialValueText(QString());
    }
}

void RulesUI::Private::maxEVPerStatChanged(const int maxEVPerStat)
{
    m_rules->setMaxEVPerStat(maxEVPerStat);
}

void RulesUI::Private::boxesChanged(const int boxes)
{
    m_rules->setNumBoxes(boxes);
    ui_boxSize->setEnabled(boxes);
}

void RulesUI::Private::boxSizeChanged(const int boxSize)
{
    m_rules->setBoxSize(boxSize);
}

void RulesUI::Private::maxPartyChanged(const int maxParty)
{
    m_rules->setMaxParty(maxParty);
    ui_maxFight->setMaximum(maxParty);
    ui_maxFight->setEnabled(0 < maxParty);
}

void RulesUI::Private::maxFightChanged(const int maxFight)
{
    m_rules->setMaxFight(maxFight);
}

void RulesUI::Private::maxPlayersChanged(const int maxPlayers)
{
    m_rules->setMaxPlayers(maxPlayers);
}

void RulesUI::Private::maxMovesChanged(const int maxMoves)
{
    m_rules->setMaxMoves(maxMoves);
}

void RulesUI::Private::maxLevelChanged(const int maxLevel)
{
    m_rules->setMaxLevel(maxLevel);
}

void RulesUI::Private::maxHeldItemsChanged(const int maxHeldItems)
{
    m_rules->setMaxHeldItems(maxHeldItems);
}

void RulesUI::Private::maxNaturesChanged(const int maxNatures)
{
    m_rules->setMaxNatures(maxNatures);
}

void RulesUI::Private::maxAbilitiesChanged(const int maxAbilities)
{
    m_rules->setMaxAbilities(maxAbilities);
}

void RulesUI::Private::maxStagesChanged(const int maxStages)
{
    m_rules->setMaxStages(maxStages);
}

void RulesUI::Private::maxMoneyChanged(const int maxMoney)
{
    m_rules->setMaxMoney(maxMoney);
}

void RulesUI::Private::maxTotalWeightChanged(const int maxTotalWeight)
{
    m_rules->setMaxTotalWeight(maxTotalWeight);
}
