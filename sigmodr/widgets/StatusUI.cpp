/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "StatusUI.h"
#include "StatusUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Status.h>

// KDE includes
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

StatusUI::StatusUI(Status* status, QWidget* parent) :
        ObjectUI(status, parent),
        d(new Private(new Status(*status)))
{
    setPrivate(d);
}

void StatusUI::apply()
{
    *qobject_cast<Status*>(m_object) = *d->m_status;
    ObjectUI::apply();
}

void StatusUI::discard()
{
    *d->m_status = *qobject_cast<Status*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

StatusUI::Private::Private(Status* status) :
        ObjectUIPrivate(status),
        m_status(status)
{
}

StatusUI::Private::~Private()
{
    delete m_status;
}

QWidget* StatusUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/status.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_battleScript = form->findChild<ScriptWidget*>("varBattleScript");
    ui_worldScript = form->findChild<ScriptWidget*>("varWorldScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_battleScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(battleScriptChanged(Sigcore::Script)));
    connect(ui_worldScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(worldScriptChanged(Sigcore::Script)));
    return form;
}

void StatusUI::Private::resetGui()
{
    ui_name->setText(m_status->name());
    ui_battleScript->setValue(m_status->battleScript());
    ui_worldScript->setValue(m_status->worldScript());
}

void StatusUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_status->setName(name);
    ui_name->setCursorPosition(cursor);
}

void StatusUI::Private::battleScriptChanged(const Script& battleScript)
{
    m_status->setBattleScript(battleScript);
}

void StatusUI::Private::worldScriptChanged(const Script& worldScript)
{
    m_status->setWorldScript(worldScript);
}
