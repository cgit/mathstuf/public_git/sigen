/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ObjectUIPrivate.h"

// Sigmodr widget includes
#include "ObjectUI.h"

// Sigmod includes
#include <sigmod/Object.h>

// KDE includes
#include <KDialog>
#include <KMessageBox>

// Qt includes
#include <QtCore/QFile>
#include <QtUiTools/QUiLoader>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

ObjectUIPrivate::ObjectUIPrivate(Sigmod::Object* object) :
        QObject(NULL)
{
    connect(object, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(object, SIGNAL(error(QString)), this, SLOT(resetGui()));
    connect(object, SIGNAL(error(QString)), this, SLOT(errorMessage(QString)));
    connect(object, SIGNAL(warning(QString)), this, SLOT(warningMessage(QString)));
}

ObjectUIPrivate::~ObjectUIPrivate()
{
}

void ObjectUIPrivate::refreshGui()
{
    resetGui();
}

QWidget* ObjectUIPrivate::openUiFile(const QString& filename, ObjectUI* parent)
{
    makeConnections(parent);
    QFile file(filename);
    file.open(QFile::ReadOnly);
    QWidget *formWidget = QUiLoader().load(&file, parent);
    file.close();
    return formWidget;
}

void ObjectUIPrivate::errorMessage(const QString& message)
{
    KMessageBox::error(NULL, QString("Oops! The developers forgot something! Please report the error to them and it will be addressed in the following release.\n%1").arg(message), "Error");
}

void ObjectUIPrivate::warningMessage(const QString& message)
{
    KMessageBox::warningContinueCancel(NULL, QString("Oops. Something isn\'t quite right. Please report the problem and it will be addressed in the following release.\n%1").arg(message), "Warning");
}

void ObjectUIPrivate::makeConnections(ObjectUI* widget)
{
    connect(this, SIGNAL(changed()), widget, SIGNAL(changed()));
}
