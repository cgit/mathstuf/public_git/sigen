/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_TYPEUI_P
#define SIGMODRWIDGETS_TYPEUI_P

// Header include
#include "TypeUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KLineEdit;
namespace Sigcore
{
class Fraction;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT TypeUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Type* type);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Type* m_type;
    public slots:
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void stabChanged(const Sigcore::Fraction& stab);
    private:
        KLineEdit* ui_name;
        CoreWidgets::FractionWidget* ui_stab;
};
}
}

#endif
