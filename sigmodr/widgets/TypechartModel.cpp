/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TypechartModel.h"

// KDE includes
#include <KColorScheme>

using namespace Sigcore;
using namespace Sigmodr::Widgets;

TypechartModel::TypechartModel(Matrix<Fraction>* typechart, const QStringList& types) :
        QAbstractTableModel(),
        m_typechart(*typechart),
        m_types(types)
{
}

QVariant TypechartModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();
    if (role == Qt::DisplayRole)
        return QString::number(m_typechart(index.row(), index.column()), 'g', 3);
    else if (role == Qt::EditRole)
        return QVariant::fromValue(m_typechart(index.row(), index.column()));
    else if (role == Qt::BackgroundRole)
    {
        double multiplier = m_typechart(index.row(), index.column());
        if (multiplier <= .5)
            return KStatefulBrush(KColorScheme::View, KColorScheme::NegativeBackground).brush(QPalette::Normal);
        else if (multiplier < 2.)
            return KStatefulBrush(KColorScheme::View, KColorScheme::NeutralBackground).brush(QPalette::Normal);
        else
            return KStatefulBrush(KColorScheme::View, KColorScheme::PositiveBackground).brush(QPalette::Normal);
    }
    return QVariant();
}

QVariant TypechartModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    Q_UNUSED(orientation)
    if (role == Qt::DisplayRole)
        return m_types.at(section);
    return QVariant();
}

int TypechartModel::rowCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_typechart.height();
}

int TypechartModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent)
    return m_typechart.width();
}

Qt::ItemFlags TypechartModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return 0;
    return Qt::ItemIsEnabled | Qt::ItemIsSelectable;
}

bool TypechartModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
    if (!index.isValid())
        return false;
    if (role == Qt::EditRole)
    {
        if (value.canConvert<Fraction>())
        {
            m_typechart(index.row(), index.column()) = value.value<Fraction>();
            emit(dataChanged(index, index));
            return true;
        }
    }
    return false;
}

void TypechartModel::discarded()
{
    emit(reset());
}
