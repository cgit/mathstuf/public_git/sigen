/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "CoinListUI.h"
#include "CoinListUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/CoinList.h>
#include <sigmod/Game.h>

// KDE includes
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

CoinListUI::CoinListUI(CoinList* coinList, QWidget* parent) :
        ObjectUI(coinList, parent),
        d(new Private(new CoinList(*coinList)))
{
    setPrivate(d);
}

void CoinListUI::apply()
{
    *qobject_cast<CoinList*>(m_object) = *d->m_coinList;
    ObjectUI::apply();
}

void CoinListUI::discard()
{
    *d->m_coinList = *qobject_cast<CoinList*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

CoinListUI::Private::Private(CoinList* coinList) :
        ObjectUIPrivate(coinList),
        m_coinList(coinList)
{
}

CoinListUI::Private::~Private()
{
    delete m_coinList;
}

QWidget* CoinListUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/coinlist.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void CoinListUI::Private::resetGui()
{
    ui_name->setText(m_coinList->name());
    ui_script->setValue(m_coinList->script());
}

void CoinListUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_coinList->setName(name);
    ui_name->setCursorPosition(cursor);
}

void CoinListUI::Private::scriptChanged(const Script& script)
{
    m_coinList->setScript(script);
}
