/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TrainerUI.h"
#include "TrainerUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Rules.h>
#include <sigmod/Skin.h>
#include <sigmod/Trainer.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

TrainerUI::TrainerUI(Trainer* trainer, QWidget* parent) :
        ObjectUI(trainer, parent),
        d(new Private(new Trainer(*trainer)))
{
    setPrivate(d);
}

void TrainerUI::apply()
{
    *qobject_cast<Trainer*>(m_object) = *d->m_trainer;
    ObjectUI::apply();
}

void TrainerUI::discard()
{
    *d->m_trainer = *qobject_cast<Trainer*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

TrainerUI::Private::Private(Trainer* trainer) :
        ObjectUIPrivate(trainer),
        m_trainer(trainer)
{
}

TrainerUI::Private::~Private()
{
    delete m_trainer;
}

QWidget* TrainerUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/trainer.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_moneyFactor = form->findChild<KIntNumInput*>("varMoneyFactor");
    ui_skin = form->findChild<KComboBox*>("varSkin");
    ui_depth = form->findChild<KIntNumInput*>("varDepth");
    ui_teamIntel = form->findChild<KComboBox*>("varTeamIntel");
    ui_moveIntel = form->findChild<KComboBox*>("varMoveIntel");
    ui_itemIntel = form->findChild<KComboBox*>("varItemIntel");
    ui_abilityIntel = form->findChild<KComboBox*>("varAbilityIntel");
    ui_statIntel = form->findChild<KComboBox*>("varStatIntel");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_moneyFactor, SIGNAL(valueChanged(int)), this, SLOT(moneyFactorChanged(int)));
    connect(ui_skin, SIGNAL(currentIndexChanged(int)), this, SLOT(skinChanged(int)));
    connect(ui_depth, SIGNAL(valueChanged(int)), this, SLOT(depthChanged(int)));
    connect(ui_teamIntel, SIGNAL(currentIndexChanged(int)), this, SLOT(teamIntelChanged(int)));
    connect(ui_moveIntel, SIGNAL(currentIndexChanged(int)), this, SLOT(moveIntelChanged(int)));
    connect(ui_itemIntel, SIGNAL(currentIndexChanged(int)), this, SLOT(itemIntelChanged(int)));
    connect(ui_abilityIntel, SIGNAL(currentIndexChanged(int)), this, SLOT(abilityIntelChanged(int)));
    connect(ui_statIntel, SIGNAL(currentIndexChanged(int)), this, SLOT(statIntelChanged(int)));
    ui_teamIntel->addItems(Trainer::IntelligenceStr);
    ui_moveIntel->addItems(Trainer::IntelligenceStr);
    ui_itemIntel->addItems(Trainer::IntelligenceStr);
    ui_abilityIntel->addItems(Trainer::IntelligenceStr);
    ui_statIntel->addItems(Trainer::IntelligenceStr);
    return form;
}

void TrainerUI::Private::refreshGui()
{
    const bool blocked = ui_skin->blockSignals(true);
    ui_skin->clear();
    for (int i = 0; i < m_trainer->game()->skinCount(); ++i)
        ui_skin->addItem(m_trainer->game()->skin(i)->name());
    ui_skin->blockSignals(blocked);
    ui_itemIntel->setEnabled(m_trainer->game()->rules()->maxHeldItems());
    ui_abilityIntel->setEnabled(m_trainer->game()->rules()->maxAbilities());
    ObjectUIPrivate::refreshGui();
}

void TrainerUI::Private::resetGui()
{
    ui_name->setText(m_trainer->name());
    ui_moneyFactor->setValue(m_trainer->moneyFactor());
    ui_skin->setCurrentIndex(m_trainer->game()->skinIndex(m_trainer->skin()));
    ui_depth->setValue(m_trainer->depth());
    ui_teamIntel->setCurrentIndex(m_trainer->teamIntel());
    ui_moveIntel->setCurrentIndex(m_trainer->moveIntel());
    ui_itemIntel->setCurrentIndex(m_trainer->itemIntel());
    ui_abilityIntel->setCurrentIndex(m_trainer->abilityIntel());
    ui_statIntel->setCurrentIndex(m_trainer->statIntel());
}

void TrainerUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_trainer->setName(name);
    ui_name->setCursorPosition(cursor);
}

void TrainerUI::Private::moneyFactorChanged(const int moneyFactor)
{
    m_trainer->setMoneyFactor(moneyFactor);
}

void TrainerUI::Private::skinChanged(const int skin)
{
    if (0 <= skin)
        m_trainer->setSkin(m_trainer->game()->skin(skin)->id());
}

void TrainerUI::Private::depthChanged(const int depth)
{
    m_trainer->setDepth(depth);
}

void TrainerUI::Private::teamIntelChanged(const int teamIntel)
{
    m_trainer->setTeamIntel(static_cast<Trainer::Intelligence>(teamIntel));
}

void TrainerUI::Private::moveIntelChanged(const int moveIntel)
{
    m_trainer->setMoveIntel(static_cast<Trainer::Intelligence>(moveIntel));
}

void TrainerUI::Private::itemIntelChanged(const int itemIntel)
{
    m_trainer->setItemIntel(static_cast<Trainer::Intelligence>(itemIntel));
}

void TrainerUI::Private::abilityIntelChanged(const int abilityIntel)
{
    m_trainer->setAbilityIntel(static_cast<Trainer::Intelligence>(abilityIntel));
}

void TrainerUI::Private::statIntelChanged(const int statIntel)
{
    m_trainer->setStatIntel(static_cast<Trainer::Intelligence>(statIntel));
}
