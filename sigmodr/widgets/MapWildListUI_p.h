/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPWILDLISTUI_P
#define SIGMODRWIDGETS_MAPWILDLISTUI_P

// Header include
#include "MapWildListUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KLineEdit;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MapWildListUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::MapWildList* wildList);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::MapWildList* m_wildList;
    public slots:
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
    private:
        KLineEdit* ui_name;
};
}
}

#endif
