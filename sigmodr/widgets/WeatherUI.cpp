/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "WeatherUI.h"
#include "WeatherUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Weather.h>

// KDE includes
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

WeatherUI::WeatherUI(Weather* weather, QWidget* parent) :
        ObjectUI(weather, parent),
        d(new Private(new Weather(*weather)))
{
    setPrivate(d);
}

void WeatherUI::apply()
{
    *qobject_cast<Weather*>(m_object) = *d->m_weather;
    ObjectUI::apply();
}

void WeatherUI::discard()
{
    *d->m_weather = *qobject_cast<Weather*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

WeatherUI::Private::Private(Weather* weather) :
        ObjectUIPrivate(weather),
        m_weather(weather)
{
}

WeatherUI::Private::~Private()
{
    delete m_weather;
}

QWidget* WeatherUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/weather.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void WeatherUI::Private::resetGui()
{
    ui_name->setText(m_weather->name());
    ui_script->setValue(m_weather->script());
}

void WeatherUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_weather->setName(name);
    ui_name->setCursorPosition(cursor);
}

void WeatherUI::Private::scriptChanged(const Script& script)
{
    m_weather->setScript(script);
}
