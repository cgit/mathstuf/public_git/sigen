/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_SPECIESUI_P
#define SIGMODRWIDGETS_SPECIESUI_P

// Header include
#include "SpeciesUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KActionSelector;
class KComboBox;
class KIntNumInput;
class KLineEdit;
class QCheckBox;
class QGroupBox;
class QListWidgetItem;
class QTableWidget;
namespace Sigcore
{
class Fraction;
class Script;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
class ScriptWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT SpeciesUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Species* species);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Species* m_species;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void baseStatChanged(const int row);
        void baseStatValueChanged(const int baseStat);
        void effortValueChanged(const int row);
        void effortValueValueChanged(const int effortValue);
        void growthChanged(const int growth);
        void experienceValueChanged(const int experienceValue);
        void catchValueChanged(const int catchValue);
        void maxHoldWeightChanged(const int maxHoldWeight);
        void runChanceChanged(const Sigcore::Fraction& runChance);
        void fleeChanceChanged(const Sigcore::Fraction& fleeChance);
        void itemChanceChanged(const Sigcore::Fraction& itemChance);
        void encyclopediaNumberChanged(const int encyclopediaNumber);
        void weightChanged(const int weight);
        void heightChanged(const int height);
        void encyclopediaEntryChanged(const QString& encyclopediaEntry);
        void maleFrontChanged(const int maleFront);
        void maleBackChanged(const int maleBack);
        void femaleFrontChanged(const int femaleFront);
        void femaleBackChanged(const int femaleBack);
        void skinChanged(const int skin);
        void hasGenderChanged(const bool hasGender);
        void genderChanceChanged(const Sigcore::Fraction& genderChance);
        void eggSpeciesChanged(const int eggSpecies);
        void eggStepsChanged(const int eggSteps);
        void evolutionChanged(const Sigcore::Script& evolution);
        void typeAdded(QListWidgetItem* item);
        void typeRemoved(QListWidgetItem* item);
        void eggGroupAdded(QListWidgetItem* item);
        void eggGroupRemoved(QListWidgetItem* item);
        void abilityAdded(QListWidgetItem* item);
        void abilityRemoved(QListWidgetItem* item);
        void abilityChanged(QListWidgetItem* item);
        void abilityWeightChanged(const int abilityWeight);
        void itemAdded(QListWidgetItem* item);
        void itemRemoved(QListWidgetItem* item);
        void itemChanged(QListWidgetItem* item);
        void itemWeightChanged(const int itemWeight);
    private:
        KLineEdit* ui_name;
        QTableWidget* ui_baseStat;
        KIntNumInput* ui_baseStatValue;
        QGroupBox* ui_boxEffortValues;
        QTableWidget* ui_effortValue;
        KIntNumInput* ui_effortValueValue;
        KComboBox* ui_growth;
        KIntNumInput* ui_experienceValue;
        KIntNumInput* ui_catchValue;
        KIntNumInput* ui_maxHoldWeight;
        CoreWidgets::FractionWidget* ui_runChance;
        CoreWidgets::FractionWidget* ui_fleeChance;
        CoreWidgets::FractionWidget* ui_itemChance;
        KIntNumInput* ui_encyclopediaNumber;
        KIntNumInput* ui_weight;
        KIntNumInput* ui_height;
        KLineEdit* ui_encyclopediaEntry;
        KComboBox* ui_maleFront;
        KComboBox* ui_maleBack;
        KComboBox* ui_femaleFront;
        KComboBox* ui_femaleBack;
        KComboBox* ui_skin;
        QWidget* ui_genetics;
        QCheckBox* ui_hasGender;
        CoreWidgets::FractionWidget* ui_genderChance;
        QGroupBox* ui_boxEggGroups;
        KComboBox* ui_eggSpecies;
        KIntNumInput* ui_eggSteps;
        CoreWidgets::ScriptWidget* ui_evolution;
        KActionSelector* ui_types;
        KActionSelector* ui_eggGroups;
        QGroupBox* ui_boxAbilities;
        KActionSelector* ui_abilities;
        KIntNumInput* ui_abilityWeight;
        QGroupBox* ui_boxItems;
        KActionSelector* ui_items;
        KIntNumInput* ui_itemWeight;
};
}
}

#endif
