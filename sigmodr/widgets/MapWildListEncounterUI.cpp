/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWildListEncounterUI.h"
#include "MapWildListEncounterUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/MapWildListEncounter.h>
#include <sigmod/Rules.h>
#include <sigmod/Species.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapWildListEncounterUI::MapWildListEncounterUI(MapWildListEncounter* encounter, QWidget* parent) :
        ObjectUI(encounter, parent),
        d(new Private(new MapWildListEncounter(*encounter)))
{
    setPrivate(d);
}

void MapWildListEncounterUI::apply()
{
    *qobject_cast<MapWildListEncounter*>(m_object) = *d->m_encounter;
    ObjectUI::apply();
}

void MapWildListEncounterUI::discard()
{
    *d->m_encounter = *qobject_cast<MapWildListEncounter*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapWildListEncounterUI::Private::Private(MapWildListEncounter* encounter) :
        ObjectUIPrivate(encounter),
        m_encounter(encounter)
{
}

MapWildListEncounterUI::Private::~Private()
{
    delete m_encounter;
}

QWidget* MapWildListEncounterUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/mapwildlistencounter.ui", widget);
    ui_species = form->findChild<KComboBox*>("varSpecies");
    ui_level = form->findChild<KIntNumInput*>("varLevel");
    ui_weight = form->findChild<KIntNumInput*>("varWeight");
    connect(ui_species, SIGNAL(currentIndexChanged(int)), this, SLOT(speciesChanged(int)));
    connect(ui_level, SIGNAL(valueChanged(int)), this, SLOT(levelChanged(int)));
    connect(ui_weight, SIGNAL(valueChanged(int)), this, SLOT(weightChanged(int)));
    return form;
}

void MapWildListEncounterUI::Private::refreshGui()
{
    const bool blocked = ui_species->blockSignals(true);
    ui_species->clear();
    for (int i = 0; i < m_encounter->game()->speciesCount(); ++i)
        ui_species->addItem(m_encounter->game()->species(i)->name());
    ui_species->blockSignals(blocked);
    ui_level->setMaximum(m_encounter->game()->rules()->maxLevel());
    ObjectUIPrivate::refreshGui();
}

void MapWildListEncounterUI::Private::resetGui()
{
    ui_species->setCurrentIndex(m_encounter->game()->speciesIndex(m_encounter->species()));
    ui_level->setValue(m_encounter->level());
    ui_weight->setValue(m_encounter->weight());
}

void MapWildListEncounterUI::Private::speciesChanged(const int species)
{
    if (0 <= species)
        m_encounter->setSpecies(m_encounter->game()->species(species)->id());
}

void MapWildListEncounterUI::Private::levelChanged(const int level)
{
    m_encounter->setLevel(level);
}

void MapWildListEncounterUI::Private::weightChanged(const int weight)
{
    m_encounter->setWeight(weight);
}
