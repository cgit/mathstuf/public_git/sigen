/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_NATUREUI_P
#define SIGMODRWIDGETS_NATUREUI_P

// Header include
#include "NatureUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KIntNumInput;
class KLineEdit;
class QTableWidget;
namespace Sigcore
{
class Fraction;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT NatureUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Nature* nature);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Nature* m_nature;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void statChanged(const int row);
        void statMultiplierChanged(const Sigcore::Fraction& multiplier);
        void weightChanged(const int weight);
    private:
        KLineEdit* ui_name;
        QTableWidget* ui_stat;
        CoreWidgets::FractionWidget* ui_statMultiplier;
        KIntNumInput* ui_weight;
};
}
}

#endif
