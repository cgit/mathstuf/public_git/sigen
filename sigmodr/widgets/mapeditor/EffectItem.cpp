/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "EffectItem.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/MapEffect.h>
#include <sigmod/Skin.h>

// Qt includes
#include <QtGui/QPainter>

// Standard includes
#include <climits>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

EffectItem::EffectItem(Sigmod::Map* map, MapEffect* effect, QGraphicsScene* parent) :
        MapItem(map, parent),
        m_effect(effect)
{
    connect(m_effect, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(m_effect, SIGNAL(changed()), this, SLOT(effectChanged()));
    connect(m_effect, SIGNAL(error(QString)), this, SLOT(effectChanged()));
    setOpacity(.5);
    setZValue(INT_MAX);
    m_tag->setText(QString::number(m_effect->id()));
    effectChanged();
}

QRectF EffectItem::boundingRect() const
{
    if (m_size.isEmpty())
    {
        QPainterPath path = m_effect->area();
        if (path.isEmpty())
            return QRect(0, 0, 32, 32);
        return m_effect->area().boundingRect();
    }
    return QRect(QPoint(), m_size);
}

void EffectItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setBrush(QBrush(Qt::red));
    painter->setPen(QPen(Qt::black, 2));
    if (m_size.isEmpty())
    {
        QPainterPath path = m_effect->area();
        if (path.isEmpty())
            painter->drawRect(boundingRect());
        else
            painter->drawPath(path);
    }
    else
        painter->drawRect(QRect(QPoint(), m_size));
    MapItem::paint(painter, option, widget);
}

int EffectItem::type() const
{
    return Type;
}

void EffectItem::moveTo(const QPoint& point)
{
    m_effect->setPosition(point);
}

void EffectItem::effectChanged()
{
    setPos(m_effect->position());
    const Skin* skin = m_effect->game()->skinById(m_effect->skin());
    if (skin)
        m_size = skin->size();
    else
        m_size = QSize();
    resetLabel();
    update();
}

void EffectItem::resetLabel()
{
    m_label->setText(m_effect->name());
    QSizeF size = m_label->boundingRect().size() / 2 - boundingRect().size() / 2;
    m_label->setPos(-size.width(), -size.height());
}
