/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TrainerItem.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/MapTrainer.h>
#include <sigmod/Skin.h>
#include <sigmod/Trainer.h>

// Qt includes
#include <QtGui/QPainter>

// Standard includes
#include <climits>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

TrainerItem::TrainerItem(Sigmod::Map* map, MapTrainer* trainer, QGraphicsScene* parent) :
        MapItem(map, parent),
        m_trainer(trainer)
{
    connect(m_trainer, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(m_trainer, SIGNAL(changed()), this, SLOT(trainerChanged()));
    connect(m_trainer, SIGNAL(error(QString)), this, SLOT(trainerChanged()));
    setOpacity(.5);
    setZValue(INT_MAX);
    m_tag->setText(QString::number(m_trainer->id()));
    trainerChanged();
}

QRectF TrainerItem::boundingRect() const
{
    return QRect(QPoint(), m_size);
}

void TrainerItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setBrush(QBrush(Qt::blue));
    painter->setPen(QPen(Qt::black, 2));
    painter->drawRect(boundingRect());
    MapItem::paint(painter, option, widget);
}

int TrainerItem::type() const
{
    return Type;
}

void TrainerItem::moveTo(const QPoint& point)
{
    m_trainer->setPosition(point);
}

void TrainerItem::trainerChanged()
{
    const Trainer* trainer = m_trainer->game()->trainerById(m_trainer->trainerClass());
    m_size = QSize(32, 32);
    if (trainer)
    {
        const Skin* skin = m_trainer->game()->skinById(trainer->skin());
        if (skin)
            m_size = skin->size();
    }
    setPos(m_trainer->position());
    m_label->setText(m_trainer->name());
    resetLabel();
    update();
}

void TrainerItem::resetLabel()
{
    QSizeF size = m_label->boundingRect().size() / 2 - m_size / 2;
    m_label->setPos(-size.width(), -size.height());
}
