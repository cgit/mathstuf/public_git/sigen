/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_EFFECTITEM
#define SIGMODRWIDGETS_EFFECTITEM

// Sigmodr widget includes
#include "MapItem.h"

// Forward declarations
namespace Sigmod
{
class MapEffect;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT EffectItem : public MapItem
{
    Q_OBJECT
    
    public:
        enum
        {
            Type = UserType
        };
        
        EffectItem(Sigmod::Map* map, Sigmod::MapEffect* effect, QGraphicsScene* parent);
        
        QRectF boundingRect() const;
        
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
        
        int type() const;
    protected:
        void moveTo(const QPoint& point);
    protected slots:
        void effectChanged();
    private:
        void resetLabel();
        
        Sigmod::MapEffect* m_effect;
        QSize m_size;
};
}
}

#endif
