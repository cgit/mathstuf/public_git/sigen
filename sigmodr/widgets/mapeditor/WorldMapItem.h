/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_WORLDMAPITEM
#define SIGMODRWIDGETS_WORLDMAPITEM

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtCore/QObject>
#include <QtGui/QGraphicsItemGroup>

// Forward declarations
class QGraphicsScene;
namespace Sigmod
{
class Game;
class Map;
}

namespace Sigmodr
{
namespace Widgets
{
class TileItem;

class SIGMODRWIDGETS_NO_EXPORT WorldMapItem : public QObject, public QGraphicsItemGroup
{
    Q_OBJECT
    
    public:
        enum
        {
            Type = UserType + 4
        };
        
        WorldMapItem(Sigmod::Game* game, Sigmod::Map* map, QGraphicsScene* parent);
        ~WorldMapItem();
        
        QRectF boundingRect() const;
        
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);
        
        bool collidesWithItem(const QGraphicsItem* other, const Qt::ItemSelectionMode mode = Qt::IntersectsItemShape) const;
        
        int type() const;
        
        bool isLocked() const;
    signals:
        void changed();
        
        void maskTiles(const bool mask);
    protected:
        void focusInEvent(QFocusEvent* event);
        void focusOutEvent(QFocusEvent* event);
        void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
        void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
        void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
        void keyPressEvent(QKeyEvent* event);
        
        void moveTo(const QPoint& point);
    protected slots:
        void mapChanged();
    private:
        void resetLabel();
        
        Sigmod::Game* m_game;
        Sigmod::Map* m_map;
        bool m_locked;
        QList<TileItem*> m_tiles;
        QGraphicsSimpleTextItem* m_tag;
        QGraphicsSimpleTextItem* m_label;
};
}
}

#endif
