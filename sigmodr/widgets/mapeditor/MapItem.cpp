/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapItem.h"

// Sigmod includes
#include <sigmod/Map.h>

// KDE includes
#include <KColorScheme>

// Qt includes
#include <QtGui/QGraphicsScene>
#include <QtGui/QGraphicsSceneMouseEvent>
#include <QtGui/QGraphicsSimpleTextItem>
#include <QtGui/QPainter>
#include <QtGui/QStyleOptionGraphicsItem>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapItem::MapItem(Map* map, QGraphicsScene* parent) :
        QObject(parent),
        m_map(map),
        m_tag(new QGraphicsSimpleTextItem(this)),
        m_label(new QGraphicsSimpleTextItem(this))
{
    setAcceptHoverEvents(true);
    setFlags(ItemIsMovable | ItemIsSelectable | ItemDoesntPropagateOpacityToChildren);
    m_tag->setPos(-5, -8);
    m_label->hide();
}

MapItem::~MapItem()
{
}

void MapItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(widget)
    if (option->state & QStyle::State_MouseOver)
    {
        painter->setPen(QPen(KStatefulBrush(KColorScheme::View, KColorScheme::HoverColor).brush(QPalette::Active), 3));
        painter->drawRect(boundingRect());
    }
    if (option->state & QStyle::State_Selected)
    {
        painter->setPen(QPen(KStatefulBrush(KColorScheme::Selection, KColorScheme::ActiveBackground).brush(QPalette::Active), 3));
        painter->drawRect(boundingRect());
    }
}

void MapItem::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    m_label->show();
    QGraphicsItem::hoverEnterEvent(event);
    update();
}

void MapItem::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    m_label->hide();
    QGraphicsItem::hoverLeaveEvent(event);
    update();
}

void MapItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QRectF mapRect = QRect(0, 0, m_map->width(), m_map->height());
    QRectF rect = QRectF(event->scenePos() - event->buttonDownPos(Qt::LeftButton), boundingRect().size());
    if (!mapRect.contains(rect))
    {
        if (rect.left() < 0)
            rect.moveLeft(0);
        else if (mapRect.right() < rect.right())
            rect.moveRight(mapRect.right());
        if (rect.top() < 0)
            rect.moveTop(0);
        else if (mapRect.bottom() < rect.bottom())
            rect.moveBottom(mapRect.bottom());
        setPos(rect.topLeft().toPoint());
        event->accept();
    }
    else
        QGraphicsItem::mouseMoveEvent(event);
    moveTo(scenePos().toPoint());
    update();
}
