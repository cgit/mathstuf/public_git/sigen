/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_WORLDMAPPLACEMENT
#define SIGMODRWIDGETS_WORLDMAPPLACEMENT

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QRect>
#include <QtGui/QPolygon>

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT WorldMapPlacement
{
    public:
        WorldMapPlacement(const QSize& size);
        
        void addRect(const QRectF& rect);
        
        QPoint find(const QPoint& point);
        
        static void invalidateCache();
    private:
        void finalize();
        
        static bool m_cacheGood;
        static QSize m_size;
        QList<QRect> m_rects;
        static QList<QPolygon> m_polygons;
};
}
}

#endif
