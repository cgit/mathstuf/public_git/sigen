/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "WarpItem.h"

// Sigmod includes
#include <sigmod/MapWarp.h>

// Qt includes
#include <QtGui/QPainter>

// Standard includes
#include <climits>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

WarpItem::WarpItem(Sigmod::Map* map, MapWarp* warp, QGraphicsScene* parent) :
        MapItem(map, parent),
        m_warp(warp)
{
    connect(m_warp, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(m_warp, SIGNAL(changed()), this, SLOT(warpChanged()));
    connect(m_warp, SIGNAL(error(QString)), this, SLOT(warpChanged()));
    setOpacity(.5);
    setZValue(INT_MAX);
    m_tag->setText(QString::number(m_warp->id()));
    warpChanged();
}

QRectF WarpItem::boundingRect() const
{
    QPainterPath path = m_warp->area();
    if (path.isEmpty())
        return QRect(0, 0, 32, 32);
    return m_warp->area().boundingRect();
}

void WarpItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    painter->setBrush(QBrush(Qt::green));
    painter->setPen(QPen(Qt::black, 2));
    QPainterPath path = m_warp->area();
    if (path.isEmpty())
        painter->drawRect(0, 0, 32, 32);
    else
        painter->drawPath(path);
    MapItem::paint(painter, option, widget);
}

int WarpItem::type() const
{
    return Type;
}

void WarpItem::moveTo(const QPoint& point)
{
    m_warp->setPosition(point);
}

void WarpItem::warpChanged()
{
    setPos(m_warp->position());
    resetLabel();
    update();
}

void WarpItem::resetLabel()
{
    m_label->setText(m_warp->name());
    QSizeF size = m_label->boundingRect().size() / 2 - boundingRect().size() / 2;
    m_label->setPos(-size.width(), -size.height());
}
