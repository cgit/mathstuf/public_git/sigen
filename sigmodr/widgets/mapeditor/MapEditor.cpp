/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapEditor.h"

// Sigmodr widget includes
#include "MapGrid.h"
#include "MapScene.h"

// Sigmod includes
#include <sigmod/Map.h>

// KDE includes
#include <KIcon>
#include <KDoubleNumInput>
#include <KIntNumInput>
#include <KPushButton>

// Qt includes
#include <QtCore/QFile>
#include <QtCore/QSignalMapper>
#include <QtGui/QCheckBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QVBoxLayout>
#include <QtUiTools/QUiLoader>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapEditor::MapEditor(Map* map, QWidget* parent) :
        QWidget(parent),
        m_map(map),
        m_scene(NULL),
        m_grid(NULL)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    QFile file(":/gui/mapeditor.ui");
    file.open(QFile::ReadOnly);
    QWidget *formWidget = QUiLoader().load(&file, this);
    file.close();
    ui_view = formWidget->findChild<QGraphicsView*>("varView");
    ui_width = formWidget->findChild<KIntNumInput*>("varWidth");
    ui_gridSize = formWidget->findChild<KIntNumInput*>("varGridSize");
    ui_zoom = formWidget->findChild<KDoubleNumInput*>("varZoom");
    ui_height = formWidget->findChild<KIntNumInput*>("varHeight");
    ui_buttonAdd = formWidget->findChild<KPushButton*>("buttonAdd");
    ui_buttonRemove = formWidget->findChild<KPushButton*>("buttonRemove");
    ui_buttonTop = formWidget->findChild<KPushButton*>("buttonTop");
    ui_buttonUp = formWidget->findChild<KPushButton*>("buttonUp");
    ui_buttonDown = formWidget->findChild<KPushButton*>("buttonDown");
    ui_buttonBottom = formWidget->findChild<KPushButton*>("buttonBottom");
    ui_showEffects = formWidget->findChild<QCheckBox*>("varShowEffects");
    ui_showTiles = formWidget->findChild<QCheckBox*>("varShowTiles");
    ui_showTrainers = formWidget->findChild<QCheckBox*>("varShowTrainers");
    ui_showWarps = formWidget->findChild<QCheckBox*>("varShowWarps");
    ui_drawMask = formWidget->findChild<QCheckBox*>("varDrawMask");
    ui_zoom->setRange(.1, 10, .1);
    ui_buttonAdd->setIcon(KIcon("list-add"));
    ui_buttonRemove->setIcon(KIcon("list-remove"));
    ui_buttonTop->setIcon(KIcon("arrow-up-double"));
    ui_buttonUp->setIcon(KIcon("arrow-up"));
    ui_buttonDown->setIcon(KIcon("arrow-down"));
    ui_buttonBottom->setIcon(KIcon("arrow-down-double"));
    ui_buttonRemove->setEnabled(false);
    ui_buttonTop->setEnabled(false);
    ui_buttonUp->setEnabled(false);
    ui_buttonDown->setEnabled(false);
    ui_buttonBottom->setEnabled(false);
    connect(ui_width, SIGNAL(valueChanged(int)), this, SLOT(setMapWidth(int)));
    connect(ui_height, SIGNAL(valueChanged(int)), this, SLOT(setMapHeight(int)));
    connect(ui_gridSize, SIGNAL(valueChanged(int)), this, SLOT(setGridSize(int)));
    connect(ui_zoom, SIGNAL(valueChanged(double)), this, SLOT(setZoom(double)));
    reset();
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(formWidget);
    setLayout(layout);
}

void MapEditor::setMap(Map* map)
{
    m_map = map;
    delete m_grid;
    delete m_scene;
    reset();
}

void MapEditor::reset()
{
    m_scene = new MapScene(m_map, this);
    m_grid = new MapGrid;
    m_grid->setRect(QRect(0, 0, m_map->width(), m_map->height()));
    m_scene->addItem(m_grid);
    ui_view->setScene(m_scene);
    ui_width->setValue(m_map->width());
    ui_height->setValue(m_map->height());
    m_grid->setGridSize(ui_gridSize->value());
    ui_zoom->setValue(1);
    ui_drawMask->setChecked(false);
    makeConnections();
}

void MapEditor::setMapWidth(const int width)
{
    m_map->setWidth(width);
    m_grid->setRect(QRect(0, 0, m_map->width(), m_map->height()));
}

void MapEditor::setMapHeight(const int height)
{
    m_map->setHeight(height);
    m_grid->setRect(QRect(0, 0, m_map->width(), m_map->height()));
}

void MapEditor::setGridSize(const int gridSize)
{
    m_grid->setGridSize(gridSize);
}

void MapEditor::setZoom(const double zoom)
{
    ui_view->setMatrix(QMatrix().scale(zoom, zoom));
}

void MapEditor::makeConnections()
{
    connect(m_scene, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(ui_buttonAdd, SIGNAL(pressed()), m_scene, SLOT(addTile()));
    connect(ui_buttonRemove, SIGNAL(pressed()), m_scene, SLOT(removeSelected()));
    connect(m_scene, SIGNAL(itemsSelected(bool)), ui_buttonRemove, SLOT(setEnabled(bool)));
    connect(ui_buttonTop, SIGNAL(pressed()), m_scene, SLOT(moveToTop()));
    connect(m_scene, SIGNAL(tilesSelected(bool)), ui_buttonTop, SLOT(setEnabled(bool)));
    connect(ui_buttonUp, SIGNAL(pressed()), m_scene, SLOT(moveUp()));
    connect(m_scene, SIGNAL(tilesSelected(bool)), ui_buttonUp, SLOT(setEnabled(bool)));
    connect(ui_buttonDown, SIGNAL(pressed()), m_scene, SLOT(moveDown()));
    connect(m_scene, SIGNAL(tilesSelected(bool)), ui_buttonDown, SLOT(setEnabled(bool)));
    connect(ui_buttonBottom, SIGNAL(pressed()), m_scene, SLOT(moveToBottom()));
    connect(m_scene, SIGNAL(tilesSelected(bool)), ui_buttonBottom, SLOT(setEnabled(bool)));
    connect(ui_showEffects, SIGNAL(stateChanged(int)), m_scene, SLOT(showEffects(int)));
    connect(ui_showTiles, SIGNAL(stateChanged(int)), m_scene, SLOT(showTiles(int)));
    connect(ui_showTrainers, SIGNAL(stateChanged(int)), m_scene, SLOT(showTrainers(int)));
    connect(ui_showWarps, SIGNAL(stateChanged(int)), m_scene, SLOT(showWarps(int)));
    connect(ui_drawMask, SIGNAL(toggled(bool)), m_scene, SIGNAL(maskTiles(bool)));
}
