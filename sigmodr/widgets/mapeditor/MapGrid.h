/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPGRID
#define SIGMODRWIDGETS_MAPGRID

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtGui/QGraphicsItem>

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MapGrid : public QGraphicsItem
{
    public:
        MapGrid(const int fringe = 5);
        
        QRectF boundingRect() const;
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget);
        
        void setRect(const QRectF& rect);
        void setGridSize(const int gridSize);
    private:
        QRectF m_rect;
        int m_fringe;
        int m_gridSize;
};
}
}

#endif
