/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "WorldMapScene.h"

// Sigmodr widget includes
#include "WorldMapItem.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Map.h>

// Qt includes
#include <QtGui/QGraphicsView>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

WorldMapScene::WorldMapScene(Game* game, QObject* parent) :
        QGraphicsScene(parent),
        m_game(game)
{
    for (int i = 0; i < m_game->mapCount(); ++i)
    {
        Map* map = m_game->map(i);
        if (!map->isWorld())
            continue;
        WorldMapItem* item = new WorldMapItem(m_game, map, this);
        connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
        connect(this, SIGNAL(maskTiles(bool)), item, SIGNAL(maskTiles(bool)));
        m_maps.append(item);
        addItem(item);
    }
}
