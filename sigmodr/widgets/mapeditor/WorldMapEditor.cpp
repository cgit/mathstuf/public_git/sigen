/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "WorldMapEditor.h"

// Sigmodr widget includes
#include "MapGrid.h"
#include "WorldMapScene.h"

// Sigmod includes
#include <sigmod/Game.h>

// KDE includes
#include <KDoubleNumInput>
#include <KIntNumInput>

// Qt includes
#include <QtCore/QFile>
#include <QtGui/QCheckBox>
#include <QtGui/QGraphicsView>
#include <QtGui/QVBoxLayout>
#include <QtUiTools/QUiLoader>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

WorldMapEditor::WorldMapEditor(Game* game, QWidget* parent) :
        QWidget(parent),
        m_game(game),
        m_scene(NULL),
        m_grid(NULL)
{
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Expanding);
    QFile file(":/gui/worldmapeditor.ui");
    file.open(QFile::ReadOnly);
    QWidget *formWidget = QUiLoader().load(&file, this);
    file.close();
    ui_gridSize = formWidget->findChild<KIntNumInput*>("varGridSize");
    ui_zoom = formWidget->findChild<KDoubleNumInput*>("varZoom");
    ui_drawMasked = formWidget->findChild<QCheckBox*>("varDrawMasked");
    ui_view = formWidget->findChild<QGraphicsView*>("varView");
    ui_zoom->setRange(.1, 10, .1);
    connect(ui_gridSize, SIGNAL(valueChanged(int)), this, SLOT(setGridSize(int)));
    connect(ui_zoom, SIGNAL(valueChanged(double)), this, SLOT(setZoom(double)));
    reset();
    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(formWidget);
    setLayout(layout);
}

void WorldMapEditor::setGame(Game* game)
{
    m_game = game;
    delete m_grid;
    delete m_scene;
    reset();
}

void WorldMapEditor::reset()
{
    m_scene = new WorldMapScene(m_game, this);
    m_grid = new MapGrid(0);
    m_grid->setRect(m_scene->itemsBoundingRect());
    m_scene->addItem(m_grid);
    ui_view->setScene(m_scene);
    m_grid->setGridSize(ui_gridSize->value());
    ui_zoom->setValue(1);
    ui_drawMasked->setChecked(false);
    resizeGrid(m_scene->itemsBoundingRect());
    makeConnections();
}

void WorldMapEditor::setGridSize(const int gridSize)
{
    m_grid->setGridSize(gridSize);
}

void WorldMapEditor::setZoom(const double zoom)
{
    ui_view->setMatrix(QMatrix().scale(zoom, zoom));
}

void WorldMapEditor::resizeGrid(const QRectF& rect)
{
    m_grid->setRect(rect.adjusted(5, 8, 0, 0));
}

void WorldMapEditor::makeConnections()
{
    connect(m_scene, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(m_scene, SIGNAL(sceneRectChanged(QRectF)), this, SLOT(resizeGrid(QRectF)));
    connect(ui_drawMasked, SIGNAL(toggled(bool)), m_scene, SIGNAL(maskTiles(bool)));
}
