/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_TILEITEM
#define SIGMODRWIDGETS_TILEITEM

// Sigmodr widget includes
#include "MapItem.h"

// Forward declarations
namespace Sigmod
{
class MapTile;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT TileItem : public MapItem
{
    Q_OBJECT
    
    public:
        enum
        {
            Type = UserType + 1
        };
        
        TileItem(Sigmod::Map* map, Sigmod::MapTile* tile, QGraphicsScene* parent);
        
        QRectF boundingRect() const;
        
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
        
        void setZIndex(const int zIndex);
        
        int type() const;
    public slots:
        void drawCollisionMask(const bool mask);
        
        void setSprite(const int spriteId, const bool walkable);
    protected:
        void moveTo(const QPoint& point);
    protected slots:
        void tileChanged();
    private:
        void resetLabel();
        
        Sigmod::MapTile* m_tile;
        int m_tileIndex;
        bool m_mask;
        QPixmap m_pixmap;
        QPixmap m_masked;
};
}
}

#endif
