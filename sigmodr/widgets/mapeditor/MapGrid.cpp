/*
 * Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapGrid.h"

// Qt includes
#include <QtGui/QPainter>

// Standard includes
#include <climits>

using namespace Sigmodr::Widgets;

MapGrid::MapGrid(const int fringe) :
        m_fringe(fringe),
        m_gridSize(0)
{
    setZValue(INT_MIN);
}

QRectF MapGrid::boundingRect() const
{
    return m_rect.adjusted(-m_fringe, -m_fringe, m_fringe, m_fringe);
}

void MapGrid::paint(QPainter* painter, const QStyleOptionGraphicsItem* options, QWidget* widget)
{
    Q_UNUSED(options)
    Q_UNUSED(widget)
    painter->save();
    if (m_gridSize)
    {
        painter->save();
        painter->setOpacity(.25);
        const int top = m_rect.top() - m_fringe;
        const int bottom = m_rect.bottom() + m_fringe;
        const int left = m_rect.left() - m_fringe;
        const int right = m_rect.right() + m_fringe;
        for (int i = m_rect.left(); i < m_rect.right(); i += m_gridSize)
            painter->drawLine(i, top, i, bottom);
        painter->drawLine(m_rect.right(), top, m_rect.right(), bottom);
        for (int i = m_rect.top(); i < m_rect.bottom(); i += m_gridSize)
            painter->drawLine(left, i, right, i);
        painter->drawLine(left, m_rect.bottom(), right, m_rect.bottom());
        painter->setOpacity(1);
        painter->restore();
    }
    painter->setPen(QPen(Qt::red, 3));
    painter->drawRect(m_rect);
    painter->restore();
}

void MapGrid::setRect(const QRectF& rect)
{
    prepareGeometryChange();
    m_rect = rect;
    update();
}

void MapGrid::setGridSize(const int gridSize)
{
    m_gridSize = gridSize;
    update();
}
