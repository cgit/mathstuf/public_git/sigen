/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapScene.h"

// Sigmodr widget includes
#include "EffectItem.h"
#include "TileItem.h"
#include "TrainerItem.h"
#include "WarpItem.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Map.h>
#include <sigmod/MapEffect.h>
#include <sigmod/MapTile.h>
#include <sigmod/MapTrainer.h>
#include <sigmod/MapWarp.h>
#include <sigmod/Sprite.h>
#include <sigmod/Tile.h>

// KDE includes
#include <KComboBox>
#include <KDialog>
#include <KMessageBox>

// Qt includes
#include <QtGui/QGraphicsView>

// Standard includes
#include <climits>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapScene::MapScene(Map* map, QObject* parent) :
        QGraphicsScene(parent),
        m_map(map),
        m_tileBox(new KComboBox)
{
    for (int i = 0; i < m_map->effectCount(); ++i)
    {
        MapEffect* effect = m_map->effect(i);
        EffectItem* item = new EffectItem(m_map, effect, this);
        connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
        m_effects[effect] = item;
        addItem(item);
    }
    for (int i = 0; i < m_map->tileCount(); ++i)
    {
        MapTile* tile = m_map->tile(i);
        TileItem* item = new TileItem(m_map, tile, this);
        connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
        connect(this, SIGNAL(maskTiles(bool)), item, SLOT(drawCollisionMask(bool)));
        m_tiles[tile] = item;
        addItem(item);
    }
    for (int i = 0; i < m_map->trainerCount(); ++i)
    {
        MapTrainer* trainer = m_map->trainer(i);
        TrainerItem* item = new TrainerItem(m_map, trainer, this);
        connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
        m_trainers[trainer] = item;
        addItem(item);
    }
    for (int i = 0; i < m_map->warpCount(); ++i)
    {
        MapWarp* warp = m_map->warp(i);
        WarpItem* item = new WarpItem(m_map, warp, this);
        connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
        m_warps[warp] = item;
        addItem(item);
    }
    int maxWidth = 0;
    int maxHeight = 0;
    for (int i = 0; i < m_map->game()->tileCount(); ++i)
    {
        const Tile* tile = m_map->game()->tile(i);
        const Sprite* sprite = m_map->game()->spriteById(tile->preview());
        QPixmap pixmap;
        if (sprite)
            pixmap.loadFromData(sprite->sprite());
        m_tileBox->addItem(pixmap, tile->name(), tile->id());
        maxWidth = qMax(maxWidth, pixmap.width());
        maxHeight = qMax(maxHeight, pixmap.height());
    }
    m_tileBox->setIconSize(QSize(maxWidth, maxHeight));
    connect(this, SIGNAL(selectionChanged()), SLOT(itemSelectionChanged()));
}

MapScene::~MapScene()
{
    delete m_tileBox;
}

void MapScene::addTile()
{
    if (!m_tileBox->count())
    {
        KMessageBox::error(NULL, "There are no tiles defined!", "No tiles");
        return;
    }
    int tileId = -1;
    KDialog* dialog = new KDialog;
    dialog->setButtons(KDialog::Ok | KDialog::Cancel);
    dialog->setCaption("New Tile");
    dialog->setMainWidget(m_tileBox);
    if (dialog->exec() == QDialog::Accepted)
        tileId = m_tileBox->itemData(m_tileBox->currentIndex()).toInt();
    m_tileBox->setParent(NULL);
    delete dialog;
    if (tileId < 0)
        return;
    MapTile* tile = m_map->newTile();
    tile->setTile(tileId);
    QList<QGraphicsView*> viewList = views();
    if (viewList.size())
        tile->setPosition(viewList[0]->mapToScene(QPoint(0, 0)).toPoint());
    TileItem* item = new TileItem(m_map, tile, this);
    connect(item, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(this, SIGNAL(maskTiles(bool)), item, SLOT(drawCollisionMask(bool)));
    m_tiles[tile] = item;
    addItem(item);
    emit(changed());
}

void MapScene::removeSelected()
{
    QList<QGraphicsItem*> items = selectedItems();
    foreach (QGraphicsItem* item, items)
    {
        EffectItem* effectItem = qgraphicsitem_cast<EffectItem*>(item);
        TileItem* tileItem = qgraphicsitem_cast<TileItem*>(item);
        TrainerItem* trainerItem = qgraphicsitem_cast<TrainerItem*>(item);
        WarpItem* warpItem = qgraphicsitem_cast<WarpItem*>(item);
        if (effectItem)
        {
            MapEffect* effect = m_effects.key(effectItem);
            m_map->deleteEffectById(effect->id());
            delete effectItem;
            m_effects.remove(effect);
        }
        else if (tileItem)
        {
            MapTile* tile = m_tiles.key(tileItem);
            m_map->deleteTileById(tile->id());
            delete tileItem;
            m_tiles.remove(tile);
        }
        else if (trainerItem)
        {
            MapTrainer* trainer = m_trainers.key(trainerItem);
            m_map->deleteTrainerById(trainer->id());
            delete trainerItem;
            m_trainers.remove(trainer);
        }
        else if (warpItem)
        {
            MapWarp* warp = m_warps.key(warpItem);
            m_map->deleteWarpById(warp->id());
            delete warpItem;
            m_warps.remove(warp);
        }
    }
}

void MapScene::moveToTop()
{
    QList<QGraphicsItem*> items = selectedItems();
    foreach (QGraphicsItem* item, items)
    {
        TileItem* tileItem = qgraphicsitem_cast<TileItem*>(item);
        if (tileItem)
            tileItem->setZIndex(INT_MAX - 1);
    }
}

void MapScene::moveUp()
{
    QList<QGraphicsItem*> items = selectedItems();
    foreach (QGraphicsItem* item, items)
    {
        TileItem* tileItem = qgraphicsitem_cast<TileItem*>(item);
        const int zValue = tileItem->zValue();
        if (tileItem && (zValue < (INT_MAX - 1)))
            tileItem->setZIndex(zValue + 1);
    }
}

void MapScene::moveDown()
{
    QList<QGraphicsItem*> items = selectedItems();
    foreach (QGraphicsItem* item, items)
    {
        TileItem* tileItem = qgraphicsitem_cast<TileItem*>(item);
        const int zValue = tileItem->zValue();
        if (tileItem && (INT_MIN < zValue))
            tileItem->setZIndex(zValue - 1);
    }
}

void MapScene::moveToBottom()
{
    QList<QGraphicsItem*> items = selectedItems();
    foreach (QGraphicsItem* item, items)
    {
        TileItem* tileItem = qgraphicsitem_cast<TileItem*>(item);
        if (tileItem)
            tileItem->setZIndex(INT_MIN);
    }
}

void MapScene::showEffects(const int state)
{
    QList<EffectItem*> effects = m_effects.values();
    foreach (EffectItem* effect, effects)
    {
        if (state < Qt::PartiallyChecked)
            effect->hide();
        else
            effect->show();
        effect->setFlag(QGraphicsItem::ItemIsMovable, Qt::PartiallyChecked < state);
        effect->setFlag(QGraphicsItem::ItemIsSelectable, Qt::PartiallyChecked < state);
    }
}

void MapScene::showTiles(const int state)
{
    QList<TileItem*> tiles = m_tiles.values();
    foreach (TileItem* tile, tiles)
    {
        if (state < Qt::PartiallyChecked)
            tile->hide();
        else
            tile->show();
        tile->setFlag(QGraphicsItem::ItemIsMovable, Qt::PartiallyChecked < state);
        tile->setFlag(QGraphicsItem::ItemIsSelectable, Qt::PartiallyChecked < state);
    }
}

void MapScene::showTrainers(const int state)
{
    QList<TrainerItem*> trainers = m_trainers.values();
    foreach (TrainerItem* trainer, trainers)
    {
        if (state < Qt::PartiallyChecked)
            trainer->hide();
        else
            trainer->show();
        trainer->setFlag(QGraphicsItem::ItemIsMovable, Qt::PartiallyChecked < state);
        trainer->setFlag(QGraphicsItem::ItemIsSelectable, Qt::PartiallyChecked < state);
    }
}

void MapScene::showWarps(const int state)
{
    QList<WarpItem*> warps = m_warps.values();
    foreach (WarpItem* warp, warps)
    {
        if (state < Qt::PartiallyChecked)
            warp->hide();
        else
            warp->show();
        warp->setFlag(QGraphicsItem::ItemIsMovable, Qt::PartiallyChecked < state);
        warp->setFlag(QGraphicsItem::ItemIsSelectable, Qt::PartiallyChecked < state);
    }
}

void MapScene::itemSelectionChanged()
{
    QList<QGraphicsItem*> items = selectedItems();
    bool selected = false;
    foreach (QGraphicsItem* item, items)
    {
        if (qgraphicsitem_cast<TileItem*>(item))
        {
            selected = true;
            break;
        }
    }
    emit(itemsSelected(items.size()));
    emit(tilesSelected(selected));
}
