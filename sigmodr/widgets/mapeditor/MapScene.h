/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPSCENE
#define SIGMODRWIDGETS_MAPSCENE

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtCore/QMap>
#include <QtGui/QGraphicsScene>

// Forward declarations
class KComboBox;
namespace Sigmod
{
class Map;
class MapEffect;
class MapTile;
class MapTrainer;
class MapWarp;
}

namespace Sigmodr
{
namespace Widgets
{
class EffectItem;
class TileItem;
class TrainerItem;
class WarpItem;

class SIGMODRWIDGETS_NO_EXPORT MapScene : public QGraphicsScene
{
    Q_OBJECT
    
    public:
        MapScene(Sigmod::Map* map, QObject* parent);
        ~MapScene();
    public slots:
        void addTile();
        void removeSelected();
        
        void moveToTop();
        void moveUp();
        void moveDown();
        void moveToBottom();
        
        void showEffects(const int state);
        void showTiles(const int state);
        void showTrainers(const int state);
        void showWarps(const int state);
    signals:
        void changed();
        
        void maskTiles(const bool mask);
        
        void itemsSelected(const bool selected);
        void tilesSelected(const bool selected);
    protected slots:
        void itemSelectionChanged();
    private:
        Sigmod::Map* m_map;
        QMap<Sigmod::MapEffect*, EffectItem*> m_effects;
        QMap<Sigmod::MapTile*, TileItem*> m_tiles;
        QMap<Sigmod::MapTrainer*, TrainerItem*> m_trainers;
        QMap<Sigmod::MapWarp*, WarpItem*> m_warps;
        KComboBox* m_tileBox;
};
}
}

#endif
