/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPITEM
#define SIGMODRWIDGETS_MAPITEM

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtCore/QObject>
#include <QtGui/QGraphicsItem>

// Forward declarations
class QGraphicsScene;
namespace Sigmod
{
class Map;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MapItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    
    public:
        MapItem(Sigmod::Map* map, QGraphicsScene* parent);
        virtual ~MapItem();
        
        virtual QRectF boundingRect() const = 0;
        
        virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);
        
        virtual int type() const = 0;
    signals:
        void changed();
    protected:
        virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
        virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
        virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
        
        virtual void moveTo(const QPoint& point) = 0;
        
        virtual void resetLabel() = 0;
        
        Sigmod::Map* m_map;
        QGraphicsSimpleTextItem* m_tag;
        QGraphicsSimpleTextItem* m_label;
};
}
}

#endif
