/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPEDITOR
#define SIGMODRWIDGETS_MAPEDITOR

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtGui/QWidget>

// Forward declarations
class QCheckBox;
class QGraphicsView;
class KComboBox;
class KDoubleNumInput;
class KIntNumInput;
class KPushButton;
namespace Sigmod
{
class Map;
}

namespace Sigmodr
{
namespace Widgets
{
class MapGrid;
class MapScene;

class SIGMODRWIDGETS_NO_EXPORT MapEditor : public QWidget
{
    Q_OBJECT
    
    public:
        MapEditor(Sigmod::Map* map, QWidget* parent);
        
        void setMap(Sigmod::Map* map);
        
        void reset();
    signals:
        void showEffects(const bool show);
        void showTiles(const bool show);
        void showTrainers(const bool show);
        void showWarps(const bool show);
        
        void changed();
    protected slots:
        void setMapWidth(const int width);
        void setMapHeight(const int height);
        
        void setGridSize(const int gridSize);
        void setZoom(const double zoom);
    protected:
        void makeConnections();
    private:
        Sigmod::Map* m_map;
        MapScene* m_scene;
        MapGrid* m_grid;
        
        KIntNumInput* ui_width;
        KIntNumInput* ui_height;
        KIntNumInput* ui_gridSize;
        KDoubleNumInput* ui_zoom;
        KPushButton* ui_buttonAdd;
        KPushButton* ui_buttonRemove;
        KPushButton* ui_buttonTop;
        KPushButton* ui_buttonUp;
        KPushButton* ui_buttonDown;
        KPushButton* ui_buttonBottom;
        QCheckBox* ui_showEffects;
        QCheckBox* ui_showTiles;
        QCheckBox* ui_showTrainers;
        QCheckBox* ui_showWarps;
        QCheckBox* ui_drawMask;
        QGraphicsView* ui_view;
};
}
}

#endif
