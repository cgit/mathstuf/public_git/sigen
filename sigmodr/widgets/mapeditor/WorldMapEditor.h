/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_WORLDMAPEDITOR
#define SIGMODRWIDGETS_WORLDMAPEDITOR

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtGui/QWidget>

// Forward declarations
class QCheckBox;
class QGraphicsView;
class KDoubleNumInput;
class KIntNumInput;
namespace Sigmod
{
class Game;
}

namespace Sigmodr
{
namespace Widgets
{
class MapGrid;
class WorldMapScene;

class SIGMODRWIDGETS_NO_EXPORT WorldMapEditor : public QWidget
{
    Q_OBJECT
    
    public:
        WorldMapEditor(Sigmod::Game* game, QWidget* parent);
        
        void setGame(Sigmod::Game* game);
        
        void reset();
    signals:
        void changed();
    protected slots:
        void setGridSize(const int gridSize);
        void setZoom(const double zoom);
        
        void resizeGrid(const QRectF& rect);
    protected:
        void makeConnections();
    private:
        Sigmod::Game* m_game;
        WorldMapScene* m_scene;
        MapGrid* m_grid;
        
        KIntNumInput* ui_gridSize;
        KDoubleNumInput* ui_zoom;
        QCheckBox* ui_drawMasked;
        QGraphicsView* ui_view;
};
}
}

#endif
