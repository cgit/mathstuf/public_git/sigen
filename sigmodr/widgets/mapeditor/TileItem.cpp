/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TileItem.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/MapTile.h>
#include <sigmod/Sprite.h>
#include <sigmod/Tile.h>

// KDE includes
#include <KColorScheme>

// Qt includes
#include <QtGui/QBitmap>
#include <QtGui/QPainter>

// Standard includes
#include <climits>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

TileItem::TileItem(Sigmod::Map* map, MapTile* tile, QGraphicsScene* parent) :
        MapItem(map, parent),
        m_tile(tile),
        m_tileIndex(INT_MIN),
        m_mask(false)
{
    connect(m_tile, SIGNAL(changed()), this, SIGNAL(changed()));
    connect(m_tile, SIGNAL(changed()), this, SLOT(tileChanged()));
    connect(m_tile, SIGNAL(error(QString)), this, SLOT(tileChanged()));
    setZValue(m_tile->zIndex());
    m_tag->setText(QString::number(m_tile->id()));
    tileChanged();
}

QRectF TileItem::boundingRect() const
{
    return QRect(QPoint(), m_pixmap.isNull() ? QSize(32, 32) : m_pixmap.size()).adjusted(-3, -3, 3, 3);
}

void TileItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    if (m_pixmap.isNull())
    {
        QPainterPath path;
        path.moveTo(8, 24);
        path.lineTo(24, 8);
        path.moveTo(8, 8);
        path.lineTo(24, 24);
        painter->setPen(QPen(Qt::red, 5));
        painter->setBrush(Qt::NoBrush);
        painter->drawPath(path);
    }
    else
        painter->drawPixmap(0, 0, m_mask ? m_masked : m_pixmap);
    painter->setBrush(KStatefulBrush(KColorScheme::Selection, KColorScheme::HoverColor).brush(QPalette::Active));
    painter->setOpacity(.25);
    MapItem::paint(painter, option, widget);
}

void TileItem::setZIndex(const int zIndex)
{
    m_tile->setZIndex(zIndex);
}

int TileItem::type() const
{
    return Type;
}

void TileItem::drawCollisionMask(const bool mask)
{
    m_mask = mask;
    update();
}

void TileItem::setSprite(const int spriteId, const bool walkable)
{
    if (spriteId == m_tileIndex)
        return;
    const Sprite* sprite = m_tile->game()->spriteById(spriteId);
    if (!sprite || !m_pixmap.loadFromData(sprite->sprite()))
    {
        m_pixmap = QPixmap();
        m_masked = QPixmap();
    }
    if (!m_pixmap.isNull())
    {
        if (walkable)
        {
            m_masked = QPixmap(m_pixmap.size());
            m_masked.fill(Qt::color0);
        }
        else
        {
            m_masked = m_pixmap.alphaChannel();
            m_masked.setMask(m_masked.createMaskFromColor(Qt::black));
            m_masked.fill(Qt::color1);
        }
    }
    prepareGeometryChange();
    m_tileIndex = spriteId;
}

void TileItem::moveTo(const QPoint& point)
{
    m_tile->setPosition(point);
}

void TileItem::tileChanged()
{
    const Tile* tile = m_tile->game()->tileById(m_tile->tile());
    if (tile)
        setSprite(tile->preview(), tile->walkable());
    setPos(m_tile->position());
    setZValue(m_tile->zIndex());
    resetLabel();
    update();
}

void TileItem::resetLabel()
{
    const Tile* tile = m_tile->game()->tileById(m_tile->tile());
    if (tile)
    {
        m_label->setText(tile->name());
        QSizeF size = m_label->boundingRect().size() / 2 - (m_pixmap.isNull() ? QSize(32, 32) : m_pixmap.size()) / 2;
        m_label->setPos(-size.width(), -size.height());
    }
}
