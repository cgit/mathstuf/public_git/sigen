/*
* Copyright 2009 Ben Boeckel <MathStuf@gmail.com>
* 
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
* 
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License along
* with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SIGMODRWIDGETS_SHAPEITEM
#define SIGMODRWIDGETS_SHAPEITEM

// Sigmodr widget includes
#include "../Global.h"

// Qt includes
#include <QtGui/QGraphicsItem>

namespace Sigmodr
{
namespace Widgets
{
class ShapeItem : public QObject, public QGraphicsItem
{
    Q_OBJECT
    
    public:
        enum Shape
        {
            Path = 0,
            Rectangle = 1,
            PolyLine = 2,
            Polygon = 3,
            Ellipse = 4,
            Arc = 5,
            Chord = 6,
            Pie = 7
        };
        
        ShapeItem(const Shape shape);
        ShapeItem(const QPainterPath& path);
        
        Shape shapeType() const;
        QWidget* widget();
        
        void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);
        QRectF boundingRect() const;
        QPainterPath shape() const;
    protected:
        void contextMenuEvent(QGraphicsSceneContextMenuEvent* event);
        void focusInEvent(QFocusEvent* event);
        void focusOutEvent(QFocusEvent* event);
        void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
        void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
        void mousePressEvent(QGraphicsSceneMouseEvent* event);
        void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
        void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
        void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event);
    protected slots:
        void setPosition(const QPointF& position);
        void setWidth(const int width);
        void setHeight(const int height);
        void addPoint();
        void removePoint();
        void setStartAngle(const double startAngle);
        void setSpanAngle(const double spanAngle);
        void setThickness(const double thickness);
        void setRotation(const double rotation);
        void setXShear(const double xShear);
        void setYShear(const double yShear);
    private:
        void recreatePath();
        void restroke();
        
        const Shape m_shape;
        QPainterPath m_path;
        QPainterPath m_strokePath;
        QRectF m_boundingRect;
        QVector<QPointF> m_points;
        qreal m_startAngle;
        qreal m_spanAngle;
        QRectF m_rect;
        qreal m_thickness;
        qreal m_rotation;
        qreal m_xShear;
        qreal m_yShear;
};
}
}

#endif
