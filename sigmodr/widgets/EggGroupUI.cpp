/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "EggGroupUI.h"
#include "EggGroupUI_p.h"

// Sigmod includes
#include <sigmod/EggGroup.h>

// KDE includes
#include <KLineEdit>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

EggGroupUI::EggGroupUI(EggGroup* eggGroup, QWidget* parent) :
        ObjectUI(eggGroup, parent),
        d(new Private(new EggGroup(*eggGroup)))
{
    setPrivate(d);
}

void EggGroupUI::apply()
{
    *qobject_cast<EggGroup*>(m_object) = *d->m_eggGroup;
    ObjectUI::apply();
}

void EggGroupUI::discard()
{
    *d->m_eggGroup = *qobject_cast<EggGroup*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

EggGroupUI::Private::Private(EggGroup* eggGroup) :
        ObjectUIPrivate(eggGroup),
        m_eggGroup(eggGroup)
{
}

EggGroupUI::Private::~Private()
{
    delete m_eggGroup;
}

QWidget* EggGroupUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/egggroup.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    return form;
}

void EggGroupUI::Private::resetGui()
{
    ui_name->setText(m_eggGroup->name());
}

void EggGroupUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_eggGroup->setName(name);
    ui_name->setCursorPosition(cursor);
}
