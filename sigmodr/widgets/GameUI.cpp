/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "GameUI.h"
#include "GameUI_p.h"

// Sigmodr widget includes
#include "TypechartModel.h"
#include "mapeditor/WorldMapEditor.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Map.h>
#include <sigmod/MapWarp.h>
#include <sigmod/Type.h>

// KDE includes
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>
#include <QtGui/QLabel>
#include <QtGui/QTableView>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

GameUI::GameUI(Game* game, QWidget* parent) :
        ObjectUI(game, parent),
        d(new Private(new Game(*game)))
{
    setPrivate(d);
}

void GameUI::apply()
{
    *qobject_cast<Game*>(m_object) = *d->m_game;
    ObjectUI::apply();
}

void GameUI::discard()
{
    *d->m_game = *qobject_cast<Game*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

GameUI::Private::Private(Game* game) :
        ObjectUIPrivate(game),
        m_game(game)
{
}

GameUI::Private::~Private()
{
    delete m_game;
}

QWidget* GameUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/game.ui", widget);
    ui_title = form->findChild<KLineEdit*>("varTitle");
    ui_version = form->findChild<KLineEdit*>("varVersion");
    ui_description = form->findChild<KLineEdit*>("varDescription");
    ui_singlePlayer = form->findChild<QCheckBox*>("varSinglePlayer");
    ui_startScript = form->findChild<ScriptWidget*>("varStartScript");
    ui_typechart = form->findChild<QTableView*>("varTypechart");
    ui_labelTypes = form->findChild<QLabel*>("labelTypes");
    ui_effectiveness = form->findChild<FractionWidget*>("varEffectiveness");
    QGridLayout* layout = form->findChild<QGridLayout*>("worldMapLayout");
    ui_worldMapEditor = new WorldMapEditor(m_game, widget);
    layout->addWidget(ui_worldMapEditor, 0, 0);
    widget->setTabOrder(ui_effectiveness, ui_worldMapEditor);
    connect(ui_title, SIGNAL(textChanged(QString)), this, SLOT(titleChanged(QString)));
    connect(ui_version, SIGNAL(textChanged(QString)), this, SLOT(versionChanged(QString)));
    connect(ui_description, SIGNAL(textChanged(QString)), this, SLOT(descriptionChanged(QString)));
    connect(ui_singlePlayer, SIGNAL(toggled(bool)), this, SLOT(singlePlayerChanged(bool)));
    connect(ui_startScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(startScriptChanged(Sigcore::Script)));
    connect(ui_typechart, SIGNAL(activated(QModelIndex)), this, SLOT(typechartChanged(QModelIndex)));
    connect(ui_effectiveness, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(effectivenessChanged(Sigcore::Fraction)));
    return form;
}

void GameUI::Private::refreshGui()
{
    delete ui_typechart->model();
    QStringList types;
    for (int i = 0; i < m_game->typeCount(); ++i)
        types << m_game->type(i)->name();
    delete ui_typechart->model();
    ui_typechart->setModel(new TypechartModel(m_game->typechart(), types));
    ui_effectiveness->setEnabled(false);
    ObjectUIPrivate::refreshGui();
}

void GameUI::Private::resetGui()
{
    ui_title->setText(m_game->title());
    ui_version->setText(m_game->version());
    ui_description->setText(m_game->description());
    ui_singlePlayer->setCheckState(m_game->singlePlayer() ? Qt::Checked : Qt::Unchecked);
    ui_startScript->setValue(m_game->startScript());
    ui_worldMapEditor->setGame(m_game);
}

void GameUI::Private::titleChanged(const QString& title)
{
    const int cursor = ui_title->cursorPosition();
    m_game->setTitle(title);
    ui_title->setCursorPosition(cursor);
}

void GameUI::Private::versionChanged(const QString& version)
{
    const int cursor = ui_version->cursorPosition();
    m_game->setVersion(version);
    ui_version->setCursorPosition(cursor);
}

void GameUI::Private::descriptionChanged(const QString& description)
{
    const int cursor = ui_description->cursorPosition();
    m_game->setDescription(description);
    ui_description->setCursorPosition(cursor);
}

void GameUI::Private::singlePlayerChanged(const bool singlePlayer)
{
    m_game->setSinglePlayer(singlePlayer);
}

void GameUI::Private::startScriptChanged(const Script& startScript)
{
    m_game->setStartScript(startScript);
}

void GameUI::Private::typechartChanged(const QModelIndex& index)
{
    ui_effectiveness->setEnabled(true);
    ui_labelTypes->setText(QString("%1 vs. %2").arg(m_game->type(index.row())->name()).arg(m_game->type(index.column())->name()));
    ui_effectiveness->setValue(ui_typechart->model()->data(index, Qt::EditRole).value<Fraction>());
}

void GameUI::Private::effectivenessChanged(const Fraction& multiplier)
{
    ui_typechart->model()->setData(ui_typechart->currentIndex(), QVariant::fromValue(multiplier), Qt::EditRole);
    emit(changed());
}
