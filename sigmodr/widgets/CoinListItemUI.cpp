/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "CoinListItemUI.h"
#include "CoinListItemUI_p.h"

// Sigmod includes
#include <sigmod/CoinListItem.h>
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/Species.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>

// Qt includes
#include <QtGui/QButtonGroup>
#include <QtGui/QRadioButton>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

CoinListItemUI::CoinListItemUI(CoinListItem* item, QWidget* parent) :
        ObjectUI(item, parent),
        d(new Private(new CoinListItem(*item)))
{
    setPrivate(d);
}

void CoinListItemUI::apply()
{
    *qobject_cast<CoinListItem*>(m_object) = *d->m_item;
    ObjectUI::apply();
}

void CoinListItemUI::discard()
{
    *d->m_item = *qobject_cast<CoinListItem*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

CoinListItemUI::Private::Private(CoinListItem* item) :
        ObjectUIPrivate(item),
        m_item(item)
{
}

CoinListItemUI::Private::~Private()
{
    delete m_item;
}

QWidget* CoinListItemUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/coinlistitem.ui", widget);
    QRadioButton* item = form->findChild<QRadioButton*>("varItem");
    QRadioButton* species = form->findChild<QRadioButton*>("varSpecies");
    ui_object = form->findChild<KComboBox*>("varObject");
    ui_cost = form->findChild<KIntNumInput*>("varCost");
    ui_type = new QButtonGroup(widget);
    ui_type->addButton(item, CoinListItem::Item);
    ui_type->addButton(species, CoinListItem::Species);
    connect(ui_type, SIGNAL(buttonClicked(int)), this, SLOT(typeChanged(int)));
    connect(ui_object, SIGNAL(currentIndexChanged(int)), this, SLOT(objectChanged(int)));
    connect(ui_cost, SIGNAL(valueChanged(int)), this, SLOT(costChanged(int)));
    return form;
}

void CoinListItemUI::Private::resetGui()
{
    ui_type->button(m_item->type())->setChecked(true);
    if (m_item->type() == CoinListItem::Item)
        ui_object->setCurrentIndex(m_item->game()->itemIndex(m_item->object()));
    else
        ui_object->setCurrentIndex(m_item->game()->speciesIndex(m_item->object()));
    ui_cost->setValue(m_item->cost());
}

void CoinListItemUI::Private::typeChanged(const int type)
{
    m_item->setType(static_cast<CoinListItem::Type>(type));
    const bool blocked = ui_object->blockSignals(true);
    ui_object->clear();
    if (m_item->type() == CoinListItem::Item)
    {
        for (int i = 0; i < m_item->game()->itemCount(); ++i)
            ui_object->addItem(m_item->game()->item(i)->name());
    }
    else
    {
        for (int i = 0; i < m_item->game()->speciesCount(); ++i)
            ui_object->addItem(m_item->game()->species(i)->name());
    }
    ui_object->blockSignals(blocked);
}

void CoinListItemUI::Private::objectChanged(const int object)
{
    if (0 <= object)
    {
        if (m_item->type() == CoinListItem::Item)
            m_item->setObject(m_item->game()->item(object)->id());
        else
            m_item->setObject(m_item->game()->species(object)->id());
    }
}

void CoinListItemUI::Private::costChanged(const int cost)
{
    m_item->setCost(cost);
}
