/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SkinUI.h"
#include "SkinUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Skin.h>

// KDE includes
#include <KIntNumInput>
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

SkinUI::SkinUI(Skin* skin, QWidget* parent) :
        ObjectUI(skin, parent),
        d(new Private(new Skin(*skin)))
{
    setPrivate(d);
}

void SkinUI::apply()
{
    *qobject_cast<Skin*>(m_object) = *d->m_skin;
    ObjectUI::apply();
}

void SkinUI::discard()
{
    *d->m_skin = *qobject_cast<Skin*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

SkinUI::Private::Private(Skin* skin) :
        ObjectUIPrivate(skin),
        m_skin(skin)
{
}

SkinUI::Private::~Private()
{
    delete m_skin;
}

QWidget* SkinUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/skin.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_width = form->findChild<KIntNumInput*>("varWidth");
    ui_height = form->findChild<KIntNumInput*>("varHeight");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_width, SIGNAL(valueChanged(int)), this, SLOT(widthChanged(int)));
    connect(ui_height, SIGNAL(valueChanged(int)), this, SLOT(heightChanged(int)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void SkinUI::Private::resetGui()
{
    ui_name->setText(m_skin->name());
    ui_script->setValue(m_skin->script());
}

void SkinUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_skin->setName(name);
    ui_name->setCursorPosition(cursor);
}

void SkinUI::Private::widthChanged(const int width)
{
    QSize size = m_skin->size();
    size.setWidth(width);
    m_skin->setSize(size);
}

void SkinUI::Private::heightChanged(const int height)
{
    QSize size = m_skin->size();
    size.setHeight(height);
    m_skin->setSize(size);
}

void SkinUI::Private::scriptChanged(const Script& script)
{
    m_skin->setScript(script);
}
