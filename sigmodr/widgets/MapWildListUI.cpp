/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWildListUI.h"
#include "MapWildListUI_p.h"

// Sigmod includes
#include <sigmod/MapWildList.h>

// KDE includes
#include <KLineEdit>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapWildListUI::MapWildListUI(MapWildList* wildList, QWidget* parent) :
        ObjectUI(wildList, parent),
        d(new Private(new MapWildList(*wildList)))
{
    setPrivate(d);
}

void MapWildListUI::apply()
{
    *qobject_cast<MapWildList*>(m_object) = *d->m_wildList;
    ObjectUI::apply();
}

void MapWildListUI::discard()
{
    *d->m_wildList = *qobject_cast<MapWildList*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapWildListUI::Private::Private(MapWildList* wildList) :
        ObjectUIPrivate(wildList),
        m_wildList(wildList)
{
}

MapWildListUI::Private::~Private()
{
    delete m_wildList;
}

QWidget* MapWildListUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/mapwildlist.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    return form;
}

void MapWildListUI::Private::resetGui()
{
    ui_name->setText(m_wildList->name());
}

void MapWildListUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_wildList->setName(name);
    ui_name->setCursorPosition(cursor);
}
