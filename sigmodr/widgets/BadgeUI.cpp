/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "BadgeUI.h"
#include "BadgeUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Badge.h>
#include <sigmod/Game.h>
#include <sigmod/Rules.h>
#include <sigmod/Sprite.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QHeaderView>
#include <QtGui/QTableWidget>
#include <QtGui/QTableWidgetItem>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

BadgeUI::BadgeUI(Badge* badge, QWidget* parent) :
        ObjectUI(badge, parent),
        d(new Private(new Badge(*badge)))
{
    setPrivate(d);
}

void BadgeUI::apply()
{
    *qobject_cast<Badge*>(m_object) = *d->m_badge;
    ObjectUI::apply();
}

void BadgeUI::discard()
{
    *d->m_badge = *qobject_cast<Badge*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

BadgeUI::Private::Private(Badge* badge) :
        ObjectUIPrivate(badge),
        m_badge(badge)
{
}

BadgeUI::Private::~Private()
{
    delete m_badge;
}

QWidget* BadgeUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/badge.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_obey= form->findChild<KIntNumInput*>("varObey");
    ui_face = form->findChild<KComboBox*>("varFace");
    ui_badge = form->findChild<KComboBox*>("varBadge");
    ui_stat = form->findChild<QTableWidget*>("varStat");
    ui_statMultiplier = form->findChild<FractionWidget*>("varStatMultiplier");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_obey, SIGNAL(valueChanged(int)), this, SLOT(obeyChanged(int)));
    connect(ui_face, SIGNAL(currentIndexChanged(int)), this, SLOT(faceChanged(int)));
    connect(ui_badge, SIGNAL(currentIndexChanged(int)), this, SLOT(badgeChanged(int)));
    connect(ui_stat, SIGNAL(currentCellChanged(int,int,int,int)), this, SLOT(statChanged(int)));
    connect(ui_statMultiplier, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(statMultiplierChanged(Sigcore::Fraction)));
    ui_stat->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    return form;
}

void BadgeUI::Private::refreshGui()
{
    int maxHeight = 0;
    int maxWidth = 0;
    ui_obey->setMaximum(m_badge->game()->rules()->maxLevel());
    const bool blockedFace = ui_face->blockSignals(true);
    const bool blockedBadge = ui_badge->blockSignals(true);
    ui_face->clear();
    ui_badge->clear();
    for (int i = 0; i < m_badge->game()->spriteCount(); ++i)
    {
        const Sprite* sprite = m_badge->game()->sprite(i);
        QPixmap icon;
        icon.loadFromData(sprite->sprite());
        maxHeight = qMax(maxHeight, icon.height());
        maxWidth = qMax(maxWidth, icon.width());
        ui_face->addItem(icon, sprite->name());
        ui_badge->addItem(icon, sprite->name());
    }
    ui_face->blockSignals(blockedFace);
    ui_badge->blockSignals(blockedBadge);
    const QSize maxSize(maxWidth, maxHeight);
    ui_face->setIconSize(maxSize);
    ui_badge->setIconSize(maxSize);
    const bool isSplit = m_badge->game()->rules()->specialSplit();
    ui_stat->clear();
    ui_stat->setRowCount((isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1);
    ui_stat->setVerticalHeaderLabels((isSplit ? StatGSCStr : StatRBYStr).mid(ST_Attack, (isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1));
    ui_stat->setColumnCount(1);
    ui_stat->setHorizontalHeaderLabels(QStringList() << "Multiplier");
    ui_stat->verticalHeaderItem(ST_Attack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Attack));
    ui_stat->verticalHeaderItem(ST_Defense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Defense));
    ui_stat->verticalHeaderItem(ST_Speed - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Speed));
    if (isSplit)
    {
        ui_stat->verticalHeaderItem(ST_SpecialAttack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialAttack));
        ui_stat->verticalHeaderItem(ST_SpecialDefense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialDefense));
    }
    else
        ui_stat->verticalHeaderItem(ST_Special - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Special));
    ui_statMultiplier->setEnabled(false);
    QTableWidgetItem* item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Attack - ST_Attack, 0, item);
    ui_stat->setCurrentItem(item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Defense- ST_Attack, 0, item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Speed - ST_Attack, 0, item);
    if (isSplit)
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_SpecialAttack - ST_Attack, 0, item);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_SpecialDefense - ST_Attack, 0, item);
    }
    else
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_Special - ST_Attack, 0, item);
    }
    ObjectUIPrivate::refreshGui();
}

void BadgeUI::Private::resetGui()
{
    ui_name->setText(m_badge->name());
    ui_obey->setValue(m_badge->obey());
    ui_face->setCurrentIndex(m_badge->game()->spriteIndex(m_badge->face()));
    ui_badge->setCurrentIndex(m_badge->game()->spriteIndex(m_badge->badge()));
    ui_stat->item(ST_Attack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_Attack), 'g', 7));
    ui_stat->item(ST_Defense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_Defense), 'g', 7));
    ui_stat->item(ST_Speed - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_Speed), 'g', 7));
    if (m_badge->game()->rules()->specialSplit())
    {
        ui_stat->item(ST_SpecialAttack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_SpecialAttack), 'g', 7));
        ui_stat->item(ST_SpecialDefense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_SpecialDefense), 'g', 7));
    }
    else
        ui_stat->item(ST_Special - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_badge->stat(ST_Special), 'g', 7));
}

void BadgeUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_badge->setName(name);
    ui_name->setCursorPosition(cursor);
}

void BadgeUI::Private::obeyChanged(const int obey)
{
    m_badge->setObey(obey);
}

void BadgeUI::Private::faceChanged(const int face)
{
    if (0 <= face)
        m_badge->setFace(m_badge->game()->sprite(face)->id());
}

void BadgeUI::Private::badgeChanged(const int badge)
{
    if (0 <= badge)
        m_badge->setBadge(m_badge->game()->sprite(badge)->id());
}

void BadgeUI::Private::statChanged(const int row)
{
    ui_statMultiplier->setEnabled(true);
    ui_statMultiplier->setValue(m_badge->stat(ui_stat->verticalHeaderItem(row)->data(Qt::UserRole).value<Stat>()));
}

void BadgeUI::Private::statMultiplierChanged(const Fraction& multiplier)
{
    m_badge->setStat(ui_stat->verticalHeaderItem(ui_stat->currentRow())->data(Qt::UserRole).value<Stat>(), multiplier);
    emit(changed());
}
