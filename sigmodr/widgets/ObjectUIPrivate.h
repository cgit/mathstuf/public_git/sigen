/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_OBJECTUIPRIVATE
#define SIGMODRWIDGETS_OBJECTUIPRIVATE

// Sigmodr widget includes
#include "Global.h"

// Qt includes
#include <QtCore/QObject>

// Forward declarations
namespace Sigmod
{
class Object;
}

namespace Sigmodr
{
namespace Widgets
{
class ObjectUI;

class SIGMODRWIDGETS_NO_EXPORT ObjectUIPrivate : public QObject
{
    Q_OBJECT
    
    public:
        ObjectUIPrivate(Sigmod::Object* object);
        virtual ~ObjectUIPrivate();
        
        virtual QWidget* makeWidgets(ObjectUI* widget) = 0;
    public slots:
        virtual void refreshGui();
        virtual void resetGui() = 0;
    signals:
        bool changed();
    protected:
        QWidget* openUiFile(const QString& filename, ObjectUI* parent);
    protected slots:
        void errorMessage(const QString& message);
        void warningMessage(const QString& message);
    private:
        void makeConnections(ObjectUI* widget);
};
}
}

#endif
