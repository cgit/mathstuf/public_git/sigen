/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPWARPUI_P
#define SIGMODRWIDGETS_MAPWARPUI_P

// Header include
#include "MapWarpUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KLineEdit;
namespace Sigcore
{
class Script;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class ScriptWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MapWarpUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::MapWarp* warp);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::MapWarp* m_warp;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void toMapChanged(const int toMap);
        void toWarpChanged(const int toWarp);
        void scriptChanged(const Sigcore::Script& script);
    private:
        KLineEdit* ui_name;
        KComboBox* ui_toMap;
        KComboBox* ui_toWarp;
        CoreWidgets::ScriptWidget* ui_script;
        
        int m_lastMap;
};
}
}

#endif
