/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TypeUI.h"
#include "TypeUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>

// Sigmod includes
#include <sigmod/Type.h>

// KDE includes
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

TypeUI::TypeUI(Type* type, QWidget* parent) :
        ObjectUI(type, parent),
        d(new Private(new Type(*type)))
{
    setPrivate(d);
}

void TypeUI::apply()
{
    *qobject_cast<Type*>(m_object) = *d->m_type;
    ObjectUI::apply();
}

void TypeUI::discard()
{
    *d->m_type = *qobject_cast<Type*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

TypeUI::Private::Private(Type* type) :
        ObjectUIPrivate(type),
        m_type(type)
{
}

TypeUI::Private::~Private()
{
    delete m_type;
}

QWidget* TypeUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/type.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_stab = form->findChild<FractionWidget*>("varSTAB");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_stab, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(stabChanged(Sigcore::Fraction)));
    return form;
}

void TypeUI::Private::resetGui()
{
    ui_name->setText(m_type->name());
    ui_stab->setValue(m_type->stab());
}

void TypeUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_type->setName(name);
    ui_name->setCursorPosition(cursor);
}

void TypeUI::Private::stabChanged(const Fraction& stab)
{
    m_type->setStab(stab);
}
