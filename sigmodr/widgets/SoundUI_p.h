/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_SOUNDUI_P
#define SIGMODRWIDGETS_SOUNDUI_P

// Header include
#include "SoundUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Phonon includes
#include <Phonon/Global>

// Forward declarations
class KLineEdit;
class KPushButton;
class QBuffer;
class QLabel;
namespace Phonon
{
class AudioOutput;
class MediaObject;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT SoundUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Sound* sound);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Sound* m_sound;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void browse();
        
        void stateChanged(Phonon::State newState);
        void resetAudioData();
        void tick(qint64 time);
    private:
        KLineEdit* ui_name;
        QLabel* ui_time;
        KPushButton* ui_play;
        KPushButton* ui_stop;
        
        Phonon::AudioOutput* m_output;
        Phonon::MediaObject* m_media;
        QBuffer* m_buffer;
};
}
}

#endif
