/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MOVEUI_P
#define SIGMODRWIDGETS_MOVEUI_P

// Header include
#include "MoveUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KIntNumInput;
class KLineEdit;
class QCheckBox;
namespace Sigcore
{
class Fraction;
class Script;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
class ScriptWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MoveUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Move* move);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Move* m_move;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void priorityChanged(const int priority);
        void accuracyChanged(const Sigcore::Fraction& accuracy);
        void powerChanged(const int power);
        void typeChanged(const int type);
        void powerPointsChanged(const int powerPoints);
        void specialChanged(const bool special);
        void descriptionChanged(const QString& description);
        void battleScriptChanged(const Sigcore::Script& battleScript);
        void worldScriptChanged(const Sigcore::Script& worldScript);
        void priorityScriptChanged(const Sigcore::Script& priorityScript);
    private:
        KLineEdit* ui_name;
        KIntNumInput* ui_priority;
        CoreWidgets::FractionWidget* ui_accuracy;
        KIntNumInput* ui_power;
        KComboBox* ui_type;
        KIntNumInput* ui_powerPoints;
        QCheckBox* ui_special;
        KLineEdit* ui_description;
        CoreWidgets::ScriptWidget* ui_battleScript;
        CoreWidgets::ScriptWidget* ui_worldScript;
        CoreWidgets::ScriptWidget* ui_priorityScript;
};
}
}

#endif
