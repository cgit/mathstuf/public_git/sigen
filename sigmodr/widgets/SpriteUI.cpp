/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpriteUI.h"
#include "SpriteUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Sprite.h>

// KDE includes
#include <KFileDialog>
#include <KImageFilePreview>
#include <KLineEdit>
#include <KMessageBox>
#include <KPushButton>
#include <KIO/NetAccess>

// Qt includes
#include <QtCore/QBuffer>
#include <QtGui/QLabel>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

SpriteUI::SpriteUI(Sprite* sprite, QWidget* parent) :
        ObjectUI(sprite, parent),
        d(new Private(new Sprite(*sprite)))
{
    setPrivate(d);
}

void SpriteUI::apply()
{
    *qobject_cast<Sprite*>(m_object) = *d->m_sprite;
    ObjectUI::apply();
}

void SpriteUI::discard()
{
    *d->m_sprite = *qobject_cast<Sprite*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

SpriteUI::Private::Private(Sprite* sprite) :
        ObjectUIPrivate(sprite),
        m_sprite(sprite)
{
}

SpriteUI::Private::~Private()
{
    delete m_sprite;
}

QWidget* SpriteUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/sprite.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_sprite = form->findChild<QLabel*>("varSprite");
    KPushButton* buttonBrowse = form->findChild<KPushButton*>("buttonBrowse");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(buttonBrowse, SIGNAL(clicked(bool)), this, SLOT(browse()));
    buttonBrowse->setIcon(KIcon("document-open"));
    return form;
}

void SpriteUI::Private::resetGui()
{
    ui_name->setText(m_sprite->name());
    QPixmap icon;
    icon.loadFromData(m_sprite->sprite());
    ui_sprite->setPixmap(icon);
}

void SpriteUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_sprite->setName(name);
    ui_name->setCursorPosition(cursor);
}

void SpriteUI::Private::browse()
{
    KFileDialog* dialog = new KFileDialog(KUrl("kfiledialog:///image"), "image/png image/jpeg image/bmp image/gif", NULL);
    dialog->setCaption("Use Image File");
    dialog->setOperationMode(KFileDialog::Opening);
    dialog->setPreviewWidget(new KImageFilePreview);
    if (dialog->exec() == QDialog::Accepted)
    {
        KUrl url = dialog->selectedFile();
        if (url.isValid())
        {
            QString path;
            bool load = true;
            bool removeTempFile = false;
            if (url.isLocalFile())
                path = url.path();
            else
            {
                if (KIO::NetAccess::download(url, path, NULL))
                    removeTempFile = true;
                else
                {
                    KMessageBox::error(NULL, KIO::NetAccess::lastErrorString(), "KIO Error");
                    load = false;
                }
            }
            if (load)
            {
                QPixmap image(url.path());
                QByteArray bytes;
                QBuffer buffer(&bytes);
                buffer.open(QIODevice::WriteOnly);
                image.save(&buffer, "PNG");
                ui_sprite->setPixmap(image);
                m_sprite->setSprite(bytes);
            }
            if (removeTempFile)
                KIO::NetAccess::removeTempFile(path);
        }
        else
            KMessageBox::error(NULL, "The URL is not valid", "Malformed URL");
    }
    delete dialog;
}
