/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SoundUI.h"
#include "SoundUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Sound.h>

// KDE includes
#include <KFileDialog>
#include <KLineEdit>
#include <KMessageBox>
#include <KPushButton>
#include <KUrl>
#include <KIO/NetAccess>

// Phonon includes
#include <Phonon/AudioOutput>
#include <Phonon/MediaObject>
#include <Phonon/SeekSlider>
#include <Phonon/VolumeSlider>

// Qt includes
#include <QtCore/QBuffer>
#include <QtCore/QTime>
#include <QtGui/QLabel>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

SoundUI::SoundUI(Sound* sound, QWidget* parent) :
        ObjectUI(sound, parent),
        d(new Private(new Sound(*sound)))
{
    setPrivate(d);
}

void SoundUI::apply()
{
    *qobject_cast<Sound*>(m_object) = *d->m_sound;
    ObjectUI::apply();
}

void SoundUI::discard()
{
    *d->m_sound = *qobject_cast<Sound*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

SoundUI::Private::Private(Sound* sound) :
        ObjectUIPrivate(sound),
        m_sound(sound),
        m_output(new Phonon::AudioOutput(Phonon::MusicCategory, this)),
        m_media(new Phonon::MediaObject(this)),
        m_buffer(new QBuffer)
{
}

SoundUI::Private::~Private()
{
    delete m_sound;
    delete m_media;
    delete m_buffer;
}

QWidget* SoundUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/sound.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    Phonon::SeekSlider* seeker = form->findChild<Phonon::SeekSlider*>("seeker");
    ui_time = form->findChild<QLabel*>("label");
    ui_play = form->findChild<KPushButton*>("buttonPlay");
    ui_stop = form->findChild<KPushButton*>("buttonStop");
    Phonon::VolumeSlider* sliderVolume = form->findChild<Phonon::VolumeSlider*>("sliderVolume");
    KPushButton* buttonBrowse = form->findChild<KPushButton*>("buttonBrowse");
    connect(m_media, SIGNAL(tick(qint64)), this, SLOT(tick(qint64)));
    connect(m_media, SIGNAL(stateChanged(Phonon::State, Phonon::State)), this, SLOT(stateChanged(Phonon::State)));
    connect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
    connect(ui_stop, SIGNAL(clicked()), m_media, SLOT(stop()));
    Phonon::createPath(m_media, m_output);
    m_media->setTickInterval(100);
    seeker->setMediaObject(m_media);
    sliderVolume->setAudioOutput(m_output);
    ui_play->setIcon(KIcon("media-playback-start"));
    ui_stop->setIcon(KIcon("media-playback-stop"));
    buttonBrowse->setIcon(KIcon("document-open"));
    return form;
}

void SoundUI::Private::refreshGui()
{
    resetAudioData();
    ObjectUIPrivate::refreshGui();
}

void SoundUI::Private::resetGui()
{
    ui_name->setText(m_sound->name());
}

void SoundUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_sound->setName(name);
    ui_name->setCursorPosition(cursor);
}

void SoundUI::Private::browse()
{
    KFileDialog* dialog = new KFileDialog(KUrl("kfiledialog:///audio"), "audio/*|Audio Files", NULL);
    dialog->setCaption("Use Audio File");
    dialog->setOperationMode(KFileDialog::Opening);
    if (dialog->exec() == QDialog::Accepted)
    {
        KUrl url = dialog->selectedFile();
        if (url.isValid())
        {
            QString path;
            bool load = true;
            bool removeTempFile = false;
            if (url.isLocalFile())
                path = url.path();
            else
            {
                if (KIO::NetAccess::download(url, path, NULL))
                    removeTempFile = true;
                else
                {
                    KMessageBox::error(NULL, KIO::NetAccess::lastErrorString(), "KIO Error");
                    load = false;
                }
            }
            if (load)
            {
                QFile file(url.path());
                file.open(QIODevice::ReadOnly);
                m_sound->setData(file.readAll());
                file.close();
                resetAudioData();
            }
            if (removeTempFile)
                KIO::NetAccess::removeTempFile(path);
        }
        else
            KMessageBox::error(NULL, "The URL is not valid", "Malformed URL");
    }
    delete dialog;
}

void SoundUI::Private::stateChanged(Phonon::State newState)
{
    switch (newState)
    {
        case Phonon::ErrorState:
            if (m_media->errorType() == Phonon::FatalError)
                KMessageBox::warningContinueCancel(NULL, "Fatal Error", m_media->errorString());
            else
                KMessageBox::warningContinueCancel(NULL, "Error", m_media->errorString());
            break;
        case Phonon::PlayingState:
            disconnect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
            connect(ui_play, SIGNAL(clicked()), m_media, SLOT(pause()));
            ui_play->setIcon(KIcon("media-playback-pause"));
            ui_stop->setEnabled(true);
            break;
        case Phonon::StoppedState:
            disconnect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
            disconnect(ui_play, SIGNAL(clicked()), m_media, SLOT(pause()));
            connect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
            ui_play->setIcon(KIcon("media-playback-start"));
            ui_stop->setEnabled(false);
            tick(0);
            break;
        case Phonon::PausedState:
            disconnect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
            disconnect(ui_play, SIGNAL(clicked()), m_media, SLOT(pause()));
            connect(ui_play, SIGNAL(clicked()), m_media, SLOT(play()));
            ui_play->setIcon(KIcon("media-playback-start"));
            ui_stop->setEnabled(true);
            break;
        default:
            break;
    }
}

void SoundUI::Private::resetAudioData()
{
    m_media->stop();
    m_buffer->setData(m_sound->data());
    m_media->setCurrentSource(m_buffer);
    tick(0);
}

void SoundUI::Private::tick(qint64 time)
{
    QTime currentTime(0, (time / 60000) % 60, (time / 1000) % 60, time % 1000);
    qint64 total = ((m_media->state() == Phonon::BufferingState) ? 0 : m_media->totalTime());
    QTime totalTime(0, (total / 60000) % 60, (total / 1000) % 60, total % 1000);
    ui_time->setText(QString("%1 / %2").arg(currentTime.toString("mm:ss.zzz")).arg(totalTime.toString("mm:ss.zzz")));
}
