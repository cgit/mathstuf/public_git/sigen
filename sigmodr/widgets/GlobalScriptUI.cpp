/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "GlobalScriptUI.h"
#include "GlobalScriptUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/GlobalScript.h>

// KDE includes
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

GlobalScriptUI::GlobalScriptUI(GlobalScript* globalScript, QWidget* parent) :
        ObjectUI(globalScript, parent),
        d(new Private(new GlobalScript(*globalScript)))
{
    setPrivate(d);
}

void GlobalScriptUI::apply()
{
    *qobject_cast<GlobalScript*>(m_object) = *d->m_globalScript;
    ObjectUI::apply();
}

void GlobalScriptUI::discard()
{
    *d->m_globalScript = *qobject_cast<GlobalScript*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

GlobalScriptUI::Private::Private(GlobalScript* globalScript) :
        ObjectUIPrivate(globalScript),
        m_globalScript(globalScript)
{
}

GlobalScriptUI::Private::~Private()
{
    delete m_globalScript;
}

QWidget* GlobalScriptUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/globalscript.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void GlobalScriptUI::Private::resetGui()
{
    ui_name->setText(m_globalScript->name());
    ui_script->setValue(m_globalScript->script());
}

void GlobalScriptUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_globalScript->setName(name);
    ui_name->setCursorPosition(cursor);
}

void GlobalScriptUI::Private::scriptChanged(const Script& script)
{
    m_globalScript->setScript(script);
}
