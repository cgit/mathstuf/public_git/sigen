/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_MAPTRAINERTEAMMEMBERUI_P
#define SIGMODRWIDGETS_MAPTRAINERTEAMMEMBERUI_P

// Header include
#include "MapTrainerTeamMemberUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KActionSelector;
class KComboBox;
class KIntNumInput;
class QGroupBox;
class QListWidgetItem;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT MapTrainerTeamMemberUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::MapTrainerTeamMember* teamMember);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::MapTrainerTeamMember* m_teamMember;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void speciesChanged(const int species);
        void levelChanged(const int level);
        void abilityAdded(QListWidgetItem* item);
        void abilityRemoved(QListWidgetItem* item);
        void itemAdded(QListWidgetItem* item);
        void itemRemoved(QListWidgetItem* item);
        void itemChanged(QListWidgetItem* item);
        void itemCountChanged(const int itemCount);
        void moveAdded(QListWidgetItem* item);
        void moveRemoved(QListWidgetItem* item);
        void natureAdded(QListWidgetItem* item);
        void natureRemoved(QListWidgetItem* item);
    private:
        KComboBox* ui_species;
        KIntNumInput* ui_level;
        KActionSelector* ui_abilities;
        QGroupBox* ui_boxItems;
        KActionSelector* ui_items;
        KIntNumInput* ui_itemCount;
        KActionSelector* ui_moves;
        KActionSelector* ui_natures;
};
}
}

#endif
