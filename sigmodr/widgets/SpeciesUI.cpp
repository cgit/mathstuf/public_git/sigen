/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "SpeciesUI.h"
#include "SpeciesUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/EggGroup.h>
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/Rules.h>
#include <sigmod/Skin.h>
#include <sigmod/Species.h>
#include <sigmod/Sprite.h>
#include <sigmod/Type.h>

// KDE includes
#include <KActionSelector>
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>
#include <QtGui/QGroupBox>
#include <QtGui/QHeaderView>
#include <QtGui/QListWidget>
#include <QtGui/QTableWidget>
#include <QtGui/QTableWidgetItem>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

SpeciesUI::SpeciesUI(Species* species, QWidget* parent) :
        ObjectUI(species, parent),
        d(new Private(new Species(*species)))
{
    setPrivate(d);
}

void SpeciesUI::apply()
{
    *qobject_cast<Species*>(m_object) = *d->m_species;
    ObjectUI::apply();
}

void SpeciesUI::discard()
{
    *d->m_species = *qobject_cast<Species*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

SpeciesUI::Private::Private(Species* species) :
        ObjectUIPrivate(species),
        m_species(species)
{
}

SpeciesUI::Private::~Private()
{
    delete m_species;
}

QWidget* SpeciesUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/species.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_baseStat = form->findChild<QTableWidget*>("varBaseStat");
    ui_baseStatValue = form->findChild<KIntNumInput*>("varBaseStatValue");
    ui_boxEffortValues = form->findChild<QGroupBox*>("boxEffortValues");
    ui_effortValue = form->findChild<QTableWidget*>("varEffortValue");
    ui_effortValueValue = form->findChild<KIntNumInput*>("varEffortValueValue");
    ui_growth = form->findChild<KComboBox*>("varGrowth");
    ui_experienceValue = form->findChild<KIntNumInput*>("varExperienceValue");
    ui_catchValue = form->findChild<KIntNumInput*>("varCatchValue");
    ui_maxHoldWeight = form->findChild<KIntNumInput*>("varMaxHoldWeight");
    ui_runChance = form->findChild<FractionWidget*>("varRunChance");
    ui_fleeChance = form->findChild<FractionWidget*>("varFleeChance");
    ui_itemChance = form->findChild<FractionWidget*>("varItemChance");
    ui_encyclopediaNumber = form->findChild<KIntNumInput*>("varEncyclopediaNumber");
    ui_weight = form->findChild<KIntNumInput*>("varWeight");
    ui_height = form->findChild<KIntNumInput*>("varHeight");
    ui_encyclopediaEntry = form->findChild<KLineEdit*>("varEncyclopediaEntry");
    ui_maleFront = form->findChild<KComboBox*>("varMaleFront");
    ui_maleBack = form->findChild<KComboBox*>("varMaleBack");
    ui_femaleFront = form->findChild<KComboBox*>("varFemaleFront");
    ui_femaleBack = form->findChild<KComboBox*>("varFemaleBack");
    ui_skin = form->findChild<KComboBox*>("varSkin");
    ui_genetics = form->findChild<QWidget*>("tabGenetics");
    ui_hasGender = form->findChild<QCheckBox*>("varHasGender");
    ui_genderChance = form->findChild<FractionWidget*>("varGenderChance");
    ui_boxEggGroups = form->findChild<QGroupBox*>("boxEggGroups");
    ui_eggSpecies = form->findChild<KComboBox*>("varEggSpecies");
    ui_eggSteps = form->findChild<KIntNumInput*>("varEggSteps");
    ui_evolution = form->findChild<ScriptWidget*>("varEvolution");
    ui_types = form->findChild<KActionSelector*>("varTypes");
    ui_eggGroups = form->findChild<KActionSelector*>("varEggGroups");
    ui_boxAbilities = form->findChild<QGroupBox*>("boxAbilities");
    ui_abilities = form->findChild<KActionSelector*>("varAbilities");
    ui_abilityWeight = form->findChild<KIntNumInput*>("varAbilityWeight");
    ui_boxItems = form->findChild<QGroupBox*>("boxItems");
    ui_items = form->findChild<KActionSelector*>("varItems");
    ui_itemWeight = form->findChild<KIntNumInput*>("varItemWeight");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_baseStat, SIGNAL(currentCellChanged(int,int,int,int)), this, SLOT(baseStatChanged(int)));
    connect(ui_baseStatValue, SIGNAL(valueChanged(int)), this, SLOT(baseStatValueChanged(int)));
    connect(ui_effortValue, SIGNAL(currentCellChanged(int,int,int,int)), this, SLOT(effortValueChanged(int)));
    connect(ui_effortValueValue, SIGNAL(valueChanged(int)), this, SLOT(effortValueValueChanged(int)));
    connect(ui_growth, SIGNAL(currentIndexChanged(int)), this, SLOT(growthChanged(int)));
    connect(ui_experienceValue, SIGNAL(valueChanged(int)), this, SLOT(experienceValueChanged(int)));
    connect(ui_catchValue, SIGNAL(valueChanged(int)), this, SLOT(catchValueChanged(int)));
    connect(ui_maxHoldWeight, SIGNAL(valueChanged(int)), this, SLOT(maxHoldWeightChanged(int)));
    connect(ui_runChance, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(runChanceChanged(Sigcore::Fraction)));
    connect(ui_fleeChance, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(fleeChanceChanged(Sigcore::Fraction)));
    connect(ui_itemChance, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(itemChanceChanged(Sigcore::Fraction)));
    connect(ui_encyclopediaNumber, SIGNAL(valueChanged(int)), this, SLOT(encyclopediaNumberChanged(int)));
    connect(ui_weight, SIGNAL(valueChanged(int)), this, SLOT(weightChanged(int)));
    connect(ui_height, SIGNAL(valueChanged(int)), this, SLOT(heightChanged(int)));
    connect(ui_encyclopediaEntry, SIGNAL(textChanged(QString)), this, SLOT(encyclopediaEntryChanged(QString)));
    connect(ui_maleFront, SIGNAL(currentIndexChanged(int)), this, SLOT(maleFrontChanged(int)));
    connect(ui_maleBack, SIGNAL(currentIndexChanged(int)), this, SLOT(maleBackChanged(int)));
    connect(ui_femaleFront, SIGNAL(currentIndexChanged(int)), this, SLOT(femaleFrontChanged(int)));
    connect(ui_femaleBack, SIGNAL(currentIndexChanged(int)), this, SLOT(femaleBackChanged(int)));
    connect(ui_skin, SIGNAL(currentIndexChanged(int)), this, SLOT(skinChanged(int)));
    connect(ui_hasGender, SIGNAL(toggled(bool)), this, SLOT(hasGenderChanged(bool)));
    connect(ui_genderChance, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(genderChanceChanged(Sigcore::Fraction)));
    connect(ui_eggSpecies, SIGNAL(currentIndexChanged(int)), this, SLOT(eggSpeciesChanged(int)));
    connect(ui_eggSteps, SIGNAL(valueChanged(int)), this, SLOT(eggStepsChanged(int)));
    connect(ui_evolution, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(evolutionChanged(Sigcore::Script)));
    connect(ui_types, SIGNAL(added(QListWidgetItem*)), this, SLOT(typeAdded(QListWidgetItem*)));
    connect(ui_types, SIGNAL(removed(QListWidgetItem*)), this, SLOT(typeRemoved(QListWidgetItem*)));
    connect(ui_eggGroups, SIGNAL(added(QListWidgetItem*)), this, SLOT(eggGroupAdded(QListWidgetItem*)));
    connect(ui_eggGroups, SIGNAL(removed(QListWidgetItem*)), this, SLOT(eggGroupRemoved(QListWidgetItem*)));
    connect(ui_abilities, SIGNAL(added(QListWidgetItem*)), this, SLOT(abilityAdded(QListWidgetItem*)));
    connect(ui_abilities, SIGNAL(removed(QListWidgetItem*)), this, SLOT(abilityRemoved(QListWidgetItem*)));
    connect(ui_abilityWeight, SIGNAL(valueChanged(int)), this, SLOT(abilityWeightChanged(int)));
    connect(ui_items, SIGNAL(added(QListWidgetItem*)), this, SLOT(itemAdded(QListWidgetItem*)));
    connect(ui_items, SIGNAL(removed(QListWidgetItem*)), this, SLOT(itemRemoved(QListWidgetItem*)));
    connect(ui_itemWeight, SIGNAL(valueChanged(int)), this, SLOT(itemWeightChanged(int)));
    connect(ui_abilities->selectedListWidget(), SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(speciesChanged(QListWidgetItem*)));
    connect(ui_items->selectedListWidget(), SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(itemChanged(QListWidgetItem*)));
    ui_growth->addItems(Species::StyleStr);
    ui_baseStat->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    ui_effortValue->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    ui_maleFront->setEnabled(m_species->genderFactor() < 1);
    ui_maleBack->setEnabled(m_species->genderFactor() < 1);
    ui_femaleFront->setEnabled(0 < m_species->genderFactor());
    ui_femaleBack->setEnabled(0 < m_species->genderFactor());
    return form;
}

void SpeciesUI::Private::refreshGui()
{
    const bool isSplit = m_species->game()->rules()->specialSplit();
    ui_baseStat->clear();
    ui_baseStat->setRowCount((isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1);
    ui_baseStat->setVerticalHeaderLabels((isSplit ? StatGSCStr : StatRBYStr).mid(ST_Attack, (isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1));
    ui_baseStat->setColumnCount(1);
    ui_baseStat->setHorizontalHeaderLabels(QStringList() << "Value");
    ui_baseStat->verticalHeaderItem(ST_Attack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Attack));
    ui_baseStat->verticalHeaderItem(ST_Defense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Defense));
    ui_baseStat->verticalHeaderItem(ST_Speed - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Speed));
    if (isSplit)
    {
        ui_baseStat->verticalHeaderItem(ST_SpecialAttack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialAttack));
        ui_baseStat->verticalHeaderItem(ST_SpecialDefense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialDefense));
    }
    else
        ui_baseStat->verticalHeaderItem(ST_Special - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Special));
    ui_baseStatValue->setEnabled(false);
    QTableWidgetItem* item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_baseStat->setItem(ST_Attack - ST_Attack, 0, item);
    ui_baseStat->setCurrentItem(item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_baseStat->setItem(ST_Defense- ST_Attack, 0, item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_baseStat->setItem(ST_Speed - ST_Attack, 0, item);
    if (isSplit)
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_baseStat->setItem(ST_SpecialAttack - ST_Attack, 0, item);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_baseStat->setItem(ST_SpecialDefense - ST_Attack, 0, item);
    }
    else
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_baseStat->setItem(ST_Special - ST_Attack, 0, item);
    }
    if (0 < m_species->game()->rules()->maxTotalEV())
    {
        ui_effortValue->clear();
        ui_effortValue->setRowCount((isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1);
        ui_effortValue->setVerticalHeaderLabels((isSplit ? StatGSCStr : StatRBYStr).mid(ST_Attack, (isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1));
        ui_effortValue->setColumnCount(1);
        ui_effortValue->setHorizontalHeaderLabels(QStringList() << "Value");
        ui_effortValue->verticalHeaderItem(ST_Attack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Attack));
        ui_effortValue->verticalHeaderItem(ST_Defense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Defense));
        ui_effortValue->verticalHeaderItem(ST_Speed - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Speed));
        if (isSplit)
        {
            ui_effortValue->verticalHeaderItem(ST_SpecialAttack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialAttack));
            ui_effortValue->verticalHeaderItem(ST_SpecialDefense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialDefense));
        }
        else
            ui_effortValue->verticalHeaderItem(ST_Special - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Special));
        ui_effortValue->setEnabled(false);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_effortValue->setItem(ST_Attack - ST_Attack, 0, item);
        ui_effortValue->setCurrentItem(item);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_effortValue->setItem(ST_Defense- ST_Attack, 0, item);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_effortValue->setItem(ST_Speed - ST_Attack, 0, item);
        if (isSplit)
        {
            item = new QTableWidgetItem;
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui_effortValue->setItem(ST_SpecialAttack - ST_Attack, 0, item);
            item = new QTableWidgetItem;
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui_effortValue->setItem(ST_SpecialDefense - ST_Attack, 0, item);
        }
        else
        {
            item = new QTableWidgetItem;
            item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
            ui_effortValue->setItem(ST_Special - ST_Attack, 0, item);
        }
    }
    else
        ui_boxEffortValues->setEnabled(false);
    if (!m_species->game()->rules()->maxHeldItems())
        ui_itemChance->setEnabled(false);
    int maxHeight = 0;
    int maxWidth = 0;
    const bool blockedMaleFront = ui_maleFront->blockSignals(true);
    ui_maleFront->clear();
    const bool blockedMaleBack = ui_maleBack->blockSignals(true);
    ui_maleBack->clear();
    const bool blockedFemaleFront = ui_femaleFront->blockSignals(true);
    ui_femaleFront->clear();
    const bool blockedFemaleBack = ui_femaleBack->blockSignals(true);
    ui_femaleBack->clear();
    for (int i = 0; i < m_species->game()->spriteCount(); ++i)
    {
        const Sprite* sprite = m_species->game()->sprite(i);
        QPixmap icon;
        icon.loadFromData(sprite->sprite());
        maxHeight = qMax(maxHeight, icon.height());
        maxWidth = qMax(maxWidth, icon.width());
        ui_maleFront->addItem(icon, sprite->name());
        ui_maleBack->addItem(icon, sprite->name());
        ui_femaleFront->addItem(icon, sprite->name());
        ui_femaleBack->addItem(icon, sprite->name());
    }
    ui_maleFront->blockSignals(blockedMaleFront);
    ui_maleBack->blockSignals(blockedMaleBack);
    ui_femaleFront->blockSignals(blockedFemaleFront);
    ui_femaleBack->blockSignals(blockedFemaleBack);
    const QSize maxSize(maxWidth, maxHeight);
    ui_maleFront->setIconSize(maxSize);
    ui_maleBack->setIconSize(maxSize);
    ui_femaleFront->setIconSize(maxSize);
    ui_femaleBack->setIconSize(maxSize);
    ui_femaleFront->setEnabled(m_species->game()->rules()->genderAllowed());
    ui_femaleBack->setEnabled(m_species->game()->rules()->genderAllowed());
    const bool blockedSkin = ui_skin->blockSignals(true);
    ui_skin->clear();
    for (int i = 0; i < m_species->game()->skinCount(); ++i)
        ui_skin->addItem(m_species->game()->skin(i)->name());
    ui_skin->blockSignals(blockedSkin);
    const bool blockedEggSpecies = ui_eggSpecies->blockSignals(true);
    ui_eggSpecies->clear();
    for (int i = 0; i < m_species->game()->speciesCount(); ++i)
        ui_eggSpecies->addItem(m_species->game()->species(i)->name());
    ui_eggSpecies->setEnabled(m_species->game()->rules()->breedingAllowed());
    ui_eggSpecies->blockSignals(blockedEggSpecies);
    ui_eggSteps->setEnabled(m_species->game()->rules()->breedingAllowed());
    const bool blockedEggGroups = ui_eggGroups->blockSignals(true);
    ui_eggGroups->availableListWidget()->clear();
    ui_eggGroups->selectedListWidget()->clear();
    for (int i = 0; i < m_species->game()->eggGroupCount(); ++i)
    {
        const EggGroup* eggGroup = m_species->game()->eggGroup(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(eggGroup->name(), ui_eggGroups->availableListWidget());
        widgetItem->setData(Qt::UserRole, eggGroup->id());
    }
    ui_eggGroups->blockSignals(blockedEggGroups);
    ui_eggGroups->setButtonsEnabled();
    ui_genetics->setEnabled(m_species->game()->rules()->genderAllowed());
    const bool blockedTypes = ui_types->blockSignals(true);
    ui_types->availableListWidget()->clear();
    ui_types->selectedListWidget()->clear();
    for (int i = 0; i < m_species->game()->typeCount(); ++i)
    {
        const Type* type = m_species->game()->type(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(type->name(), ui_types->selectedListWidget());
        widgetItem->setData(Qt::UserRole, type->id());
    }
    ui_types->blockSignals(blockedTypes);
    ui_types->setButtonsEnabled();
    const bool blockedAbilities = ui_abilities->blockSignals(true);
    ui_abilities->availableListWidget()->clear();
    ui_abilities->selectedListWidget()->clear();
    for (int i = 0; i < m_species->game()->speciesCount(); ++i)
    {
        const Species* species = m_species->game()->species(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(species->name(), ui_abilities->availableListWidget());
        widgetItem->setData(Qt::UserRole, species->id());
    }
    ui_abilities->blockSignals(blockedAbilities);
    ui_abilities->setButtonsEnabled();
    ui_abilityWeight->setEnabled(false);
    ui_boxAbilities->setEnabled(m_species->game()->rules()->maxAbilities());
    const bool blockedItems = ui_items->blockSignals(true);
    ui_items->availableListWidget()->clear();
    ui_items->selectedListWidget()->clear();
    for (int i = 0; i < m_species->game()->itemCount(); ++i)
    {
        const Item* item = m_species->game()->item(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(item->name(), ui_items->availableListWidget());
        widgetItem->setData(Qt::UserRole, item->id());
    }
    ui_items->blockSignals(blockedItems);
    ui_items->setButtonsEnabled();
    ui_itemWeight->setEnabled(false);
    ui_boxItems->setEnabled(m_species->game()->rules()->maxHeldItems());
    ObjectUIPrivate::refreshGui();
}

void SpeciesUI::Private::resetGui()
{
    ui_name->setText(m_species->name());
    ui_baseStat->item(ST_Attack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_Attack)));
    ui_baseStat->item(ST_Defense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_Defense)));
    ui_baseStat->item(ST_Speed - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_Speed)));
    if (m_species->game()->rules()->specialSplit())
    {
        ui_baseStat->item(ST_SpecialAttack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_SpecialAttack)));
        ui_baseStat->item(ST_SpecialDefense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_SpecialDefense)));
    }
    else
        ui_baseStat->item(ST_Special - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->baseStat(ST_Special)));
    if (0 < m_species->game()->rules()->maxTotalEV())
    {
        ui_effortValue->item(ST_Attack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_Attack)));
        ui_effortValue->item(ST_Defense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_Defense)));
        ui_effortValue->item(ST_Speed - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_Speed)));
        if (m_species->game()->rules()->specialSplit())
        {
            ui_effortValue->item(ST_SpecialAttack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_SpecialAttack)));
            ui_effortValue->item(ST_SpecialDefense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_SpecialDefense)));
        }
        else
            ui_effortValue->item(ST_Special - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_species->effortValue(ST_Special)));
    }
    ui_growth->setCurrentIndex(m_species->growth());
    ui_experienceValue->setValue(m_species->experienceValue());
    ui_catchValue->setValue(m_species->catchValue());
    ui_maxHoldWeight->setValue(m_species->maxHoldWeight());
    ui_runChance->setValue(m_species->runChance());
    ui_fleeChance->setValue(m_species->fleeChance());
    ui_itemChance->setValue(m_species->itemChance());
    ui_encyclopediaNumber->setValue(m_species->encyclopediaNumber());
    ui_weight->setValue(m_species->weight());
    ui_height->setValue(m_species->height());
    ui_encyclopediaEntry->setText(m_species->encyclopediaEntry());
    ui_maleFront->setCurrentIndex(m_species->game()->spriteIndex(m_species->frontMaleSprite()));
    ui_maleBack->setCurrentIndex(m_species->game()->spriteIndex(m_species->backMaleSprite()));
    ui_femaleFront->setCurrentIndex(m_species->game()->spriteIndex(m_species->frontFemaleSprite()));
    ui_femaleBack->setCurrentIndex(m_species->game()->spriteIndex(m_species->backFemaleSprite()));
    ui_skin->setCurrentIndex(ui_skin->findData(m_species->skin()));
    ui_hasGender->setCheckState((m_species->genderFactor() < 0) ? Qt::Checked : Qt::Unchecked);
    ui_genderChance->setValue(m_species->genderFactor());
    ui_eggSpecies->setCurrentIndex(m_species->game()->speciesIndex(m_species->eggSpecies()));
    ui_eggSteps->setValue(m_species->eggSteps());
    ui_evolution->setValue(m_species->evolution());
    for (int i = 0; i < ui_types->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_types->availableListWidget()->item(i);
        if (m_species->type(widgetItem->data(Qt::UserRole).toInt()))
            ui_types->selectedListWidget()->addItem(ui_types->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_types->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_types->selectedListWidget()->item(i);
        if (!m_species->type(widgetItem->data(Qt::UserRole).toInt()))
            ui_types->availableListWidget()->addItem(ui_types->selectedListWidget()->takeItem(i--));
    }
    ui_types->setButtonsEnabled();
    for (int i = 0; i < ui_eggGroups->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_eggGroups->availableListWidget()->item(i);
        if (m_species->eggGroup(widgetItem->data(Qt::UserRole).toInt()))
            ui_eggGroups->selectedListWidget()->addItem(ui_eggGroups->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_eggGroups->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_eggGroups->selectedListWidget()->item(i);
        if (!m_species->eggGroup(widgetItem->data(Qt::UserRole).toInt()))
            ui_eggGroups->availableListWidget()->addItem(ui_eggGroups->selectedListWidget()->takeItem(i--));
    }
    ui_eggGroups->setButtonsEnabled();
    for (int i = 0; i < ui_abilities->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_abilities->availableListWidget()->item(i);
        if (m_species->ability(widgetItem->data(Qt::UserRole).toInt()))
            ui_abilities->selectedListWidget()->addItem(ui_abilities->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_abilities->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_abilities->selectedListWidget()->item(i);
        if (!m_species->ability(widgetItem->data(Qt::UserRole).toInt()))
            ui_abilities->availableListWidget()->addItem(ui_abilities->selectedListWidget()->takeItem(i--));
    }
    ui_abilities->setButtonsEnabled();
    for (int i = 0; i < ui_items->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->availableListWidget()->item(i);
        if (m_species->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->selectedListWidget()->addItem(ui_items->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_items->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->selectedListWidget()->item(i);
        if (!m_species->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->availableListWidget()->addItem(ui_items->selectedListWidget()->takeItem(i--));
    }
    ui_items->setButtonsEnabled();
}

void SpeciesUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_species->setName(name);
    ui_name->setCursorPosition(cursor);
}

void SpeciesUI::Private::baseStatChanged(const int row)
{
    ui_baseStatValue->setEnabled(true);
    ui_baseStatValue->setValue(m_species->baseStat(ui_baseStat->verticalHeaderItem(row)->data(Qt::UserRole).value<Stat>()));
}

void SpeciesUI::Private::baseStatValueChanged(const int baseStat)
{
    m_species->setBaseStat(ui_baseStat->verticalHeaderItem(ui_baseStat->currentRow())->data(Qt::UserRole).value<Stat>(), baseStat);
}

void SpeciesUI::Private::effortValueChanged(const int row)
{
    ui_effortValueValue->setEnabled(true);
    ui_effortValueValue->setValue(m_species->effortValue(ui_effortValue->verticalHeaderItem(row)->data(Qt::UserRole).value<Stat>()));
}

void SpeciesUI::Private::effortValueValueChanged(const int effortValue)
{
    m_species->setEffortValue(ui_effortValue->verticalHeaderItem(ui_effortValue->currentRow())->data(Qt::UserRole).value<Stat>(), effortValue);
}

void SpeciesUI::Private::growthChanged(const int growth)
{
    m_species->setGrowth(static_cast<Species::Style>(growth));
}

void SpeciesUI::Private::experienceValueChanged(const int experienceValue)
{
    m_species->setExperienceValue(experienceValue);
}

void SpeciesUI::Private::catchValueChanged(const int catchValue)
{
    m_species->setCatchValue(catchValue);
}

void SpeciesUI::Private::maxHoldWeightChanged(const int maxHoldWeight)
{
    m_species->setMaxHoldWeight(maxHoldWeight);
}

void SpeciesUI::Private::runChanceChanged(const Fraction& runChance)
{
    m_species->setRunChance(runChance);
}

void SpeciesUI::Private::fleeChanceChanged(const Fraction& fleeChance)
{
    m_species->setFleeChance(fleeChance);
}

void SpeciesUI::Private::itemChanceChanged(const Fraction& itemChance)
{
    m_species->setItemChance(itemChance);
}

void SpeciesUI::Private::encyclopediaNumberChanged(const int encyclopediaNumber)
{
    m_species->setEncyclopediaNumber(encyclopediaNumber);
}

void SpeciesUI::Private::weightChanged(const int weight)
{
    m_species->setWeight(weight);
}

void SpeciesUI::Private::heightChanged(const int height)
{
    m_species->setHeight(height);
}

void SpeciesUI::Private::encyclopediaEntryChanged(const QString& encyclopediaEntry)
{
    const int cursor = ui_encyclopediaEntry->cursorPosition();
    m_species->setEncyclopediaEntry(encyclopediaEntry);
    ui_encyclopediaEntry->setCursorPosition(cursor);
}

void SpeciesUI::Private::maleFrontChanged(const int maleFront)
{
    if (0 <= maleFront)
        m_species->setFrontMaleSprite(m_species->game()->sprite(maleFront)->id());
}

void SpeciesUI::Private::maleBackChanged(const int maleBack)
{
    if (0 <= maleBack)
        m_species->setBackMaleSprite(m_species->game()->sprite(maleBack)->id());
}

void SpeciesUI::Private::femaleFrontChanged(const int femaleFront)
{
    if (0 <= femaleFront)
        m_species->setFrontFemaleSprite(m_species->game()->sprite(femaleFront)->id());
}

void SpeciesUI::Private::femaleBackChanged(const int femaleBack)
{
    if (0 <= femaleBack)
        m_species->setBackFemaleSprite(m_species->game()->sprite(femaleBack)->id());
}

void SpeciesUI::Private::skinChanged(const int skin)
{
    if (0 <= skin)
        m_species->setSkin(m_species->game()->skin(skin)->id());
}

void SpeciesUI::Private::hasGenderChanged(const bool hasGender)
{
    m_species->setGenderFactor(Fraction((hasGender ? 1 : -1), 1));
    ui_femaleFront->setEnabled(0 < m_species->genderFactor());
    ui_femaleBack->setEnabled(0 < m_species->genderFactor());
}

void SpeciesUI::Private::genderChanceChanged(const Fraction& genderChance)
{
    m_species->setGenderFactor(genderChance);
    ui_maleFront->setEnabled(genderChance < 1);
    ui_maleBack->setEnabled(genderChance < 1);
    ui_femaleFront->setEnabled(0 < genderChance);
    ui_femaleBack->setEnabled(0 < genderChance);
}

void SpeciesUI::Private::eggSpeciesChanged(const int eggSpecies)
{
    m_species->setEggSpecies(m_species->game()->species(eggSpecies)->id());
}

void SpeciesUI::Private::eggStepsChanged(const int eggSteps)
{
    m_species->setEggSteps(eggSteps);
}

void SpeciesUI::Private::evolutionChanged(const Script& evolution)
{
    m_species->setEvolution(evolution);
}

void SpeciesUI::Private::typeAdded(QListWidgetItem* item)
{
    m_species->setType(item->data(Qt::UserRole).toInt(), true);
}

void SpeciesUI::Private::typeRemoved(QListWidgetItem* item)
{
    m_species->setType(item->data(Qt::UserRole).toInt(), false);
}

void SpeciesUI::Private::eggGroupAdded(QListWidgetItem* item)
{
    m_species->setEggGroup(item->data(Qt::UserRole).toInt(), true);
    ui_boxEggGroups->setEnabled(m_species->game()->rules()->breedingAllowed() && m_species->eggGroup().size());
}

void SpeciesUI::Private::eggGroupRemoved(QListWidgetItem* item)
{
    m_species->setEggGroup(item->data(Qt::UserRole).toInt(), false);
    ui_boxEggGroups->setEnabled(m_species->game()->rules()->breedingAllowed() && m_species->eggGroup().size());
}

void SpeciesUI::Private::abilityAdded(QListWidgetItem* item)
{
    m_species->setAbility(item->data(Qt::UserRole).toInt(), true);
}

void SpeciesUI::Private::abilityRemoved(QListWidgetItem* item)
{
    m_species->setAbility(item->data(Qt::UserRole).toInt(), false);
}

void SpeciesUI::Private::abilityChanged(QListWidgetItem* item)
{
    if (item)
        ui_abilityWeight->setValue(m_species->ability(item->data(Qt::UserRole).toInt()));
    ui_abilityWeight->setEnabled(!!item);
}

void SpeciesUI::Private::abilityWeightChanged(const int abilityWeight)
{
    QListWidgetItem* item = ui_abilities->selectedListWidget()->currentItem();
    m_species->setAbility(item->data(Qt::UserRole).toInt(), abilityWeight);
    emit(changed());
}

void SpeciesUI::Private::itemAdded(QListWidgetItem* item)
{
    m_species->setItem(item->data(Qt::UserRole).toInt(), 1);
}

void SpeciesUI::Private::itemRemoved(QListWidgetItem* item)
{
    m_species->setItem(item->data(Qt::UserRole).toInt(), 0);
}

void SpeciesUI::Private::itemChanged(QListWidgetItem* item)
{
    if (item)
        ui_itemWeight->setValue(m_species->item(item->data(Qt::UserRole).toInt()));
    ui_itemWeight->setEnabled(!!item);
}

void SpeciesUI::Private::itemWeightChanged(const int itemWeight)
{
    QListWidgetItem* item = ui_items->selectedListWidget()->currentItem();
    m_species->setItem(item->data(Qt::UserRole).toInt(), itemWeight);
    emit(changed());
}
