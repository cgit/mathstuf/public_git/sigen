/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_ITEMTYPEUI_P
#define SIGMODRWIDGETS_ITEMTYPEUI_P

// Header include
#include "ItemTypeUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KIntNumInput;
class KLineEdit;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT ItemTypeUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::ItemType* itemType);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::ItemType* m_itemType;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void computerChanged(const int computer);
        void playerChanged(const int player);
        void maxWeightChanged(const int maxWeight);
        void countChanged(const int count);
    private:
        KLineEdit* ui_name;
        KIntNumInput* ui_computer;
        KIntNumInput* ui_player;
        KIntNumInput* ui_maxWeight;
        KComboBox* ui_count;
};
}
}

#endif
