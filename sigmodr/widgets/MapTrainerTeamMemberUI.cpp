/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTrainerTeamMemberUI.h"
#include "MapTrainerTeamMemberUI_p.h"

// Sigmod includes
#include <sigmod/Ability.h>
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/MapTrainerTeamMember.h>
#include <sigmod/Move.h>
#include <sigmod/Nature.h>
#include <sigmod/Rules.h>
#include <sigmod/Species.h>

// KDE includes
#include <KActionSelector>
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QGroupBox>
#include <QtGui/QListWidgetItem>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapTrainerTeamMemberUI::MapTrainerTeamMemberUI(MapTrainerTeamMember* teamMember, QWidget* parent) :
        ObjectUI(teamMember, parent),
        d(new Private(new MapTrainerTeamMember(*teamMember)))
{
    setPrivate(d);
}

void MapTrainerTeamMemberUI::apply()
{
    *qobject_cast<MapTrainerTeamMember*>(m_object) = *d->m_teamMember;
    ObjectUI::apply();
}

void MapTrainerTeamMemberUI::discard()
{
    *d->m_teamMember = *qobject_cast<MapTrainerTeamMember*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapTrainerTeamMemberUI::Private::Private(MapTrainerTeamMember* teamMember) :
        ObjectUIPrivate(teamMember),
        m_teamMember(teamMember)
{
}

MapTrainerTeamMemberUI::Private::~Private()
{
    delete m_teamMember;
}

QWidget* MapTrainerTeamMemberUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/maptrainerteammember.ui", widget);
    ui_species = form->findChild<KComboBox*>("varSpecies");
    ui_level = form->findChild<KIntNumInput*>("varLevel");
    ui_abilities = form->findChild<KActionSelector*>("varAbilities");
    ui_boxItems = form->findChild<QGroupBox*>("boxItems");
    ui_items = form->findChild<KActionSelector*>("varItems");
    ui_itemCount = form->findChild<KIntNumInput*>("varItemCount");
    ui_moves = form->findChild<KActionSelector*>("varMoves");
    ui_natures = form->findChild<KActionSelector*>("varNatures");
    connect(ui_species, SIGNAL(currentIndexChanged(int)), this, SLOT(speciesChanged(int)));
    connect(ui_level, SIGNAL(valueChanged(int)), this, SLOT(levelChanged(int)));
    connect(ui_abilities, SIGNAL(added(QListWidgetItem*)), this, SLOT(teamMemberAdded(QListWidgetItem*)));
    connect(ui_abilities, SIGNAL(removed(QListWidgetItem*)), this, SLOT(teamMemberRemoved(QListWidgetItem*)));
    connect(ui_items, SIGNAL(added(QListWidgetItem*)), this, SLOT(itemAdded(QListWidgetItem*)));
    connect(ui_items, SIGNAL(removed(QListWidgetItem*)), this, SLOT(itemRemoved(QListWidgetItem*)));
    connect(ui_itemCount, SIGNAL(valueChanged(int)), this, SLOT(itemCountChanged(int)));
    connect(ui_moves, SIGNAL(added(QListWidgetItem*)), this, SLOT(moveAdded(QListWidgetItem*)));
    connect(ui_moves, SIGNAL(removed(QListWidgetItem*)), this, SLOT(moveRemoved(QListWidgetItem*)));
    connect(ui_natures, SIGNAL(added(QListWidgetItem*)), this, SLOT(natureAdded(QListWidgetItem*)));
    connect(ui_natures, SIGNAL(removed(QListWidgetItem*)), this, SLOT(natureRemoved(QListWidgetItem*)));
    connect(ui_items->selectedListWidget(), SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(selectedItemChanged(QListWidgetItem*)));
    return form;
}

void MapTrainerTeamMemberUI::Private::refreshGui()
{
    const bool blockedSpecies = ui_species->blockSignals(true);
    ui_species->clear();
    for (int i = 0; i < m_teamMember->game()->speciesCount(); ++i)
        ui_species->addItem(m_teamMember->game()->species(i)->name());
    ui_species->blockSignals(blockedSpecies);
    ui_level->setMaximum(m_teamMember->game()->rules()->maxLevel());
    const bool blockedAbilities = ui_abilities->blockSignals(true);
    ui_abilities->availableListWidget()->clear();
    ui_abilities->selectedListWidget()->clear();
    for (int i = 0; i < m_teamMember->game()->abilityCount(); ++i)
    {
        const Ability* ability = m_teamMember->game()->ability(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(ability->name(), ui_abilities->availableListWidget());
        widgetItem->setData(Qt::UserRole, ability->id());
    }
    ui_abilities->blockSignals(blockedAbilities);
    ui_abilities->setButtonsEnabled();
    ui_abilities->setEnabled(m_teamMember->game()->rules()->maxAbilities());
    const bool blockedItems = ui_items->blockSignals(true);
    ui_items->availableListWidget()->clear();
    ui_items->selectedListWidget()->clear();
    for (int i = 0; i < m_teamMember->game()->itemCount(); ++i)
    {
        const Item* item = m_teamMember->game()->item(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(item->name(), ui_items->availableListWidget());
        widgetItem->setData(Qt::UserRole, item->id());
    }
    ui_items->blockSignals(blockedItems);
    ui_items->setButtonsEnabled();
    ui_itemCount->setEnabled(false);
    ui_boxItems->setEnabled(m_teamMember->game()->rules()->maxHeldItems());
    const bool blockedMoves = ui_moves->blockSignals(true);
    ui_moves->availableListWidget()->clear();
    ui_moves->selectedListWidget()->clear();
    for (int i = 0; i < m_teamMember->game()->moveCount(); ++i)
    {
        const Move* move = m_teamMember->game()->move(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(move->name(), ui_moves->availableListWidget());
        widgetItem->setData(Qt::UserRole, move->id());
    }
    ui_moves->blockSignals(blockedMoves);
    ui_moves->setButtonsEnabled();
    const bool blockedNatures = ui_natures->blockSignals(true);
    ui_natures->availableListWidget()->clear();
    ui_natures->selectedListWidget()->clear();
    for (int i = 0; i < m_teamMember->game()->natureCount(); ++i)
    {
        const Nature* nature = m_teamMember->game()->nature(i);
        QListWidgetItem* widgetItem = new QListWidgetItem(nature->name(), ui_natures->availableListWidget());
        widgetItem->setData(Qt::UserRole, nature->id());
    }
    ui_natures->blockSignals(blockedNatures);
    ui_natures->setButtonsEnabled();
    ui_natures->setEnabled(m_teamMember->game()->rules()->maxNatures());
    ObjectUIPrivate::refreshGui();
}

void MapTrainerTeamMemberUI::Private::resetGui()
{
    ui_species->setCurrentIndex(m_teamMember->game()->speciesIndex(m_teamMember->species()));
    ui_level->setValue(m_teamMember->level());
    for (int i = 0; i < ui_abilities->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_abilities->availableListWidget()->item(i);
        if (m_teamMember->ability(widgetItem->data(Qt::UserRole).toInt()))
            ui_abilities->selectedListWidget()->addItem(ui_abilities->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_abilities->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_abilities->selectedListWidget()->item(i);
        if (!m_teamMember->ability(widgetItem->data(Qt::UserRole).toInt()))
            ui_abilities->availableListWidget()->addItem(ui_abilities->selectedListWidget()->takeItem(i--));
    }
    ui_abilities->setButtonsEnabled();
    for (int i = 0; i < ui_items->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->availableListWidget()->item(i);
        if (m_teamMember->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->selectedListWidget()->addItem(ui_items->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_items->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_items->selectedListWidget()->item(i);
        if (!m_teamMember->item(widgetItem->data(Qt::UserRole).toInt()))
            ui_items->availableListWidget()->addItem(ui_items->selectedListWidget()->takeItem(i--));
    }
    ui_items->setButtonsEnabled();
    for (int i = 0; i < ui_moves->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_moves->availableListWidget()->item(i);
        if (m_teamMember->move(widgetItem->data(Qt::UserRole).toInt()))
            ui_moves->selectedListWidget()->addItem(ui_moves->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_moves->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_moves->selectedListWidget()->item(i);
        if (!m_teamMember->move(widgetItem->data(Qt::UserRole).toInt()))
            ui_moves->availableListWidget()->addItem(ui_moves->selectedListWidget()->takeItem(i--));
    }
    ui_moves->setButtonsEnabled();
    for (int i = 0; i < ui_natures->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_natures->availableListWidget()->item(i);
        if (m_teamMember->nature(widgetItem->data(Qt::UserRole).toInt()))
            ui_natures->selectedListWidget()->addItem(ui_natures->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_natures->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_natures->selectedListWidget()->item(i);
        if (!m_teamMember->nature(widgetItem->data(Qt::UserRole).toInt()))
            ui_natures->availableListWidget()->addItem(ui_natures->selectedListWidget()->takeItem(i--));
    }
    ui_natures->setButtonsEnabled();
}

void MapTrainerTeamMemberUI::Private::speciesChanged(const int species)
{
    if (0 <= species)
        m_teamMember->setSpecies(m_teamMember->game()->species(species)->id());
}

void MapTrainerTeamMemberUI::Private::levelChanged(const int level)
{
    m_teamMember->setLevel(level);
}

void MapTrainerTeamMemberUI::Private::abilityAdded(QListWidgetItem* item)
{
    m_teamMember->setAbility(item->data(Qt::UserRole).toInt(), true);
}

void MapTrainerTeamMemberUI::Private::abilityRemoved(QListWidgetItem* item)
{
    m_teamMember->setAbility(item->data(Qt::UserRole).toInt(), false);
}

void MapTrainerTeamMemberUI::Private::itemAdded(QListWidgetItem* item)
{
    m_teamMember->setItem(item->data(Qt::UserRole).toInt(), 1);
}

void MapTrainerTeamMemberUI::Private::itemRemoved(QListWidgetItem* item)
{
    m_teamMember->setItem(item->data(Qt::UserRole).toInt(), 0);
}

void MapTrainerTeamMemberUI::Private::itemChanged(QListWidgetItem* item)
{
    if (item)
        ui_itemCount->setValue(m_teamMember->item(item->data(Qt::UserRole).toInt()));
    ui_itemCount->setEnabled(!!item);
}

void MapTrainerTeamMemberUI::Private::itemCountChanged(const int itemCount)
{
    QListWidgetItem* item = ui_items->selectedListWidget()->currentItem();
    m_teamMember->setItem(item->data(Qt::UserRole).toInt(), itemCount);
    emit(changed());
}

void MapTrainerTeamMemberUI::Private::moveAdded(QListWidgetItem* item)
{
    m_teamMember->setMove(item->data(Qt::UserRole).toInt(), true);
}

void MapTrainerTeamMemberUI::Private::moveRemoved(QListWidgetItem* item)
{
    m_teamMember->setMove(item->data(Qt::UserRole).toInt(), false);
}

void MapTrainerTeamMemberUI::Private::natureAdded(QListWidgetItem* item)
{
    m_teamMember->setNature(item->data(Qt::UserRole).toInt(), true);
}

void MapTrainerTeamMemberUI::Private::natureRemoved(QListWidgetItem* item)
{
    m_teamMember->setNature(item->data(Qt::UserRole).toInt(), false);
}
