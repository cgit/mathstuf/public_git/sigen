/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_TRAINERUI_P
#define SIGMODRWIDGETS_TRAINERUI_P

// Header include
#include "TrainerUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KIntNumInput;
class KLineEdit;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT TrainerUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Trainer* trainer);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Trainer* m_trainer;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void moneyFactorChanged(const int moneyFactor);
        void skinChanged(const int skin);
        void depthChanged(const int depth);
        void teamIntelChanged(const int teamIntel);
        void moveIntelChanged(const int moveIntel);
        void itemIntelChanged(const int itemIntel);
        void abilityIntelChanged(const int abilityIntel);
        void statIntelChanged(const int statIntel);
    private:
        KLineEdit* ui_name;
        KIntNumInput* ui_moneyFactor;
        KComboBox* ui_skin;
        KIntNumInput* ui_depth;
        KComboBox* ui_teamIntel;
        KComboBox* ui_moveIntel;
        KComboBox* ui_itemIntel;
        KComboBox* ui_abilityIntel;
        KComboBox* ui_statIntel;
};
}
}

#endif
