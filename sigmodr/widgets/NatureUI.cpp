/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "NatureUI.h"
#include "NatureUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Nature.h>
#include <sigmod/Rules.h>

// KDE includes
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QHeaderView>
#include <QtGui/QTableWidget>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

NatureUI::NatureUI(Nature* nature, QWidget* parent) :
        ObjectUI(nature, parent),
        d(new Private(new Nature(*nature)))
{
    setPrivate(d);
}

void NatureUI::apply()
{
    *qobject_cast<Nature*>(m_object) = *d->m_nature;
    ObjectUI::apply();
}

void NatureUI::discard()
{
    *d->m_nature = *qobject_cast<Nature*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

NatureUI::Private::Private(Nature* nature) :
        ObjectUIPrivate(nature),
        m_nature(nature)
{
}

NatureUI::Private::~Private()
{
    delete m_nature;
}

QWidget* NatureUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/nature.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_stat = form->findChild<QTableWidget*>("varStat");
    ui_statMultiplier = form->findChild<FractionWidget*>("varStatMultiplier");
    ui_weight = form->findChild<KIntNumInput*>("varWeight");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_stat, SIGNAL(currentCellChanged(int,int,int,int)), this, SLOT(statChanged(int)));
    connect(ui_statMultiplier, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(statMultiplierChanged(Sigcore::Fraction)));
    connect(ui_weight, SIGNAL(valueChanged(int)), this, SLOT(weightChanged(int)));
    ui_stat->horizontalHeader()->setResizeMode(QHeaderView::Stretch);
    return form;
}

void NatureUI::Private::refreshGui()
{
    const bool isSplit = m_nature->game()->rules()->specialSplit();
    ui_stat->clear();
    ui_stat->setRowCount((isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1);
    ui_stat->setVerticalHeaderLabels((isSplit ? StatGSCStr : StatRBYStr).mid(ST_Attack, (isSplit ? ST_SpecialDefense : ST_Special) - ST_Attack + 1));
    ui_stat->setColumnCount(1);
    ui_stat->setHorizontalHeaderLabels(QStringList() << "Multiplier");
    ui_stat->verticalHeaderItem(ST_Attack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Attack));
    ui_stat->verticalHeaderItem(ST_Defense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Defense));
    ui_stat->verticalHeaderItem(ST_Speed - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Speed));
    if (isSplit)
    {
        ui_stat->verticalHeaderItem(ST_SpecialAttack - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialAttack));
        ui_stat->verticalHeaderItem(ST_SpecialDefense - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_SpecialDefense));
    }
    else
        ui_stat->verticalHeaderItem(ST_Special - ST_Attack)->setData(Qt::UserRole, QVariant::fromValue(ST_Special));
    ui_stat->setItem(ST_Attack - ST_Attack, 0, new QTableWidgetItem);
    ui_stat->setItem(ST_Defense- ST_Attack, 0, new QTableWidgetItem);
    ui_stat->setItem(ST_Speed - ST_Attack, 0, new QTableWidgetItem);
    if (isSplit)
    {
        ui_stat->setItem(ST_SpecialAttack - ST_Attack, 0, new QTableWidgetItem);
        ui_stat->setItem(ST_SpecialDefense - ST_Attack, 0, new QTableWidgetItem);
    }
    else
        ui_stat->setItem(ST_Special - ST_Attack, 0, new QTableWidgetItem);
    ui_statMultiplier->setEnabled(false);
    QTableWidgetItem* item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Attack - ST_Attack, 0, item);
    ui_stat->setCurrentItem(item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Defense- ST_Attack, 0, item);
    item = new QTableWidgetItem;
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ui_stat->setItem(ST_Speed - ST_Attack, 0, item);
    if (isSplit)
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_SpecialAttack - ST_Attack, 0, item);
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_SpecialDefense - ST_Attack, 0, item);
    }
    else
    {
        item = new QTableWidgetItem;
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ui_stat->setItem(ST_Special - ST_Attack, 0, item);
    }
    ObjectUIPrivate::refreshGui();
}

void NatureUI::Private::resetGui()
{
    ui_name->setText(m_nature->name());
    ui_stat->item(ST_Attack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_Attack), 'g', 7));
    ui_stat->item(ST_Defense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_Defense), 'g', 7));
    ui_stat->item(ST_Speed - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_Speed), 'g', 7));
    if (m_nature->game()->rules()->specialSplit())
    {
        ui_stat->item(ST_SpecialAttack - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_SpecialAttack), 'g', 7));
        ui_stat->item(ST_SpecialDefense - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_SpecialDefense), 'g', 7));
    }
    else
        ui_stat->item(ST_Special - ST_Attack, 0)->setData(Qt::DisplayRole, QString::number(m_nature->stat(ST_Special), 'g', 7));
    ui_weight->setValue(m_nature->weight());
}

void NatureUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_nature->setName(name);
    ui_name->setCursorPosition(cursor);
}

void NatureUI::Private::statChanged(const int row)
{
    ui_statMultiplier->setEnabled(true);
    ui_statMultiplier->setValue(m_nature->stat(ui_stat->verticalHeaderItem(row)->data(Qt::UserRole).value<Stat>()));
}

void NatureUI::Private::statMultiplierChanged(const Fraction& multiplier)
{
    m_nature->setStat(ui_stat->verticalHeaderItem(ui_stat->currentRow())->data(Qt::UserRole).value<Stat>(), multiplier);
    emit(changed());
}

void NatureUI::Private::weightChanged(const int weight)
{
    m_nature->setWeight(weight);
}
