/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_RULESUI_P
#define SIGMODRWIDGETS_RULESUI_P

// Header include
#include "RulesUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KIntNumInput;
class QCheckBox;
namespace Sigmod
{
class Rules;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT RulesUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Rules* rules);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Rules* m_rules;
    public slots:
        void resetGui();
    protected slots:
        void gendersChanged(const bool genders);
        void breedingChanged(const bool breeding);
        void criticalDomainsChanged(const bool criticalDomains);
        void splitSpecialChanged(const bool specialSplit);
        void splitSpecialDVChanged(const bool specialSplitDV);
        void maxEVChanged(const int maxEV);
        void maxEVPerStatChanged(const int maxEVPerStat);
        void boxesChanged(const int boxes);
        void boxSizeChanged(const int boxSize);
        void maxPartyChanged(const int maxParty);
        void maxFightChanged(const int maxFight);
        void maxPlayersChanged(const int maxPlayers);
        void maxMovesChanged(const int maxMoves);
        void maxLevelChanged(const int maxLevel);
        void maxHeldItemsChanged(const int maxHeldItems);
        void maxNaturesChanged(const int maxNatures);
        void maxAbilitiesChanged(const int maxAbilities);
        void maxStagesChanged(const int maxStages);
        void maxMoneyChanged(const int maxMoney);
        void maxTotalWeightChanged(const int maxTotalWeight);
    private:
        QCheckBox* ui_genders;
        QCheckBox* ui_breeding;
        QCheckBox* ui_criticalDomains;
        QCheckBox* ui_splitSpecial;
        QCheckBox* ui_splitSpecialDV;
        KIntNumInput* ui_maxEV;
        KIntNumInput* ui_maxEVPerStat;
        KIntNumInput* ui_boxes;
        KIntNumInput* ui_boxSize;
        KIntNumInput* ui_maxParty;
        KIntNumInput* ui_maxFight;
        KIntNumInput* ui_maxPlayers;
        KIntNumInput* ui_maxMoves;
        KIntNumInput* ui_maxLevel;
        KIntNumInput* ui_maxHeldItems;
        KIntNumInput* ui_maxNatures;
        KIntNumInput* ui_maxAbilities;
        KIntNumInput* ui_maxStages;
        KIntNumInput* ui_maxMoney;
        KIntNumInput* ui_maxTotalWeight;
};
}
}

#endif
