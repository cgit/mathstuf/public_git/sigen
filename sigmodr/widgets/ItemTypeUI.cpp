/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ItemTypeUI.h"
#include "ItemTypeUI_p.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/ItemType.h>
#include <sigmod/Rules.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

ItemTypeUI::ItemTypeUI(ItemType* itemType, QWidget* parent) :
        ObjectUI(itemType, parent),
        d(new Private(new ItemType(*itemType)))
{
    setPrivate(d);
}

void ItemTypeUI::apply()
{
    *qobject_cast<ItemType*>(m_object) = *d->m_itemType;
    ObjectUI::apply();
}

void ItemTypeUI::discard()
{
    *d->m_itemType = *qobject_cast<ItemType*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

ItemTypeUI::Private::Private(ItemType* itemType) :
        ObjectUIPrivate(itemType),
        m_itemType(itemType)
{
}

ItemTypeUI::Private::~Private()
{
    delete m_itemType;
}

QWidget* ItemTypeUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/itemtype.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_computer = form->findChild<KIntNumInput*>("varComputer");
    ui_player = form->findChild<KIntNumInput*>("varPlayer");
    ui_maxWeight = form->findChild<KIntNumInput*>("varMaxWeight");
    ui_count = form->findChild<KComboBox*>("varCount");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_computer, SIGNAL(valueChanged(int)), this, SLOT(computerChanged(int)));
    connect(ui_player, SIGNAL(valueChanged(int)), this, SLOT(playerChanged(int)));
    connect(ui_maxWeight, SIGNAL(valueChanged(int)), this, SLOT(maxWeightChanged(int)));
    connect(ui_count, SIGNAL(currentIndexChanged(int)), this, SLOT(countChanged(int)));
    ui_count->addItems(ItemType::CountStr);
    return form;
}

void ItemTypeUI::Private::refreshGui()
{
    ui_maxWeight->setMaximum(m_itemType->game()->rules()->maxTotalWeight());
    ui_maxWeight->setEnabled(0 < m_itemType->game()->rules()->maxTotalWeight());
    ObjectUIPrivate::refreshGui();
}

void ItemTypeUI::Private::resetGui()
{
    ui_name->setText(m_itemType->name());
    ui_computer->setValue(m_itemType->computer());
    ui_player->setValue(m_itemType->player());
    ui_maxWeight->setValue(m_itemType->maxWeight());
    ui_count->setCurrentIndex(m_itemType->count());
}

void ItemTypeUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_itemType->setName(name);
    ui_name->setCursorPosition(cursor);
}

void ItemTypeUI::Private::computerChanged(const int computer)
{
    m_itemType->setComputer(computer);
}

void ItemTypeUI::Private::playerChanged(const int player)
{
    m_itemType->setPlayer(player);
}

void ItemTypeUI::Private::maxWeightChanged(const int maxWeight)
{
    m_itemType->setMaxWeight(maxWeight);
}

void ItemTypeUI::Private::countChanged(const int count)
{
    m_itemType->setCount(static_cast<ItemType::Count>(count));
}
