/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TimeUI.h"
#include "TimeUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Time.h>

// KDE includes
#include <KLineEdit>

// Qt includes
#include <QtGui/QTimeEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

TimeUI::TimeUI(Time* time, QWidget* parent) :
        ObjectUI(time, parent),
        d(new Private(new Time(*time)))
{
    setPrivate(d);
}

void TimeUI::apply()
{
    *qobject_cast<Time*>(m_object) = *d->m_time;
    ObjectUI::apply();
}

void TimeUI::discard()
{
    *d->m_time = *qobject_cast<Time*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

TimeUI::Private::Private(Time* time) :
        ObjectUIPrivate(time),
        m_time(time)
{
}

TimeUI::Private::~Private()
{
    delete m_time;
}

QWidget* TimeUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/time.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_time = form->findChild<QTimeEdit*>("varTime");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_time, SIGNAL(timeChanged(QTime)), this, SLOT(timeChanged(QTime)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    return form;
}

void TimeUI::Private::resetGui()
{
    ui_name->setText(m_time->name());
    ui_time->setTime(QTime(m_time->hour(), m_time->minute()));
    ui_script->setValue(m_time->script());
}

void TimeUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_time->setName(name);
    ui_name->setCursorPosition(cursor);
}

void TimeUI::Private::timeChanged(const QTime& time)
{
    m_time->setHour(time.hour());
    m_time->setMinute(time.minute());
}

void TimeUI::Private::scriptChanged(const Script& script)
{
    m_time->setScript(script);
}
