/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapUI.h"
#include "MapUI_p.h"

// Sigmodr widget includes
#include "mapeditor/MapEditor.h"

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Map.h>
#include <sigmod/MapWarp.h>
#include <sigmod/Sprite.h>
#include <sigmod/Tile.h>

// KDE includes
#include <KComboBox>
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>
#include <QtGui/QGridLayout>

using namespace Sigmod;
using namespace Sigmodr::Widgets;

MapUI::MapUI(Map* map, QWidget* parent) :
        ObjectUI(map, parent),
        d(new Private(new Map(*map)))
{
    setPrivate(d);
}

void MapUI::apply()
{
    *qobject_cast<Map*>(m_object) = *d->m_map;
    ObjectUI::apply();
}

void MapUI::discard()
{
    *d->m_map = *qobject_cast<Map*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapUI::Private::Private(Map* map) :
        ObjectUIPrivate(map),
        m_map(map)
{
}

MapUI::Private::~Private()
{
    delete m_map;
}

QWidget* MapUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/map.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_isWorld = form->findChild<QCheckBox*>("varIsWorld");
    QGridLayout* editorLayout = form->findChild<QGridLayout*>("editorLayout");
    ui_editor = new MapEditor(m_map, widget);
    editorLayout->addWidget(ui_editor, 0, 0);
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_isWorld, SIGNAL(toggled(bool)), this, SLOT(isWorldChanged(bool)));
    connect(ui_editor, SIGNAL(changed()), this, SIGNAL(changed()));
    widget->setTabOrder(ui_name, ui_editor);
    return form;
}

void MapUI::Private::resetGui()
{
    ui_name->setText(m_map->name());
    ui_isWorld->setCheckState(m_map->isWorld() ? Qt::Checked : Qt::Unchecked);
    ui_editor->setMap(m_map);
}

void MapUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_map->setName(name);
    ui_name->setCursorPosition(cursor);
}

void MapUI::Private::isWorldChanged(const bool isWorld)
{
    m_map->setIsWorld(isWorld);
}
