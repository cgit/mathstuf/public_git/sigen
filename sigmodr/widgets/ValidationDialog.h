/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_VALIDATIONDIALOG
#define SIGMODRWIDGETS_VALIDATIONDIALOG

// Sigmodr widget includes
#include "Global.h"

// Qt includes
#include <QtCore/QPair>
#include <QtCore/QStack>
#include <QtGui/QWidget>

// Forward declarations
class QBrush;
class QTreeWidget;
class QTreeWidgetItem;
namespace Sigmod
{
class Object;
}

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT ValidationDialog : public QWidget
{
    Q_OBJECT
    
    public:
        typedef QPair<QTreeWidgetItem*, int> ObjectErrorCount;
        
        ValidationDialog(Sigmod::Object* object, QWidget* parent);
        
        int errors() const;
        int warnings() const;
    protected:
        void insertMessage(const QString& msg, const QBrush& brush);
    public slots:
        void process();
        void show();
        
        void addMessage(const QString& msg);
        void addWarning(const QString& msg);
        void addError(const QString& msg);
    private slots:
        void objectChanged();
    private:
        bool m_processed;
        Sigmod::Object* m_object;
        QStack<ObjectErrorCount> m_parents;
        QTreeWidget* m_valTree;
        int m_errors;
        int m_warnings;
};
}
}

#endif
