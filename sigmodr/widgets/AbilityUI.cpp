/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "AbilityUI.h"
#include "AbilityUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Ability.h>

// KDE includes
#include <KIntNumInput>
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

AbilityUI::AbilityUI(Ability* ability, QWidget* parent) :
        ObjectUI(ability, parent),
        d(new Private(new Ability(*ability)))
{
    setPrivate(d);
}

void AbilityUI::apply()
{
    *qobject_cast<Ability*>(m_object) = *d->m_ability;
    ObjectUI::apply();
}

void AbilityUI::discard()
{
    *d->m_ability = *qobject_cast<Ability*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

AbilityUI::Private::Private(Ability* ability) :
        ObjectUIPrivate(ability),
        m_ability(ability)
{
}

AbilityUI::Private::~Private()
{
    delete m_ability;
}

QWidget* AbilityUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/ability.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_priority = form->findChild<KIntNumInput*>("varPriority");
    ui_description = form->findChild<KLineEdit*>("varDescription");
    ui_battleScript = form->findChild<ScriptWidget*>("varBattleScript");
    ui_worldScript = form->findChild<ScriptWidget*>("varWorldScript");
    ui_priorityScript = form->findChild<ScriptWidget*>("varPriorityScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_priority, SIGNAL(valueChanged(int)), this, SLOT(priorityChanged(int)));
    connect(ui_description, SIGNAL(textChanged(QString)), this, SLOT(descriptionChanged(QString)));
    connect(ui_battleScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(battleScriptChanged(Sigcore::Script)));
    connect(ui_worldScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(worldScriptChanged(Sigcore::Script)));
    connect(ui_priorityScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(priorityScriptChanged(Sigcore::Script)));
    return form;
}

void AbilityUI::Private::resetGui()
{
    ui_name->setText(m_ability->name());
    ui_priority->setValue(m_ability->priority());
    ui_description->setText(m_ability->description());
    ui_battleScript->setValue(m_ability->battleScript());
    ui_worldScript->setValue(m_ability->worldScript());
    ui_priorityScript->setValue(m_ability->priorityScript());
}

void AbilityUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_ability->setName(name);
    ui_name->setCursorPosition(cursor);
}

void AbilityUI::Private::priorityChanged(const int priority)
{
    m_ability->setPriority(priority);
}

void AbilityUI::Private::descriptionChanged(const QString& description)
{
    const int cursor = ui_description->cursorPosition();
    m_ability->setDescription(description);
    ui_description->setCursorPosition(cursor);
}

void AbilityUI::Private::battleScriptChanged(const Script& battleScript)
{
    m_ability->setBattleScript(battleScript);
}

void AbilityUI::Private::worldScriptChanged(const Script& worldScript)
{
    m_ability->setWorldScript(worldScript);
}

void AbilityUI::Private::priorityScriptChanged(const Script& priorityScript)
{
    m_ability->setPriorityScript(priorityScript);
}
