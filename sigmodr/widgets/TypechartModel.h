/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_TYPECHARTMODEL
#define SIGMODRWIDGETS_TYPECHARTMODEL

// Sigmodr widget includes
#include "Global.h"

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Matrix.h>

// Qt includes
#include <QtCore/QAbstractTableModel>
#include <QtCore/QStringList>
#include <QtCore/QVariant>

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT TypechartModel : public QAbstractTableModel
{
    Q_OBJECT
    
    public:
        TypechartModel(Sigcore::Matrix<Sigcore::Fraction>* typechart, const QStringList& types);
        
        QVariant data(const QModelIndex& index, int role) const;
        QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
        
        int rowCount(const QModelIndex& parent) const;
        int columnCount(const QModelIndex& parent) const;
        
        Qt::ItemFlags flags(const QModelIndex& index) const;
        
        bool setData(const QModelIndex& index, const QVariant& value, int role);
        
        void discarded();
    private:
        Sigcore::Matrix<Sigcore::Fraction>& m_typechart;
        const QStringList m_types;
};
}
}

#endif
