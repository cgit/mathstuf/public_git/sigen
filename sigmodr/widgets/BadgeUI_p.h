/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_BADGEUI_P
#define SIGMODRWIDGETS_BADGEUI_P

// Header include
#include "BadgeUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KIntNumInput;
class KLineEdit;
class QTableWidget;
namespace Sigcore
{
class Fraction;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
class ScriptWidget;
}

namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT BadgeUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Badge* badge);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Badge* m_badge;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void obeyChanged(const int obey);
        void faceChanged(const int face);
        void badgeChanged(const int badge);
        void statChanged(const int row);
        void statMultiplierChanged(const Sigcore::Fraction& multiplier);
    private:
        KLineEdit* ui_name;
        KIntNumInput* ui_obey;
        KComboBox* ui_face;
        KComboBox* ui_badge;
        QTableWidget* ui_stat;
        CoreWidgets::FractionWidget* ui_statMultiplier;
};
}
}

#endif
