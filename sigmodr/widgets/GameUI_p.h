/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_GAMEUI_P
#define SIGMODRWIDGETS_GAMEUI_P

// Header include
#include "GameUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Qt includes
#include <QtCore/QModelIndex>

// Forward declarations
class KLineEdit;
class QCheckBox;
class QLabel;
class QTableView;
namespace Sigcore
{
class Fraction;
class Script;
}

namespace Sigmodr
{
namespace CoreWidgets
{
class FractionWidget;
class ScriptWidget;
}

namespace Widgets
{
class WorldMapEditor;

class SIGMODRWIDGETS_NO_EXPORT GameUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Game* game);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Game* m_game;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void titleChanged(const QString& title);
        void versionChanged(const QString& version);
        void descriptionChanged(const QString& description);
        void singlePlayerChanged(const bool singlePlayer);
        void startScriptChanged(const Sigcore::Script& startScript);
        void typechartChanged(const QModelIndex& index);
        void effectivenessChanged(const Sigcore::Fraction& multiplier);
    private:
        KLineEdit* ui_title;
        KLineEdit* ui_version;
        KLineEdit* ui_description;
        QCheckBox* ui_singlePlayer;
        CoreWidgets::ScriptWidget* ui_startScript;
        QTableView* ui_typechart;
        QLabel* ui_labelTypes;
        CoreWidgets::FractionWidget* ui_effectiveness;
        WorldMapEditor* ui_worldMapEditor;
};
}
}

#endif
