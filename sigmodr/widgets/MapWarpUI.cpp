/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWarpUI.h"
#include "MapWarpUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Map.h>
#include <sigmod/MapWarp.h>

// KDE includes
#include <KComboBox>
#include <KLineEdit>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

MapWarpUI::MapWarpUI(MapWarp* warp, QWidget* parent) :
        ObjectUI(warp, parent),
        d(new Private(new MapWarp(*warp)))
{
    setPrivate(d);
}

void MapWarpUI::apply()
{
    *qobject_cast<MapWarp*>(m_object) = *d->m_warp;
    ObjectUI::apply();
}

void MapWarpUI::discard()
{
    *d->m_warp = *qobject_cast<MapWarp*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapWarpUI::Private::Private(MapWarp* warp) :
        ObjectUIPrivate(warp),
        m_warp(warp)
{
}

MapWarpUI::Private::~Private()
{
    delete m_warp;
}

QWidget* MapWarpUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/mapwarp.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_toMap = form->findChild<KComboBox*>("varToMap");
    ui_toWarp = form->findChild<KComboBox*>("varToWarp");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_toMap, SIGNAL(currentIndexChanged(int)), this, SLOT(toMapChanged(int)));
    connect(ui_toWarp, SIGNAL(currentIndexChanged(int)), this, SLOT(toWarpChanged(int)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    ui_toWarp->setEnabled(0 <= m_warp->toMap());
    return form;
}

void MapWarpUI::Private::refreshGui()
{
    const bool blocked = ui_toMap->blockSignals(true);
    ui_toMap->clear();
    for (int i = 0; i < m_warp->game()->mapCount(); ++i)
        ui_toMap->addItem(m_warp->game()->map(i)->name());
    ui_toMap->blockSignals(blocked);
    ui_toWarp->setEnabled(false);
    ObjectUIPrivate::refreshGui();
}

void MapWarpUI::Private::resetGui()
{
    ui_name->setText(m_warp->name());
    ui_toMap->setCurrentIndex(m_warp->game()->mapIndex(m_warp->toMap()));
    const Map* map = m_warp->game()->mapById(m_warp->toMap());
    ui_toWarp->setCurrentIndex(map ? map->warpIndex(m_warp->toWarp()) : -1);
    ui_script->setValue(m_warp->script());
}

void MapWarpUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_warp->setName(name);
    ui_name->setCursorPosition(cursor);
}

void MapWarpUI::Private::toMapChanged(const int toMap)
{
    if (0 <= toMap)
    {
        const Map* map = m_warp->game()->map(toMap);
        m_warp->setToMap(map->id());
        ui_toWarp->setEnabled(true);
        const bool blocked = ui_toWarp->blockSignals(true);
        ui_toWarp->clear();
        if (map)
        {
            for (int i = 0; i < map->warpCount(); ++i)
                ui_toWarp->addItem(map->warp(i)->name());
        }
        ui_toWarp->blockSignals(blocked);
    }
    else
        ui_toWarp->setEnabled(false);
}

void MapWarpUI::Private::toWarpChanged(const int toWarp)
{
    if (0 <= toWarp)
    {
        const Map* map = m_warp->game()->map(m_warp->toMap());
        if (map)
            m_warp->setToWarp(map->warp(toWarp)->id());
    }
}

void MapWarpUI::Private::scriptChanged(const Script& script)
{
    m_warp->setScript(script);
}
