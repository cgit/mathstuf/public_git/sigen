/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTrainerUI.h"
#include "MapTrainerUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/MapTrainer.h>
#include <sigmod/MapTrainerTeamMember.h>
#include <sigmod/Rules.h>
#include <sigmod/Species.h>
#include <sigmod/Trainer.h>

// KDE includes
#include <KActionSelector>
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QListWidget>
#include <QtGui/QListWidgetItem>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

MapTrainerUI::MapTrainerUI(MapTrainer* trainer, QWidget* parent) :
        ObjectUI(trainer, parent),
        d(new Private(new MapTrainer(*trainer)))
{
    setPrivate(d);
}

void MapTrainerUI::apply()
{
    *qobject_cast<MapTrainer*>(m_object) = *d->m_trainer;
    ObjectUI::apply();
}

void MapTrainerUI::discard()
{
    *d->m_trainer = *qobject_cast<MapTrainer*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MapTrainerUI::Private::Private(MapTrainer* trainer) :
        ObjectUIPrivate(trainer),
        m_trainer(trainer)
{
}

MapTrainerUI::Private::~Private()
{
    delete m_trainer;
}

QWidget* MapTrainerUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/maptrainer.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_trainerClass = form->findChild<KComboBox*>("varTrainerClass");
    ui_numberFight = form->findChild<KIntNumInput*>("varNumberFight");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    ui_leadTeamMembers = form->findChild<KActionSelector*>("varLeadTeamMembers");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_trainerClass, SIGNAL(currentIndexChanged(int)), this, SLOT(trainerClassChanged(int)));
    connect(ui_numberFight, SIGNAL(valueChanged(int)), this, SLOT(numberFightChanged(int)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    connect(ui_leadTeamMembers, SIGNAL(added(QListWidgetItem*)), this, SLOT(leadTeamMemberAdded(QListWidgetItem*)));
    connect(ui_leadTeamMembers, SIGNAL(removed(QListWidgetItem*)), this, SLOT(leadTeamMemberRemoved(QListWidgetItem*)));
    return form;
}

void MapTrainerUI::Private::refreshGui()
{
    const bool blockedTrainerClass = ui_trainerClass->blockSignals(true);
    ui_trainerClass->clear();
    for (int i = 0; i < m_trainer->game()->trainerCount(); ++i)
        ui_trainerClass->addItem(m_trainer->game()->trainer(i)->name());
    ui_trainerClass->blockSignals(blockedTrainerClass);
    ui_numberFight->setMaximum(m_trainer->game()->rules()->maxFight());
    const bool blockedLeadTeamMember = ui_leadTeamMembers->blockSignals(true);
    ui_leadTeamMembers->availableListWidget()->clear();
    ui_leadTeamMembers->selectedListWidget()->clear();
    for (int i = 0; i < m_trainer->teamMemberCount(); ++i)
    {
        QString speciesName;
        const MapTrainerTeamMember* teamMember = m_trainer->teamMember(i);
        const Species* species = m_trainer->game()->species(teamMember->species());
        speciesName = species ? species->name() : "(Invalid)";
        QListWidgetItem* widgetItem = new QListWidgetItem(QString("%1 level %2 (%3)").arg(speciesName, teamMember->level(), i), ui_leadTeamMembers->availableListWidget());
        widgetItem->setData(Qt::UserRole, teamMember->id());
    }
    ui_leadTeamMembers->blockSignals(blockedLeadTeamMember);
    ui_leadTeamMembers->setButtonsEnabled();
    ObjectUIPrivate::refreshGui();
}

void MapTrainerUI::Private::resetGui()
{
    ui_name->setText(m_trainer->name());
    ui_trainerClass->setCurrentIndex(m_trainer->game()->trainerIndex(m_trainer->trainerClass()));
    ui_numberFight->setValue(m_trainer->numberFight());
    ui_script->setValue(m_trainer->script());
    for (int i = 0; i < ui_leadTeamMembers->availableListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_leadTeamMembers->availableListWidget()->item(i);
        if (m_trainer->leadTeamMember(widgetItem->data(Qt::UserRole).toInt()))
            ui_leadTeamMembers->selectedListWidget()->addItem(ui_leadTeamMembers->availableListWidget()->takeItem(i--));
    }
    for (int i = 0; i < ui_leadTeamMembers->selectedListWidget()->count(); ++i)
    {
        QListWidgetItem* widgetItem = ui_leadTeamMembers->selectedListWidget()->item(i);
        if (!m_trainer->leadTeamMember(widgetItem->data(Qt::UserRole).toInt()))
            ui_leadTeamMembers->availableListWidget()->addItem(ui_leadTeamMembers->selectedListWidget()->takeItem(i--));
    }
    ui_leadTeamMembers->setButtonsEnabled();
}

void MapTrainerUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_trainer->setName(name);
    ui_name->setCursorPosition(cursor);
}

void MapTrainerUI::Private::trainerClassChanged(const int trainerClass)
{
    if (0 <= trainerClass)
        m_trainer->setTrainerClass(m_trainer->game()->trainer(trainerClass)->id());
}

void MapTrainerUI::Private::numberFightChanged(const int numberFight)
{
    m_trainer->setNumberFight(numberFight);
}

void MapTrainerUI::Private::scriptChanged(const Script& script)
{
    m_trainer->setScript(script);
}

void MapTrainerUI::Private::leadTeamMemberAdded(QListWidgetItem* item)
{
    m_trainer->setLeadTeamMember(item->data(Qt::UserRole).toInt(), true);
}

void MapTrainerUI::Private::leadTeamMemberRemoved(QListWidgetItem* item)
{
    m_trainer->setLeadTeamMember(item->data(Qt::UserRole).toInt(), false);
}
