/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "ItemUI.h"
#include "ItemUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Item.h>
#include <sigmod/ItemType.h>
#include <sigmod/Rules.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Standard includes
#include <climits>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

ItemUI::ItemUI(Item* item, QWidget* parent) :
        ObjectUI(item, parent),
        d(new Private(new Item(*item)))
{
    setPrivate(d);
}

void ItemUI::apply()
{
    *qobject_cast<Item*>(m_object) = *d->m_item;
    ObjectUI::apply();
}

void ItemUI::discard()
{
    *d->m_item = *qobject_cast<Item*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

ItemUI::Private::Private(Item* item) :
        ObjectUIPrivate(item),
        m_item(item)
{
}

ItemUI::Private::~Private()
{
    delete m_item;
}

QWidget* ItemUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/item.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_type = form->findChild<KComboBox*>("varType");
    ui_price = form->findChild<KIntNumInput*>("varPrice");
    ui_sellPrice = form->findChild<KIntNumInput*>("varSellPrice");
    ui_weight = form->findChild<KIntNumInput*>("varWeight");
    ui_description = form->findChild<KLineEdit*>("varDescription");
    ui_script = form->findChild<ScriptWidget*>("varScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_type, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged(int)));
    connect(ui_price, SIGNAL(valueChanged(int)), this, SLOT(priceChanged(int)));
    connect(ui_sellPrice, SIGNAL(valueChanged(int)), this, SLOT(sellPriceChanged(int)));
    connect(ui_weight, SIGNAL(valueChanged(int)), this, SLOT(weightChanged(int)));
    connect(ui_description, SIGNAL(textChanged(QString)), this, SLOT(descriptionChanged(QString)));
    connect(ui_script, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(scriptChanged(Sigcore::Script)));
    if (0 <= m_item->type())
    {
        const ItemType* itemType = m_item->game()->itemType(m_item->type());
        ui_weight->setMaximum(itemType->maxWeight());
        ui_weight->setEnabled(0 < itemType->maxWeight());
    }
    else
        ui_weight->setMaximum(INT_MAX);
    return form;
}

void ItemUI::Private::refreshGui()
{
    const bool blocked = ui_type->blockSignals(true);
    ui_type->clear();
    for (int i = 0; i < m_item->game()->itemTypeCount(); ++i)
        ui_type->addItem(m_item->game()->itemType(i)->name());
    ui_type->blockSignals(blocked);
    ui_price->setMaximum(m_item->game()->rules()->maxMoney());
    ui_price->setEnabled(0 < m_item->game()->rules()->maxMoney());
    ui_sellPrice->setMaximum(m_item->game()->rules()->maxMoney());
    ObjectUIPrivate::refreshGui();
}

void ItemUI::Private::resetGui()
{
    ui_name->setText(m_item->name());
    ui_type->setCurrentIndex(m_item->game()->itemTypeIndex(m_item->type()));
    ui_price->setValue(m_item->price());
    ui_sellPrice->setValue(m_item->sellPrice());
    ui_weight->setValue(m_item->weight());
    ui_description->setText(m_item->description());
    ui_script->setValue(m_item->script());
}

void ItemUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_item->setName(name);
    ui_name->setCursorPosition(cursor);
}

void ItemUI::Private::typeChanged(const int type)
{
    if (0 <= type)
    {
        const ItemType* itemType = m_item->game()->itemType(type);
        m_item->setType(itemType->id());
        ui_weight->setMaximum(itemType->maxWeight());
        ui_weight->setEnabled(0 < itemType->maxWeight());
    }
    else
        ui_weight->setMaximum(INT_MAX);
}

void ItemUI::Private::priceChanged(const int price)
{
    m_item->setPrice(price);
}

void ItemUI::Private::sellPriceChanged(const int sellPrice)
{
    m_item->setSellPrice(sellPrice);
}

void ItemUI::Private::weightChanged(const int weight)
{
    m_item->setWeight(weight);
}

void ItemUI::Private::descriptionChanged(const QString& description)
{
    const int cursor = ui_description->cursorPosition();
    m_item->setDescription(description);
    ui_description->setCursorPosition(cursor);
}

void ItemUI::Private::scriptChanged(const Script& script)
{
    m_item->setScript(script);
}
