/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_OBJECTUI
#define SIGMODRWIDGETS_OBJECTUI

// Sigmodr widget includes
#include "Global.h"

// Qt includes
#include <QtGui/QWidget>

// Forward declarations
namespace Sigmod
{
class Game;
class Object;
}

namespace Sigmodr
{
namespace Widgets
{
class ObjectUIPrivate;
class ValidationDialog;

class SIGMODRWIDGETS_EXPORT ObjectUI : public QWidget
{
    Q_OBJECT
    
    public:
        ObjectUI(Sigmod::Object* object, QWidget* parent);
        virtual ~ObjectUI();
        
        const Sigmod::Object* object() const;
        Sigmod::Object* object();
    public slots:
        virtual void apply();
        virtual void discard();
        void validate();
    signals:
        void saved();
        void changed(const bool changed = true);
    protected:
        void setPrivate(ObjectUIPrivate* priv);
        
        Sigmod::Object* const m_object;
    private:
        bool m_changed;
        
        ValidationDialog* m_validator;
    private slots:
        void setChanged(const bool changed = true);
};
}
}

#endif
