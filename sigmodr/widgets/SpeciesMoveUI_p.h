/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_SPECIESMOVEUI_P
#define SIGMODRWIDGETS_SPECIESMOVEUI_P

// Header include
#include "SpeciesMoveUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KComboBox;
class KIntNumInput;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT SpeciesMoveUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::SpeciesMove* move);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::SpeciesMove* m_move;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void moveChanged(const int move);
        void levelChanged(const int level);
        void wildLevelChanged(const int wildLevel);
    private:
        KComboBox* ui_move;
        KIntNumInput* ui_level;
        KIntNumInput* ui_wildLevel;
};
}
}

#endif
