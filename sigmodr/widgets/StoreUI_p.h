/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMODRWIDGETS_STOREUI_P
#define SIGMODRWIDGETS_STOREUI_P

// Header include
#include "StoreUI.h"

// Sigmodr widget includes
#include "ObjectUIPrivate.h"

// Forward declarations
class KActionSelector;
class KLineEdit;
class QListWidgetItem;

namespace Sigmodr
{
namespace Widgets
{
class SIGMODRWIDGETS_NO_EXPORT StoreUI::Private : public ObjectUIPrivate
{
    Q_OBJECT
    
    public:
        Private(Sigmod::Store* store);
        ~Private();
        
        QWidget* makeWidgets(ObjectUI* widget);
        
        Sigmod::Store* m_store;
    public slots:
        void refreshGui();
        void resetGui();
    protected slots:
        void nameChanged(const QString& name);
        void itemAdded(QListWidgetItem* item);
        void itemRemoved(QListWidgetItem* item);
    private:
        KLineEdit* ui_name;
        KActionSelector* ui_items;
};
}
}

#endif
