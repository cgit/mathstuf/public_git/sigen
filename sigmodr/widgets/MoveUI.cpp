/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MoveUI.h"
#include "MoveUI_p.h"

// Sigmodr core widget includes
#include <sigmodr/corewidgets/FractionWidget.h>
#include <sigmodr/corewidgets/ScriptWidget.h>

// Sigmod includes
#include <sigmod/Game.h>
#include <sigmod/Move.h>
#include <sigmod/Type.h>

// KDE includes
#include <KComboBox>
#include <KIntNumInput>
#include <KLineEdit>

// Qt includes
#include <QtGui/QCheckBox>

using namespace Sigcore;
using namespace Sigmod;
using namespace Sigmodr::CoreWidgets;
using namespace Sigmodr::Widgets;

MoveUI::MoveUI(Move* move, QWidget* parent) :
        ObjectUI(move, parent),
        d(new Private(new Move(*move)))
{
    setPrivate(d);
}

void MoveUI::apply()
{
    *qobject_cast<Move*>(m_object) = *d->m_move;
    ObjectUI::apply();
}

void MoveUI::discard()
{
    *d->m_move = *qobject_cast<Move*>(m_object);
    d->resetGui();
    ObjectUI::discard();
}

MoveUI::Private::Private(Move* move) :
        ObjectUIPrivate(move),
        m_move(move)
{
}

MoveUI::Private::~Private()
{
    delete m_move;
}

QWidget* MoveUI::Private::makeWidgets(ObjectUI* widget)
{
    QWidget *form = openUiFile(":/gui/move.ui", widget);
    ui_name = form->findChild<KLineEdit*>("varName");
    ui_priority = form->findChild<KIntNumInput*>("varPriority");
    ui_accuracy = form->findChild<FractionWidget*>("varAccuracy");
    ui_power = form->findChild<KIntNumInput*>("varPower");
    ui_type = form->findChild<KComboBox*>("varType");
    ui_powerPoints = form->findChild<KIntNumInput*>("varPowerPoints");
    ui_special = form->findChild<QCheckBox*>("varSpecial");
    ui_description = form->findChild<KLineEdit*>("varDescription");
    ui_battleScript = form->findChild<ScriptWidget*>("varBattleScript");
    ui_worldScript = form->findChild<ScriptWidget*>("varWorldScript");
    ui_priorityScript = form->findChild<ScriptWidget*>("varPriorityScript");
    connect(ui_name, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
    connect(ui_priority, SIGNAL(valueChanged(int)), this, SLOT(priorityChanged(int)));
    connect(ui_accuracy, SIGNAL(valueChanged(Sigcore::Fraction)), this, SLOT(accuracyChanged(Sigcore::Fraction)));
    connect(ui_power, SIGNAL(valueChanged(int)), this, SLOT(powerChanged(int)));
    connect(ui_type, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged(int)));
    connect(ui_powerPoints, SIGNAL(valueChanged(int)), this, SLOT(powerPointsChanged(int)));
    connect(ui_special, SIGNAL(toggled(bool)), this, SLOT(specialChanged(bool)));
    connect(ui_description, SIGNAL(textChanged(QString)), this, SLOT(descriptionChanged(QString)));
    connect(ui_battleScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(battleScriptChanged(Sigcore::Script)));
    connect(ui_worldScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(worldScriptChanged(Sigcore::Script)));
    connect(ui_priorityScript, SIGNAL(valueChanged(Sigcore::Script)), this, SLOT(priorityScriptChanged(Sigcore::Script)));
    return form;
}

void MoveUI::Private::refreshGui()
{
    const bool blocked = ui_type->blockSignals(true);
    ui_type->clear();
    for (int i = 0; i < m_move->game()->typeCount(); ++i)
        ui_type->addItem(m_move->game()->type(i)->name());
    ui_type->blockSignals(blocked);
    ObjectUIPrivate::refreshGui();
}

void MoveUI::Private::resetGui()
{
    ui_name->setText(m_move->name());
    ui_priority->setValue(m_move->priority());
    ui_accuracy->setValue(m_move->accuracy());
    ui_power->setValue(m_move->power());
    ui_type->setCurrentIndex(m_move->game()->typeIndex(m_move->type()));
    ui_powerPoints->setValue(m_move->powerPoints());
    ui_special->setChecked(m_move->special() ? Qt::Checked : Qt::Unchecked);
    ui_description->setText(m_move->description());
    ui_battleScript->setValue(m_move->battleScript());
    ui_worldScript->setValue(m_move->worldScript());
    ui_priorityScript->setValue(m_move->priorityScript());
}

void MoveUI::Private::nameChanged(const QString& name)
{
    const int cursor = ui_name->cursorPosition();
    m_move->setName(name);
    ui_name->setCursorPosition(cursor);
}

void MoveUI::Private::priorityChanged(const int priority)
{
    m_move->setPriority(priority);
}

void MoveUI::Private::accuracyChanged(const Fraction& accuracy)
{
    m_move->setAccuracy(accuracy);
}

void MoveUI::Private::powerChanged(const int power)
{
    m_move->setPower(power);
}

void MoveUI::Private::typeChanged(const int type)
{
    if (0 <= type)
        m_move->setType(m_move->game()->type(type)->id());
}

void MoveUI::Private::powerPointsChanged(const int powerPoints)
{
    m_move->setPowerPoints(powerPoints);
}

void MoveUI::Private::specialChanged(const bool special)
{
    m_move->setSpecial(special);
}

void MoveUI::Private::descriptionChanged(const QString& description)
{
    const int cursor = ui_description->cursorPosition();
    m_move->setDescription(description);
    ui_description->setCursorPosition(cursor);
}

void MoveUI::Private::battleScriptChanged(const Script& battleScript)
{
    m_move->setBattleScript(battleScript);
}

void MoveUI::Private::worldScriptChanged(const Script& worldScript)
{
    m_move->setWorldScript(worldScript);
}

void MoveUI::Private::priorityScriptChanged(const Script& priorityScript)
{
    m_move->setPriorityScript(priorityScript);
}
