project(sigmodr)

add_subdirectory(corewidgets)
add_subdirectory(widgets)
add_subdirectory(tree)

include_directories(
    ${PROJECT_BINARY_DIR}
)

set(sigmodr_FORMS
    sigmodr.ui
)
kde4_add_ui_files(sigmodr_UI_HEADERS ${sigmodr_FORMS})
set(sigmodr_KCFGC_FILES
    SigmodrPreferences.kcfgc
)
kde4_add_kcfg_files(sigmodr_KCFGC_SRCS ${sigmodr_KCFGC_FILES})
set(sigmodr_SRCS
    Sigmodr.cpp
    SigmodrPreferencesWidget.cpp
    SigmodrUI.cpp
)
set(sigmodr_RC_FILES
    sigmodrui.rc
)
set(sigmodr_KCFG_FILES
    sigmodr.kcfg
)
set(sigmodr_DESKTOP_FILES
    sigmodr.desktop
)

kde4_add_executable(sigmodr
    ${sigmodr_SRCS}
    ${sigmodr_UI_HEADERS}
    ${sigmodr_KCFGC_SRCS}
)
set_target_properties(sigmodr
    PROPERTIES
        LINK_INTERFACE_LIBRARIES ""
)
target_link_libraries(sigmodr
    ${QT_QTXML_LIBRARY}
    ${KDE4_KDEUI_LIBRARY}
    ${KDE4_KIO_LIBRARY}
    ${KDE4_KNEWSTUFF2_LIBRARY}
    sigmod
    sigmodrwidgets
    sigmodrtree
)

add_subdirectory(doc)

install(
    TARGETS
        sigmodr
    DESTINATION
        ${BIN_INSTALL_DIR}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigmodr_RC_FILES}
    DESTINATION
        ${DATA_INSTALL_DIR}/${PROJECT_NAME}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigmodr_KCFG_FILES}
    DESTINATION
        ${KCFG_INSTALL_DIR}
    COMPONENT
        runtime
)

install(
    FILES
        ${sigmodr_DESKTOP_FILES}
    DESTINATION
        ${XDG_APPS_INSTALL_DIR}
    COMPONENT
        metadata
)
