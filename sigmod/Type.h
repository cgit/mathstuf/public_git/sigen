/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Type.h
 */

#ifndef SIGMOD_TYPE
#define SIGMOD_TYPE

// Sigcore includes
#include <sigcore/Fraction.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Type Type.h sigmod/Type.h
 * \brief Types label species and moves.
 * 
 * Types allow for grouping of moves and species for effectiveness or
 * other effects (e.g. a move using water may put out a fire).
 */
class SIGMOD_EXPORT Type : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param type The type to copy.
         */
        Type(const Type& type);
        /**
         * Create a new type belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        Type(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p type as a child of \p parent with id \p id.
         * 
         * \param type The type to copy the data from.
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        Type(const Type& type, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        Type(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the type's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the type in XML format.
         * 
         * \return The XML structure representing the type.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the type.
         * 
         * \param name The name of the type.
         */
        void setName(const QString& name);
        /**
         * Sets the STAB (Same Type Attack Bnus) for the type. When a species that has this type
         * uses a move of the same type, it gets a bonus multiplier.
         * 
         * \param stab the STAB of the type.
         */
        void setStab(const Sigcore::Fraction& stab);
        
        /**
         * \sa setName
         * 
         * \return The name of the type.
         */
        QString name() const;
        /**
         * \sa setStab
         * 
         * \return The STAB of the type.
         */
        Sigcore::Fraction stab() const;
        
        bool nameCheck(const QString& name) const;
        bool stabCheck(const Sigcore::Fraction& stab) const;
        
        Type& operator=(const Type& rhs);
    private:
        QString m_name;
        Sigcore::Fraction m_stab;
};
}

#endif
