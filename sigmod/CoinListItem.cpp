/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/CoinListItem.cpp
 */

// Header include
#include "CoinListItem.h"

// Sigmod includes
#include "CoinList.h"
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

const QStringList CoinListItem::TypeStr = QStringList() << "Item" << "Team Member";

CoinListItem::CoinListItem(const CoinListItem& item) :
        Object(item.parent(), item.id())
{
    *this = item;
}

CoinListItem::CoinListItem(const CoinList* parent, const int id) :
        Object(parent, id),
        m_type(Item),
        m_object(-1),
        m_cost(0)
{
}

CoinListItem::CoinListItem(const CoinListItem& item, const CoinList* parent, const int id) :
        Object(parent, id)
{
    *this = item;
}

CoinListItem::CoinListItem(const QDomElement& xml, const CoinList* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void CoinListItem::validate()
{
    TEST_BEGIN();
    TEST(object);
    TEST(cost);
    TEST_END();
}

void CoinListItem::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD_ENUM(type, Type);
    LOAD(object);
    LOAD(cost);
}

QDomElement CoinListItem::save() const
{
    SAVE_CREATE();
    SAVE_ENUM(type, Type);
    SAVE(object);
    SAVE(cost);
    return xml;
}

SETTER(CoinListItem, Type, Type, type)
SETTER(CoinListItem, int, Object, object)
SETTER(CoinListItem, int, Cost, cost)

GETTER(CoinListItem, CoinListItem::Type, type)
GETTER(CoinListItem, int, object)
GETTER(CoinListItem, int, cost)

CHECK(CoinListItem, Type, type)
CHECK_BEGIN(CoinListItem, int, object)
    switch (m_type)
    {
        case Item:
            IBOUNDS(object, game(), item)
            break;
        case Species:
            IBOUNDS(object, game(), species)
            break;
    }
CHECK_END()
CHECK_BOUNDS(CoinListItem, int, cost, 1, INT_MAX)

CoinListItem& CoinListItem::operator=(const CoinListItem& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(type);
    COPY(object);
    COPY(cost);
    return *this;
}
