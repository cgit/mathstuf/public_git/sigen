/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Tile.cpp
 */

// Header include
#include "Tile.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Tile::Tile(const Tile& tile) :
        Object(tile.parent(), tile.id())
{
    *this = tile;
}

Tile::Tile(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_walkable(true),
        m_script("", ""),
        m_preview(-1)
{
}

Tile::Tile(const Tile& tile, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = tile;
}

Tile::Tile(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Tile::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void Tile::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(walkable);
    LOAD(script);
    LOAD(preview);
}

QDomElement Tile::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(walkable);
    SAVE(script);
    SAVE(preview);
    return xml;
}

SETTER(Tile, QString&, Name, name)
SETTER(Tile, bool, Walkable, walkable)
SETTER(Tile, Script&, Script, script)
SETTER(Tile, int, Preview, preview)

GETTER(Tile, QString, name)
GETTER(Tile, bool, walkable)
GETTER(Tile, Script, script)
GETTER(Tile, int, preview)

CHECK(Tile, QString&, name)
CHECK(Tile, bool, walkable)
CHECK(Tile, Script&, script)
CHECK_INDEX(Tile, int, preview, game(), sprite)

Tile& Tile::operator=(const Tile& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(walkable);
    COPY(script);
    COPY(preview);
    return *this;
}
