/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Nature.h
 */

#ifndef SIGMOD_NATURE
#define SIGMOD_NATURE

// Sigcore includes
#include <sigcore/Fraction.h>

// Sigmod includes
#include "Object.h"
#include "Stat.h"

// Qt includes
#include <QtCore/QVarLengthArray>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Nature Nature.h sigmod/Nature.h
 * \brief Class describing a nature.
 * 
 * Natures are qualities of creatures that slightly modify the stats to help
 * individualize them.
 */
class SIGMOD_EXPORT Nature : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param badge The badge to copy.
         */
        Nature(const Nature& nature);
        /**
         * Create a new badge belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Nature(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p badge as a child of \p parent and id \p id.
         * 
         * \param badge The badge to copy the data from.
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Nature(const Nature& nature, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Nature(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the badge's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the badge in XML format.
         * 
         * \return The XML structure representing the badge.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the nature.
         * 
         * \param name The name of the nature.
         */
        void setName(const QString& name);
        /**
         * Sets a multiplier for a stat for creatures with the nature.
         * 
         * \param stat The stat to set the multiplier for.
         * \param multiplier The multiplier for the stat.
         */
        void setStat(const Stat stat, const Sigcore::Fraction& multiplier);
        /**
         * Sets the relative chance of having this nature compared to other natures.
         * 
         * \param weight The weight of the nature.
         */
        void setWeight(const int weight);
        
        /**
         * \sa setNature
         * 
         * \return The name of the nature.
         */
        QString name() const;
        /**
         * \sa setStat
         * 
         * \param stat The stat to get the multiplier for.
         * 
         * \return The multiplier for \p stat.
         */
        Sigcore::Fraction stat(const Stat stat) const;
        /**
         * \sa setNature
         * 
         * \return The weighting of the nature.
         */
        int weight() const;
        
        bool nameCheck(const QString& name) const;
        bool statCheck(const Stat stat, const Sigcore::Fraction& multiplier) const;
        bool weightCheck(const int weight) const;
        
        Nature& operator=(const Nature& rhs);
    private:
        void setStat(const Sigcore::Fraction& multiplier);
        
        QString m_name;
        QVarLengthArray<Sigcore::Fraction, ST_SpecialDefense - ST_Attack + 1> m_stat;
        int m_weight;
};
}

#endif
