/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapWarp.cpp
 */

// Header include
#include "MapWarp.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Map.h"

using namespace Sigcore;
using namespace Sigmod;

MapWarp::MapWarp(const MapWarp& warp) :
        Object(warp.parent(), warp.id())
{
    *this = warp;
}

MapWarp::MapWarp(const Map* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_position(0, 0),
        m_area(),
        m_toMap(INT_MAX),
        m_toWarp(INT_MAX),
        m_script("", "")
{
}

MapWarp::MapWarp(const MapWarp& warp, const Map* parent, const int id) :
        Object(parent, id)
{
    *this = warp;
}

MapWarp::MapWarp(const QDomElement& xml, const Map* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void MapWarp::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(position);
    TEST(area);
    TEST(toMap);
    TEST(toWarp);
    TEST_END();
}

void MapWarp::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(position);
    LOAD(area);
    LOAD(toMap);
    LOAD(toWarp);
    LOAD(script);
}

QDomElement MapWarp::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(position);
    SAVE(area);
    SAVE(toMap);
    SAVE(toWarp);
    SAVE(script);
    return xml;
}

SETTER(MapWarp, QString&, Name, name)
SETTER(MapWarp, QPoint&, Position, position)
SETTER(MapWarp, QPainterPath&, Area, area)
SETTER(MapWarp, int, ToMap, toMap)
SETTER(MapWarp, int, ToWarp, toWarp)
SETTER(MapWarp, Script&, Script, script)

GETTER(MapWarp, QString, name)
GETTER(MapWarp, QPoint, position)
GETTER(MapWarp, QPainterPath, area)
GETTER(MapWarp, int, toMap)
GETTER(MapWarp, int, toWarp)
GETTER(MapWarp, Script, script)

CHECK(MapWarp, QString&, name)
CHECK_BEGIN(MapWarp, QPoint&, position)
    const Map* map = qobject_cast<const Map*>(parent());
    TBOUNDS_MOD(position_x, 0, map->width() - 1, position.x())
    TBOUNDS_MOD(position_y, 0, map->height() - 1, position.y())
CHECK_END()
CHECK_BEGIN(MapWarp, QPainterPath&, area)
    if (area.isEmpty())
    {
        ERROR("Warp has no area");
        return false;
    }
    const Map* map = qobject_cast<const Map*>(parent());
    if (!area.intersects(QRect(-m_position.x(), -m_position.y(), map->width(), map->height())))
    {
        ERROR("Warp is not inside the map");
        return false;
    }
CHECK_END()
CHECK_INDEX(MapWarp, int, toMap, game(), map)
CHECK_BEGIN(MapWarp, int, toWarp)
    const Map* map = game()->mapById(m_toMap);
    if (!map)
    {
        EBOUNDS_IDX(m_toMap);
        return false;
    }
    IBOUNDS(toWarp, map, warp)
CHECK_END()
CHECK(MapWarp, Script&, script)

MapWarp& MapWarp::operator=(const MapWarp& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(position);
    COPY(area);
    COPY(toMap);
    COPY(toWarp);
    COPY(script);
    return *this;
}
