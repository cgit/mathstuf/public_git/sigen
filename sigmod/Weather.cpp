/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Weather.cpp
 */

// Header include
#include "Weather.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Weather::Weather(const Weather& weather) :
        Object(weather.parent(), weather.id())
{
    *this = weather;
}

Weather::Weather(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_script("", "")
{
}

Weather::Weather(const Weather& weather, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = weather;
}

Weather::Weather(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Weather::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void Weather::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(script);
}

QDomElement Weather::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(script);
    return xml;
}

SETTER(Weather, QString&, Name, name)
SETTER(Weather, Script&, Script, script)

GETTER(Weather, QString, name)
GETTER(Weather, Script, script)

CHECK(Weather, QString&, name)
CHECK(Weather, Script&, script)

Weather& Weather::operator=(const Weather& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(script);
    return *this;
}
