/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Author.h
 */

#ifndef SIGMOD_AUTHOR
#define SIGMOD_AUTHOR

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Author Author.h sigmod/Author.h
 * \brief Class describing an author of the sigmod.
 * 
 * Authors are used to allow scripts to create credits.
 */
class SIGMOD_EXPORT Author : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param author The author to copy.
         */
        Author(const Author& author);
        /**
         * Create a new author belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the author.
         * \param id The id number for the author.
         */
        Author(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p author as a child of \p parent and id \p id.
         * 
         * \param author The author to copy the data from.
         * \param parent The parent of the author.
         * \param id The id number for the author.
         */
        Author(const Author& author, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the author.
         * \param id The id number for the author.
         */
        Author(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the author's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the author in XML format.
         * 
         * \return The XML structure representing the author.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the author.
         * 
         * \param name The name of the author.
         */
        void setName(const QString& name);
        /**
         * Sets the email of the author. It is not checked to be well-formed until the author is \link Author::validate \p validated \endlink.
         * 
         * \param email The email of the author.
         */
        void setEmail(const QString& email);
        /**
         * Sets what the author did for the sigmod. The default credits generator assumes that the roles are comma-separated.
         * 
         * \param role The role(s) of the author.
         */
        void setRole(const QString& role);
        
        /**
         * \sa setName
         * 
         * \return The name of the author.
         */
        QString name() const;
        /**
         * \sa setEmail
         * 
         * \return The email address of the author.
         */
        QString email() const;
        /**
         * \sa setRole
         * 
         * \return The role(s) of the author.
         */
        QString role() const;
        
        bool nameCheck(const QString& name) const;
        bool emailCheck(const QString& email) const;
        bool roleCheck(const QString& role) const;
        
        Author& operator=(const Author& rhs);
    private:
        QString m_name;
        QString m_email;
        QString m_role;
};
}

#endif
