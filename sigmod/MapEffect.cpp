/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapEffect.cpp
 */

// Header include
#include "MapEffect.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Map.h"

using namespace Sigcore;
using namespace Sigmod;

MapEffect::MapEffect(const MapEffect& effect) :
        Object(effect.parent(), effect.id())
{
    *this = effect;
}

MapEffect::MapEffect(const Map* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_position(0, 0),
        m_area(),
        m_skin(-1),
        m_isGhost(false),
        m_script("", "")
{
}

MapEffect::MapEffect(const MapEffect& effect, const Map* parent, const int id) :
        Object(parent, id)
{
    *this = effect;
}

MapEffect::MapEffect(const QDomElement& xml, const Map* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void MapEffect::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(position);
    TEST(skin);
    TEST_END();
}

void MapEffect::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(position);
    LOAD(area);
    LOAD(skin);
    LOAD(isGhost);
    LOAD(script);
}

QDomElement MapEffect::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(position);
    SAVE(area);
    SAVE(skin);
    SAVE(isGhost);
    SAVE(script);
    return xml;
}

SETTER(MapEffect, QString&, Name, name)
SETTER(MapEffect, QPoint&, Position, position)
SETTER(MapEffect, QPainterPath&, Area, area)
SETTER(MapEffect, int, Skin, skin)
SETTER(MapEffect, bool, IsGhost, isGhost)
SETTER(MapEffect, Script&, Script, script)

GETTER(MapEffect, QString, name)
GETTER(MapEffect, QPoint, position)
GETTER(MapEffect, QPainterPath, area)
GETTER(MapEffect, int, skin)
GETTER(MapEffect, bool, isGhost)
GETTER(MapEffect, Script, script)

CHECK(MapEffect, QString&, name)
CHECK_BEGIN(MapEffect, QPoint&, position)
    const Map* map = qobject_cast<const Map*>(parent());
    TBOUNDS_MOD(position_x, 0, map->width() - 1, position.x())
    TBOUNDS_MOD(position_y, 0, map->height() - 1, position.y())
CHECK_END()
CHECK(MapEffect, QPainterPath&, area)
CHECK_BEGIN(MapEffect, int, skin)
    if (0 <= skin)
        IBOUNDS(skin, game(), skin)
CHECK_END()
CHECK(MapEffect, bool, isGhost)
CHECK(MapEffect, Script&, script)

MapEffect& MapEffect::operator=(const MapEffect& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(position);
    COPY(area);
    COPY(skin);
    COPY(isGhost);
    COPY(script);
    return *this;
}
