/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_SPECIES
#define SIGMOD_SPECIES

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"
#include "Stat.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QVarLengthArray>

namespace Sigmod
{
// Forward declarations
class Game;
class SpeciesMove;

class SIGMOD_EXPORT Species : public Object
{
    Q_OBJECT
    Q_ENUMS(Style)
    
    public:
        enum Style
        {
            Fluctuating = 0,
            Fading = 1,
            Slow = 2,
            Normal = 3,
            Fast = 4,
            Erratic = 5
        };
        static const QStringList StyleStr;
        
        Species(const Species& species);
        Species(const Game* parent, const int id);
        Species(const Species& species, const Game* parent, const int id);
        Species(const QDomElement& xml, const Game* parent, const int id = -1);
        ~Species();
        
        void validate();
        
        void load(const QDomElement& xml);
        QDomElement save() const;
        
        void setName(const QString& name);
        void setBaseStat(const Stat stat, const int base);
        void setEffortValue(const Stat stat, const int effortValue);
        void setGrowth(const Style growth);
        void setExperienceValue(const int experienceValue);
        void setCatchValue(const int catchValue);
        void setMaxHoldWeight(const int maxHoldWeight);
        void setRunChance(const Sigcore::Fraction& runChance);
        void setFleeChance(const Sigcore::Fraction& fleeChance);
        void setItemChance(const Sigcore::Fraction& itemChance);
        void setEncyclopediaNumber(const int encyclopediaNumber);
        void setWeight(const int weight);
        void setHeight(const int height);
        void setEncyclopediaEntry(const QString& encyclopediaEntry);
        void setFrontMaleSprite(const int frontMaleSprite);
        void setBackMaleSprite(const int backMaleSprite);
        void setFrontFemaleSprite(const int frontFemaleSprite);
        void setBackFemaleSprite(const int backFemaleSprite);
        void setGenderFactor(const Sigcore::Fraction& genderFactor);
        void setSkin(const int skin);
        void setEggSpecies(const int eggSpecies);
        void setEggSteps(const int eggSteps);
        void setType(const int type, const bool state);
        void setEggGroup(const int eggGroup, const bool state);
        void setEvolution(const Sigcore::Script& evolution);
        void setAbility(const int ability, const int weight);
        void setItem(const int item, const int weight);
        
        QString name() const;
        int baseStat(const Stat stat) const;
        int effortValue(const Stat stat) const;
        Style growth() const;
        int experienceValue() const;
        int catchValue() const;
        int maxHoldWeight() const;
        Sigcore::Fraction runChance() const;
        Sigcore::Fraction fleeChance() const;
        Sigcore::Fraction itemChance() const;
        int encyclopediaNumber() const;
        int weight() const;
        int height() const;
        QString encyclopediaEntry() const;
        int frontMaleSprite() const;
        int backMaleSprite() const;
        int frontFemaleSprite() const;
        int backFemaleSprite() const;
        int skin() const;
        Sigcore::Fraction genderFactor() const;
        int eggSpecies() const;
        int eggSteps() const;
        bool type(const int type) const;
        QList<int> type() const;
        bool eggGroup(const int eggGroup) const;
        QList<int> eggGroup() const;
        Sigcore::Script evolution() const;
        int ability(const int ability) const;
        QMap<int, int> ability() const;
        int item(const int item) const;
        QMap<int, int> item() const;
        
        bool nameCheck(const QString& name) const;
        bool baseStatCheck(const Stat stat, const int base) const;
        bool effortValueCheck(const Stat stat, const int effortValue) const;
        bool growthCheck(const Style growth) const;
        bool experienceValueCheck(const int experienceValue) const;
        bool catchValueCheck(const int catchValue) const;
        bool maxHoldWeightCheck(const int maxHoldWeight) const;
        bool runChanceCheck(const Sigcore::Fraction& runChance) const;
        bool fleeChanceCheck(const Sigcore::Fraction& fleeChance) const;
        bool itemChanceCheck(const Sigcore::Fraction& itemChance) const;
        bool encyclopediaNumberCheck(const int encyclopediaNumber) const;
        bool weightCheck(const int weight) const;
        bool heightCheck(const int height) const;
        bool encyclopediaEntryCheck(const QString& encyclopediaEntry) const;
        bool frontMaleSpriteCheck(const int frontMaleSprite) const;
        bool backMaleSpriteCheck(const int backMaleSprite) const;
        bool frontFemaleSpriteCheck(const int frontFemaleSprite) const;
        bool backFemaleSpriteCheck(const int backFemaleSprite) const;
        bool genderFactorCheck(const Sigcore::Fraction& genderFactor) const;
        bool skinCheck(const int skin) const;
        bool eggSpeciesCheck(const int eggSpecies) const;
        bool eggStepsCheck(const int eggSteps) const;
        bool typeCheck(const int type) const;
        bool eggGroupCheck(const int eggGroup) const;
        bool evolutionCheck(const Sigcore::Script& evolution) const;
        bool abilityCheck(const int item, const int count) const;
        bool itemCheck(const int item, const int count) const;
        
        const SpeciesMove* move(const int index) const;
        SpeciesMove* move(const int index);
        const SpeciesMove* moveById(const int id) const;
        SpeciesMove* moveById(const int id);
        int moveIndex(const int id) const;
        int moveCount() const;
        SpeciesMove* newMove();
        SpeciesMove* newMove(const QDomElement& xml);
        SpeciesMove* newMove(const SpeciesMove& move);
        void deleteMove(const int index);
        void deleteMoveById(const int id);
        
        Species& operator=(const Species& rhs);
    private:
        int newMoveId() const;
        SpeciesMove* newMove(SpeciesMove* move);
        
        void clear();
        
        QString m_name;
        QVarLengthArray<int, ST_SpecialDefense - ST_HP + 1> m_baseStat;
        QVarLengthArray<int, ST_SpecialDefense - ST_HP + 1> m_effortValue;
        Style m_growth;
        int m_experienceValue;
        int m_catchValue;
        int m_maxHoldWeight;
        Sigcore::Fraction m_runChance;
        Sigcore::Fraction m_fleeChance;
        Sigcore::Fraction m_itemChance;
        int m_encyclopediaNumber;
        int m_weight;
        int m_height;
        int m_frontMaleSprite;
        int m_backMaleSprite;
        int m_frontFemaleSprite;
        int m_backFemaleSprite;
        int m_skin;
        QString m_encyclopediaEntry;
        Sigcore::Fraction m_genderFactor;
        int m_eggSpecies;
        int m_eggSteps;
        QList<int> m_type;
        QList<int> m_eggGroup;
        Sigcore::Script m_evolution;
        QMap<int, int> m_ability;
        QMap<int, int> m_item;
        QList<SpeciesMove*> m_moves;
};
}
Q_DECLARE_METATYPE(Sigmod::Species::Style)

#endif
