/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Weather.h
 */

#ifndef SIGMOD_WEATHER
#define SIGMOD_WEATHER

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Weather Weather.h sigmod/Weather.h
 * \brief Weathers can be used to add additional arena-wide effects in battle.
 * 
 * Weathers can change the status of the arena with effects such as rain, snow, or sand.
 */
class SIGMOD_EXPORT Weather : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param weather The weather to copy.
         */
        Weather(const Weather& weather);
        /**
         * Create a new weather belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the weather.
         * \param id The id number for the weather.
         */
        Weather(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p weather as a child of \p parent with id \p id.
         * 
         * \param weather The weather to copy the data from.
         * \param parent The parent of the weather.
         * \param id The id number for the weather.
         */
        Weather(const Weather& weather, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the weather.
         * \param id The id number for the weather.
         */
        Weather(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the weather's values are valid.
         * \note This does not check the script for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the weather in XML format.
         * 
         * \return The XML structure representing the weather.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the weather.
         * 
         * \param name The name of the weather.
         */
        void setName(const QString& name);
        /**
         * Sets the script for the weather. What the weather does is defined by this script.
         * The following objects are available to the script:
         * 
         *  - \b weather -- The \link Sigscript::WeatherWrapper weather \endlink being used.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which started the weather.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the weather is being used in.
         * 
         * \param script The script for the weather.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the weather.
         */
        QString name() const;
        /**
         * \sa setScript
         * 
         * \return The script for the weather.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        Weather& operator=(const Weather& rhs);
    private:
        QString m_name;
        Sigcore::Script m_script;
};
}

#endif
