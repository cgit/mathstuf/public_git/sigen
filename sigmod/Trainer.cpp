/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Trainer.cpp
 */

// Header include
#include "Trainer.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

const QStringList Trainer::IntelligenceStr = QStringList() << "Ignorant" << "Remember" << "Cheating";

Trainer::Trainer(const Trainer& trainer) :
        Object(trainer.parent(), trainer.id())
{
    *this = trainer;
}

Trainer::Trainer(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_moneyFactor(0),
        m_skin(-1),
        m_depth(0),
        m_teamIntel(Remember),
        m_moveIntel(Remember),
        m_itemIntel(Remember),
        m_abilityIntel(Remember),
        m_statIntel(Remember)
{
}

Trainer::Trainer(const Trainer& trainer, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = trainer;
}

Trainer::Trainer(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Trainer::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(moneyFactor);
    TEST(skin);
    TEST(depth);
    TEST_END();
}

void Trainer::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(moneyFactor);
    LOAD(skin);
    LOAD(depth);
    LOAD_ENUM(teamIntel, Intelligence);
    LOAD_ENUM(moveIntel, Intelligence);
    LOAD_ENUM(itemIntel, Intelligence);
    LOAD_ENUM(abilityIntel, Intelligence);
    LOAD_ENUM(statIntel, Intelligence);
}

QDomElement Trainer::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(moneyFactor);
    SAVE(skin);
    SAVE(depth);
    SAVE_ENUM(teamIntel, Intelligence);
    SAVE_ENUM(moveIntel, Intelligence);
    SAVE_ENUM(itemIntel, Intelligence);
    SAVE_ENUM(abilityIntel, Intelligence);
    SAVE_ENUM(statIntel, Intelligence);
    return xml;
}

SETTER(Trainer, QString&, Name, name)
SETTER(Trainer, int, MoneyFactor, moneyFactor)
SETTER(Trainer, int, Skin, skin)
SETTER(Trainer, int, Depth, depth)
SETTER(Trainer, Intelligence, TeamIntel, teamIntel)
SETTER(Trainer, Intelligence, MoveIntel, moveIntel)
SETTER(Trainer, Intelligence, ItemIntel, itemIntel)
SETTER(Trainer, Intelligence, AbilityIntel, abilityIntel)
SETTER(Trainer, Intelligence, StatIntel, statIntel)

GETTER(Trainer, QString, name)
GETTER(Trainer, int, moneyFactor)
GETTER(Trainer, int, skin)
GETTER(Trainer, int, depth)
GETTER(Trainer, Trainer::Intelligence, teamIntel)
GETTER(Trainer, Trainer::Intelligence, moveIntel)
GETTER(Trainer, Trainer::Intelligence, itemIntel)
GETTER(Trainer, Trainer::Intelligence, abilityIntel)
GETTER(Trainer, Trainer::Intelligence, statIntel)

CHECK(Trainer, QString&, name)
CHECK_BOUNDS(Trainer, int, moneyFactor, 0, INT_MAX)
CHECK_INDEX(Trainer, int, skin, game(), skin)
CHECK_BOUNDS(Trainer, int, depth, 0, INT_MAX)
CHECK(Trainer, Intelligence, teamIntel)
CHECK(Trainer, Intelligence, moveIntel)
CHECK(Trainer, Intelligence, itemIntel)
CHECK(Trainer, Intelligence, abilityIntel)
CHECK(Trainer, Intelligence, statIntel)

Trainer& Trainer::operator=(const Trainer& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(moneyFactor);
    COPY(skin);
    COPY(depth);
    COPY(teamIntel);
    COPY(moveIntel);
    COPY(itemIntel);
    COPY(abilityIntel);
    COPY(statIntel);
    return *this;
}
