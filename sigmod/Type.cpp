/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Type.cpp
 */

// Header include
#include "Type.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Type::Type(const Type& type) :
        Object(type.parent(), type.id())
{
    *this = type;
}

Type::Type(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_stab(1, 1)
{
}

Type::Type(const Type& type, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = type;
}

Type::Type(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Type::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(stab);
    TEST_END();
}

void Type::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(stab);
}

QDomElement Type::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(stab);
    return xml;
}

SETTER(Type, QString&, Name, name)
SETTER(Type, Fraction&, Stab, stab)

GETTER(Type, QString, name)
GETTER(Type, Fraction, stab)

CHECK(Type, QString&, name)
CHECK_BOUNDS(Type, Fraction&, stab, 1, INT_MAX)

Type& Type::operator=(const Type& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(stab);
    return *this;
}
