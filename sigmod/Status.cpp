/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Status.cpp
 */

// Header include
#include "Status.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Status::Status(const Status& status) :
        Object(status.parent(), status.id())
{
    *this = status;
}

Status::Status(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_battleScript("", ""),
        m_worldScript("", "")
{
}

Status::Status(const Status& status, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = status;
}

Status::Status(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Status::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void Status::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(battleScript);
    LOAD(worldScript);
}

QDomElement Status::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(battleScript);
    SAVE(worldScript);
    return xml;
}

SETTER(Status, QString&, Name, name)
SETTER(Status, Script&, BattleScript, battleScript)
SETTER(Status, Script&, WorldScript, worldScript)

GETTER(Status, QString, name)
GETTER(Status, Script, battleScript)
GETTER(Status, Script, worldScript)

CHECK(Status, QString&, name)
CHECK(Status, Script&, battleScript)
CHECK(Status, Script&, worldScript)

Status& Status::operator=(const Status& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(battleScript);
    COPY(worldScript);
    return *this;
}
