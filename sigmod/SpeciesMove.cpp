/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/SpeciesMove.cpp
 */

// Header include
#include "SpeciesMove.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Rules.h"
#include "Species.h"

using namespace Sigmod;

SpeciesMove::SpeciesMove(const SpeciesMove& move) :
        Object(move.parent(), move.id())
{
    *this = move;
}

SpeciesMove::SpeciesMove(const Species* parent, const int id) :
        Object(parent, id),
        m_move(INT_MAX),
        m_level(0),
        m_wild(0)
{
}

SpeciesMove::SpeciesMove(const SpeciesMove& move, const Species* parent, const int id) :
        Object(parent, id)
{
    *this = move;
}

SpeciesMove::SpeciesMove(const QDomElement& xml, const Species* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void SpeciesMove::validate()
{
    TEST_BEGIN();
    TEST(move);
    TEST(level);
    TEST(wild);
    TEST_END();
}

void SpeciesMove::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(move);
    LOAD(level);
    LOAD(wild);
}

QDomElement SpeciesMove::save() const
{
    SAVE_CREATE();
    SAVE(move);
    SAVE(level);
    SAVE(wild);
    return xml;
}

SETTER(SpeciesMove, int, Move, move)
SETTER(SpeciesMove, int, Level, level)
SETTER(SpeciesMove, int, Wild, wild)

GETTER(SpeciesMove, int, move)
GETTER(SpeciesMove, int, level)
GETTER(SpeciesMove, int, wild)

CHECK_INDEX(SpeciesMove, int, move, game(), move)
CHECK_BOUNDS(SpeciesMove, int, level, -1, game()->rules()->maxLevel())
CHECK_BOUNDS(SpeciesMove, int, wild, -1, game()->rules()->maxLevel())

SpeciesMove& SpeciesMove::operator=(const SpeciesMove& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(move);
    COPY(level);
    COPY(wild);
    return *this;
}
