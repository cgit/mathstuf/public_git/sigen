/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_RULES
#define SIGMOD_RULES

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

class SIGMOD_EXPORT Rules : public Object
{
    Q_OBJECT
    
    public:
        static const QStringList DVStr;
        
        Rules(const Rules& rules);
        Rules(const Game* parent);
        Rules(const Rules& rules, const Game* parent);
        Rules(const QDomElement& xml, const Game* parent);
        
        void validate();
        
        void load(const QDomElement& xml);
        QDomElement save() const;
        
        void setGenderAllowed(const bool genderAllowed);
        void setBreedingAllowed(const bool breedingAllowed);
        void setCriticalDomains(const bool criticalDomains);
        void setNumBoxes(const int numBoxes);
        void setBoxSize(const int boxSize);
        void setMaxParty(const int maxParty);
        void setMaxFight(const int maxFight);
        void setMaxPlayers(const int maxPlayers);
        void setMaxHeldItems(const int maxHeldItems);
        void setMaxAbilities(const int maxAbilities);
        void setMaxNatures(const int maxNatures);
        void setMaxMoves(const int maxMoves);
        void setMaxLevel(const int maxLevel);
        void setMaxStages(const int maxStage);
        void setMaxMoney(const int maxMoney);
        void setMaxTotalWeight(const int maxTotalWeight);
        void setSpecialSplit(const bool specialSplit);
        void setSpecialDVSplit(const bool specialDVSplit);
        void setMaxTotalEV(const int maxTotalEV);
        void setMaxEVPerStat(const int maxEVPerStat);
        
        bool genderAllowed() const;
        bool breedingAllowed() const;
        bool criticalDomains() const;
        bool useATB() const;
        int numBoxes() const;
        int boxSize() const;
        int maxParty() const;
        int maxFight() const;
        int maxPlayers() const;
        int maxHeldItems() const;
        int maxAbilities() const;
        int maxNatures() const;
        int maxMoves() const;
        int maxLevel() const;
        int maxStages() const;
        int maxMoney() const;
        int maxTotalWeight() const;
        bool specialSplit() const;
        bool specialDVSplit() const;
        int maxTotalEV() const;
        int maxEVPerStat() const;
        
        bool genderAllowedCheck(const bool genderAllowed) const;
        bool breedingAllowedCheck(const bool breedingAllowed) const;
        bool criticalDomainsCheck(const bool criticalDomains) const;
        bool numBoxesCheck(const int numBoxes) const;
        bool boxSizeCheck(const int boxSize) const;
        bool maxPartyCheck(const int maxParty) const;
        bool maxFightCheck(const int maxFight) const;
        bool maxPlayersCheck(const int maxPlayers) const;
        bool maxHeldItemsCheck(const int maxHeldItems) const;
        bool maxAbilitiesCheck(const int maxAbilities) const;
        bool maxNaturesCheck(const int maxNatures) const;
        bool maxMovesCheck(const int maxMoves) const;
        bool maxLevelCheck(const int maxLevel) const;
        bool maxStagesCheck(const int maxStage) const;
        bool maxMoneyCheck(const int maxMoney) const;
        bool maxTotalWeightCheck(const int maxTotalWeight) const;
        bool specialSplitCheck(const bool specialSplit) const;
        bool specialDVSplitCheck(const bool specialDVSplit) const;
        bool maxTotalEVCheck(const int maxTotalEV) const;
        bool maxEVPerStatCheck(const int maxEVPerStat) const;
        
        Rules& operator=(const Rules& rhs);
    private:
        bool m_genderAllowed;
        bool m_breedingAllowed;
        bool m_criticalDomains;
        int m_numBoxes;
        int m_boxSize;
        int m_maxParty;
        int m_maxFight;
        int m_maxPlayers;
        int m_maxHeldItems;
        int m_maxAbilities;
        int m_maxNatures;
        int m_maxMoves;
        int m_maxLevel;
        int m_maxStages;
        int m_maxMoney;
        int m_maxTotalWeight;
        bool m_specialSplit;
        bool m_specialDVSplit;
        int m_maxTotalEV;
        int m_maxEVPerStat;
};
}

#endif
