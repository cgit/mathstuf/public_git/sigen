/*
 * Copyright 2009 Abhishek Mukherjee <linkinpark342@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Stat.h
 */

#ifndef SIGMOD_STAT
#define SIGMOD_STAT

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QStringList>

namespace Sigmod
{
/**
 * \enum Stat
 * \brief Enumeration of stats.
 */
enum Stat
{
    ST_HP = 0,
    ST_Attack = 1,
    ST_Defense = 2,
    ST_Speed = 3,
    ST_Special = 4,
    ST_SpecialAttack = 4,
    ST_SpecialDefense = 5,
    ST_Accuracy = 6,
    ST_Evasion = 7
};
/// Strings for the stats for a combined Special stat.
const QStringList StatRBYStr = QStringList() << "HP" << "Attack" << "Defense" << "Speed" << "Special" << "" << "Accuracy" << "Evasion";
/// Strings for the stats when the Special stat is split.
const QStringList StatGSCStr = QStringList() << "HP" << "Attack" << "Defense" << "Speed" << "Special Attack" << "Special Defense" << "Accuracy" << "Evasion";

}
Q_DECLARE_METATYPE(Sigmod::Stat)

#endif
