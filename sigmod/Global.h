/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * Copyright 2009 Abhishek Mukherjee <linkinpark342@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Global.h
 */

#ifndef SIGMOD_GLOBAL
#define SIGMOD_GLOBAL

// KDE includes
#include <kdemacros.h>

#ifndef SIGMOD_EXPORT
#   ifdef MAKE_SIGMOD_LIB
#       define SIGMOD_EXPORT KDE_EXPORT /// Export the symbol if building the library.
#   else
#       define SIGMOD_EXPORT KDE_IMPORT /// Import the symbol if including the library.
#   endif
#   define SIGMOD_NO_EXPORT KDE_NO_EXPORT
#endif

#ifndef SIGMOD_EXPORT_DEPRECATED
#   define SIGMOD_EXPORT_DEPRECATED KDE_DEPRECATED SIGMOD_EXPORT /// Mark as deprecated
#endif

#endif
