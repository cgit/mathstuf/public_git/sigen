/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapEffect.h
 */

#ifndef SIGMOD_MAPEFFECT
#define SIGMOD_MAPEFFECT

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtGui/QPainterPath>

namespace Sigmod
{
// Forward declarations
class Map;

/**
 * \class Sigmod::MapEffect MapEffect.h sigmod/MapEffect.h
 * \brief Class describing an effect on a map.
 * 
 * Effects include NPCs that roam the world map, items that can be found,
 * as well as any environmental effects that can occur (moving the player around,
 * traps, or switches).
 */
class SIGMOD_EXPORT MapEffect : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param effect The effect to copy.
         */
        MapEffect(const MapEffect& effect);
        /**
         * Create a new effect belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the effect.
         * \param id The id number for the effect.
         */
        MapEffect(const Map* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p effect as a child of \p parent with id \p id.
         * 
         * \param effect The effect to copy the data from.
         * \param parent The parent of the effect.
         * \param id The id number for the effect.
         */
        MapEffect(const MapEffect& effect, const Map* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the effect.
         * \param id The id number for the effect.
         */
        MapEffect(const QDomElement& xml, const Map* parent, const int id = -1);
        
        /**
         * Check to make sure the effect's values are valid.
         * \note This does not check the scripts for effect.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the effect in XML format.
         * 
         * \return The XML structure representing the effect.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the effect. This is only used internally.
         * 
         * \param name The name of the effect.
         */
        void setName(const QString& name);
        /**
         * Sets the position of the effect on the map.
         * 
         * \param position The position of the effect on the map.
         */
        void setPosition(const QPoint& position);
        /**
         * Sets the area of the map the effect takes up. It is only used if the skin is not valid.
         * 
         * \param area The area of the map the effect takes up.
         */
        void setArea(const QPainterPath& area);
        /**
         * Sets the id of the skin used to represent the effect on the map.
         * 
         * \param skin The id of the skin used for the effect.
         */
        void setSkin(const int skin);
        /**
         * Sets whether the effect is collidable in the map or not. If not, it can be moved though
         * regardless of its image.
         * 
         * \param isGhost Whether the effect can be moved through or not.
         */
        void setIsGhost(const bool isGhost);
        /**
         * Sets the script that controls the effect on the map. The following objects are available to the script:
         * 
         *  - \b item -- The \link Sigworld::MapItem map item \endlink that the script controls.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the player is on.
         * 
         * \param script The script that controls the effect.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the effect.
         */
        QString name() const;
        /**
         * \sa setPosition
         * 
         * \return The position of the effect on the map.
         */
        QPoint position() const;
        /**
         * \sa setArea
         * 
         * \return The area of the effect.
         */
        QPainterPath area() const;
        /**
         * \sa setSkin
         * 
         * \return The id of the skin that represents the effect.
         */
        int skin() const;
        /**
         * \sa setIsGhost
         * 
         * \return Whether the effect can be moved through or not.
         */
        bool isGhost() const;
        /**
         * \sa setScript
         * 
         * \return The script that controls the effect.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool positionCheck(const QPoint& point) const;
        bool areaCheck(const QPainterPath& area) const;
        bool skinCheck(const int skin) const;
        bool isGhostCheck(const bool isGhost) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        MapEffect& operator=(const MapEffect& rhs);
    private:
        QString m_name;
        QPoint m_position;
        QPainterPath m_area;
        int m_skin;
        bool m_isGhost;
        Sigcore::Script m_script;
};
}

#endif
