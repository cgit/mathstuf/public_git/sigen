/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Sound.h
 */

#ifndef SIGMOD_SOUND
#define SIGMOD_SOUND

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QByteArray>
#include <QtCore/QMetaType>
#include <QtCore/QString>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Sound Sound.h sigmod/Sound.h
 * \brief Class describing a sound.
 * 
 * Any sound in the sigmod must be declared as an instance of this class.
 * Sounds can be effects (can make as many as desired, but stop once done
 * playing) or music (can only have one instance and loops by default).
 */
class SIGMOD_EXPORT Sound : public Object
{
    Q_OBJECT
    Q_ENUMS(Type)
    
    public:
        /**
         * \enum Type
         * \brief The type of the sound stored.
         */
        enum Type
        {
            SoundEffect = 0,
            Music = 1
        };
        /**
         * \var TypeStr
         * String values for the types of sound.
         */
        static const QStringList TypeStr;
        
        /**
         * Copy constructor.
         * 
         * \param sound The sound to copy.
         */
        Sound(const Sound& sound);
        /**
         * Create a new sound belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the sound.
         * \param id The id number for the sound.
         */
        Sound(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p sound as a child of \p parent with id \p id.
         * 
         * \param sound The sound to copy the data from.
         * \param parent The parent of the sound.
         * \param id The id number for the sound.
         */
        Sound(const Sound& sound, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the sound.
         * \param id The id number for the sound.
         */
        Sound(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the sound's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the sound in XML format.
         * 
         * \return The XML structure representing the sound.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the sound. This is only used internally.
         * 
         * \param name The name of the sound.
         */
        void setName(const QString& name);
        /**
         * Sets the type of the sound.
         * 
         * \param type The type of the sound.
         */
        void setType(const Type type);
        /**
         * Sets the data for the sound. Any formats supported by \link http://phonon.kde.org/ Phonon \endlink
         * are supported here.
         * 
         * \param data The data for the sound.
         */
        void setData(const QByteArray& data);
        
        /**
         * \sa setName
         * 
         * \return The name of the sound.
         */
        QString name() const;
        /**
         * \sa setType
         * 
         * \return The type of sound.
         */
        Type type() const;
        /**
         * \sa setData
         * 
         * \return The data for the sound.
         */
        QByteArray data() const;
        
        bool nameCheck(const QString& name) const;
        bool typeCheck(const Type type) const;
        bool dataCheck(const QByteArray& data) const;
        
        Sound& operator=(const Sound& rhs);
    private:
        QString m_name;
        Type m_type;
        QByteArray m_data;
};
}
Q_DECLARE_METATYPE(Sigmod::Sound::Type)

#endif
