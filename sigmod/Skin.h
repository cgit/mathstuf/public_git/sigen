/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Skin.h
 */

#ifndef SIGMOD_SKIN
#define SIGMOD_SKIN

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QSize>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Skin Skin.h sigmod/Skin.h
 * \brief Class that controls the images of things that move on the world map.
 * 
 * Skins use a script to controls what an item on the map looks like while it moves around.
 */
class SIGMOD_EXPORT Skin : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param skin The skin to copy.
         */
        Skin(const Skin& skin);
        /**
         * Create a new skin belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the skin.
         * \param id The id number for the skin.
         */
        Skin(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p skin as a child of \p parent with id \p id.
         * 
         * \param skin The skin to copy the data from.
         * \param parent The parent of the skin.
         * \param id The id number for the skin.
         */
        Skin(const Skin& skin, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the skin.
         * \param id The id number for the skin.
         */
        Skin(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the skin's values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the skin in XML format.
         * 
         * \return The XML structure representing the skin.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the skin. This is only used internally.
         * 
         * \param name The name of the skin
         */
        void setName(const QString& name);
        /**
         * Sets the size of the skin. It is not allowed to draw outside of the rectangle from (0, 0) to its size.
         * 
         * \param size The size of the skin.
         */
        void setSize(const QSize& size);
        /**
         * Sets the script that manages the images for the skin. The following objects will be available
         * to the script:
         * 
         *  - \b skin -- The \link Sigscript::SkinWrapper skin \endlink the script controls.
         *  - \b item -- The \link Sigworld::MapItem map item \endlink the skin controls the images for.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the map item is on.
         * 
         * The \b item object will have signals for when it moves, turns, and stops. The script should animate \b item for each
         * action. This can usually be done by using a \link QTimeLine \endlink in order to keep track of frames. By connecting
         * the timeline's <i>frameChanged(int)</i> signal a function should convert it into the corresponding sprite and set it
         * for \b item.
         * 
         * \param script The script that controls the skin.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the skin.
         */
        QString name() const;
        /**
         * \sa setSize
         * 
         * \return The size of the skin.
         */
        QSize size() const;
        /**
         * \sa setSkin
         * 
         * \return The script for the skin.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool sizeCheck(const QSize& size) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        Skin& operator=(const Skin& rhs);
    private:
        QString m_name;
        QSize m_size;
        Sigcore::Script m_script;
};
}

#endif
