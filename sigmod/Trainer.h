/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Trainer.h
 */

#ifndef SIGMOD_TRAINER
#define SIGMOD_TRAINER

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QStringList>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Trainer Trainer.h sigmod/Trainer.h
 * \brief Class describing a trainer class.
 * 
 * A trainer class allows all trainers of a type to be defined once. They can have
 * a depth which is the farthest ahead they can perceive the battle before making
 * decisions about actions. The knowledge of different aspects of battle allow the
 * AI controlling the trainer to gather information about the battle to make better
 * decisions.
 */
class SIGMOD_EXPORT Trainer : public Object
{
    Q_OBJECT
    Q_ENUMS(Intelligence)
    
    public:
        /**
         * \enum Intelligence
         * \brief Levels of knowledge about the battle for AI.
         */
        enum Intelligence
        {
            Ignorant = 0,
            Determine = 1,
            Remember = 2,
            Cheating = 3
        };
        /**
         * \var IntelligenceStr
         * \brief String representations of Intelligence enumerations.
         */
        static const QStringList IntelligenceStr;
        
        /**
         * Copy constructor.
         * 
         * \param trainer The trainer to copy.
         */
        Trainer(const Trainer& trainer);
        /**
         * Create a new trainer belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the trainer.
         * \param id The id number for the trainer.
         */
        Trainer(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p trainer as a child of \p parent with id \p id.
         * 
         * \param trainer The trainer to copy the data from.
         * \param parent The parent of the trainer.
         * \param id The id number for the trainer.
         */
        Trainer(const Trainer& trainer, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the trainer.
         * \param id The id number for the trainer.
         */
        Trainer(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the trainer's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the trainer in XML format.
         * 
         * \return The XML structure representing the trainer.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the trainer. This is only used internally.
         * 
         * \param name The name of the trainer class.
         */
        void setName(const QString& name);
        /**
         * Sets the multiplier for money when defeated.
         * 
         * \param moneyFactor The multiplier for the money reward for defeating the trainer.
         */
        void setMoneyFactor(const int moneyFactor);
        /**
         * Sets the skin that is used to represent the trainer on the world map.
         * 
         * \param skin The id of the skin used to represent the trainer.
         */
        void setSkin(const int skin);
        /**
         * Sets the maximum depth that the AI can predict. If the battle arena uses turns,
         * the AI counts as one depth as one round of turns; in an ATB arena, one depth is
         * predicted moves until the same creature attacks again.
         * 
         * \param depth The maximum depth the AI can predict.
         */
        void setDepth(const int depth);
        /**
         * Sets the level of knowledge the trainer has about enemy teams.
         * 
         * \param teamIntel The level of knowledge the trainer has about enemy teams.
         */
        void setTeamIntel(const Intelligence teamIntel);
        /**
         * Sets the level of knowledge the trainer has about enemy moves.
         * 
         * \param moveIntel The level of knowledge the trainer has about enemy moves.
         */
        void setMoveIntel(const Intelligence moveIntel);
        /**
         * Sets the level of knowledge the trainer has about enemy items.
         * 
         * \param itemIntel The level of knowledge the trainer has about enemy items.
         */
        void setItemIntel(const Intelligence itemIntel);
        /**
         * Sets the level of knowledge the trainer has about enemy ablities.
         * 
         * \param abilityIntel The level of knowledge the trainer has about enemy abilities.
         */
        void setAbilityIntel(const Intelligence abilityIntel);
        /**
         * Sets the level of knowledge the trainer has about enemy stats.
         * 
         * \param statIntel The level of knowledge the trainer has about enemy stats.
         */
        void setStatIntel(const Intelligence statIntel);
        
        /**
         * \sa setName
         * 
         * \return The name of the trainer class.
         */
        QString name() const;
        /**
         * \sa setMoneyFactor
         * 
         * \return The money multiplier for the trainer class.
         */
        int moneyFactor() const;
        /**
         * \sa setSkin
         * 
         * \return The id of the skin of the trainer class.
         */
        int skin() const;
        /**
         * \sa setDepth
         * 
         * \return The maximum depth the AI for the trainer may think ahead.
         */
        int depth() const;
        /**
         * \sa setTeamIntel
         * 
         * \return The level of knowledge the trainer has about enemy teams.
         */
        Intelligence teamIntel() const;
        /**
         * \sa setMoveIntel
         * 
         * \return The level of knowledge the trainer has about enemy moves.
         */
        Intelligence moveIntel() const;
        /**
         * \sa setItemIntel
         * 
         * \return The level of knowledge the trainer has about enemy items.
         */
        Intelligence itemIntel() const;
        /**
         * \sa setAbilityIntel
         * 
         * \return The level of knowledge the trainer has about enemy abilities.
         */
        Intelligence abilityIntel() const;
        /**
         * \sa setStatIntel
         * 
         * \return The level of knowledge the trainer has about enemy stats.
         */
        Intelligence statIntel() const;
        
        bool nameCheck(const QString& name) const;
        bool moneyFactorCheck(const int moneyFactor) const;
        bool skinCheck(const int skin) const;
        bool depthCheck(const int depth) const;
        bool teamIntelCheck(const Intelligence teamIntel) const;
        bool moveIntelCheck(const Intelligence moveIntel) const;
        bool itemIntelCheck(const Intelligence itemIntel) const;
        bool abilityIntelCheck(const Intelligence abilityIntel) const;
        bool statIntelCheck(const Intelligence statIntel) const;
        
        Trainer& operator=(const Trainer& rhs);
    private:
        QString m_name;
        int m_moneyFactor;
        int m_skin;
        int m_depth;
        Intelligence m_teamIntel;
        Intelligence m_moveIntel;
        Intelligence m_itemIntel;
        Intelligence m_abilityIntel;
        Intelligence m_statIntel;
};
}
Q_DECLARE_METATYPE(Sigmod::Trainer::Intelligence)

#endif
