/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Badge.cpp
 */

// Header include
#include "Badge.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Rules.h"
#include "Sprite.h"

using namespace Sigcore;
using namespace Sigmod;

Badge::Badge(const Badge& badge) :
        Object(badge.parent(), badge.id()),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    *this = badge;
}

Badge::Badge(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_face(-1),
        m_badge(-1),
        m_obey(0),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    for (int i = 0; i < m_stat.size(); ++i)
        m_stat[i].set(1, 1);
}

Badge::Badge(const Badge& badge, const Game* parent, const int id) :
        Object(parent, id),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    *this = badge;
}

Badge::Badge(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    LOAD_ID();
    load(xml);
}

void Badge::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(face);
    TEST(badge);
    if (m_face == m_badge)
        emit(error("Face and badge sprites are the same"));
    TEST(obey);
    TEST_ARRAY_INDEX_OFF(stat, ST_Attack, ST_Attack);
    TEST_ARRAY_INDEX_OFF(stat, ST_Defense, ST_Attack);
    TEST_ARRAY_INDEX_OFF(stat, ST_Speed, ST_Attack);
    if (game()->rules()->specialSplit())
    {
        TEST_ARRAY_INDEX_OFF(stat, ST_SpecialAttack, ST_Attack);
        TEST_ARRAY_INDEX_OFF(stat, ST_SpecialDefense, ST_Attack);
    }
    else
        TEST_ARRAY_INDEX_OFF(stat, ST_Special, ST_Attack);
    TEST_END();
}

void Badge::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(face);
    LOAD(badge);
    LOAD(obey);
    LOAD_ARRAY(stat);
}

QDomElement Badge::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(face);
    SAVE(badge);
    SAVE(obey);
    SAVE_ARRAY(stat);
    return xml;
}

SETTER(Badge, QString&, Name, name)
SETTER(Badge, int, Face, face)
SETTER(Badge, int, Badge, badge)
SETTER(Badge, int, Obey, obey)
SETTER_ARRAY(Badge, Fraction&, Stat, stat, multiplier, Stat, stat, ST_Attack)

GETTER(Badge, QString, name)
GETTER(Badge, int, face)
GETTER(Badge, int, badge)
GETTER(Badge, int, obey)
GETTER_ARRAY(Badge, Fraction, stat, multiplier, Stat, stat, ST_Attack)

CHECK(Badge, QString&, name)
CHECK_INDEX(Badge, int, face, game(), sprite)
CHECK_INDEX(Badge, int, badge, game(), sprite)
CHECK_BOUNDS(Badge, int, obey, INT_MIN, game()->rules()->maxLevel())
CHECK_BEGIN_ARRAY(Badge, Fraction&, stat, multiplier, Stat, stat)
    switch (stat)
    {
        case ST_SpecialDefense:
            if (game()->rules()->specialSplit())
        case ST_Attack:
        case ST_Defense:
        case ST_Speed:
        case ST_Special:
            break;
        default:
            EBOUNDS(stat, "Attack", game()->rules()->specialSplit() ? "SpecialDefense" : "Special");
            return false;
    }
    TBOUNDS(multiplier, 1, INT_MAX)
CHECK_END()

Badge& Badge::operator=(const Badge& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(face);
    COPY(badge);
    COPY(obey);
    COPY(stat);
    return *this;
}
