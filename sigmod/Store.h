/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Store.h
 */

#ifndef SIGMOD_STORE
#define SIGMOD_STORE

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Store Store.h sigmod/Store.h
 * \brief Class describing a store.
 * 
 * Stores are places where items can be purchased from.
 */
class SIGMOD_EXPORT Store : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param store The store to copy.
         */
        Store(const Store& store);
        /**
         * Create a new store belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the store.
         * \param id The id number for the store.
         */
        Store(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p store as a child of \p parent with id \p id.
         * 
         * \param store The store to copy the data from.
         * \param parent The parent of the store.
         * \param id The id number for the store.
         */
        Store(const Store& store, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the store.
         * \param id The id number for the store.
         */
        Store(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the store's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the store in XML format.
         * 
         * \return The XML structure representing the store.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the store. It is only used internally.
         * 
         * \param name The name of the store.
         */
        void setName(const QString& name);
        /**
         * Sets the state of an item in the store's item list.
         * 
         * \param item The id of the item to set.
         * \param state If \p true, the item can be bought, otherwise, disable it.
         */
        void setItem(const int item, const bool state);
        
        /**
         * \sa setName
         * 
         * \return The name of the store.
         */
        QString name() const;
        /**
         * \sa setItem
         * 
         * \param item The id of the item to check for.
         * \return Whether the item can be cought at the store or not.
         */
        bool item(const int item) const;
        /**
         * \sa setItem
         * 
         * \return The list of ids of items that can be cought at the store.
         */
        QList<int> item() const;
        
        bool nameCheck(const QString& name) const;
        bool itemCheck(const int item) const;
        
        Store& operator=(const Store& rhs);
    private:
        void clear();
        
        QString m_name;
        QList<int> m_item;
};
}

#endif
