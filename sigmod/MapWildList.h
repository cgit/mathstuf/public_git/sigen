/*
 * Copyright 2007-2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_MAPWILDLIST
#define SIGMOD_MAPWILDLIST

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>

namespace Sigmod
{
// Forward declarations
class Map;
class MapWildListEncounter;

class SIGMOD_EXPORT MapWildList : public Object
{
    Q_OBJECT
    
    public:
        MapWildList(const MapWildList& wildList);
        MapWildList(const Map* parent, const int id);
        MapWildList(const MapWildList& wildList, const Map* parent, const int id);
        MapWildList(const QDomElement& xml, const Map* parent, const int id = -1);
        ~MapWildList();
        
        void validate();
        
        void load(const QDomElement& xml);
        QDomElement save() const;
        
        void setName(const QString& name);
        
        QString name() const;
        
        bool nameCheck(const QString& name) const;
        
        const MapWildListEncounter* encounter(const int index) const;
        MapWildListEncounter* encounter(const int index);
        const MapWildListEncounter* encounterById(const int id) const;
        MapWildListEncounter* encounterById(const int id);
        int encounterIndex(const int id) const;
        int encounterCount() const;
        MapWildListEncounter* newEncounter();
        MapWildListEncounter* newEncounter(const QDomElement& xml);
        MapWildListEncounter* newEncounter(const MapWildListEncounter& encounter);
        void deleteEncounter(const int index);
        void deleteEncounterById(const int id);
        
        MapWildList& operator=(const MapWildList& rhs);
    private:
        int newEncounterId() const;
        MapWildListEncounter* newEncounter(MapWildListEncounter* encounter);
        
        void clear();
        
        QString m_name;
        QList<MapWildListEncounter*> m_encounters;
};
}

#endif
