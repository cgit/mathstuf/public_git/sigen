/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/CoinListItem.h
 */

#ifndef SIGMOD_COINLISTITEM
#define SIGMOD_COINLISTITEM

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QMetaType>

namespace Sigmod
{
// Forward declarations
class CoinList;

/**
 * \class Sigmod::CoinListItem CoinListItem.h sigmod/CoinListItem.h
 * \brief An object that is sold on a coin list.
 */
class SIGMOD_EXPORT CoinListItem : public Object
{
    Q_OBJECT
    Q_ENUMS(Type)
    
    public:
        /**
         * \enum Type
         * \brief The type of object contained in the item.
         */
        enum Type
        {
            Item = 0,
            Species = 1
        };
        /**
         * \var TypeStr
         * String values for the types of an item.
         */
        static const QStringList TypeStr;
        
        /**
         * Copy constructor.
         * 
         * \param item The item to copy.
         */
        CoinListItem(const CoinListItem& item);
        /**
         * Create a new item belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        CoinListItem(const CoinList* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p item as a child of \p parent with id \p id.
         * 
         * \param item The item to copy the data from.
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        CoinListItem(const CoinListItem& item, const CoinList* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        CoinListItem(const QDomElement& xml, const CoinList* parent, const int id = -1);
        
        /**
         * Check to make sure the item's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the item in XML format.
         * 
         * \return The XML structure representing the item.
         */
        QDomElement save() const;
        
        /**
         * Sets the type of object contained in the item.
         * 
         * \param type The type of the item.
         */
        void setType(const Type type);
        /**
         * Sets the \p id of the object in the item.
         * 
         * \param object The \p id of the object contained in the item.
         */
        void setObject(const int object);
        /**
         * Sets how many coins the item costs.
         * 
         * \param cost How many coins the item costs.
         */
        void setCost(const int cost);
        
        /**
         * \sa setType
         * 
         * \return The type of object contained in the item.
         */
        Type type() const;
        /**
         * \sa setObject
         * 
         * \return The \p id of the object in the item.
         */
        int object() const;
        /**
         * \sa setCost
         * 
         * \return How many coins the item costs.
         */
        int cost() const;
        
        bool typeCheck(const Type type) const;
        bool objectCheck(const int object) const;
        bool costCheck(const int cost) const;
        
        CoinListItem& operator=(const CoinListItem& rhs);
    private:
        Type m_type;
        int m_object;
        int m_cost;
};
}
Q_DECLARE_METATYPE(Sigmod::CoinListItem::Type)

#endif
