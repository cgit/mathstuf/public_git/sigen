/*
 * Copyright 2009 Abhishek Mukherjee <linkinpark342@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Direction.h
 */

#ifndef SIGMOD_DIRECTION
#define SIGMOD_DIRECTION

// Qt includes
#include <QtCore/QMetaType>
#include <QtCore/QStringList>

namespace Sigmod
{
/**
 * \enum Direction
 * \brief Enumeration of directions.
 */
enum Direction
{
    D_Up = 0,
    D_Down = 1,
    D_Left = 2,
    D_Right = 3,
    D_None = 4
};
/// Strings for the directions.
const QStringList DirectionStr = QStringList() << "Up" << "Down" << "Left" << "Right" << "None";

}
Q_DECLARE_METATYPE(Sigmod::Direction)

#endif
