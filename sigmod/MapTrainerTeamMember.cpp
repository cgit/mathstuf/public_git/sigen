/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapTrainerTeamMember.cpp
 */

// Header include
#include "MapTrainerTeamMember.h"

// Sigmod includes
#include "Game.h"
#include "Item.h"
#include "Macros.h"
#include "MapTrainer.h"
#include "Rules.h"
#include "Species.h"
#include "SpeciesMove.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigmod;

MapTrainerTeamMember::MapTrainerTeamMember(const MapTrainerTeamMember& teamMember) :
        Object(teamMember.parent(), teamMember.id())
{
    *this = teamMember;
}

MapTrainerTeamMember::MapTrainerTeamMember(const MapTrainer* parent, const int id) :
        Object(parent, id),
        m_species(INT_MAX),
        m_level(INT_MAX)
{
}

MapTrainerTeamMember::MapTrainerTeamMember(const MapTrainerTeamMember& teamMember, const MapTrainer* parent, const int id) :
        Object(parent, id)
{
    *this = teamMember;
}

MapTrainerTeamMember::MapTrainerTeamMember(const QDomElement& xml, const MapTrainer* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void MapTrainerTeamMember::validate()
{
    TEST_BEGIN();
    TEST(species);
    TEST(level);
    if (game()->rules()->maxAbilities() < m_ability.size())
        emit(error("Too many abilities"));
    TEST_LIST(ability);
    if (game()->rules()->maxHeldItems() < m_item.size())
        emit(error("Too many held items"));
    TEST_MAP(item);
    const Species* species = game()->speciesById(m_species);
    if (species && ((species->maxHoldWeight() < heldWeight())))
        emit(error("Cannot carry that much weight"));
    if (game()->rules()->maxMoves() < m_move.size())
        emit(error("Too many moves"));
    TEST_LIST(move);
    if (game()->rules()->maxNatures() < m_nature.size())
        emit(error("Too many natures"));
    TEST_LIST(nature);
    TEST_END();
}

void MapTrainerTeamMember::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(species);
    LOAD(level);
    LOAD_LIST(ability);
    LOAD_MAP(item, count);
    LOAD_LIST(move);
    LOAD_LIST(nature);
}

QDomElement MapTrainerTeamMember::save() const
{
    SAVE_CREATE();
    SAVE(species);
    SAVE(level);
    SAVE_LIST(ability);
    SAVE_MAP(item, count);
    SAVE_LIST(move);
    SAVE_LIST(nature);
    return xml;
}

SETTER(MapTrainerTeamMember, int, Species, species)
SETTER(MapTrainerTeamMember, int, Level, level)
SETTER_LIST_LIMIT(MapTrainerTeamMember, Ability, ability, game()->rules()->maxAbilities(), "Cannot have anymore abilities")

void MapTrainerTeamMember::setItem(const int item, const int count)
{
    if (count && itemCheck(item, count) && (!m_item.contains(item) || (count != m_item[item])))
    {
        const QList<int> items = m_item.keys();
        int total = count;
        foreach (int itemCount, items)
        {
            if (itemCount == item)
                continue;
            total += m_item[itemCount];
        }
        if (total <= game()->rules()->maxHeldItems())
        {
            if (checkWeight(item, count))
            {
                m_item[item] = count;
                emit(changed());
            }
            else
                ERROR("Cannot carry that much weight");
        }
        else
            ERROR("Cannot hold that many items");
    }
    else if (!count && m_item.contains(item))
    {
        m_item.remove(item);
        emit(changed());
    }
}

SETTER_LIST_LIMIT(MapTrainerTeamMember, Move, move, game()->rules()->maxMoves(), "Cannot know anymore moves")
SETTER_LIST_LIMIT(MapTrainerTeamMember, Nature, nature, game()->rules()->maxNatures(), "Cannot have anymore natures")

GETTER(MapTrainerTeamMember, int, species)
GETTER(MapTrainerTeamMember, int, level)
GETTER_LIST(MapTrainerTeamMember, ability)
GETTER_MAP(MapTrainerTeamMember, item)
GETTER_LIST(MapTrainerTeamMember, move)
GETTER_LIST(MapTrainerTeamMember, nature)

CHECK_INDEX(MapTrainerTeamMember, int, species, game(), species)
CHECK_BOUNDS(MapTrainerTeamMember, int, level, 1, game()->rules()->maxLevel())
CHECK_INDEX(MapTrainerTeamMember, int, ability, game(), ability)
CHECK_BEGIN_ARRAY(MapTrainerTeamMember, int, item, count, int, item)
    IBOUNDS(item, game(), item);
    TBOUNDS_MOD(item_count, 0, game()->rules()->maxHeldItems(), count)
CHECK_END()
CHECK_BEGIN(MapTrainerTeamMember, int, move)
    IBOUNDS(move, game(), move);
    if (!canLearn(move))
    {
        ERROR("Cannot learn the move");
        return false;
    }
CHECK_END()
CHECK_INDEX(MapTrainerTeamMember, int, nature, game(), nature)

MapTrainerTeamMember& MapTrainerTeamMember::operator=(const MapTrainerTeamMember& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(species);
    COPY(level);
    COPY(ability);
    COPY(item);
    COPY(move);
    COPY(nature);
    return *this;
}

int MapTrainerTeamMember::heldWeight(const QMap<int, int>& items) const
{
    int totalWeight = 0;
    QMap<int, int> itemsToCheck = (items.empty() ? m_item : items);
    QList<int> itemIds = itemsToCheck.keys();
    foreach (int itemId, itemIds)
    {
        const Item* item = game()->itemById(itemId);
        if (item)
            totalWeight += itemsToCheck[itemId] * item->weight();
    }
    return totalWeight;
}

bool MapTrainerTeamMember::checkWeight(const int item, const int count) const
{
    const Species* species = game()->speciesById(m_species);
    if (!species)
        return true;
    QMap<int, int> temp = m_item;
    temp[item] = count;
    return (heldWeight(temp) <= species->maxHoldWeight());
}

bool MapTrainerTeamMember::canLearn(const int move) const
{
    const Species* species = game()->speciesById(m_species);
    if (species)
    {
        for (int i = 0; i < species->moveCount(); ++i)
        {
            if (species->move(i)->move() == move)
                return true;
        }
    }
    return !species;
}

void MapTrainerTeamMember::clear()
{
    m_ability.clear();
    m_item.clear();
    m_move.clear();
    m_nature.clear();
}
