/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/ItemType.h
 */

#ifndef SIGMOD_ITEMTYPE
#define SIGMOD_ITEMTYPE

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QMetaType>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::ItemType ItemType.h sigmod/ItemType.h
 * \brief A category for items to be grouped into. Allows limits to be set for groups of items.
 */
class SIGMOD_EXPORT ItemType : public Object
{
    Q_OBJECT
    Q_ENUMS(Count)
    
    public:
        /**
         * \enum Count
         * \brief The ways items can be counted.
         */
        enum Count
        {
            Distinct = 0,
            Total = 1
        };
        /**
         * \var CountStr
         * String values for the ways count of an item.
         */
        static QStringList CountStr;
        
        /**
         * Copy constructor.
         * 
         * \param itemType The type to copy.
         */
        ItemType(const ItemType& itemType);
        /**
         * Create a new type belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        ItemType(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p itemType as a child of \p parent with id \p id.
         * 
         * \param itemType The type to copy the data from.
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        ItemType(const ItemType& itemType, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the type.
         * \param id The id number for the type.
         */
        ItemType(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the type's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the item in XML format.
         * 
         * \return The XML structure representing the type.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the type.
         * 
         * \param name The name of the type.
         */
        void setName(const QString& name);
        /**
         * Sets the number of items that can be stored in the computer at one time. Use -1 for unlimited.
         * 
         * \param computer The number of items of this type that can be stored in the computer at once.
         */
        void setComputer(const int computer);
        /**
         * Sets the number of items that can can be carried by the player at one time. Use 0 for unlimited.
         * 
         * \param player number of items of this type that can be carried by the player at once.
         */
        void setPlayer(const int player);
        /**
         * Sets the maximum weight of items of this type that can be carried at once. Set to -1 for no limit.
         * 
         * \param maxWeight The maximum weight of items of this type that can be carried.
         */
        void setMaxWeight(const int maxWeight);
        /**
         * Sets the way items count towards their maximum count. If \p Distinct is used, items stack and multiples
         * of the same item only count once. With \p Total, each item counts towards the limit.
         * 
         * \param count The way items of this type contribute towards the maximum count.
         */
        void setCount(const Count count);
        
        /**
         * \sa setName
         * 
         * \return The name of the type.
         */
        QString name() const;
        /**
         * \sa setComputer
         * 
         * \return The number of items of this type that can be stored in the computer at once.
         */
        int computer() const;
        /**
         * \sa setPlayer
         * 
         * \return The number of items of this type that can be carried by the player at once.
         */
        int player() const;
        /**
         * \sa setMaxWeight
         * 
         * \return The maximum weight of items of this type that can be carried.
         */
        int maxWeight() const;
        /**
         * \sa setCount
         * 
         * \return The way items of this type contribute towards the maximum count.
         */
        Count count() const;
        
        bool nameCheck(const QString& name) const;
        bool computerCheck(const int computer) const;
        bool playerCheck(const int player) const;
        bool maxWeightCheck(const int maxWeight) const;
        bool countCheck(const Count count) const;
        
        ItemType& operator=(const ItemType& rhs);
    private:
        QString m_name;
        int m_computer;
        int m_player;
        int m_maxWeight;
        Count m_count;
};
}
Q_DECLARE_METATYPE(Sigmod::ItemType::Count)

#endif
