/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Object.h
 */

#ifndef SIGMOD_OBJECT
#define SIGMOD_OBJECT

// Sigmod includes
#include "Global.h"

// Qt includes
#include <QtCore/QObject>
#include <QtXml/QDomElement>

// Forward decarations
namespace Sigcore
{
class Fraction;
}

namespace Sigmod
{
class Game;

/**
 * \class Sigmod::Object Object.h sigmod/Object.h
 * \brief Base class for all data structures in a game.
 * 
 * The Object class sets up a parenting structure for all Sigmod classes.
 * It also provides an interface for validation, saving to XML format,
 * as well as loading from XML.
 */
class SIGMOD_EXPORT Object : public QObject
{
    Q_OBJECT
    
    public:
        /**
         * Constructor.
         * 
         * \param parent The parent of the object.
         * \param id The id of the object.
         */
        Object(const Object* parent, const int id);
        /**
         * Destructor.
         */
        virtual ~Object();
        
        /**
         * \return The parent of the object.
         */
        const Object* parent() const;
        /**
         * \return The game that the object belongs to.
         */
        const Game* game() const;
        
        /**
         * \return The id of the object.
         */
        int id() const;
        /**
         * \return The name of the class of the object without the namespace.
         */
        QString className() const;
        
        /**
         * Convenience function to create a QDomDocument from the QDomElement that is returned from \link Object::save save \endlink.
         * 
         * \param object The object to create an XML structure from.
         * \return The XML representation of \p object.
         */
        static QDomDocument xml(const Object* object);
        
        /**
         * Method for validating the object.
         */
        virtual void validate() = 0;
        
        /**
         * Loads data from an XML structure.
         * 
         * \param xml The XML data structure to load data from.
         */
        virtual void load(const QDomElement& xml) = 0;
        /**
         * Saves the data of the object to XML.
         * 
         * \return An XML representation of the class.
         */
        virtual QDomElement save() const = 0;
    signals:
        /**
         * Signal for when the object encounters something unexpected, but it does not invalid when validating.
         * 
         * \param message The message.
         */
        void valMessage(const QString& message) const;
        /**
         * Signal for when the object encounters a value that is either confusing or not advised, but does not
         * render the game as inconsistent.
         * 
         * \param message The warning.
         */
        void valWarning(const QString& message) const;
        /**
         * Signal for when the object encounters an unexpected value that introduces an inconsistency in the game.
         * 
         * \param message The error.
         */
        void valError(const QString& message) const;
        /**
         * Signal for when a value was set, but it may not be optimal.
         * 
         * \param message The warning.
         */
        void warning(const QString& message) const;
        /**
         * Signal for when a value assignment was attempted, but the value was invalid.
         * 
         * \param message The error.
         */
        void error(const QString& message) const;
        
        /**
         * Signal for when the object has changed.
         */
        void changed() const;
    protected:
        void setId(const int id);
        bool idCheck(const int id) const;
        
        static QString unused(const QString& variable);
        static QString bounds(const QString& variable, const int min, const int max, const int value);
        static QString bounds(const QString& variable, const int min, const int max, const Sigcore::Fraction& value);
        static QString bounds(const QString& variable, const QString& min, const QString& max, const int value);
        static QString bounds(const QString& variable, const int value);
        static QString bounds(const QString& variable, const QString& value);
        static QString subclass(const QString& subclass, const int id);
        static QString subclass(const QString& subclass, const QString& name);
        
        virtual void clear();
    private:
        int m_id;
        const Object* const m_parent;
};
}

#endif
