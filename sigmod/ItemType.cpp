/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/ItemType.cpp
 */

// Header include
#include "ItemType.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Rules.h"

using namespace Sigmod;

QStringList ItemType::CountStr = QStringList() << "Distinct" << "Total" << "Weight";

ItemType::ItemType(const ItemType& itemType) :
        Object(itemType.parent(), itemType.id())
{
    *this = itemType;
}

ItemType::ItemType(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_computer(0),
        m_player(1),
        m_maxWeight(0),
        m_count(Distinct)
{
}

ItemType::ItemType(const ItemType& itemType, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = itemType;
}

ItemType::ItemType(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void ItemType::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(computer);
    TEST(player);
    TEST(maxWeight);
    TEST_END();
}

void ItemType::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(computer);
    LOAD(player);
    LOAD(maxWeight);
    LOAD_ENUM(count, Count);
}

QDomElement ItemType::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(computer);
    SAVE(player);
    SAVE(maxWeight);
    SAVE_ENUM(count, Count);
    return xml;
}

SETTER(ItemType, QString&, Name, name)
SETTER(ItemType, int, Computer, computer)
SETTER(ItemType, int, Player, player)
SETTER(ItemType, int, MaxWeight, maxWeight)
SETTER(ItemType, Count, Count, count)

GETTER(ItemType, QString, name)
GETTER(ItemType, int, computer)
GETTER(ItemType, int, player)
GETTER(ItemType, int, maxWeight)
GETTER(ItemType, ItemType::Count, count)

CHECK(ItemType, QString&, name)
CHECK_BOUNDS(ItemType, int, computer, -1, INT_MAX)
CHECK_BOUNDS(ItemType, int, player, 0, INT_MAX)
CHECK_BOUNDS(ItemType, int, maxWeight, -1, (game()->rules()->maxTotalWeight() == -1) ? INT_MAX : game()->rules()->maxTotalWeight())
CHECK(ItemType, Count, count)

ItemType& ItemType::operator=(const ItemType& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(computer);
    COPY(player);
    COPY(maxWeight);
    COPY(count);
    return *this;
}
