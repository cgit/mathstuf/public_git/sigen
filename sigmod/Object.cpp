/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Object.cpp
 */

// Header include
#include "Object.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Object::Object(const Object* parent, const int id) :
        QObject(NULL),
        m_id(id),
        m_parent(parent)
{
}

Object::~Object()
{
}

const Object* Object::parent() const
{
    return m_parent;
}

const Game* Object::game() const
{
    if (m_parent)
        return m_parent->game();
    return qobject_cast<const Game*>(this);
}

GETTER(Object, int, id)

SETTER(Object, int, Id, id)

CHECK_BOUNDS(Object, int, id, 0, INT_MAX)

QDomDocument Object::xml(const Object* object)
{
    QDomDocument xml(object->className());
    xml.appendChild(object->save());
    return xml;
}

QString Object::className() const
{
    return QString(metaObject()->className()).section(':', -1);
}

QString Object::unused(const QString& variable)
{
    return QString("Setting unused variable %1").arg(variable);
}

// TODO: Use minus sign in negative representations of bounds
QString Object::bounds(const QString& variable, const int min, const int max, const int value)
{
    QString msg = QString::fromUtf8("Value for %1 out-of-bounds (%2 – %3) (%4)").arg(variable);
    if (min == INT_MIN)
        msg = msg.arg(QString::fromUtf8("−∞"));
    else
        msg = msg.arg(min);
    if (max == INT_MAX)
        msg = msg.arg(QString::fromUtf8("∞"));
    else
        msg = msg.arg(max);
    return msg.arg(value);
}

QString Object::bounds(const QString& variable, const int min, const int max, const Fraction& value)
{
    QString msg = QString::fromUtf8("Value for %1 out-of-bounds (%2 – %3) (%4 / %5)").arg(variable);
    if (min == INT_MIN)
        msg = msg.arg(QString::fromUtf8("−∞"));
    else
        msg = msg.arg(min);
    if (max == INT_MAX)
        msg = msg.arg(QString::fromUtf8("∞"));
    else
        msg = msg.arg(max);
    return msg.arg(value.numerator()).arg(value.denominator());
}

QString Object::bounds(const QString& variable, const QString& min, const QString& max, const int value)
{
    return QString::fromUtf8("Value for %1 out-of-bounds (%2 – %3) (%4)").arg(variable).arg(min).arg(max).arg(value);
}

QString Object::bounds(const QString& variable, const int value)
{
    return QString("Value for %1 out-of-bounds (invalid id) (%2)").arg(variable).arg(value);
}

QString Object::bounds(const QString& variable, const QString& value)
{
    return QString("Value for %1 out-of-bounds (%2)").arg(variable).arg(value);
}

QString Object::subclass(const QString& subclass, const int id)
{
    return QString("Duplicate %1 with id %2").arg(subclass, id);
}

QString Object::subclass(const QString& subclass, const QString& name)
{
    return QString("Duplicate %1 with name %2").arg(subclass, name);
}

void Object::clear()
{
}
