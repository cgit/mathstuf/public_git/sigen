/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/CoinList.cpp
 */

// Header include
#include "CoinList.h"

// Sigmod includes
#include "CoinListItem.h"
#include "Game.h"
#include "Macros.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigcore;
using namespace Sigmod;

CoinList::CoinList(const CoinList& coinList) :
        Object(coinList.parent(), coinList.id())
{
    *this = coinList;
}

CoinList::CoinList(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_script("", "")
{
}

CoinList::CoinList(const CoinList& coinList, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = coinList;
}

CoinList::CoinList(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

CoinList::~CoinList()
{
    clear();
}

void CoinList::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (!itemCount())
        emit(error("There are no items"));
    QSet<int> idChecker;
    QSet<int> itemChecker;
    QSet<int> speciesChecker;
    TEST_SUB_BEGIN(CoinListItem, items);
        TEST_SUB("item", id);
        if (object->type() == CoinListItem::Item)
        {
            TEST_SUB_RAW(item, "item item", object);
        }
        else if (object->type() == CoinListItem::Species)
        {
            TEST_SUB_RAW(species, "item species", object);
        }
    TEST_SUB_END();
    TEST_END();
}

void CoinList::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(script);
    LOAD_SUB(newItem, CoinListItem);
}

QDomElement CoinList::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(script);
    SAVE_SUB(CoinListItem, items);
    return xml;
}

SETTER(CoinList, QString&, Name, name)
SETTER(CoinList, Script&, Script, script)

GETTER(CoinList, QString, name)
GETTER(CoinList, Script, script)

CHECK(CoinList, QString&, name)
CHECK(CoinList, Script&, script)

SUBCLASS(CoinList, Item, item, items)

CoinList& CoinList::operator=(const CoinList& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY(script);
    COPY_SUB(CoinListItem, items);
    return *this;
}

void CoinList::clear()
{
    SUBCLASS_CLEAR(items);
}
