/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Author.cpp
 */

// Header include
#include "Author.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

// Qt includes
#include <QtCore/QRegExp>

using namespace Sigmod;

Author::Author(const Author& author) :
        Object(author.parent(), author.id())
{
    *this = author;
}

Author::Author(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_email(""),
        m_role("")
{
}

Author::Author(const Author& author, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = author;
}

Author::Author(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Author::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (m_email.isEmpty())
        emit(warning("Email is empty"));
    else if (!QRegExp("[a-zA-Z0-9%-_\\.]+@[a-zA-Z0-9\\-]+(\\.[a-zA-Z]+)*\\.[a-zA-Z]{2,4}").exactMatch(m_email))
        emit(error("Email is invalid"));
    if (m_role.isEmpty())
        emit(error("Role is empty"));
    TEST_END();
}

void Author::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(email);
    LOAD(role);
}

QDomElement Author::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(email);
    SAVE(role);
    return xml;
}

SETTER(Author, QString&, Name, name)
SETTER(Author, QString&, Email, email)
SETTER(Author, QString&, Role, role)

GETTER(Author, QString, name)
GETTER(Author, QString, email)
GETTER(Author, QString, role)

CHECK(Author, QString&, name)
CHECK(Author, QString&, email)
CHECK(Author, QString&, role)

Author& Author::operator=(const Author& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(email);
    COPY(role);
    return *this;
}
