/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapTrainer.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Map.h"
#include "MapTrainerTeamMember.h"
#include "Rules.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigcore;
using namespace Sigmod;

MapTrainer::MapTrainer(const MapTrainer& trainer) :
        Object(trainer.parent(), trainer.id())
{
    *this = trainer;
}

MapTrainer::MapTrainer(const Map* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_trainerClass(INT_MAX),
        m_position(0, 0),
        m_numberFight(1),
        m_script("", "")
{
}

MapTrainer::MapTrainer(const MapTrainer& trainer, const Map* parent, const int id) :
        Object(parent, id)
{
    *this = trainer;
}

MapTrainer::MapTrainer(const QDomElement& xml, const Map* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

MapTrainer::~MapTrainer()
{
    clear();
}

void MapTrainer::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(trainerClass);
    TEST(position);
    TEST(numberFight);
    if (!m_leadTeamMember.size())
        emit(error("There are no leading team members set"));
    else
        TEST_LIST(leadTeamMember);
    if (!teamMemberCount())
        emit(error("There are no team members"));
    else if (teamMemberCount() < m_numberFight)
        emit(warning("There less team members than set to fight"));
    QSet<int> idChecker;
    TEST_SUB_BEGIN(MapTrainerTeamMember, teamMembers);
        TEST_SUB("team member", id);
    TEST_SUB_END();
    TEST_END();
}

void MapTrainer::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(trainerClass);
    LOAD(position);
    LOAD(numberFight);
    LOAD(script);
    LOAD_LIST(leadTeamMember);
    LOAD_SUB(newTeamMember, MapTrainerTeamMember);
}

QDomElement MapTrainer::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(trainerClass);
    SAVE(position);
    SAVE(numberFight);
    SAVE(script);
    SAVE_LIST(leadTeamMember);
    SAVE_SUB(MapTrainerTeamMember, teamMembers);
    return xml;
}

SETTER(MapTrainer, QString&, Name, name)
SETTER(MapTrainer, int, TrainerClass, trainerClass)
SETTER(MapTrainer, QPoint&, Position, position)
SETTER(MapTrainer, int, NumberFight, numberFight)
SETTER(MapTrainer, Script&, Script, script)
SETTER_LIST_LIMIT(MapTrainer, LeadTeamMember, leadTeamMember, m_numberFight, "More lead team members than fighting")

GETTER(MapTrainer, QString, name)
GETTER(MapTrainer, int, trainerClass)
GETTER(MapTrainer, QPoint, position)
GETTER(MapTrainer, int, numberFight)
GETTER(MapTrainer, Script, script)
GETTER_LIST(MapTrainer, leadTeamMember)

CHECK(MapTrainer, QString&, name)
CHECK_INDEX(MapTrainer, int, trainerClass, game(), trainer)
CHECK_BEGIN(MapTrainer, QPoint&, position)
    const Map* map = qobject_cast<const Map*>(parent());
    TBOUNDS_MOD(position_x, 0, map->width() - 1, position.x())
    TBOUNDS_MOD(position_y, 0, map->height() - 1, position.y())
CHECK_END()
CHECK_BOUNDS(MapTrainer, int, numberFight, 1, game()->rules()->maxFight())
CHECK(MapTrainer, Script&, script)
CHECK_BEGIN(MapTrainer, int, leadTeamMember)
    if (!teamMemberById(leadTeamMember))
    {
        EBOUNDS_IDX(leadTeamMember);
        return false;
    }
CHECK_END()

SUBCLASS(MapTrainer, TeamMember, teamMember, teamMembers)

MapTrainer& MapTrainer::operator=(const MapTrainer& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY(trainerClass);
    COPY(position);
    COPY(numberFight);
    COPY(script);
    COPY(leadTeamMember);
    COPY_SUB(MapTrainerTeamMember, teamMembers);
    return *this;
}

void MapTrainer::clear()
{
    m_leadTeamMember.clear();
    SUBCLASS_CLEAR(teamMembers);
}
