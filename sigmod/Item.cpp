/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Item.cpp
 */

// Header include
#include "Item.h"

// Sigmod includes
#include "Game.h"
#include "ItemType.h"
#include "Macros.h"
#include "Rules.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigcore;
using namespace Sigmod;

Item::Item(const Item& item) :
        Object(item.parent(), item.id())
{
    *this = item;
}

Item::Item(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_type(-1),
        m_price(0),
        m_sellPrice(0),
        m_weight(0),
        m_description(""),
        m_script("", "")
{
}

Item::Item(const Item& item, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = item;
}

Item::Item(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Item::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(type);
    TEST(price);
    TEST(sellPrice);
    TEST(weight);
    TEST_END();
}

void Item::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(type);
    LOAD(price);
    LOAD(sellPrice);
    LOAD(weight);
    LOAD(description);
    LOAD(script);
}

QDomElement Item::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(type);
    SAVE(price);
    SAVE(sellPrice);
    SAVE(weight);
    SAVE(description);
    SAVE(script);
    return xml;
}

SETTER(Item, QString&, Name, name)
SETTER(Item, int, Type, type)
SETTER(Item, int, Price, price)
SETTER(Item, int, SellPrice, sellPrice)
SETTER(Item, int, Weight, weight)
SETTER(Item, QString&, Description, description)
SETTER(Item, Script&, Script, script)

GETTER(Item, QString, name)
GETTER(Item, int, type)
GETTER(Item, int, price)
GETTER(Item, int, sellPrice)
GETTER(Item, int, weight)
GETTER(Item, QString, description)
GETTER(Item, Script, script)

CHECK(Item, QString&, name)
CHECK_INDEX(Item, int, type, game(), itemType)
CHECK_BOUNDS(Item, int, price, 1, game()->rules()->maxMoney())
CHECK_BOUNDS(Item, int, sellPrice, -1, m_price)
CHECK_BEGIN(Item, int, weight)
    const ItemType* type = game()->itemTypeById(m_type);
    if (type)
        TBOUNDS(weight, 0, type->maxWeight())
CHECK_END()
CHECK(Item, QString&, description)
CHECK(Item, Script&, script)

Item& Item::operator=(const Item& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(type);
    COPY(price);
    COPY(sellPrice);
    COPY(weight);
    COPY(description);
    COPY(script);
    return *this;
}
