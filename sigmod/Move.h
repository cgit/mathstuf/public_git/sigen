/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Move.h
 */

#ifndef SIGMOD_MOVE
#define SIGMOD_MOVE

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Move Move.h sigmod/Move.h
 * \brief Class describing a move.
 * 
 * Moves are the standard action in battle. They typically hinder enemies or
 * benefit allies. Most of the damage during battle is inflicted through moves.
 */
class SIGMOD_EXPORT Move : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param move The move to copy.
         */
        Move(const Move& move);
        /**
         * Create a new move belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        Move(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p move as a child of \p parent with id \p id.
         * 
         * \param move The move to copy the data from.
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        Move(const Move& move, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        Move(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the move's values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the move in XML format.
         * 
         * \return The XML structure representing the move.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the move.
         * 
         * \param name The name of the move.
         */
        void setName(const QString& name);
        /**
         * Sets the accuracy of the move. If an accuracy check fails, no effects are done (except
         * for effects that occur when the move misses).
         * 
         * \param accuracy The accuracy of the move.
         */
        void setAccuracy(const Sigcore::Fraction& accuracy);
        /**
         * Sets the power of the move. If the power is 0, no damage calculations are done. All
         * attacks do at least one hit point of damage.
         * 
         * \param power The power of the move.
         */
        void setPower(const int power);
        /**
         * Sets the id of the type of the move.
         * 
         * \param type The id of the type of the move.
         */
        void setType(const int type);
        /**
         * Sets whether the move uses special stats or not.
         * 
         * \param special Whether the move uses special stats or not.
         */
        void setSpecial(const bool special);
        /**
         * Sets the number of power points for the move. It can be increased by items or other
         * effects, but this is a minimum.
         * 
         * \param powerPoints The number of power points for the move.
         */
        void setPowerPoints(const int powerPoints);
        /**
         * Sets the priority of the move when attacking. It can be changed using the priority
         * script during battle.
         * 
         * \param priority The priority of the move when attacking.
         */
        void setPriority(const int priority);
        /**
         * Sets the description of the move. It should be brief, but still descriptive.
         * 
         * \param description A description of the move.
         */
        void setDescription(const QString& description);
        /**
         * Set the script for the move when in battle. This script defines the behavior of the move when in a battle.
         * The following objects are available to the script:
         * 
         *  - \b move -- The \link Sigscritp::MoveWrapper move \endlink the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which is using the move.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the move is being used in.
         *  - \b targetN -- The \link Sigencore::TeamMember targets \endlink of the move (target0, target1, etc.).
         * 
         * This script should control damage, status effects inflicted, and other side effects of the move (PP is automatically
         * reduced by one when using it, though more can be done).
         * 
         * \param battleScript The script for the move when in battle.
         */
        void setBattleScript(const Sigcore::Script& battleScript);
        /**
         * Set the script for the move when on the world map. This script defines the behavior of the move when on the world map.
         * The following objects are available to the script:
         * 
         *  - \b move -- The \link Sigscritp::MoveWrapper move \endlink the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which knows the move.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the \b player is on.
         * 
         * If the move does nothing outside of battle, the script should be empty. Otherwise, it should listen to the appropriate
         * signals and react to them.
         * 
         * \param worldScript The script for the move when on the world map.
         */
        void setWorldScript(const Sigcore::Script& worldScript);
        /**
         * Set the script that determines the priority of the move. This script allows the move to change its priority based on what
         * is happening in the \link Sigencore::Arena arena \endlink. The following objects are available to the script:
         * 
         *  - \b move -- The \link Sigscript::MoveWrapper move \endlink the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which is using the move.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the move is being used in.
         * 
         * To override the priority given by the move, set the \p move-priority-%name (where "%name" is the name of the move)
         * value in \p owner. Setting it in the \link Sigscript::SigmodWrapper Sigmod \endlink will cause all instances of the
         * move to have the new priority.
         * 
         * \param priorityScript The script that determines the priority of the move.
         */
        void setPriorityScript(const Sigcore::Script& priorityScript);
        
        /**
         * \sa setMove
         * 
         * \return The name of the move.
         */
        QString name() const;
        /**
         * \sa setAccuracy
         * 
         * \return The accuracy of the move.
         */
        Sigcore::Fraction accuracy() const;
        /**
         * \sa setPower
         * 
         * \return The power of the move.
         */
        int power() const;
        /**
         * \sa setType
         * 
         * \return The id of the type of the move.
         */
        int type() const;
        /**
         * \sa setSpecial
         * 
         * \return Whether the move uses pecial stats or not.
         */
        bool special() const;
        /**
         * \sa setPowerPoints
         * 
         * \return The number of power points the move has.
         */
        int powerPoints() const;
        /**
         * \sa setPriority
         * 
         * \return The priority of the move.
         */
        int priority() const;
        /**
         * \sa setDescription
         * 
         * \return A description of the move.
         */
        QString description() const;
        /**
         * \sa setBattleScript
         * 
         * \return The script for the move in battle.
         */
        Sigcore::Script battleScript() const;
        /**
         * \sa setWorldScript
         * 
         * \return The script for the move when in the world map.
         */
        Sigcore::Script worldScript() const;
        /**
         * \sa setPriorityScript
         * 
         * \return The script for determining priority in battle.
         */
        Sigcore::Script priorityScript() const;
        
        bool nameCheck(const QString& name) const;
        bool accuracyCheck(const Sigcore::Fraction& accuracy) const;
        bool powerCheck(const int power) const;
        bool typeCheck(const int type) const;
        bool specialCheck(const bool special) const;
        bool powerPointsCheck(const int powerPoints) const;
        bool priorityCheck(const int priority) const;
        bool descriptionCheck(const QString& description) const;
        bool battleScriptCheck(const Sigcore::Script& battleScript) const;
        bool worldScriptCheck(const Sigcore::Script& worldScript) const;
        bool priorityScriptCheck(const Sigcore::Script& priorityScript) const;
        
        Move& operator=(const Move& rhs);
    private:
        QString m_name;
        Sigcore::Fraction m_accuracy;
        int m_power;
        int m_type;
        bool m_special;
        int m_powerPoints;
        int m_priority;
        QString m_description;
        Sigcore::Script m_battleScript;
        Sigcore::Script m_worldScript;
        Sigcore::Script m_priorityScript;
};
}

#endif
