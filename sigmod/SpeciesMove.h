/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/SpeciesMove.h
 */

#ifndef SIGMOD_SPECIESMOVE
#define SIGMOD_SPECIESMOVE

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Species;

/**
 * \class Sigmod::SpeciesMove SpeciesMove.h sigmod/SpeciesMove.h
 * \brief Class that gives species the ability to learn moves.
 * 
 * Moves that can be learned must be in the species' move list, which is created by
 * creating an instance of this class for each move. Moves that have both level and wild
 * set to -1 can be learned, but not through traditional methods (e.g. items, tutors, or
 * breeding).
 */
class SIGMOD_EXPORT SpeciesMove : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param move The move to copy.
         */
        SpeciesMove(const SpeciesMove& move);
        /**
         * Create a new move belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        SpeciesMove(const Species* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p move as a child of \p parent with id \p id.
         * 
         * \param move The move to copy the data from.
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        SpeciesMove(const SpeciesMove& move, const Species* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the move.
         * \param id The id number for the move.
         */
        SpeciesMove(const QDomElement& xml, const Species* parent, const int id = -1);
        
        /**
         * Check to make sure the move's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the move in XML format.
         * 
         * \return The XML structure representing the move.
         */
        QDomElement save() const;
        
        /**
         * Sets the id of the move that can be learned.
         * 
         * \param move The id of the move that is learned.
         */
        void setMove(const int move);
        /**
         * Sets the level at which the move is learned when leveled up by a trainer. If set to -1,
         * it cannot be learned by leveling up.
         * 
         * \param level The level at which the move is learned normally.
         */
        void setLevel(const int level);
        /**
         * Sets the level at which the move is learned in the wild. This is used to allow randomly
         * encountered creatures to have moves that would normally not be allowed until higher level
         * or at all. If set to -1, it cannot be learned in the wild.
         * 
         * \param wild The level at which the move is learned in the wild.
         */
        void setWild(const int wild);
        
        /**
         * \sa setMove
         * 
         * \return The id of the move that is learned.
         */
        int move() const;
        /**
         * \sa setLevel
         * 
         * \return The level at which the move is learned at when captured.
         */
        int level() const;
        /**
         * \sa setWild
         * 
         * \return The level at which the move is learned at in the wild.
         */
        int wild() const;
        
        bool moveCheck(const int move) const;
        bool levelCheck(const int level) const;
        bool wildCheck(const int wild) const;
        
        SpeciesMove& operator=(const SpeciesMove& rhs);
    private:
        int m_move;
        int m_level;
        int m_wild;
};
}

#endif
