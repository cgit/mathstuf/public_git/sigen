/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "MapWildList.h"

// Sigmod includes
#include "Macros.h"
#include "Map.h"
#include "MapWildListEncounter.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigmod;

MapWildList::MapWildList(const MapWildList& wildList) :
        Object(wildList.parent(), wildList.id())
{
    *this = wildList;
}

MapWildList::MapWildList(const Map* parent, const int id) :
        Object(parent, id),
        m_name("")
{
}

MapWildList::MapWildList(const MapWildList& wildList, const Map* parent, const int id) :
        Object(parent, id)
{
    *this = wildList;
}

MapWildList::MapWildList(const QDomElement& xml, const Map* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

MapWildList::~MapWildList()
{
    clear();
}

void MapWildList::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (!encounterCount())
        emit(error("There are no encounters"));
    QSet<int> idChecker;
    TEST_SUB_BEGIN(MapWildListEncounter, encounters);
        TEST_SUB("encounter", id);
    TEST_SUB_END();
    TEST_END();
}

void MapWildList::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD_SUB(newEncounter, MapWildListEncounter);
}

QDomElement MapWildList::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE_SUB(MapWildListEncounter, encounters);
    return xml;
}

SETTER(MapWildList, QString&, Name, name)

GETTER(MapWildList, QString, name)

CHECK(MapWildList, QString&, name)

SUBCLASS(MapWildList, Encounter, encounter, encounters)

MapWildList& MapWildList::operator=(const MapWildList& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY_SUB(MapWildListEncounter, encounters);
    return *this;
}

void MapWildList::clear()
{
    SUBCLASS_CLEAR(encounters);
}
