/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Time.cpp
 */

// Header include
#include "Time.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Time::Time(const Time& time) :
        Object(time.parent(), time.id())
{
    *this = time;
}

Time::Time(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_hour(0),
        m_minute(0),
        m_script("", "")
{
}

Time::Time(const Time& time, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = time;
}

Time::Time(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Time::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(hour);
    TEST(minute);
    TEST_END();
}

void Time::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(hour);
    LOAD(minute);
    LOAD(script);
}

QDomElement Time::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(hour);
    SAVE(minute);
    SAVE(script);
    return xml;
}

SETTER(Time, QString&, Name, name)
SETTER(Time, int, Hour, hour)
SETTER(Time, int, Minute, minute)
SETTER(Time, Script&, Script, script)

GETTER(Time, QString, name)
GETTER(Time, int, hour)
GETTER(Time, int, minute)
GETTER(Time, Script, script)

CHECK(Time, QString&, name)
CHECK_BOUNDS(Time, int, hour, 0, 23)
CHECK_BOUNDS(Time, int, minute, 0, 59)
CHECK(Time, Script&, script)

Time& Time::operator=(const Time& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(hour);
    COPY(minute);
    COPY(script);
    return *this;
}
