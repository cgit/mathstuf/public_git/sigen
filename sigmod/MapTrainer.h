/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_MAPTRAINER
#define SIGMOD_MAPTRAINER

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QPoint>

namespace Sigmod
{
// Forward declarations
class Map;
class MapTrainerTeamMember;

class SIGMOD_EXPORT MapTrainer : public Object
{
    Q_OBJECT
    
    public:
        MapTrainer(const MapTrainer& trainer);
        MapTrainer(const Map* parent, const int id);
        MapTrainer(const MapTrainer& trainer, const Map* parent, const int id);
        MapTrainer(const QDomElement& xml, const Map* parent, const int id = -1);
        ~MapTrainer();
        
        void validate();
        
        void load(const QDomElement& xml);
        QDomElement save() const;
        
        void setName(const QString& name);
        void setTrainerClass(const int trainerClass);
        void setPosition(const QPoint& position);
        void setNumberFight(const int numberFight);
        void setScript(const Sigcore::Script& script);
        void setLeadTeamMember(const int leadTeamMember, const bool state);
        
        QString name() const;
        int trainerClass() const;
        QPoint position() const;
        int numberFight() const;
        Sigcore::Script script() const;
        bool leadTeamMember(const int teamMember) const;
        QList<int> leadTeamMember() const;
        
        bool nameCheck(const QString& name) const;
        bool trainerClassCheck(const int trainerClass) const;
        bool positionCheck(const QPoint& position) const;
        bool numberFightCheck(const int numberFight) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        bool leadTeamMemberCheck(const int leadTeamMember) const;
        
        const MapTrainerTeamMember* teamMember(const int index) const;
        MapTrainerTeamMember* teamMember(const int index);
        const MapTrainerTeamMember* teamMemberById(const int id) const;
        MapTrainerTeamMember* teamMemberById(const int id);
        int teamMemberIndex(const int id) const;
        int teamMemberCount() const;
        MapTrainerTeamMember* newTeamMember();
        MapTrainerTeamMember* newTeamMember(const QDomElement& xml);
        MapTrainerTeamMember* newTeamMember(const MapTrainerTeamMember& teamMember);
        void deleteTeamMember(const int index);
        void deleteTeamMemberById(const int id);
        
        MapTrainer& operator=(const MapTrainer& rhs);
    private:
        int newTeamMemberId() const;
        MapTrainerTeamMember* newTeamMember(MapTrainerTeamMember* teamMember);
        
        void clear();
        
        QString m_name;
        int m_trainerClass;
        QPoint m_position;
        int m_numberFight;
        Sigcore::Script m_script;
        QList<int> m_leadTeamMember;
        QList<MapTrainerTeamMember*> m_teamMembers;
};
}

#endif
