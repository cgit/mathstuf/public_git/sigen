/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSpecies.h"

// Sigmod includes
#include "../Game.h"
#include "../Rules.h"
#include "../Species.h"
#include "../SpeciesMove.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestSpecies::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_species1 = m_game->newSpecies();
    m_species2 = m_game->newSpecies();
    m_species3 = m_game->newSpecies();
}

void TestSpecies::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestSpecies::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_species1);
    makeConnections(m_species2);
    makeConnections(m_species3);
}

void TestSpecies::cleanup()
{
    closeConnections(m_species1);
    closeConnections(m_species2);
    closeConnections(m_species3);
    
    TestSigmodObject::cleanup();
}

void TestSpecies::validation()
{
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_species1->setName("Foo");
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->rules()->setMaxEVPerStat(10);
    
    m_species1->setEffortValue(ST_HP, 5);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 13);
    
    m_game->rules()->setMaxEVPerStat(2);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 18);
    
    m_game->rules()->setMaxEVPerStat(20);
    
    m_species1->setEffortValue(ST_Attack, 15);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 22);
    
    m_game->rules()->setMaxEVPerStat(10);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 27);
    
    m_game->rules()->setMaxEVPerStat(30);
    
    m_species1->setEffortValue(ST_Defense, 25);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 31);
    
    m_game->rules()->setMaxEVPerStat(20);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 36);
    
    m_game->rules()->setMaxEVPerStat(40);
    
    m_species1->setEffortValue(ST_Speed, 35);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 40);
    
    m_game->rules()->setMaxEVPerStat(30);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 45);
    
    m_game->rules()->setMaxEVPerStat(50);
    
    m_species1->setEffortValue(ST_SpecialAttack, 45);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 49);
    
    m_game->rules()->setMaxEVPerStat(40);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 54);
    
    m_game->rules()->setSpecialSplit(true);
    m_game->rules()->setMaxEVPerStat(60);
    
    m_species1->setEffortValue(ST_SpecialDefense, 55);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 58);
    
    m_game->rules()->setSpecialSplit(true);
    m_game->rules()->setMaxEVPerStat(50);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 63);
    
    m_game->rules()->setSpecialSplit(false);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 67);
    
    m_game->newSprite();
    
    m_species1->setFrontMaleSprite(0);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 70);
    
    m_game->newSprite();
    
    m_species1->setBackMaleSprite(1);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 72);
    
    m_game->rules()->setGenderAllowed(true);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 76);
    
    m_game->newSprite();
    
    m_species1->setFrontFemaleSprite(2);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 79);
    
    m_game->newSprite();
    
    m_species1->setBackFemaleSprite(3);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 81);
    
    m_game->newSkin();
    
    m_species1->setSkin(0);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 82);
    
    m_species1->setEncyclopediaNumber(-1);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 83);
    
    m_species1->setEncyclopediaNumber(0);
    m_species1->setEncyclopediaEntry("Foo");
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 84);
    
    m_game->rules()->setBreedingAllowed(true);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 85);
    
    m_game->newEggGroup();
    
    m_species1->setEggGroup(0, true);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 87);
    
    m_game->newSpecies();
    
    m_species1->setEggSpecies(1);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 89);
    
    m_game->species(1)->setGrowth(Species::Fast);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 92);
    
    m_species1->setGrowth(Species::Fast);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 94);
    
    m_species1->setEggSteps(10);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 95);
    
    m_game->rules()->setMaxAbilities(1);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 96);
    
    m_game->newAbility();
    
    m_species1->setAbility(0, 1);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 97);
    
    m_game->rules()->setMaxHeldItems(1);
    
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 98);
    
    m_game->newItem();
    
    m_species1->setItem(0, 1);
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 99);
    
    m_species1->newMove();
    m_species1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 99);
    
    m_game->newType();
    m_game->newEggGroup();
    
    m_species1->setType(0, true);
    m_species1->setEggGroup(0, true);
}

void TestSpecies::saving()
{
    QDomDocument xml = Object::xml(m_species1);
    QFile file("species.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestSpecies::loading()
{
    QDomDocument xml;
    QFile file("species.xml");
    
    m_game->rules()->setSpecialSplit(true);
    m_game->rules()->setMaxEVPerStat(60);
    m_game->newSkin();
    m_game->newType();
    m_game->newEggGroup();
    
    m_species1->setName("Bar");
    m_species1->setEffortValue(ST_HP, 10);
    m_species1->setEffortValue(ST_Attack, 10);
    m_species1->setEffortValue(ST_Defense, 10);
    m_species1->setEffortValue(ST_Speed, 10);
    m_species1->setEffortValue(ST_SpecialAttack, 10);
    m_species1->setEffortValue(ST_SpecialDefense, 10);
    m_species1->setEffortValue(ST_HP, 18);
    m_species1->setEffortValue(ST_Attack, 18);
    m_species1->setEffortValue(ST_Defense, 18);
    m_species1->setEffortValue(ST_Speed, 18);
    m_species1->setEffortValue(ST_SpecialAttack, 18);
    m_species1->setEffortValue(ST_SpecialDefense, 18);
    m_species1->setGrowth(Species::Normal);
    m_species1->setExperienceValue(10);
    m_species1->setCatchValue(10);
    m_species1->setMaxHoldWeight(100);
    m_species1->setRunChance(Fraction(1, 2));
    m_species1->setFleeChance(Fraction(1, 2));
    m_species1->setItemChance(Fraction(1, 2));
    m_species1->setEncyclopediaNumber(1);
    m_species1->setWeight(100);
    m_species1->setHeight(100);
    m_species1->setFrontMaleSprite(3);
    m_species1->setBackMaleSprite(2);
    m_species1->setFrontFemaleSprite(1);
    m_species1->setBackFemaleSprite(0);
    m_species1->setSkin(1);
    m_species1->setEncyclopediaEntry("Bar");
    m_species1->setGenderFactor(Fraction(3, 4));
    m_species1->setEggSpecies(2);
    m_species1->setEggSteps(100);
    m_species1->setType(0, false);
    m_species1->setType(1, true);
    m_species1->setEggGroup(0, false);
    m_species1->setEggGroup(1, true);
    m_species1->setEvolution(Script("python", "import os"));
    m_species1->setAbility(0, 0);
    m_species1->setItem(0, 0);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_species1->load(xml.firstChildElement("Species"));
    
    QCOMPARE(m_species1->name(), QString("Foo"));
    QCOMPARE(m_species1->baseStat(ST_HP), 1);
    QCOMPARE(m_species1->baseStat(ST_Attack), 1);
    QCOMPARE(m_species1->baseStat(ST_Defense), 1);
    QCOMPARE(m_species1->baseStat(ST_Speed), 1);
    QCOMPARE(m_species1->baseStat(ST_SpecialAttack), 1);
    QCOMPARE(m_species1->baseStat(ST_SpecialDefense), 1);
    QCOMPARE(m_species1->effortValue(ST_HP), 5);
    QCOMPARE(m_species1->effortValue(ST_Attack), 15);
    QCOMPARE(m_species1->effortValue(ST_Defense), 25);
    QCOMPARE(m_species1->effortValue(ST_Speed), 35);
    QCOMPARE(m_species1->effortValue(ST_SpecialAttack), 45);
    QCOMPARE(m_species1->effortValue(ST_SpecialDefense), 55);
    QCOMPARE(m_species1->growth(), Species::Fast);
    QCOMPARE(m_species1->experienceValue(), 0);
    QCOMPARE(m_species1->catchValue(), 0);
    QCOMPARE(m_species1->maxHoldWeight(), 0);
    QCOMPARE(m_species1->runChance(), Fraction(1, 1));
    QCOMPARE(m_species1->fleeChance(), Fraction(1, 1));
    QCOMPARE(m_species1->itemChance(), Fraction(1, 1));
    QCOMPARE(m_species1->encyclopediaNumber(), 0);
    QCOMPARE(m_species1->weight(), 0);
    QCOMPARE(m_species1->height(), 0);
    QCOMPARE(m_species1->encyclopediaEntry(), QString("Foo"));
    QCOMPARE(m_species1->frontMaleSprite(), 0);
    QCOMPARE(m_species1->backMaleSprite(), 1);
    QCOMPARE(m_species1->frontFemaleSprite(), 2);
    QCOMPARE(m_species1->backFemaleSprite(), 3);
    QCOMPARE(m_species1->skin(), 0);
    QCOMPARE(m_species1->genderFactor(), Fraction(1, 2));
    QCOMPARE(m_species1->eggSpecies(), 1);
    QCOMPARE(m_species1->eggSteps(), 10);
    QCOMPARE(m_species1->type(0), true);
    QCOMPARE(m_species1->type(1), false);
    QCOMPARE(m_species1->eggGroup(0), true);
    QCOMPARE(m_species1->eggGroup(1), false);
    QCOMPARE(m_species1->evolution(), Script());
    QCOMPARE(m_species1->ability(0), 1);
    QCOMPARE(m_species1->item(0), 1);
}

void TestSpecies::setName()
{
    m_species2->setName("Foo");
    m_species2->setName("Foo");
    
    QCOMPARE(m_species2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setBaseStat()
{
    m_species2->setBaseStat(ST_HP, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setBaseStat(ST_HP, 10);
    m_species2->setBaseStat(ST_HP, 10);
    
    QCOMPARE(m_species2->baseStat(ST_HP), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setBaseStat(ST_Attack, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setBaseStat(ST_Attack, 10);
    m_species2->setBaseStat(ST_Attack, 10);
    
    QCOMPARE(m_species2->baseStat(ST_Attack), 10);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setBaseStat(ST_Defense, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_species2->setBaseStat(ST_Defense, 10);
    m_species2->setBaseStat(ST_Defense, 10);
    
    QCOMPARE(m_species2->baseStat(ST_Defense), 10);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_species2->setBaseStat(ST_Speed, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_species2->setBaseStat(ST_Speed, 10);
    m_species2->setBaseStat(ST_Speed, 10);
    
    QCOMPARE(m_species2->baseStat(ST_Speed), 10);
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_species2->setBaseStat(ST_SpecialAttack, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_species2->setBaseStat(ST_SpecialAttack, 10);
    m_species2->setBaseStat(ST_SpecialAttack, 10);
    
    QCOMPARE(m_species2->baseStat(ST_SpecialAttack), 10);
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->rules()->setSpecialSplit(true);
    
    m_species2->setBaseStat(ST_SpecialDefense, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_species2->setBaseStat(ST_SpecialDefense, 10);
    m_species2->setBaseStat(ST_SpecialDefense, 10);
    
    QCOMPARE(m_species2->baseStat(ST_SpecialDefense), 10);
    
    QCOMPARE(m_changedCount, 6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_game->rules()->setSpecialSplit(false);
    
    m_species2->setBaseStat(ST_SpecialDefense, 10);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
}

void TestSpecies::setEffortValue()
{
    m_species2->setEffortValue(ST_HP, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setEffortValue(ST_HP, 18);
    m_species2->setEffortValue(ST_HP, 18);
    
    QCOMPARE(m_species2->effortValue(ST_HP), 18);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setEffortValue(ST_Attack, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setEffortValue(ST_Attack, 18);
    m_species2->setEffortValue(ST_Attack, 18);
    
    QCOMPARE(m_species2->effortValue(ST_Attack), 18);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setEffortValue(ST_Defense, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_species2->setEffortValue(ST_Defense, 18);
    m_species2->setEffortValue(ST_Defense, 18);
    
    QCOMPARE(m_species2->effortValue(ST_Defense), 18);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_species2->setEffortValue(ST_Speed, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_species2->setEffortValue(ST_Speed, 18);
    m_species2->setEffortValue(ST_Speed, 18);
    
    QCOMPARE(m_species2->effortValue(ST_Speed), 18);
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_species2->setEffortValue(ST_SpecialAttack, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_species2->setEffortValue(ST_SpecialAttack, 18);
    m_species2->setEffortValue(ST_SpecialAttack, 18);
    
    QCOMPARE(m_species2->effortValue(ST_SpecialAttack), 18);
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->rules()->setSpecialSplit(true);
    
    m_species2->setEffortValue(ST_SpecialDefense, -1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_game->rules()->setMaxEVPerStat(20);
    
    m_species2->setEffortValue(ST_SpecialDefense, 25);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_species2->setEffortValue(ST_SpecialDefense, 18);
    m_species2->setEffortValue(ST_SpecialDefense, 18);
    
    QCOMPARE(m_species2->effortValue(ST_SpecialDefense), 18);
    
    QCOMPARE(m_changedCount, 6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->rules()->setSpecialSplit(false);
    
    m_species2->setEffortValue(ST_SpecialDefense, 18);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
}

void TestSpecies::setGrowth()
{
    m_species2->setGrowth(Species::Erratic);
    m_species2->setGrowth(Species::Erratic);
    
    QCOMPARE(m_species2->growth(), Species::Erratic);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setExperienceValue()
{
    m_species2->setExperienceValue(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setExperienceValue(10);
    m_species2->setExperienceValue(10);
    
    QCOMPARE(m_species2->experienceValue(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setCatchValue()
{
    m_species2->setCatchValue(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setCatchValue(256);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setCatchValue(10);
    m_species2->setCatchValue(10);
    
    QCOMPARE(m_species2->catchValue(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setMaxHoldWeight()
{
    m_species2->setMaxHoldWeight(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setMaxHoldWeight(10);
    m_species2->setMaxHoldWeight(10);
    
    QCOMPARE(m_species2->maxHoldWeight(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setRunChance()
{
    m_species2->setRunChance(Fraction(-1, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setRunChance(Fraction(2, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setRunChance(Fraction(3, 4));
    m_species2->setRunChance(Fraction(3, 4));
    
    QCOMPARE(m_species2->runChance(), Fraction(3, 4));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setFleeChance()
{
    m_species2->setFleeChance(Fraction(-1, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setFleeChance(Fraction(2, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setFleeChance(Fraction(3, 4));
    m_species2->setFleeChance(Fraction(3, 4));
    
    QCOMPARE(m_species2->fleeChance(), Fraction(3, 4));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setItemChance()
{
    m_species2->setItemChance(Fraction(-1, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setItemChance(Fraction(2, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setItemChance(Fraction(3, 4));
    m_species2->setItemChance(Fraction(3, 4));
    
    QCOMPARE(m_species2->itemChance(), Fraction(3, 4));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setEncyclopediaNumber()
{
    m_species2->setEncyclopediaNumber(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setEncyclopediaNumber(5);
    m_species2->setEncyclopediaNumber(5);
    
    QCOMPARE(m_species2->encyclopediaNumber(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setWeight()
{
    m_species2->setWeight(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setWeight(10);
    m_species2->setWeight(10);
    
    QCOMPARE(m_species2->weight(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setHeight()
{
    m_species2->setHeight(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setHeight(10);
    m_species2->setHeight(10);
    
    QCOMPARE(m_species2->height(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setFrontMaleSprite()
{
    m_species2->setFrontMaleSprite(4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setGenderFactor(Fraction(1, 1));
    m_game->newSprite();
    
    m_species2->setFrontMaleSprite(4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setGenderFactor(Fraction(1, 2));
    
    m_species2->setFrontMaleSprite(4);
    m_species2->setFrontMaleSprite(4);
    
    QCOMPARE(m_species2->frontMaleSprite(), 4);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setBackMaleSprite()
{
    m_species2->setBackMaleSprite(5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setGenderFactor(Fraction(1, 1));
    m_game->newSprite();
    
    m_species2->setBackMaleSprite(5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setGenderFactor(Fraction(1, 2));
    
    m_species2->setBackMaleSprite(5);
    m_species2->setBackMaleSprite(5);
    
    QCOMPARE(m_species2->backMaleSprite(), 5);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setFrontFemaleSprite()
{
    m_game->rules()->setGenderAllowed(true);
    
    m_species2->setFrontFemaleSprite(6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setGenderFactor(Fraction(0, 1));
    m_game->newSprite();
    
    m_species2->setFrontFemaleSprite(6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setGenderFactor(Fraction(1, 2));
    
    m_species2->setFrontFemaleSprite(6);
    m_species2->setFrontFemaleSprite(6);
    
    QCOMPARE(m_species2->frontFemaleSprite(), 6);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setGenderAllowed(false);
    
    m_species2->setFrontFemaleSprite(6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestSpecies::setBackFemaleSprite()
{
    m_game->rules()->setGenderAllowed(true);
    
    m_species2->setBackFemaleSprite(7);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setGenderFactor(Fraction(0, 1));
    m_game->newSprite();
    
    m_species2->setBackFemaleSprite(7);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setGenderFactor(Fraction(1, 2));
    
    m_species2->setBackFemaleSprite(7);
    m_species2->setBackFemaleSprite(7);
    
    QCOMPARE(m_species2->backFemaleSprite(), 7);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setGenderAllowed(false);
    
    m_species2->setBackFemaleSprite(7);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestSpecies::setSkin()
{
    m_species2->setSkin(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSkin();
    
    m_species2->setSkin(2);
    m_species2->setSkin(2);
    
    QCOMPARE(m_species2->skin(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setEncyclopediaEntry()
{
    m_species2->setEncyclopediaEntry("Foo");
    m_species2->setEncyclopediaEntry("Foo");
    
    QCOMPARE(m_species2->encyclopediaEntry(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setGenderFactor()
{
    m_species2->setGenderFactor(Fraction(-2, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setGenderFactor(Fraction(2, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setGenderFactor(Fraction(1, 4));
    m_species2->setGenderFactor(Fraction(1, 4));
    
    QCOMPARE(m_species2->genderFactor(), Fraction(1, 4));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setEggSpecies()
{
    m_species2->setEggSpecies(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_game->newSpecies();
    
    m_species2->setEggSpecies(2);
    m_species2->setEggSpecies(2);
    
    QCOMPARE(m_species2->eggSpecies(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setEggSteps()
{
    m_species2->setEggSteps(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setEggSteps(1000);
    m_species2->setEggSteps(1000);
    
    QCOMPARE(m_species2->eggSteps(), 1000);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpecies::setType()
{
    m_game->newType();
    
    m_species2->setType(0, true);
    m_species2->setType(0, true);
    
    QCOMPARE(m_species2->type(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setType(1, true);
    
    QCOMPARE(m_species2->type(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setType(1, false);
    
    QCOMPARE(m_species2->type(1), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setType(2, false);
    
    QCOMPARE(m_species2->type(2), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setEggGroup()
{
    m_game->newEggGroup();
    
    m_species2->setEggGroup(0, true);
    m_species2->setEggGroup(0, true);
    
    QCOMPARE(m_species2->eggGroup(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setEggGroup(1, true);
    
    QCOMPARE(m_species2->eggGroup(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setEggGroup(1, false);
    
    QCOMPARE(m_species2->eggGroup(1), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setEggGroup(2, false);
    
    QCOMPARE(m_species2->eggGroup(2), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setAbility()
{
    m_game->newAbility();
    
    m_species2->setAbility(0, 1);
    m_species2->setAbility(0, 1);
    
    QCOMPARE(m_species2->ability(0), 1);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setAbility(0, 0);
    
    QCOMPARE(m_species2->ability().size(), 0);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setAbility(2, 1);
    
    QCOMPARE(m_species2->ability().size(), 0);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setAbility(1, -1);
    
    QCOMPARE(m_species2->ability(1), 0);
        
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setAbility(1, 2);
    
    QCOMPARE(m_species2->ability(1), 2);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setAbility(0, 1);
    
    QCOMPARE(m_species2->ability(0), 1);
        
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::setItem()
{
    m_game->newItem();
    
    m_species2->setItem(0, 1);
    m_species2->setItem(0, 1);
    
    QCOMPARE(m_species2->item(0), 1);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setItem(0, 0);
    
    QCOMPARE(m_species2->item().size(), 0);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_species2->setItem(2, 1);
    
    QCOMPARE(m_species2->item().size(), 0);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_species2->setItem(1, -1);
    
    QCOMPARE(m_species2->item(1), 0);
        
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setItem(1, 2);
    
    QCOMPARE(m_species2->item(1), 2);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_species2->setItem(0, 1);
    
    QCOMPARE(m_species2->item(0), 1);
        
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpecies::moves()
{
    QCOMPARE(m_species2->newMove()->id(), 0);
    QCOMPARE(m_species2->newMove()->id(), 1);
    QCOMPARE(m_species2->newMove()->id(), 2);
    
    QCOMPARE(m_species2->moveCount(), 3);
    
    m_species2->deleteMove(0);
    
    QCOMPARE(m_species2->moveCount(), 2);
    
    QCOMPARE(m_species2->newMove()->id(), 0);
    
    QCOMPARE(m_species2->moveIndex(0), 2);
    
    m_species2->deleteMoveById(1);
    
    QCOMPARE(m_species2->moveIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::setEvolution()
{
    m_species2->setEvolution(Script("python", "import os"));
    m_species2->setEvolution(Script("python", "import os"));
    
    QCOMPARE(m_species2->evolution(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSpecies::assignment()
{
    *m_species3 = *m_species2;
    
    m_game->rules()->setSpecialSplit(true);
    
    QCOMPARE(m_species3->name(), QString("Foo"));
    QCOMPARE(m_species3->baseStat(ST_HP), 10);
    QCOMPARE(m_species3->baseStat(ST_Attack), 10);
    QCOMPARE(m_species3->baseStat(ST_Defense), 10);
    QCOMPARE(m_species3->baseStat(ST_Speed), 10);
    QCOMPARE(m_species3->baseStat(ST_SpecialAttack), 10);
    QCOMPARE(m_species3->baseStat(ST_SpecialDefense), 10);
    QCOMPARE(m_species3->effortValue(ST_HP), 18);
    QCOMPARE(m_species3->effortValue(ST_Attack), 18);
    QCOMPARE(m_species3->effortValue(ST_Defense), 18);
    QCOMPARE(m_species3->effortValue(ST_Speed), 18);
    QCOMPARE(m_species3->effortValue(ST_SpecialAttack), 18);
    QCOMPARE(m_species3->effortValue(ST_SpecialDefense), 18);
    QCOMPARE(m_species3->growth(), Species::Erratic);
    QCOMPARE(m_species3->experienceValue(), 10);
    QCOMPARE(m_species3->catchValue(), 10);
    QCOMPARE(m_species3->maxHoldWeight(), 10);
    QCOMPARE(m_species3->runChance(), Fraction(3, 4));
    QCOMPARE(m_species3->fleeChance(), Fraction(3, 4));
    QCOMPARE(m_species3->itemChance(), Fraction(3, 4));
    QCOMPARE(m_species3->encyclopediaNumber(), 5);
    QCOMPARE(m_species3->weight(), 10);
    QCOMPARE(m_species3->height(), 10);
    QCOMPARE(m_species3->encyclopediaEntry(), QString("Foo"));
    QCOMPARE(m_species3->frontMaleSprite(), 4);
    QCOMPARE(m_species3->backMaleSprite(), 5);
    QCOMPARE(m_species3->frontFemaleSprite(), 6);
    QCOMPARE(m_species3->backFemaleSprite(), 7);
    QCOMPARE(m_species3->skin(), 2);
    QCOMPARE(m_species3->genderFactor(), Fraction(1, 4));
    QCOMPARE(m_species3->eggSpecies(), 2);
    QCOMPARE(m_species3->eggSteps(), 1000);
    QCOMPARE(m_species3->type(0), true);
    QCOMPARE(m_species3->type(1), false);
    QCOMPARE(m_species3->type(2), false);
    QCOMPARE(m_species3->eggGroup(0), true);
    QCOMPARE(m_species3->eggGroup(1), false);
    QCOMPARE(m_species3->eggGroup(2), false);
    QCOMPARE(m_species3->evolution(), Script("python", "import os"));
    QCOMPARE(m_species3->ability(0), 1);
    QCOMPARE(m_species3->ability(1), 2);
    QCOMPARE(m_species3->item(0), 1);
    QCOMPARE(m_species3->item(1), 2);
    QCOMPARE(m_species3->moveCount(), 2);
}

QTEST_APPLESS_MAIN(TestSpecies)
