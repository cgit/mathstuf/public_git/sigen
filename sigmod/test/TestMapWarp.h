/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTMAPWARP
#define SIGMOD_TESTMAPWARP

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class MapWarp;
}

class TestMapWarp : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setName();
        void setArea();
        void setToMap();
        void setToWarp();
        void setScript();
        
        void assignment();
    private:
        Sigmod::MapWarp* m_warp1;
        Sigmod::MapWarp* m_warp2;
        Sigmod::MapWarp* m_warp3;
};

#endif
