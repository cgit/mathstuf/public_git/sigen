/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTRULES
#define SIGMOD_TESTRULES

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class Game;
class Rules;
}

class TestRules : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setGenderAllowed();
        void setBreedingAllowed();
        void setCriticalDomains();
        void setNumBoxes();
        void setBoxSize();
        void setMaxParty();
        void setMaxFight();
        void setMaxPlayers();
        void setMaxHeldItems();
        void setMaxAbilities();
        void setMaxNatures();
        void setMaxMoves();
        void setMaxLevel();
        void setMaxStages();
        void setMaxMoney();
        void setMaxTotalWeight();
        void setSpecialSplit();
        void setSpecialDVSplit();
        void setMaxTotalEV();
        void setMaxEVPerStat();
        
        void assignment();
    private:
        Sigmod::Game* m_game1;
        Sigmod::Game* m_game2;
        Sigmod::Game* m_game3;
        
        Sigmod::Rules* m_rules1;
        Sigmod::Rules* m_rules2;
        Sigmod::Rules* m_rules3;
};

#endif
