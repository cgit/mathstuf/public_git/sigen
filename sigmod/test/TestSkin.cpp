/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSkin.h"

// Sigmod includes
#include "../Game.h"
#include "../Skin.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestSkin::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_skin1 = m_game->newSkin();
    m_skin2 = m_game->newSkin();
    m_skin3 = m_game->newSkin();
}

void TestSkin::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestSkin::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_skin1);
    makeConnections(m_skin2);
    makeConnections(m_skin3);
}

void TestSkin::cleanup()
{
    closeConnections(m_skin1);
    closeConnections(m_skin2);
    closeConnections(m_skin3);
    
    TestSigmodObject::cleanup();
}

void TestSkin::validation()
{
    m_skin1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_skin1->setName("Foo");
    m_skin1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSkin::saving()
{
    QDomDocument xml = Object::xml(m_skin1);
    QFile file("skin.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestSkin::loading()
{
    QDomDocument xml;
    QFile file("skin.xml");
    
    m_skin1->setName("Bar");
    m_skin1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_skin1->load(xml.firstChildElement("Skin"));
    
    QCOMPARE(m_skin1->name(), QString("Foo"));
    QCOMPARE(m_skin1->script(), Script());
}

void TestSkin::setName()
{
    m_skin2->setName("Foo");
    m_skin2->setName("Foo");
    
    QCOMPARE(m_skin2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSkin::setScript()
{
    m_skin2->setScript(Script("python", "import os"));
    m_skin2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_skin2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSkin::assignment()
{
    *m_skin3 = *m_skin2;
    
    QCOMPARE(m_skin3->name(), QString("Foo"));
    QCOMPARE(m_skin3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestSkin)
