/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTMAPTRAINERTEAMMEMBER
#define SIGMOD_TESTMAPTRAINERTEAMMEMBER

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class MapTrainerTeamMember;
}

class TestMapTrainerTeamMember : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setSpecies();
        void setLevel();
        
        void abilities();
        void items();
        void moves();
        void natures();
        
        void assignment();
    private:
        Sigmod::MapTrainerTeamMember* m_teamMember1;
        Sigmod::MapTrainerTeamMember* m_teamMember2;
        Sigmod::MapTrainerTeamMember* m_teamMember3;
};

#endif
