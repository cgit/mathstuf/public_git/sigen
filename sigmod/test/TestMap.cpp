/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMap.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapEffect.h"
#include "../MapTile.h"
#include "../MapTrainer.h"
#include "../MapWarp.h"
#include "../MapWildList.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestMap::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_map1 = m_game->newMap();
    m_map2 = m_game->newMap();
    m_map3 = m_game->newMap();
}

void TestMap::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMap::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_map1);
    makeConnections(m_map2);
    makeConnections(m_map3);
}

void TestMap::cleanup()
{
    closeConnections(m_map1);
    closeConnections(m_map2);
    closeConnections(m_map3);
    
    TestSigmodObject::cleanup();
}

void TestMap::validation()
{
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 4);
    QCOMPARE(m_errors.size(), 2);
    
    m_map1->setName("Foo");
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 8);
    QCOMPARE(m_errors.size(), 3);
    
    m_map1->newEffect();
    
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 11);
    QCOMPARE(m_errors.size(), 4);
    
    m_map1->newTile();
    
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 13);
    QCOMPARE(m_errors.size(), 5);
    
    m_map1->newTrainer();
    
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 14);
    QCOMPARE(m_errors.size(), 6);
    
    m_map1->newWarp();
    
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 15);
    QCOMPARE(m_errors.size(), 6);
    
    m_map1->newWildList();
    
    m_map1->validate();
    
    QCOMPARE(m_warnings.size(), 15);
    QCOMPARE(m_errors.size(), 6);
}

void TestMap::saving()
{
    QDomDocument xml = Object::xml(m_map1);
    QFile file("map.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMap::loading()
{
    QDomDocument xml;
    QFile file("map.xml");
    
    m_map1->newEffect();
    m_map1->newTile();
    m_map1->newTrainer();
    m_map1->newWarp();
    m_map1->newWildList();
    
    m_map1->setName("Bar");
    m_map1->setWidth(5);
    m_map1->setHeight(5);
    m_map1->setIsWorld(true);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_map1->load(xml.firstChildElement("Map"));
    
    QCOMPARE(m_map1->name(), QString("Foo"));
    QCOMPARE(m_map1->width(), 1);
    QCOMPARE(m_map1->height(), 1);
    QCOMPARE(m_map1->isWorld(), false);
    QCOMPARE(m_map1->effectCount(), 1);
    QCOMPARE(m_map1->tileCount(), 1);
    QCOMPARE(m_map1->trainerCount(), 1);
    QCOMPARE(m_map1->warpCount(), 1);
    QCOMPARE(m_map1->wildListCount(), 1);
}

void TestMap::setName()
{
    m_map2->setName("Foo");
    m_map2->setName("Foo");
    
    QCOMPARE(m_map2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::setWidth()
{
    m_map2->setWidth(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_map2->setWidth(5);
    m_map2->setWidth(5);
    
    QCOMPARE(m_map2->width(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMap::setHeight()
{
    m_map2->setHeight(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_map2->setHeight(5);
    m_map2->setHeight(5);
    
    QCOMPARE(m_map2->height(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMap::setIsWorld()
{
    m_map2->setIsWorld(false);
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_map2->setIsWorld(true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::effects()
{
    QCOMPARE(m_map2->newEffect()->id(), 0);
    QCOMPARE(m_map2->newEffect()->id(), 1);
    QCOMPARE(m_map2->newEffect()->id(), 2);
    
    QCOMPARE(m_map2->effectCount(), 3);
    
    m_map2->deleteEffect(0);
    
    QCOMPARE(m_map2->effectCount(), 2);
    
    QCOMPARE(m_map2->newEffect()->id(), 0);
    
    QCOMPARE(m_map2->effectIndex(0), 2);
    
    m_map2->deleteEffectById(1);
    
    QCOMPARE(m_map2->effectIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::tiles()
{
    QCOMPARE(m_map2->newTile()->id(), 0);
    QCOMPARE(m_map2->newTile()->id(), 1);
    QCOMPARE(m_map2->newTile()->id(), 2);
    
    QCOMPARE(m_map2->tileCount(), 3);
    
    m_map2->deleteTile(0);
    
    QCOMPARE(m_map2->tileCount(), 2);
    
    QCOMPARE(m_map2->newTile()->id(), 0);
    
    QCOMPARE(m_map2->tileIndex(0), 2);
    
    m_map2->deleteTileById(1);
    
    QCOMPARE(m_map2->tileIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::trainers()
{
    QCOMPARE(m_map2->newTrainer()->id(), 0);
    QCOMPARE(m_map2->newTrainer()->id(), 1);
    QCOMPARE(m_map2->newTrainer()->id(), 2);
    
    QCOMPARE(m_map2->trainerCount(), 3);
    
    m_map2->deleteTrainer(0);
    
    QCOMPARE(m_map2->trainerCount(), 2);
    
    QCOMPARE(m_map2->newTrainer()->id(), 0);
    
    QCOMPARE(m_map2->trainerIndex(0), 2);
    
    m_map2->deleteTrainerById(1);
    
    QCOMPARE(m_map2->trainerIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::warps()
{
    QCOMPARE(m_map2->newWarp()->id(), 0);
    QCOMPARE(m_map2->newWarp()->id(), 1);
    QCOMPARE(m_map2->newWarp()->id(), 2);
    
    QCOMPARE(m_map2->warpCount(), 3);
    
    m_map2->deleteWarp(0);
    
    QCOMPARE(m_map2->warpCount(), 2);
    
    QCOMPARE(m_map2->newWarp()->id(), 0);
    
    QCOMPARE(m_map2->warpIndex(0), 2);
    
    m_map2->deleteWarpById(1);
    
    QCOMPARE(m_map2->warpIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::wildLists()
{
    QCOMPARE(m_map2->newWildList()->id(), 0);
    QCOMPARE(m_map2->newWildList()->id(), 1);
    QCOMPARE(m_map2->newWildList()->id(), 2);
    
    QCOMPARE(m_map2->wildListCount(), 3);
    
    m_map2->deleteWildList(0);
    
    QCOMPARE(m_map2->wildListCount(), 2);
    
    QCOMPARE(m_map2->newWildList()->id(), 0);
    
    QCOMPARE(m_map2->wildListIndex(0), 2);
    
    m_map2->deleteWildListById(1);
    
    QCOMPARE(m_map2->wildListIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMap::assignment()
{
    *m_map3 = *m_map2;
    
    QCOMPARE(m_map3->name(), QString("Foo"));
    QCOMPARE(m_map3->width(), 5);
    QCOMPARE(m_map3->height(), 5);
    QCOMPARE(m_map3->isWorld(), true);
    QCOMPARE(m_map3->effectCount(), 2);
    QCOMPARE(m_map3->tileCount(), 2);
    QCOMPARE(m_map3->trainerCount(), 2);
    QCOMPARE(m_map3->warpCount(), 2);
    QCOMPARE(m_map3->wildListCount(), 2);
}

QTEST_APPLESS_MAIN(TestMap)
