/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSpeciesMove.h"

// Sigmod includes
#include "../Game.h"
#include "../Rules.h"
#include "../Species.h"
#include "../SpeciesMove.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestSpeciesMove::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Species* species = m_game->newSpecies();
    
    m_speciesMove1 = species->newMove();
    m_speciesMove2 = species->newMove();
    m_speciesMove3 = species->newMove();
}

void TestSpeciesMove::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestSpeciesMove::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_speciesMove1);
    makeConnections(m_speciesMove2);
    makeConnections(m_speciesMove3);
}

void TestSpeciesMove::cleanup()
{
    closeConnections(m_speciesMove1);
    closeConnections(m_speciesMove2);
    closeConnections(m_speciesMove3);
    
    TestSigmodObject::cleanup();
}

void TestSpeciesMove::validation()
{
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newMove();
    
    m_speciesMove1->setMove(0);
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxLevel(25);
    
    m_speciesMove1->setLevel(15);
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxLevel(10);
    
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setMaxLevel(50);
    
    m_speciesMove1->setWild(40);
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setMaxLevel(30);
    
    m_speciesMove1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestSpeciesMove::saving()
{
    QDomDocument xml = Object::xml(m_speciesMove1);
    QFile file("speciesMove.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestSpeciesMove::loading()
{
    QDomDocument xml;
    QFile file("speciesMove.xml");
    
    m_game->newMove();
    
    m_speciesMove1->setMove(1);
    m_speciesMove1->setLevel(5);
    m_speciesMove1->setWild(5);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_speciesMove1->load(xml.firstChildElement("SpeciesMove"));
    
    QCOMPARE(m_speciesMove1->move(), 0);
    QCOMPARE(m_speciesMove1->level(), 15);
    QCOMPARE(m_speciesMove1->wild(), 40);
}

void TestSpeciesMove::setMove()
{
    m_speciesMove2->setMove(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newMove();
    
    m_speciesMove2->setMove(2);
    m_speciesMove2->setMove(2);
    
    QCOMPARE(m_speciesMove2->move(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestSpeciesMove::setLevel()
{
    m_speciesMove2->setLevel(50);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_speciesMove2->setLevel(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_speciesMove2->setLevel(15);
    m_speciesMove2->setLevel(15);
    
    QCOMPARE(m_speciesMove2->level(), 15);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpeciesMove::setWild()
{
    m_speciesMove2->setWild(50);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_speciesMove2->setWild(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_speciesMove2->setWild(15);
    m_speciesMove2->setWild(15);
    
    QCOMPARE(m_speciesMove2->wild(), 15);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestSpeciesMove::assignment()
{
    *m_speciesMove3 = *m_speciesMove2;
    
    QCOMPARE(m_speciesMove3->move(), 2);
    QCOMPARE(m_speciesMove3->level(), 15);
    QCOMPARE(m_speciesMove3->wild(), 15);
}

QTEST_APPLESS_MAIN(TestSpeciesMove)
