/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestItemType.h"

// Sigmod includes
#include "../Game.h"
#include "../ItemType.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestItemType::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_itemType1 = m_game->newItemType();
    m_itemType2 = m_game->newItemType();
    m_itemType3 = m_game->newItemType();
    
    m_itemType1->setCount(ItemType::Total);
}

void TestItemType::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestItemType::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_itemType1);
    makeConnections(m_itemType2);
    makeConnections(m_itemType3);
}

void TestItemType::cleanup()
{
    closeConnections(m_itemType1);
    closeConnections(m_itemType2);
    closeConnections(m_itemType3);
    
    TestSigmodObject::cleanup();
}

void TestItemType::validation()
{
    m_itemType1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_itemType1->setName("Foo");
    m_itemType1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxTotalWeight(100);
    
    m_itemType1->setMaxWeight(50);
    m_itemType1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxTotalWeight(25);
    
    m_itemType1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setMaxTotalWeight(100);
    
    m_itemType1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestItemType::saving()
{
    QDomDocument xml = Object::xml(m_itemType1);
    QFile file("itemType.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestItemType::loading()
{
    QDomDocument xml;
    QFile file("itemType.xml");
    
    m_itemType1->setName("Bar");
    m_itemType1->setComputer(10);
    m_itemType1->setPlayer(25);
    m_itemType1->setMaxWeight(75);
    m_itemType1->setCount(ItemType::Distinct);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_itemType1->load(xml.firstChildElement("ItemType"));
    
    QCOMPARE(m_itemType1->name(), QString("Foo"));
    QCOMPARE(m_itemType1->computer(), 0);
    QCOMPARE(m_itemType1->player(), 1);
    QCOMPARE(m_itemType1->maxWeight(), 50);
    QCOMPARE(m_itemType1->count(), ItemType::Total);
}

void TestItemType::setName()
{
    m_itemType2->setName("Foo");
    m_itemType2->setName("Foo");
    
    QCOMPARE(m_itemType2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestItemType::setComputer()
{
    m_itemType2->setComputer(-5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_itemType2->setComputer(10);
    m_itemType2->setComputer(10);
    
    QCOMPARE(m_itemType2->computer(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestItemType::setPlayer()
{
    m_itemType2->setPlayer(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_itemType2->setPlayer(5);
    m_itemType2->setPlayer(5);
    
    QCOMPARE(m_itemType2->player(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestItemType::setMaxWeight()
{
    m_itemType2->setMaxWeight(1000);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_itemType2->setMaxWeight(25);
    m_itemType2->setMaxWeight(25);
    
    QCOMPARE(m_itemType2->maxWeight(), 25);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestItemType::setCount()
{
    m_itemType2->setCount(ItemType::Total);
    m_itemType2->setCount(ItemType::Total);
    
    QCOMPARE(m_itemType2->count(), ItemType::Total);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestItemType::assignment()
{
    *m_itemType3 = *m_itemType2;
    
    QCOMPARE(m_itemType3->name(), QString("Foo"));
    QCOMPARE(m_itemType3->computer(), 10);
    QCOMPARE(m_itemType3->player(), 5);
    QCOMPARE(m_itemType3->maxWeight(), 25);
    QCOMPARE(m_itemType3->count(), ItemType::Total);
}

QTEST_APPLESS_MAIN(TestItemType)
