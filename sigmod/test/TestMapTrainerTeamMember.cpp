/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapTrainerTeamMember.h"

// Sigmod includes
#include "../Game.h"
#include "../Item.h"
#include "../Map.h"
#include "../MapTrainer.h"
#include "../MapTrainerTeamMember.h"
#include "../Rules.h"
#include "../Species.h"
#include "../SpeciesMove.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestMapTrainerTeamMember::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    MapTrainer* trainer = m_game->newMap()->newTrainer();
    
    m_teamMember1 = trainer->newTeamMember();
    m_teamMember2 = trainer->newTeamMember();
    m_teamMember3 = trainer->newTeamMember();
}

void TestMapTrainerTeamMember::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapTrainerTeamMember::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_teamMember1);
    makeConnections(m_teamMember2);
    makeConnections(m_teamMember3);
}

void TestMapTrainerTeamMember::cleanup()
{
    closeConnections(m_teamMember1);
    closeConnections(m_teamMember2);
    closeConnections(m_teamMember3);
    
    TestSigmodObject::cleanup();
}

void TestMapTrainerTeamMember::validation()
{
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->newSpecies();
    
    m_teamMember1->setSpecies(0);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_game->rules()->setMaxLevel(100);
    
    m_teamMember1->setLevel(50);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_game->rules()->setMaxLevel(40);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_teamMember1->setLevel(30);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_game->rules()->setMaxAbilities(1);
    m_game->newAbility();
    
    m_teamMember1->setAbility(0, true);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_game->rules()->setMaxAbilities(0);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->rules()->setMaxAbilities(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->rules()->setMaxHeldItems(1);
    m_game->species(0)->setMaxHoldWeight(10);
    m_game->newItem();
    m_game->item(0)->setWeight(5);
    
    m_teamMember1->setItem(0, 1);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->rules()->setMaxHeldItems(0);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->rules()->setMaxHeldItems(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->item(0)->setWeight(25);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_game->item(0)->setWeight(5);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_game->rules()->setMaxMoves(2);
    m_game->newMove();
    m_game->newMove();
    m_game->species(0)->newMove()->setMove(0);
    m_game->species(0)->newMove()->setMove(1);
    
    m_teamMember1->setMove(0, true);
    m_teamMember1->setMove(1, true);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_game->rules()->setMaxMoves(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->rules()->setMaxMoves(2);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->species(0)->deleteMove(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 10);
    
    m_game->species(0)->newMove()->setMove(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 10);
    
    m_game->rules()->setMaxNatures(1);
    m_game->newNature();
    
    m_teamMember1->setNature(0, true);
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 10);
    
    m_game->rules()->setMaxNatures(0);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 11);
    
    m_game->rules()->setMaxNatures(1);
    
    m_teamMember1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 11);
}

void TestMapTrainerTeamMember::saving()
{
    QDomDocument xml = Object::xml(m_teamMember1);
    QFile file("mapTrainerTeamMember.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapTrainerTeamMember::loading()
{
    QDomDocument xml;
    QFile file("mapTrainerTeamMember.xml");
    
    m_game->newAbility();
    m_game->newItem();
    m_game->newMove();
    m_game->newNature();
    m_game->newSpecies();
    
    m_teamMember1->setSpecies(1);
    m_teamMember1->setLevel(25);
    m_teamMember1->setAbility(0, false);
    m_teamMember1->setAbility(1, true);
    m_teamMember1->setItem(0, 0);
    m_teamMember1->setItem(1, 1);
    m_teamMember1->setMove(0, false);
    m_teamMember1->setMove(1, false);
    m_teamMember1->setMove(2, true);
    m_teamMember1->setNature(0, false);
    m_teamMember1->setNature(1, true);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_teamMember1->load(xml.firstChildElement("MapTrainerTeamMember"));
    
    QCOMPARE(m_teamMember1->species(), 0);
    QCOMPARE(m_teamMember1->level(), 30);
    QCOMPARE(m_teamMember1->ability(0), true);
    QCOMPARE(m_teamMember1->ability(1), false);
    QCOMPARE(m_teamMember1->item(0), 1);
    QCOMPARE(m_teamMember1->item(1), 0);
    QCOMPARE(m_teamMember1->move(0), true);
    QCOMPARE(m_teamMember1->move(1), true);
    QCOMPARE(m_teamMember1->move(2), false);
    QCOMPARE(m_teamMember1->nature(0), true);
    QCOMPARE(m_teamMember1->nature(1), false);
}

void TestMapTrainerTeamMember::setSpecies()
{
    m_teamMember2->setSpecies(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSpecies();
    
    m_teamMember2->setSpecies(2);
    m_teamMember2->setSpecies(2);
    
    QCOMPARE(m_teamMember2->species(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTrainerTeamMember::setLevel()
{
    m_teamMember2->setLevel(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setLevel(50);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_teamMember2->setLevel(30);
    m_teamMember2->setLevel(30);
    
    QCOMPARE(m_teamMember2->level(), 30);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTrainerTeamMember::abilities()
{
    m_game->newAbility();
    
    m_teamMember2->setAbility(0, true);
    m_teamMember2->setAbility(0, true);
    
    QCOMPARE(m_teamMember2->ability(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_teamMember2->setAbility(1, true);
    
    QCOMPARE(m_teamMember2->ability(1), false);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxAbilities(2);
    
    m_teamMember2->setAbility(1, true);
    
    QCOMPARE(m_teamMember2->ability(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setAbility(1, false);
    
    QCOMPARE(m_teamMember2->ability(1), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setAbility(2, false);
    
    QCOMPARE(m_teamMember2->ability(2), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTrainerTeamMember::items()
{
    m_game->species(2)->setMaxHoldWeight(9);
    m_game->rules()->setMaxHeldItems(2);
    m_game->newItem();
    
    m_teamMember2->setItem(0, 1);
    m_teamMember2->setItem(0, 1);
    
    QCOMPARE(m_teamMember2->item(0), 1);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_teamMember2->setItem(0, 2);
    
    QCOMPARE(m_teamMember2->item(0), 1);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setItem(1, 1);
    
    QCOMPARE(m_teamMember2->item(1), 1);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setItem(1, 2);
    
    QCOMPARE(m_teamMember2->item(1), 1);
        
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_teamMember2->setItem(1, 0);
    
    QCOMPARE(m_teamMember2->item(1), 0);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_teamMember2->setItem(2, 0);
    
    QCOMPARE(m_teamMember2->item(2), 0);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTrainerTeamMember::moves()
{
    m_game->species(2)->newMove()->setMove(0);
    m_game->species(2)->newMove()->setMove(1);
    m_game->rules()->setMaxMoves(1);
    
    m_teamMember2->setMove(0, true);
    m_teamMember2->setMove(0, true);
    
    QCOMPARE(m_teamMember2->move(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_teamMember2->setMove(1, true);
    
    QCOMPARE(m_teamMember2->move(1), false);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxMoves(3);
    
    m_teamMember2->setMove(1, true);
    
    QCOMPARE(m_teamMember2->move(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setMove(2, true);
    
    QCOMPARE(m_teamMember2->move(2), false);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->species(2)->newMove()->setMove(2);
    
    m_teamMember2->setMove(2, true);
    
    QCOMPARE(m_teamMember2->move(2), true);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_teamMember2->setMove(1, false);
    
    QCOMPARE(m_teamMember2->move(1), false);
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_teamMember2->setMove(2, false);
    
    QCOMPARE(m_teamMember2->move(2), false);
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTrainerTeamMember::natures()
{
    m_game->newNature();
    
    m_teamMember2->setNature(0, true);
    m_teamMember2->setNature(0, true);
    
    QCOMPARE(m_teamMember2->nature(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_teamMember2->setNature(1, true);
    
    QCOMPARE(m_teamMember2->nature(1), false);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxNatures(2);
    
    m_teamMember2->setNature(1, true);
    
    QCOMPARE(m_teamMember2->nature(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setNature(1, false);
    
    QCOMPARE(m_teamMember2->nature(1), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_teamMember2->setNature(2, false);
    
    QCOMPARE(m_teamMember2->nature(2), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTrainerTeamMember::assignment()
{
    *m_teamMember3 = *m_teamMember2;
    
    QCOMPARE(m_teamMember3->species(), 2);
    QCOMPARE(m_teamMember3->level(), 30);
    QCOMPARE(m_teamMember3->ability(0), true);
    QCOMPARE(m_teamMember3->ability(1), false);
    QCOMPARE(m_teamMember3->ability(2), false);
    QCOMPARE(m_teamMember3->item(0), 1);
    QCOMPARE(m_teamMember3->item(1), 0);
    QCOMPARE(m_teamMember3->item(2), 0);
    QCOMPARE(m_teamMember3->move(0), true);
    QCOMPARE(m_teamMember3->move(1), false);
    QCOMPARE(m_teamMember3->move(2), false);
    QCOMPARE(m_teamMember3->nature(0), true);
    QCOMPARE(m_teamMember3->nature(1), false);
    QCOMPARE(m_teamMember3->nature(2), false);
}

QTEST_APPLESS_MAIN(TestMapTrainerTeamMember)
