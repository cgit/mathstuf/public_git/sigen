set(sigmodtest_SRCS
    TestSigmodObject.cpp
)

kde4_add_library(sigmodtest
    SHARED
    ${sigmodtest_SRCS}
)
set_property(
    TARGET
        sigmodtest
    PROPERTY
        EXCLUDE_FROM_ALL TRUE
)
target_link_libraries(sigmodtest
    ${QT_QTCORE_LIBRARY}
    sigmod
)
target_link_libraries(sigmodtest LINK_INTERFACE_LIBRARIES
    ${QT_QTCORE_LIBRARY}
    sigmod
)

set(libraries
    sigmod
    sigmodtest
)

make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Ability)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Author)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Badge)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries CoinList)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries CoinListItem)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries EggGroup)
# make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Game)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries GlobalScript)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Item)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries ItemType)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Map)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapEffect)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapTile)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapTrainer)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapTrainerTeamMember)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapWarp)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapWildList)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries MapWildListEncounter)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Move)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Nature)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Rules)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Skin)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Sound)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Species)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries SpeciesMove)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Sprite)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Status)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Store)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Tile)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Time)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Trainer)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Type)
make_test(sigmod-tests sigmod-vtests sigmod-ctests libraries Weather)

make_test_group(sigmod sigmod-tests)
make_valgrind_group(sigmod sigmod-vtests)
make_callgrind_group(sigmod sigmod-ctests)
