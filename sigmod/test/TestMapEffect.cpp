/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapEffect.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapEffect.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestMapEffect::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Map* map = m_game->newMap();
    
    m_effect1 = map->newEffect();
    m_effect2 = map->newEffect();
    m_effect3 = map->newEffect();
}

void TestMapEffect::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapEffect::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_effect1);
    makeConnections(m_effect2);
    makeConnections(m_effect3);
}

void TestMapEffect::cleanup()
{
    closeConnections(m_effect1);
    closeConnections(m_effect2);
    closeConnections(m_effect3);
    
    TestSigmodObject::cleanup();
}

void TestMapEffect::validation()
{
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_effect1->setName("Foo");
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_game->map(0)->setWidth(25);
    m_game->map(0)->setHeight(25);
    
    m_effect1->setPosition(QPoint(20, 20));
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_game->map(0)->setWidth(20);
    m_game->map(0)->setHeight(20);
    
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_game->map(0)->setWidth(25);
    m_game->map(0)->setHeight(25);
    
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->newSkin();
    
    m_effect1->setSkin(0);
    m_effect1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
}

void TestMapEffect::saving()
{
    QDomDocument xml = Object::xml(m_effect1);
    QFile file("mapEffect.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapEffect::loading()
{
    QDomDocument xml;
    QFile file("mapEffect.xml");
    
    m_game->newSkin();
    
    m_effect1->setName("Bar");
    m_effect1->setPosition(QPoint(3, 3));
    m_effect1->setSkin(1);
    m_effect1->setIsGhost(true);
    m_effect1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_effect1->load(xml.firstChildElement("MapEffect"));
    
    QCOMPARE(m_effect1->name(), QString("Foo"));
    QCOMPARE(m_effect1->position(), QPoint(20, 20));
    QCOMPARE(m_effect1->skin(), 0);
    QCOMPARE(m_effect1->isGhost(), false);
    QCOMPARE(m_effect1->script(), Script());
}

void TestMapEffect::setName()
{
    m_effect2->setName("Foo");
    m_effect2->setName("Foo");
    
    QCOMPARE(m_effect2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapEffect::setPosition()
{
    m_effect2->setPosition(QPoint(20, 30));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_effect2->setPosition(QPoint(30, 20));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_effect2->setPosition(QPoint(20, 20));
    m_effect2->setPosition(QPoint(20, 20));
    
    QCOMPARE(m_effect2->position(), QPoint(20, 20));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapEffect::setSkin()
{
    m_effect2->setSkin(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSkin();
    
    m_effect2->setSkin(2);
    m_effect2->setSkin(2);
    
    QCOMPARE(m_effect2->skin(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapEffect::setIsGhost()
{
    m_effect2->setIsGhost(true);
    m_effect2->setIsGhost(true);
    
    QCOMPARE(m_effect2->isGhost(), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapEffect::setScript()
{
    m_effect2->setScript(Script("python", "import os"));
    m_effect2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_effect2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapEffect::assignment()
{
    *m_effect3 = *m_effect2;
    
    QCOMPARE(m_effect3->name(), QString("Foo"));
    QCOMPARE(m_effect3->position(), QPoint(20, 20));
    QCOMPARE(m_effect3->skin(), 2);
    QCOMPARE(m_effect3->isGhost(), true);
    QCOMPARE(m_effect3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestMapEffect)
