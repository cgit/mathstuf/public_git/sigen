/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapTile.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapTile.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestMapTile::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Map* map = m_game->newMap();
    
    m_mapTile1 = map->newTile();
    m_mapTile2 = map->newTile();
    m_mapTile3 = map->newTile();
}

void TestMapTile::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapTile::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_mapTile1);
    makeConnections(m_mapTile2);
    makeConnections(m_mapTile3);
}

void TestMapTile::cleanup()
{
    closeConnections(m_mapTile1);
    closeConnections(m_mapTile2);
    closeConnections(m_mapTile3);
    
    TestSigmodObject::cleanup();
}

void TestMapTile::validation()
{
    m_mapTile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newTile();
    
    m_mapTile1->setTile(0);
    m_mapTile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->map(0)->setHeight(20);
    m_game->map(0)->setWidth(20);
    
    m_mapTile1->setPosition(QPoint(10, 10));
    m_mapTile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->map(0)->setHeight(5);
    m_game->map(0)->setWidth(5);
    
    m_mapTile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTile::saving()
{
    QDomDocument xml = Object::xml(m_mapTile1);
    QFile file("mapTile.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapTile::loading()
{
    QDomDocument xml;
    QFile file("mapTile.xml");
    
    m_game->newTile();
    
    m_mapTile1->setTile(1);
    m_mapTile1->setPosition(QPoint(2, 2));
    m_mapTile1->setZIndex(5);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_mapTile1->load(xml.firstChildElement("MapTile"));
    
    QCOMPARE(m_mapTile1->tile(), 0);
    QCOMPARE(m_mapTile1->position(), QPoint(10, 10));
    QCOMPARE(m_mapTile1->zIndex(), 0);
}

void TestMapTile::setTile()
{
    m_mapTile2->setTile(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newTile();
    
    m_mapTile2->setTile(2);
    m_mapTile2->setTile(2);
    
    QCOMPARE(m_mapTile2->tile(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTile::setPosition()
{
    m_mapTile2->setPosition(QPoint(5, 25));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_mapTile2->setPosition(QPoint(25, 5));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->map(0)->setHeight(50);
    m_game->map(0)->setWidth(50);
    
    m_mapTile2->setPosition(QPoint(25, 25));
    m_mapTile2->setPosition(QPoint(25, 25));
    
    QCOMPARE(m_mapTile2->position(), QPoint(25, 25));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTile::setZIndex()
{
    m_mapTile2->setZIndex(5);
    m_mapTile2->setZIndex(5);
    
    QCOMPARE(m_mapTile2->zIndex(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapTile::assignment()
{
    *m_mapTile3 = *m_mapTile2;
    
    QCOMPARE(m_mapTile3->tile(), 2);
    QCOMPARE(m_mapTile3->position(), QPoint(25, 25));
    QCOMPARE(m_mapTile3->zIndex(), 5);
}

QTEST_APPLESS_MAIN(TestMapTile)
