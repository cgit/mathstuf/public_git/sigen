/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestAuthor.h"

// Sigmod includes
#include "../Author.h"
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestAuthor::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_author1 = m_game->newAuthor();
    m_author2 = m_game->newAuthor();
    m_author3 = m_game->newAuthor();
}

void TestAuthor::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestAuthor::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_author1);
    makeConnections(m_author2);
    makeConnections(m_author3);
}

void TestAuthor::cleanup()
{
    closeConnections(m_author1);
    closeConnections(m_author2);
    closeConnections(m_author3);
    
    TestSigmodObject::cleanup();
}

void TestAuthor::validation()
{
    m_author1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 2);
    
    m_author1->setName("Sally");
    m_author1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 3);
    
    m_author1->setEmail("Bar");
    m_author1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 5);
    
    m_author1->setEmail("foo@bar.baz");
    m_author1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 6);
    
    m_author1->setRole("Artist");
    m_author1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 6);
}

void TestAuthor::saving()
{
    QDomDocument xml = Object::xml(m_author1);
    QFile file("author.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestAuthor::loading()
{
    QDomDocument xml;
    QFile file("author.xml");
    
    m_author1->setName("Bob");
    m_author1->setEmail("nobody@example.org");
    m_author1->setName("Author");
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_author1->load(xml.firstChildElement("Author"));
    
    QCOMPARE(m_author1->name(), QString("Sally"));
    QCOMPARE(m_author1->email(), QString("foo@bar.baz"));
    QCOMPARE(m_author1->role(), QString("Artist"));
}

void TestAuthor::setName()
{
    m_author2->setName("Bob");
    m_author2->setName("Bob");
    
    QCOMPARE(m_author2->name(), QString("Bob"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAuthor::setEmail()
{
    m_author2->setEmail("nobody@example.org");
    m_author2->setEmail("nobody@example.org");
    
    QCOMPARE(m_author2->email(), QString("nobody@example.org"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAuthor::setRole()
{
    m_author2->setRole("Author");
    m_author2->setRole("Author");
    
    QCOMPARE(m_author2->role(), QString("Author"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAuthor::assignment()
{
    *m_author3 = *m_author2;
    
    QCOMPARE(m_author3->name(), QString("Bob"));
    QCOMPARE(m_author3->email(), QString("nobody@example.org"));
    QCOMPARE(m_author3->role(), QString("Author"));
}

QTEST_APPLESS_MAIN(TestAuthor)
