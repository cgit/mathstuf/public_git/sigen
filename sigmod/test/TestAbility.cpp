/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestAbility.h"

// Sigmod includes
#include "../Ability.h"
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestAbility::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_ability1 = m_game->newAbility();
    m_ability2 = m_game->newAbility();
    m_ability3 = m_game->newAbility();
}

void TestAbility::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestAbility::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_ability1);
    makeConnections(m_ability2);
    makeConnections(m_ability3);
}

void TestAbility::cleanup()
{
    closeConnections(m_ability1);
    closeConnections(m_ability2);
    closeConnections(m_ability3);
    
    TestSigmodObject::cleanup();
}

void TestAbility::validation()
{
    m_ability1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_ability1->setName("Foo");
    m_ability1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestAbility::saving()
{
    QDomDocument xml = Object::xml(m_ability1);
    QFile file("ability.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestAbility::loading()
{
    QDomDocument xml;
    QFile file("ability.xml");
    
    m_ability1->setName("Bar");
    m_ability1->setPriority(4);
    m_ability1->setDescription("Brief description.");
    m_ability1->setBattleScript(Script("python", "import os"));
    m_ability1->setWorldScript(Script("python", "import os"));
    m_ability1->setPriorityScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_ability1->load(xml.firstChildElement("Ability"));
    
    QCOMPARE(m_ability1->name(), QString("Foo"));
    QCOMPARE(m_ability1->priority(), 0);
    QCOMPARE(m_ability1->description(), QString(""));
    QCOMPARE(m_ability1->battleScript(), Script());
    QCOMPARE(m_ability1->worldScript(), Script());
    QCOMPARE(m_ability1->priorityScript(), Script());
}

void TestAbility::setName()
{
    m_ability2->setName("Foo");
    m_ability2->setName("Foo");
    
    QCOMPARE(m_ability2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::setPriority()
{
    m_ability2->setPriority(4);
    m_ability2->setPriority(4);
    
    QCOMPARE(m_ability2->priority(), 4);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::setDescription()
{
    m_ability2->setDescription("Brief description.");
    m_ability2->setDescription("Brief description.");
    
    QCOMPARE(m_ability2->description(), QString("Brief description."));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::setBattleScript()
{
    QCOMPARE(m_ability2->battleScript(), Script());
    
    m_ability2->setBattleScript(Script("python", "import os"));
    m_ability2->setBattleScript(Script("python", "import os"));
    
    QCOMPARE(m_ability2->battleScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::setWorldScript()
{
    QCOMPARE(m_ability2->worldScript(), Script());
    
    m_ability2->setWorldScript(Script("python", "import os"));
    m_ability2->setWorldScript(Script("python", "import os"));
    
    QCOMPARE(m_ability2->worldScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::setPriorityScript()
{
    QCOMPARE(m_ability2->priorityScript(), Script());
    
    m_ability2->setPriorityScript(Script("python", "import os"));
    m_ability2->setPriorityScript(Script("python", "import os"));
    
    QCOMPARE(m_ability2->priorityScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestAbility::assignment()
{
    *m_ability3 = *m_ability2;
    
    QCOMPARE(m_ability3->name(), QString("Foo"));
    QCOMPARE(m_ability3->priority(), 4);
    QCOMPARE(m_ability3->description(), QString("Brief description."));
    QCOMPARE(m_ability3->battleScript(), Script("python", "import os"));
    QCOMPARE(m_ability3->worldScript(), Script("python", "import os"));
    QCOMPARE(m_ability3->priorityScript(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestAbility)
