/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestTile.h"

// Sigmod includes
#include "../Game.h"
#include "../Tile.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestTile::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_tile1 = m_game->newTile();
    m_tile2 = m_game->newTile();
    m_tile3 = m_game->newTile();
}

void TestTile::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestTile::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_tile1);
    makeConnections(m_tile2);
    makeConnections(m_tile3);
}

void TestTile::cleanup()
{
    closeConnections(m_tile1);
    closeConnections(m_tile2);
    closeConnections(m_tile3);
    
    TestSigmodObject::cleanup();
}

void TestTile::validation()
{
    m_tile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_tile1->setName("Foo");
    m_tile1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestTile::saving()
{
    QDomDocument xml = Object::xml(m_tile1);
    QFile file("tile.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestTile::loading()
{
    QDomDocument xml;
    QFile file("tile.xml");
    
    m_tile1->setName("Bar");
    m_tile1->setWalkable(false);
    m_tile1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_tile1->load(xml.firstChildElement("Tile"));
    
    QCOMPARE(m_tile1->name(), QString("Foo"));
    QCOMPARE(m_tile1->walkable(), true);
    QCOMPARE(m_tile1->script(), Script());
}

void TestTile::setName()
{
    m_tile2->setName("Foo");
    m_tile2->setName("Foo");
    
    QCOMPARE(m_tile2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTile::setWalkable()
{
    m_tile2->setWalkable(false);
    m_tile2->setWalkable(false);
    
    QCOMPARE(m_tile2->walkable(), false);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTile::setScript()
{
    m_tile2->setScript(Script("python", "import os"));
    m_tile2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_tile2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTile::assignment()
{
    *m_tile3 = *m_tile2;
    
    QCOMPARE(m_tile3->name(), QString("Foo"));
    QCOMPARE(m_tile3->walkable(), false);
    QCOMPARE(m_tile3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestTile)
