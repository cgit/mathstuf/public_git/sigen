/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestGlobalScript.h"

// Sigmod includes
#include "../Game.h"
#include "../GlobalScript.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestGlobalScript::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_globalScript1 = m_game->newGlobalScript();
    m_globalScript2 = m_game->newGlobalScript();
    m_globalScript3 = m_game->newGlobalScript();
}

void TestGlobalScript::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestGlobalScript::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_globalScript1);
    makeConnections(m_globalScript2);
    makeConnections(m_globalScript3);
}

void TestGlobalScript::cleanup()
{
    closeConnections(m_globalScript1);
    closeConnections(m_globalScript2);
    closeConnections(m_globalScript3);
    
    TestSigmodObject::cleanup();
}

void TestGlobalScript::validation()
{
    m_globalScript1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_globalScript1->setName("Foo");
    m_globalScript1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestGlobalScript::saving()
{
    QDomDocument xml = Object::xml(m_globalScript1);
    QFile file("globalScript.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestGlobalScript::loading()
{
    QDomDocument xml;
    QFile file("globalScript.xml");
    
    m_globalScript1->setName("Bar");
    m_globalScript1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_globalScript1->load(xml.firstChildElement("GlobalScript"));
    
    QCOMPARE(m_globalScript1->name(), QString("Foo"));
    QCOMPARE(m_globalScript1->script(), Script());
}

void TestGlobalScript::setName()
{
    m_globalScript2->setName("Foo");
    m_globalScript2->setName("Foo");
    
    QCOMPARE(m_globalScript2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestGlobalScript::setScript()
{
    m_globalScript2->setScript(Script("python", "import os"));
    m_globalScript2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_globalScript2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestGlobalScript::assignment()
{
    *m_globalScript3 = *m_globalScript2;
    
    QCOMPARE(m_globalScript3->name(), QString("Foo"));
    QCOMPARE(m_globalScript3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestGlobalScript)
