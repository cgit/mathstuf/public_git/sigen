/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestCoinListItem.h"

// Sigmod includes
#include "../CoinList.h"
#include "../CoinListItem.h"
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestCoinListItem::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    CoinList* coinList = m_game->newCoinList();
    
    m_item1 = coinList->newItem();
    m_item2 = coinList->newItem();
    m_item3 = coinList->newItem();
}

void TestCoinListItem::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestCoinListItem::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_item1);
    makeConnections(m_item2);
    makeConnections(m_item3);
}

void TestCoinListItem::cleanup()
{
    closeConnections(m_item1);
    closeConnections(m_item2);
    closeConnections(m_item3);
    
    TestSigmodObject::cleanup();
}

void TestCoinListItem::validation()
{
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->newItem();
    
    m_item1->setObject(0);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_item1->setType(CoinListItem::Species);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->newSpecies();
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_item1->setCost(20);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
}

void TestCoinListItem::saving()
{
    QDomDocument xml = Object::xml(m_item1);
    QFile file("coinListItem.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestCoinListItem::loading()
{
    QDomDocument xml;
    QFile file("coinListItem.xml");
    
    m_game->newItem();
    
    m_item1->setType(CoinListItem::Item);
    m_item1->setObject(1);
    m_item1->setCost(25);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_item1->load(xml.firstChildElement("CoinListItem"));
    
    QCOMPARE(m_item1->type(), CoinListItem::Species);
    QCOMPARE(m_item1->object(), 0);
    QCOMPARE(m_item1->cost(), 20);
}

void TestCoinListItem::setType()
{
    m_item2->setType(CoinListItem::Species);
    m_item2->setType(CoinListItem::Species);
    
    QCOMPARE(m_item2->type(), CoinListItem::Species);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinListItem::setObject()
{
    m_item2->setObject(0);
    m_item2->setObject(0);
    
    QCOMPARE(m_item2->object(), 0);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinListItem::setCost()
{
    m_item2->setCost(15);
    m_item2->setCost(15);
    
    QCOMPARE(m_item2->cost(), 15);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinListItem::assignment()
{
    *m_item3 = *m_item2;
    
    QCOMPARE(m_item3->type(), CoinListItem::Species);
    QCOMPARE(m_item3->object(), 0);
    QCOMPARE(m_item3->cost(), 15);
}

QTEST_APPLESS_MAIN(TestCoinListItem)
