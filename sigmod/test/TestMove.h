/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTMOVE
#define SIGMOD_TESTMOVE

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class Move;
}

class TestMove : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setName();
        void setAccuracy();
        void setPower();
        void setType();
        void setSpecial();
        void setPowerPoints();
        void setPriority();
        void setDescription();
        void setBattleScript();
        void setWorldScript();
        void setPriorityScript();
        
        void assignment();
    private:
        Sigmod::Move* m_move1;
        Sigmod::Move* m_move2;
        Sigmod::Move* m_move3;
};

#endif
