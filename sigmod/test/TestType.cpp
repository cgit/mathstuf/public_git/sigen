/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestType.h"

// Sigmod includes
#include "../Game.h"
#include "../Type.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestType::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_type1 = m_game->newType();
    m_type2 = m_game->newType();
    m_type3 = m_game->newType();
}

void TestType::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestType::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_type1);
    makeConnections(m_type2);
    makeConnections(m_type3);
}

void TestType::cleanup()
{
    closeConnections(m_type1);
    closeConnections(m_type2);
    closeConnections(m_type3);
    
    TestSigmodObject::cleanup();
}

void TestType::validation()
{
    m_type1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_type1->setName("Foo");
    m_type1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestType::saving()
{
    QDomDocument xml = Object::xml(m_type1);
    QFile file("type.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestType::loading()
{
    QDomDocument xml;
    QFile file("type.xml");
    
    m_type1->setName("Bar");
    m_type1->setStab(Fraction(2, 1));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_type1->load(xml.firstChildElement("Type"));
    
    QCOMPARE(m_type1->name(), QString("Foo"));
    QCOMPARE(m_type1->stab(), Fraction(1, 1));
}

void TestType::setName()
{
    m_type2->setName("Foo");
    m_type2->setName("Foo");
    
    QCOMPARE(m_type2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestType::setStab()
{
    m_type2->setStab(Fraction(0, 1));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_type2->setStab(Fraction(2, 1));
    m_type2->setStab(Fraction(2, 1));
    
    QCOMPARE(m_type2->stab(), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestType::assignment()
{
    *m_type3 = *m_type2;
    
    QCOMPARE(m_type3->name(), QString("Foo"));
    QCOMPARE(m_type3->stab(), Fraction(2, 1));
}

QTEST_APPLESS_MAIN(TestType)
