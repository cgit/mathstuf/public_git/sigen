/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapTrainer.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapTrainer.h"
#include "../MapTrainerTeamMember.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestMapTrainer::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Map* map = m_game->newMap();
    
    m_trainer1 = map->newTrainer();
    m_trainer2 = map->newTrainer();
    m_trainer3 = map->newTrainer();
}

void TestMapTrainer::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapTrainer::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_trainer1);
    makeConnections(m_trainer2);
    makeConnections(m_trainer3);
}

void TestMapTrainer::cleanup()
{
    closeConnections(m_trainer1);
    closeConnections(m_trainer2);
    closeConnections(m_trainer3);
    
    TestSigmodObject::cleanup();
}

void TestMapTrainer::validation()
{
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_trainer1->setName("Foo");
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->newTrainer();
    
    m_trainer1->setTrainerClass(0);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->map(0)->setWidth(25);
    m_game->map(0)->setHeight(25);
    
    m_trainer1->setPosition(QPoint(20, 20));
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 11);
    
    m_game->map(0)->setWidth(20);
    m_game->map(0)->setHeight(20);
    
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 14);
    
    m_game->map(0)->setWidth(25);
    m_game->map(0)->setHeight(25);
    
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 16);
    
    m_game->rules()->setMaxParty(6);
    m_game->rules()->setMaxFight(6);
    
    m_trainer1->setNumberFight(3);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 18);
    
    m_game->rules()->setMaxFight(2);
    
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 21);
    
    m_game->rules()->setMaxFight(6);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 23);
    
    m_trainer1->newTeamMember();
    
    m_trainer1->setLeadTeamMember(0, true);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 23);
    
    m_trainer1->deleteTeamMember(0);
    
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 25);
    
    m_trainer1->newTeamMember();
    
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 2);
    QCOMPARE(m_errors.size(), 25);
    
    m_game->rules()->setMaxFight(2);
    m_trainer1->newTeamMember();
    
    m_trainer1->setLeadTeamMember(1, true);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 3);
    QCOMPARE(m_errors.size(), 26);
    
    m_game->rules()->setMaxFight(2);
    
    QCOMPARE(m_warnings.size(), 3);
    QCOMPARE(m_errors.size(), 26);
}

void TestMapTrainer::saving()
{
    QDomDocument xml = Object::xml(m_trainer1);
    QFile file("mapTrainer.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapTrainer::loading()
{
    QDomDocument xml;
    QFile file("mapTrainer.xml");
    
    m_game->newTrainer();
    m_trainer1->newTeamMember();
    
    m_trainer1->setName("Bar");
    m_trainer1->setTrainerClass(1);
    m_trainer1->setPosition(QPoint(3, 3));
    m_trainer1->setNumberFight(1);
    m_trainer1->setScript(Script("python", "import os"));
    m_trainer1->setLeadTeamMember(0, false);
    m_trainer1->setLeadTeamMember(1, false);
    m_trainer1->setLeadTeamMember(2, true);

    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_trainer1->load(xml.firstChildElement("MapTrainer"));
    
    QCOMPARE(m_trainer1->name(), QString("Foo"));
    QCOMPARE(m_trainer1->trainerClass(), 0);
    QCOMPARE(m_trainer1->position(), QPoint(20, 20));
    QCOMPARE(m_trainer1->numberFight(), 3);
    QCOMPARE(m_trainer1->script(), Script());
    QCOMPARE(m_trainer1->leadTeamMember(0), true);
    QCOMPARE(m_trainer1->leadTeamMember(1), true);
    QCOMPARE(m_trainer1->leadTeamMember(2), false);
    QCOMPARE(m_trainer1->teamMemberCount(), 2);
}

void TestMapTrainer::setName()
{
    m_trainer2->setName("Foo");
    m_trainer2->setName("Foo");
    
    QCOMPARE(m_trainer2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapTrainer::setTrainerClass()
{
    m_trainer2->setTrainerClass(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newTrainer();
    
    m_trainer2->setTrainerClass(2);
    m_trainer2->setTrainerClass(2);
    
    QCOMPARE(m_trainer2->trainerClass(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTrainer::setPosition()
{
    m_trainer2->setPosition(QPoint(20, 30));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->setPosition(QPoint(30, 20));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_trainer2->setPosition(QPoint(20, 20));
    m_trainer2->setPosition(QPoint(20, 20));
    
    QCOMPARE(m_trainer2->position(), QPoint(20, 20));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTrainer::setNumberFight()
{
    m_game->rules()->setMaxFight(4);
    
    m_trainer2->setNumberFight(10);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->setNumberFight(2);
    m_trainer2->setNumberFight(2);
    
    QCOMPARE(m_trainer2->numberFight(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapTrainer::setScript()
{
    m_trainer2->setScript(Script("python", "import os"));
    m_trainer2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_trainer2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapTrainer::setLeadTeamMember()
{
    m_trainer2->setNumberFight(1);
    
    QCOMPARE(m_changedCount, 1);
    
    m_trainer2->setLeadTeamMember(0, true);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->newTeamMember();
    
    m_trainer2->setLeadTeamMember(0, true);
    m_trainer2->setLeadTeamMember(0, true);
    
    QCOMPARE(m_trainer2->leadTeamMember(0), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->newTeamMember();
    
    m_trainer2->setLeadTeamMember(1, true);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_trainer2->setNumberFight(2);
    
    QCOMPARE(m_changedCount, 3);
    
    m_trainer2->setLeadTeamMember(1, true);
    
    QCOMPARE(m_trainer2->leadTeamMember(1), true);
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapTrainer::members()
{
    QCOMPARE(m_trainer2->newTeamMember()->id(), 2);
    QCOMPARE(m_trainer2->newTeamMember()->id(), 3);
    QCOMPARE(m_trainer2->newTeamMember()->id(), 4);
    
    QCOMPARE(m_trainer2->teamMemberCount(), 5);
    
    m_trainer2->deleteTeamMember(0);
    
    QCOMPARE(m_trainer2->teamMemberCount(), 4);
    
    QCOMPARE(m_trainer2->newTeamMember()->id(), 0);
    
    QCOMPARE(m_trainer2->teamMemberIndex(0), 4);
    
    m_trainer2->deleteTeamMemberById(1);
    
    QCOMPARE(m_trainer2->teamMemberIndex(0), 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapTrainer::assignment()
{
    *m_trainer3 = *m_trainer2;
    
    QCOMPARE(m_trainer3->name(), QString("Foo"));
    QCOMPARE(m_trainer3->trainerClass(), 2);
    QCOMPARE(m_trainer3->position(), QPoint(20, 20));
    QCOMPARE(m_trainer3->numberFight(), 2);
    QCOMPARE(m_trainer3->script(), Script("python", "import os"));
    QCOMPARE(m_trainer3->leadTeamMember(0), true);
    QCOMPARE(m_trainer3->leadTeamMember(1), true);
    QCOMPARE(m_trainer3->teamMemberCount(), 4);
}

QTEST_APPLESS_MAIN(TestMapTrainer)
