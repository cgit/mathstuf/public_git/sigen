/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestTrainer.h"

// Sigmod includes
#include "../Game.h"
#include "../Trainer.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestTrainer::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_trainer1 = m_game->newTrainer();
    m_trainer2 = m_game->newTrainer();
    m_trainer3 = m_game->newTrainer();
}

void TestTrainer::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestTrainer::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_trainer1);
    makeConnections(m_trainer2);
    makeConnections(m_trainer3);
}

void TestTrainer::cleanup()
{
    closeConnections(m_trainer1);
    closeConnections(m_trainer2);
    closeConnections(m_trainer3);
    
    TestSigmodObject::cleanup();
}

void TestTrainer::validation()
{
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_trainer1->setName("Foo");
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_game->newSkin();
    
    m_trainer1->setSkin(0);
    m_trainer1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
}

void TestTrainer::saving()
{
    QDomDocument xml = Object::xml(m_trainer1);
    QFile file("trainer.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestTrainer::loading()
{
    QDomDocument xml;
    QFile file("trainer.xml");
    
    m_game->newSkin();
    
    m_trainer1->setName("Bar");
    m_trainer1->setMoneyFactor(1);
    m_trainer1->setSkin(1);
    m_trainer1->setDepth(5);
    m_trainer1->setTeamIntel(Trainer::Cheating);
    m_trainer1->setMoveIntel(Trainer::Cheating);
    m_trainer1->setItemIntel(Trainer::Cheating);
    m_trainer1->setAbilityIntel(Trainer::Cheating);
    m_trainer1->setStatIntel(Trainer::Cheating);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_trainer1->load(xml.firstChildElement("Trainer"));
    
    QCOMPARE(m_trainer1->name(), QString("Foo"));
    QCOMPARE(m_trainer1->moneyFactor(), 0);
    QCOMPARE(m_trainer1->skin(), 0);
    QCOMPARE(m_trainer1->depth(), 0);
    QCOMPARE(m_trainer1->teamIntel(), Trainer::Remember);
    QCOMPARE(m_trainer1->moveIntel(), Trainer::Remember);
    QCOMPARE(m_trainer1->itemIntel(), Trainer::Remember);
    QCOMPARE(m_trainer1->abilityIntel(), Trainer::Remember);
    QCOMPARE(m_trainer1->statIntel(), Trainer::Remember);
}

void TestTrainer::setName()
{
    m_trainer2->setName("Foo");
    m_trainer2->setName("Foo");
    
    QCOMPARE(m_trainer2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::setMoneyFactor()
{
    m_trainer2->setMoneyFactor(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->setMoneyFactor(5);
    m_trainer2->setMoneyFactor(5);
    
    QCOMPARE(m_trainer2->moneyFactor(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestTrainer::setSkin()
{
    m_trainer2->setSkin(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSkin();
    
    m_trainer2->setSkin(2);
    m_trainer2->setSkin(2);
    
    QCOMPARE(m_trainer2->skin(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestTrainer::setDepth()
{
    m_trainer2->setDepth(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_trainer2->setDepth(5);
    m_trainer2->setDepth(5);
    
    QCOMPARE(m_trainer2->depth(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestTrainer::setTeamIntel()
{
    m_trainer2->setTeamIntel(Trainer::Cheating);
    m_trainer2->setTeamIntel(Trainer::Cheating);
    
    QCOMPARE(m_trainer2->teamIntel(), Trainer::Cheating);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::setMoveIntel()
{
    m_trainer2->setMoveIntel(Trainer::Cheating);
    m_trainer2->setMoveIntel(Trainer::Cheating);
    
    QCOMPARE(m_trainer2->moveIntel(), Trainer::Cheating);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::setItemIntel()
{
    m_trainer2->setItemIntel(Trainer::Cheating);
    m_trainer2->setItemIntel(Trainer::Cheating);
    
    QCOMPARE(m_trainer2->itemIntel(), Trainer::Cheating);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::setAbilityIntel()
{
    m_trainer2->setAbilityIntel(Trainer::Cheating);
    m_trainer2->setAbilityIntel(Trainer::Cheating);
    
    QCOMPARE(m_trainer2->abilityIntel(), Trainer::Cheating);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::setStatIntel()
{
    m_trainer2->setStatIntel(Trainer::Cheating);
    m_trainer2->setStatIntel(Trainer::Cheating);
    
    QCOMPARE(m_trainer2->statIntel(), Trainer::Cheating);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTrainer::assignment()
{
    *m_trainer3 = *m_trainer2;
    
    QCOMPARE(m_trainer3->name(), QString("Foo"));
    QCOMPARE(m_trainer3->moneyFactor(), 5);
    QCOMPARE(m_trainer3->skin(), 2);
    QCOMPARE(m_trainer3->depth(), 5);
    QCOMPARE(m_trainer3->teamIntel(), Trainer::Cheating);
    QCOMPARE(m_trainer3->moveIntel(), Trainer::Cheating);
    QCOMPARE(m_trainer3->itemIntel(), Trainer::Cheating);
    QCOMPARE(m_trainer3->abilityIntel(), Trainer::Cheating);
    QCOMPARE(m_trainer3->statIntel(), Trainer::Cheating);
}

QTEST_APPLESS_MAIN(TestTrainer)
