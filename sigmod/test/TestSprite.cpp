/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSprite.h"

// Sigmod includes
#include "../Game.h"
#include "../Sprite.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestSprite::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_sprite1 = m_game->newSprite();
    m_sprite2 = m_game->newSprite();
    m_sprite3 = m_game->newSprite();
}

void TestSprite::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestSprite::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_sprite1);
    makeConnections(m_sprite2);
    makeConnections(m_sprite3);
}

void TestSprite::cleanup()
{
    closeConnections(m_sprite1);
    closeConnections(m_sprite2);
    closeConnections(m_sprite3);
    
    TestSigmodObject::cleanup();
}

void TestSprite::validation()
{
    m_sprite1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_sprite1->setName("Foo");
    m_sprite1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_sprite1->setSprite("blah");
    m_sprite1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestSprite::saving()
{
    QDomDocument xml = Object::xml(m_sprite1);
    QFile file("sprite.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestSprite::loading()
{
    QDomDocument xml;
    QFile file("sprite.xml");
    
    m_sprite1->setName("Bar");
    m_sprite1->setSprite("blargh");
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_sprite1->load(xml.firstChildElement("Sprite"));
    
    QCOMPARE(m_sprite1->name(), QString("Foo"));
    QCOMPARE(m_sprite1->sprite(), QByteArray("blah"));
}

void TestSprite::setName()
{
    m_sprite2->setName("Foo");
    m_sprite2->setName("Foo");
    
    QCOMPARE(m_sprite2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSprite::setSprite()
{
    m_sprite2->setSprite("blah");
    m_sprite2->setSprite("blah");
    
    QCOMPARE(m_sprite2->sprite(), QByteArray("blah"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSprite::assignment()
{
    *m_sprite3 = *m_sprite2;
    
    QCOMPARE(m_sprite3->name(), QString("Foo"));
    QCOMPARE(m_sprite3->sprite(), QByteArray("blah"));
}

QTEST_APPLESS_MAIN(TestSprite)
