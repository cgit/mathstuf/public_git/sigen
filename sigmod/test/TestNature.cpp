/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestNature.h"

// Sigmod includes
#include "../Game.h"
#include "../Nature.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestNature::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_nature1 = m_game->newNature();
    m_nature2 = m_game->newNature();
    m_nature3 = m_game->newNature();
}

void TestNature::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestNature::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_nature1);
    makeConnections(m_nature2);
    makeConnections(m_nature3);
}

void TestNature::cleanup()
{
    closeConnections(m_nature1);
    closeConnections(m_nature2);
    closeConnections(m_nature3);
    
    TestSigmodObject::cleanup();
}

void TestNature::validation()
{
    m_nature1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_nature1->setName("Foo");
    m_nature1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestNature::saving()
{
    QDomDocument xml = Object::xml(m_nature1);
    QFile file("nature.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestNature::loading()
{
    QDomDocument xml;
    QFile file("nature.xml");
    
    m_game->rules()->setSpecialSplit(true);
    
    m_nature1->setName("Bar");
    m_nature1->setStat(ST_Attack, Fraction(2, 1));
    m_nature1->setStat(ST_Defense, Fraction(2, 1));
    m_nature1->setStat(ST_Speed, Fraction(2, 1));
    m_nature1->setStat(ST_SpecialAttack, Fraction(2, 1));
    m_nature1->setStat(ST_SpecialDefense, Fraction(2, 1));
    m_nature1->setWeight(25);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_nature1->load(xml.firstChildElement("Nature"));
    
    QCOMPARE(m_nature1->name(), QString("Foo"));
    QCOMPARE(m_nature1->stat(ST_Attack), Fraction(1, 1));
    QCOMPARE(m_nature1->stat(ST_Defense), Fraction(1, 1));
    QCOMPARE(m_nature1->stat(ST_Speed), Fraction(1, 1));
    QCOMPARE(m_nature1->stat(ST_SpecialAttack), Fraction(1, 1));
    QCOMPARE(m_nature1->stat(ST_SpecialDefense), Fraction(1, 1));
    QCOMPARE(m_nature1->weight(), 1);
}

void TestNature::setName()
{
    m_nature2->setName("Foo");
    m_nature2->setName("Foo");
    
    QCOMPARE(m_nature2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestNature::setStat()
{
    m_nature2->setStat(ST_Attack, Fraction(-1, 2));
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setSpecialSplit(false);
    
    m_nature2->setStat(ST_SpecialDefense, Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setSpecialSplit(true);
    
    m_nature2->setStat(ST_Attack, Fraction(2, 1));
    m_nature2->setStat(ST_Attack, Fraction(2, 1));
    
    QCOMPARE(m_nature2->stat(ST_Attack), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_nature2->setStat(ST_Defense, Fraction(2, 1));
    m_nature2->setStat(ST_Defense, Fraction(2, 1));
    
    QCOMPARE(m_nature2->stat(ST_Defense), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_nature2->setStat(ST_Speed, Fraction(2, 1));
    m_nature2->setStat(ST_Speed, Fraction(2, 1));
    
    QCOMPARE(m_nature2->stat(ST_Speed), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_nature2->setStat(ST_SpecialAttack, Fraction(2, 1));
    m_nature2->setStat(ST_SpecialAttack, Fraction(2, 1));
        
    QCOMPARE(m_nature2->stat(ST_SpecialAttack), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_nature2->setStat(ST_SpecialDefense, Fraction(2, 1));
    m_nature2->setStat(ST_SpecialDefense, Fraction(2, 1));
    
    QCOMPARE(m_nature2->stat(ST_SpecialDefense), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestNature::setWeight()
{
    m_nature2->setWeight(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_nature2->setWeight(5);
    m_nature2->setWeight(5);
    
    QCOMPARE(m_nature2->weight(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestNature::assignment()
{
    *m_nature3 = *m_nature2;
    
    QCOMPARE(m_nature3->name(), QString("Foo"));
    QCOMPARE(m_nature3->stat(ST_Attack), Fraction(2, 1));
    QCOMPARE(m_nature3->stat(ST_Defense), Fraction(2, 1));
    QCOMPARE(m_nature3->stat(ST_Speed), Fraction(2, 1));
    QCOMPARE(m_nature3->stat(ST_SpecialAttack), Fraction(2, 1));
    QCOMPARE(m_nature3->stat(ST_SpecialDefense), Fraction(2, 1));
    QCOMPARE(m_nature3->weight(), 5);
}

QTEST_APPLESS_MAIN(TestNature)
