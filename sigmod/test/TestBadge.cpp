/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestBadge.h"

// Sigmod includes
#include "../Badge.h"
#include "../Game.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestBadge::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_badge1 = m_game->newBadge();
    m_badge2 = m_game->newBadge();
    m_badge3 = m_game->newBadge();
}

void TestBadge::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestBadge::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_badge1);
    makeConnections(m_badge2);
    makeConnections(m_badge3);
}

void TestBadge::cleanup()
{
    closeConnections(m_badge1);
    closeConnections(m_badge2);
    closeConnections(m_badge3);
    
    TestSigmodObject::cleanup();
}

void TestBadge::validation()
{
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_badge1->setName("Foo");
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->newSprite();
    m_game->newSprite();
    
    m_badge1->setFace(0);
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_badge1->setBadge(0);
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_badge1->setBadge(1);
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->rules()->setMaxLevel(25);
    
    m_badge1->setObey(15);
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->rules()->setMaxLevel(10);
    
    m_badge1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 10);
}

void TestBadge::saving()
{
    QDomDocument xml = Object::xml(m_badge1);
    QFile file("badge.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestBadge::loading()
{
    QDomDocument xml;
    QFile file("badge.xml");
    
    m_game->rules()->setSpecialSplit(true);
    
    m_badge1->setName("Bar");
    m_badge1->setFace(1);
    m_badge1->setBadge(0);
    m_badge1->setObey(5);
    m_badge1->setStat(ST_Attack, Fraction(2, 1));
    m_badge1->setStat(ST_Defense, Fraction(2, 1));
    m_badge1->setStat(ST_Speed, Fraction(2, 1));
    m_badge1->setStat(ST_SpecialAttack, Fraction(2, 1));
    m_badge1->setStat(ST_SpecialDefense, Fraction(2, 1));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_badge1->load(xml.firstChildElement("Badge"));
    
    QCOMPARE(m_badge1->name(), QString("Foo"));
    QCOMPARE(m_badge1->face(), 0);
    QCOMPARE(m_badge1->badge(), 1);
    QCOMPARE(m_badge1->obey(), 15);
    QCOMPARE(m_badge1->stat(ST_Attack), Fraction(1, 1));
    QCOMPARE(m_badge1->stat(ST_Defense), Fraction(1, 1));
    QCOMPARE(m_badge1->stat(ST_Speed), Fraction(1, 1));
    QCOMPARE(m_badge1->stat(ST_SpecialAttack), Fraction(1, 1));
    QCOMPARE(m_badge1->stat(ST_SpecialDefense), Fraction(1, 1));
}

void TestBadge::setName()
{
    m_badge2->setName("Foo");
    m_badge2->setName("Foo");
    
    QCOMPARE(m_badge2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestBadge::setFace()
{
    m_badge2->setFace(2);
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSprite();
    
    m_badge2->setFace(2);
    m_badge2->setFace(2);
    
    QCOMPARE(m_badge2->face(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestBadge::setBadge()
{
    m_badge2->setBadge(3);
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSprite();
    
    m_badge2->setBadge(3);
    m_badge2->setBadge(3);
    
    QCOMPARE(m_badge2->badge(), 3);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestBadge::setObey()
{
    m_badge2->setObey(50);
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxLevel(100);
    
    m_badge2->setObey(50);
    m_badge2->setObey(50);
    
    QCOMPARE(m_badge2->obey(), 50);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestBadge::setStat()
{
    m_badge2->setStat(ST_Attack, Fraction(1, 2));
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setSpecialSplit(false);
    
    m_badge2->setStat(ST_SpecialDefense, Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->rules()->setSpecialSplit(true);
    
    m_badge2->setStat(ST_Attack, Fraction(2, 1));
    m_badge2->setStat(ST_Attack, Fraction(2, 1));
    
    QCOMPARE(m_badge2->stat(ST_Attack), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_badge2->setStat(ST_Defense, Fraction(2, 1));
    m_badge2->setStat(ST_Defense, Fraction(2, 1));
    
    QCOMPARE(m_badge2->stat(ST_Defense), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_badge2->setStat(ST_Speed, Fraction(2, 1));
    m_badge2->setStat(ST_Speed, Fraction(2, 1));
    
    QCOMPARE(m_badge2->stat(ST_Speed), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_badge2->setStat(ST_SpecialAttack, Fraction(2, 1));
    m_badge2->setStat(ST_SpecialAttack, Fraction(2, 1));
        
    QCOMPARE(m_badge2->stat(ST_SpecialAttack), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_badge2->setStat(ST_SpecialDefense, Fraction(2, 1));
    m_badge2->setStat(ST_SpecialDefense, Fraction(2, 1));
    
    QCOMPARE(m_badge2->stat(ST_SpecialDefense), Fraction(2, 1));
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestBadge::assignment()
{
    *m_badge3 = *m_badge2;
    
    QCOMPARE(m_badge3->name(), QString("Foo"));
    QCOMPARE(m_badge3->face(), 2);
    QCOMPARE(m_badge3->badge(), 3);
    QCOMPARE(m_badge3->obey(), 50);
    QCOMPARE(m_badge3->stat(ST_Attack), Fraction(2, 1));
    QCOMPARE(m_badge3->stat(ST_Defense), Fraction(2, 1));
    QCOMPARE(m_badge3->stat(ST_Speed), Fraction(2, 1));
    QCOMPARE(m_badge3->stat(ST_SpecialAttack), Fraction(2, 1));
    QCOMPARE(m_badge3->stat(ST_SpecialDefense), Fraction(2, 1));
}

QTEST_APPLESS_MAIN(TestBadge)
