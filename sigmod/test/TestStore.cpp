/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestStore.h"

// Sigmod includes
#include "../Game.h"
#include "../Store.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestStore::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_store1 = m_game->newStore();
    m_store2 = m_game->newStore();
    m_store3 = m_game->newStore();
}

void TestStore::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestStore::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_store1);
    makeConnections(m_store2);
    makeConnections(m_store3);
}

void TestStore::cleanup()
{
    closeConnections(m_store1);
    closeConnections(m_store2);
    closeConnections(m_store3);
    
    TestSigmodObject::cleanup();
}

void TestStore::validation()
{
    m_store1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_store1->setName("Foo");
    m_store1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_game->newItem();
    
    m_store1->setItem(0, true);
    m_store1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestStore::saving()
{
    QDomDocument xml = Object::xml(m_store1);
    QFile file("store.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestStore::loading()
{
    QDomDocument xml;
    QFile file("store.xml");
    
    m_game->newItem();
    
    m_store1->setName("Bar");
    m_store1->setItem(0, false);
    m_store1->setItem(1, true);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_store1->load(xml.firstChildElement("Store"));
    
    QCOMPARE(m_store1->name(), QString("Foo"));
    QCOMPARE(m_store1->item(0), true);
    QCOMPARE(m_store1->item(1), false);
}

void TestStore::setName()
{
    m_store2->setName("Foo");
    m_store2->setName("Foo");
    
    QCOMPARE(m_store2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestStore::items()
{
    m_game->newItem();
    
    m_store2->setItem(0, true);
    m_store2->setItem(0, true);
    
    QCOMPARE(m_store2->item(0), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_store2->setItem(1, true);
    
    QCOMPARE(m_store2->item(1), true);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_store2->setItem(1, false);
    
    QCOMPARE(m_store2->item(1), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
    
    m_store2->setItem(2, false);
    
    QCOMPARE(m_store2->item(2), false);
        
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestStore::assignment()
{
    *m_store3 = *m_store2;
    
    QCOMPARE(m_store3->name(), QString("Foo"));
    QCOMPARE(m_store3->item(0), true);
    QCOMPARE(m_store3->item(1), false);
    QCOMPARE(m_store3->item(2), false);
}

QTEST_APPLESS_MAIN(TestStore)
