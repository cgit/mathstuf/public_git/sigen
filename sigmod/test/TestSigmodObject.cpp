/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSigmodObject.h"

// Sigmod includes
#include "../Game.h"

using namespace Sigmod;

void TestSigmodObject::makeConnections(Object* object)
{
    connect(object, SIGNAL(warning(QString)), this, SLOT(warning(QString)));
    connect(object, SIGNAL(error(QString)), this, SLOT(error(QString)));
    connect(object, SIGNAL(changed()), this, SLOT(changed()));
}

void TestSigmodObject::closeConnections(Object* object)
{
    disconnect(object, 0, this, 0);
}

void TestSigmodObject::warning(const QString& warning)
{
    m_warnings.append(warning);
}

void TestSigmodObject::error(const QString& error)
{
    m_errors.append(error);
}

void TestSigmodObject::changed()
{
    ++m_changedCount;
}

void TestSigmodObject::initTestCase()
{
    m_game = new Game;
}

void TestSigmodObject::cleanupTestCase()
{
    delete m_game;
}

void TestSigmodObject::init()
{
}

void TestSigmodObject::cleanup()
{
    m_warnings.clear();
    m_errors.clear();
    m_changedCount = 0;
}
