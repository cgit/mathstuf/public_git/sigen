/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestGame.h"

// Sigmod includes
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestGame::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_game1 = new Game;
    m_game2 = new Game;
    m_game3 = new Game;
}

void TestGame::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestGame::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_game1);
    makeConnections(m_game2);
    makeConnections(m_game3);
}

void TestGame::cleanup()
{
    closeConnections(m_game1);
    closeConnections(m_game2);
    closeConnections(m_game3);
    
    TestSigmodObject::cleanup();
}

void TestGame::validation()
{
    m_game1->validate();
    
    QCOMPARE(m_warnings.size(), );
    QCOMPARE(m_errors.size(), );
    
    m_game1->set();
    m_game1->validate();
    
    QCOMPARE(m_warnings.size(), );
    QCOMPARE(m_errors.size(), );
}

void TestGame::saving()
{
    QDomDocument xml = Object::xml(m_game1);
    QFile file("sigmod.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestGame::loading()
{
    QDomDocument xml;
    QFile file("sigmod.xml");
    
    m_game1->set();
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_game1->load(xml.firstChildElement("Sigmod"));
    
    QCOMPARE(m_game1->(), );
}

void TestGame::set()
{
    m_game2->set();
    m_game2->set();
    
    QCOMPARE(m_game2->(), );
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestGame::assignment()
{
    *m_game3 = *m_game2;
    
    QCOMPARE(m_game3->(), );
}

QTEST_APPLESS_MAIN(TestGame)
