/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestRules.h"

// Sigmod includes
#include "../Game.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestRules::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_game1 = new Game;
    m_game2 = new Game;
    m_game3 = new Game;
    
    m_rules1 = m_game1->rules();
    m_rules2 = m_game2->rules();
    m_rules3 = m_game3->rules();
}

void TestRules::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
    
    delete m_game1;
    delete m_game2;
    delete m_game3;
}

void TestRules::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_rules1);
    makeConnections(m_rules2);
    makeConnections(m_rules3);
}

void TestRules::cleanup()
{
    closeConnections(m_rules1);
    closeConnections(m_rules2);
    closeConnections(m_rules3);
    
    TestSigmodObject::cleanup();
}

void TestRules::validation()
{
    m_rules1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 0);
    
    // TODO: Stress test validation here?
    
    m_rules1->setMaxMoney(5);
    m_rules1->validate();
    
    QCOMPARE(m_warnings.size(), 1);
    QCOMPARE(m_errors.size(), 0);
}

void TestRules::saving()
{
    QDomDocument xml = Object::xml(m_rules1);
    QFile file("rules.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestRules::loading()
{
    QDomDocument xml;
    QFile file("rules.xml");
    
    m_rules1->setGenderAllowed(true);
    m_rules1->setBreedingAllowed(true);
    m_rules1->setCriticalDomains(true);
    m_rules1->setNumBoxes(10);
    m_rules1->setBoxSize(15);
    m_rules1->setMaxParty(10);
    m_rules1->setMaxFight(2);
    m_rules1->setMaxPlayers(4);
    m_rules1->setMaxHeldItems(3);
    m_rules1->setMaxAbilities(3);
    m_rules1->setMaxNatures(3);
    m_rules1->setMaxMoves(3);
    m_rules1->setMaxLevel(3);
    m_rules1->setMaxStages(3);
    m_rules1->setMaxMoney(3);
    m_rules1->setMaxTotalWeight(100);
    m_rules1->setSpecialSplit(true);
    m_rules1->setSpecialDVSplit(true);
    m_rules1->setMaxTotalEV(100);
    m_rules1->setMaxEVPerStat(25);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_rules1->load(xml.firstChildElement("Rules"));
    
    QCOMPARE(m_rules1->genderAllowed(), false);
    QCOMPARE(m_rules1->breedingAllowed(), false);
    QCOMPARE(m_rules1->criticalDomains(), false);
    QCOMPARE(m_rules1->numBoxes(), 0);
    QCOMPARE(m_rules1->boxSize(), 1);
    QCOMPARE(m_rules1->maxParty(), 1);
    QCOMPARE(m_rules1->maxFight(), 1);
    QCOMPARE(m_rules1->maxPlayers(), 2);
    QCOMPARE(m_rules1->maxHeldItems(), 0);
    QCOMPARE(m_rules1->maxAbilities(), 0);
    QCOMPARE(m_rules1->maxNatures(), 0);
    QCOMPARE(m_rules1->maxMoves(), 1);
    QCOMPARE(m_rules1->maxLevel(), 1);
    QCOMPARE(m_rules1->maxStages(), 6);
    QCOMPARE(m_rules1->maxMoney(), 5);
    QCOMPARE(m_rules1->maxTotalWeight(), 0);
    QCOMPARE(m_rules1->specialSplit(), false);
    QCOMPARE(m_rules1->specialDVSplit(), false);
    QCOMPARE(m_rules1->maxTotalEV(), 0);
    QCOMPARE(m_rules1->maxEVPerStat(), 0);
}

void TestRules::setGenderAllowed()
{
    m_rules2->setGenderAllowed(true);
    m_rules2->setGenderAllowed(true);
    
    QCOMPARE(m_rules2->genderAllowed(), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestRules::setBreedingAllowed()
{
    m_rules2->setGenderAllowed(false);
    
    QCOMPARE(m_changedCount, 1);
    
    m_rules2->setBreedingAllowed(true);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setGenderAllowed(true);
    
    QCOMPARE(m_changedCount, 2);
    
    m_rules2->setBreedingAllowed(true);
    m_rules2->setBreedingAllowed(true);
    
    QCOMPARE(m_rules2->breedingAllowed(), true);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setCriticalDomains()
{
    m_rules2->setCriticalDomains(true);
    m_rules2->setCriticalDomains(true);
    
    QCOMPARE(m_rules2->criticalDomains(), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestRules::setNumBoxes()
{
    m_rules2->setNumBoxes(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setNumBoxes(5);
    m_rules2->setNumBoxes(5);
    
    QCOMPARE(m_rules2->numBoxes(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setBoxSize()
{
    m_rules2->setBoxSize(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setBoxSize(5);
    m_rules2->setBoxSize(5);
    
    QCOMPARE(m_rules2->boxSize(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxParty()
{
    m_rules2->setMaxParty(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);

    m_rules2->setMaxParty(5);
    m_rules2->setMaxParty(5);
    
    QCOMPARE(m_rules2->maxParty(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxFight()
{
    m_rules2->setMaxFight(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);

    m_rules2->setMaxFight(6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);

    m_rules2->setMaxFight(2);
    m_rules2->setMaxFight(2);
    
    QCOMPARE(m_rules2->maxFight(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestRules::setMaxPlayers()
{
    m_rules2->setMaxPlayers(1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);

    m_rules2->setMaxPlayers(3);
    m_rules2->setMaxPlayers(3);
    
    QCOMPARE(m_rules2->maxPlayers(), 3);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxHeldItems()
{
    m_rules2->setMaxHeldItems(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxHeldItems(5);
    m_rules2->setMaxHeldItems(5);
    
    QCOMPARE(m_rules2->maxHeldItems(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxAbilities()
{
    m_rules2->setMaxAbilities(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxAbilities(5);
    m_rules2->setMaxAbilities(5);
    
    QCOMPARE(m_rules2->maxAbilities(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxNatures()
{
    m_rules2->setMaxNatures(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxNatures(5);
    m_rules2->setMaxNatures(5);
    
    QCOMPARE(m_rules2->maxNatures(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxMoves()
{
    m_rules2->setMaxMoves(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxMoves(5);
    m_rules2->setMaxMoves(5);
    
    QCOMPARE(m_rules2->maxMoves(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxLevel()
{
    m_rules2->setMaxLevel(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxLevel(5);
    m_rules2->setMaxLevel(5);
    
    QCOMPARE(m_rules2->maxLevel(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxStages()
{
    m_rules2->setMaxStages(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxStages(5);
    m_rules2->setMaxStages(5);
    
    QCOMPARE(m_rules2->maxStages(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxMoney()
{
    m_rules2->setMaxMoney(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxMoney(5);
    m_rules2->setMaxMoney(5);
    
    QCOMPARE(m_rules2->maxMoney(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxTotalWeight()
{
    m_rules2->setMaxTotalWeight(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxTotalWeight(5);
    m_rules2->setMaxTotalWeight(5);
    
    QCOMPARE(m_rules2->maxTotalWeight(), 5);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setSpecialSplit()
{
    m_rules2->setSpecialSplit(true);
    m_rules2->setSpecialSplit(true);
    
    QCOMPARE(m_rules2->specialSplit(), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestRules::setSpecialDVSplit()
{
    m_rules2->setSpecialSplit(false);
    
    QCOMPARE(m_changedCount, 1);
    
    m_rules2->setSpecialDVSplit(true);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setSpecialSplit(true);
    
    QCOMPARE(m_changedCount, 2);
    
    m_rules2->setSpecialDVSplit(true);
    m_rules2->setSpecialDVSplit(true);
    
    QCOMPARE(m_rules2->specialDVSplit(), true);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxTotalEV()
{
    m_rules2->setMaxTotalEV(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    QCOMPARE(m_changedCount, 1);
    
    m_rules2->setMaxTotalEV(-1);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    QCOMPARE(m_changedCount, 3);
    
    m_rules2->setMaxTotalEV(5);
    m_rules2->setMaxTotalEV(5);
    
    QCOMPARE(m_rules2->maxTotalEV(), 5);
    
    QCOMPARE(m_changedCount, 4);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestRules::setMaxEVPerStat()
{
    m_rules2->setMaxEVPerStat(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_rules2->setMaxEVPerStat(6);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    m_rules2->setMaxEVPerStat(-1);
    
    QCOMPARE(m_changedCount, 2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_rules2->setMaxEVPerStat(6);
    
    QCOMPARE(m_changedCount, 3);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    QCOMPARE(m_changedCount, 4);
    
    m_rules2->setMaxEVPerStat(3);
    m_rules2->setMaxEVPerStat(3);
    
    QCOMPARE(m_rules2->maxEVPerStat(), 3);
    
    QCOMPARE(m_changedCount, 5);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestRules::assignment()
{
    *m_rules3 = *m_rules2;
    
    QCOMPARE(m_rules3->genderAllowed(), true);
    QCOMPARE(m_rules3->breedingAllowed(), true);
    QCOMPARE(m_rules3->criticalDomains(), true);
    QCOMPARE(m_rules3->numBoxes(), 5);
    QCOMPARE(m_rules3->boxSize(), 5);
    QCOMPARE(m_rules3->maxParty(), 5);
    QCOMPARE(m_rules3->maxFight(), 2);
    QCOMPARE(m_rules3->maxPlayers(), 3);
    QCOMPARE(m_rules3->maxHeldItems(), 5);
    QCOMPARE(m_rules3->maxAbilities(), 5);
    QCOMPARE(m_rules3->maxNatures(), 5);
    QCOMPARE(m_rules3->maxMoves(), 5);
    QCOMPARE(m_rules3->maxLevel(), 5);
    QCOMPARE(m_rules3->maxStages(), 5);
    QCOMPARE(m_rules3->maxMoney(), 5);
    QCOMPARE(m_rules3->maxTotalWeight(), 5);
    QCOMPARE(m_rules3->specialSplit(), true);
    QCOMPARE(m_rules3->specialDVSplit(), true);
    QCOMPARE(m_rules3->maxTotalEV(), 5);
    QCOMPARE(m_rules3->maxEVPerStat(), 3);
}

QTEST_APPLESS_MAIN(TestRules)
