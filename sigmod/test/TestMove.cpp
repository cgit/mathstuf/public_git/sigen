/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMove.h"

// Sigmod includes
#include "../Game.h"
#include "../Move.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestMove::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_move1 = m_game->newMove();
    m_move2 = m_game->newMove();
    m_move3 = m_game->newMove();
}

void TestMove::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMove::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_move1);
    makeConnections(m_move2);
    makeConnections(m_move3);
}

void TestMove::cleanup()
{
    closeConnections(m_move1);
    closeConnections(m_move2);
    closeConnections(m_move3);
    
    TestSigmodObject::cleanup();
}

void TestMove::validation()
{
    m_move1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_move1->setName("Foo");
    m_move1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->newType();
    
    m_move1->setType(0);
    m_move1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_move1->setPowerPoints(25);
    m_move1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
}

void TestMove::saving()
{
    QDomDocument xml = Object::xml(m_move1);
    QFile file("move.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMove::loading()
{
    QDomDocument xml;
    QFile file("move.xml");
    
    m_game->newType();
    
    m_move1->setName("Bar");
    m_move1->setAccuracy(Fraction(1, 2));
    m_move1->setPower(10);
    m_move1->setType(1);
    m_move1->setSpecial(true);
    m_move1->setPowerPoints(50);
    m_move1->setPriority(1);
    m_move1->setDescription("blah");
    m_move1->setBattleScript(Script("python", "import os"));
    m_move1->setWorldScript(Script("python", "import os"));
    m_move1->setPriorityScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_move1->load(xml.firstChildElement("Move"));
    
    QCOMPARE(m_move1->name(), QString("Foo"));
    QCOMPARE(m_move1->accuracy(), Fraction(1, 1));
    QCOMPARE(m_move1->power(), 0);
    QCOMPARE(m_move1->type(), 0);
    QCOMPARE(m_move1->special(), false);
    QCOMPARE(m_move1->powerPoints(), 25);
    QCOMPARE(m_move1->priority(), 0);
    QCOMPARE(m_move1->description(), QString(""));
    QCOMPARE(m_move1->battleScript(), Script("", ""));
    QCOMPARE(m_move1->worldScript(), Script("", ""));
    QCOMPARE(m_move1->priorityScript(), Script("", ""));
}

void TestMove::setName()
{
    m_move2->setName("Foo");
    m_move2->setName("Foo");
    
    QCOMPARE(m_move2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setAccuracy()
{
    m_move2->setAccuracy(Fraction(3, 2));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_move2->setAccuracy(Fraction(-1, 2));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_move2->setAccuracy(Fraction(0, 2));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_move2->setAccuracy(Fraction(1, 2));
    m_move2->setAccuracy(Fraction(1, 2));
    
    QCOMPARE(m_move2->accuracy(), Fraction(1, 2));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestMove::setPower()
{
    m_move2->setPower(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_move2->setPower(10);
    m_move2->setPower(10);
    
    QCOMPARE(m_move2->power(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMove::setType()
{
    m_move2->setType(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newType();
    
    m_move2->setType(2);
    m_move2->setType(2);
    
    QCOMPARE(m_move2->type(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMove::setSpecial()
{
    m_move2->setSpecial(true);
    m_move2->setSpecial(true);
    
    QCOMPARE(m_move2->special(), true);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setPowerPoints()
{
    m_move2->setPowerPoints(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_move2->setPowerPoints(10);
    m_move2->setPowerPoints(10);
    
    QCOMPARE(m_move2->powerPoints(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMove::setPriority()
{
    m_move2->setPriority(25);
    m_move2->setPriority(25);
    
    QCOMPARE(m_move2->priority(), 25);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setDescription()
{
    m_move2->setDescription("blah");
    m_move2->setDescription("blah");
    
    QCOMPARE(m_move2->description(), QString("blah"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setBattleScript()
{
    m_move2->setBattleScript(Script("python", "import os"));
    m_move2->setBattleScript(Script("python", "import os"));
    
    QCOMPARE(m_move2->battleScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setWorldScript()
{
    m_move2->setWorldScript(Script("python", "import os"));
    m_move2->setWorldScript(Script("python", "import os"));
    
    QCOMPARE(m_move2->worldScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::setPriorityScript()
{
    m_move2->setPriorityScript(Script("python", "import os"));
    m_move2->setPriorityScript(Script("python", "import os"));
    
    QCOMPARE(m_move2->priorityScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMove::assignment()
{
    *m_move3 = *m_move2;
    
    QCOMPARE(m_move3->name(), QString("Foo"));
    QCOMPARE(m_move3->accuracy(), Fraction(1, 2));
    QCOMPARE(m_move3->power(), 10);
    QCOMPARE(m_move3->type(), 2);
    QCOMPARE(m_move3->special(), true);
    QCOMPARE(m_move3->powerPoints(), 10);
    QCOMPARE(m_move3->priority(), 25);
    QCOMPARE(m_move3->description(), QString("blah"));
    QCOMPARE(m_move3->battleScript(), Script("python", "import os"));
    QCOMPARE(m_move3->worldScript(), Script("python", "import os"));
    QCOMPARE(m_move3->priorityScript(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestMove)
