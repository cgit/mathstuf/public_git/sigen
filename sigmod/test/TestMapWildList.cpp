/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapWildList.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapWildList.h"
#include "../MapWildListEncounter.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestMapWildList::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Map* map = m_game->newMap();
    
    m_wildList1 = map->newWildList();
    m_wildList2 = map->newWildList();
    m_wildList3 = map->newWildList();
}

void TestMapWildList::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapWildList::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_wildList1);
    makeConnections(m_wildList2);
    makeConnections(m_wildList3);
}

void TestMapWildList::cleanup()
{
    closeConnections(m_wildList1);
    closeConnections(m_wildList2);
    closeConnections(m_wildList3);
    
    TestSigmodObject::cleanup();
}

void TestMapWildList::validation()
{
    m_wildList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_wildList1->setName("Foo");
    m_wildList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_wildList1->newEncounter();
    m_wildList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestMapWildList::saving()
{
    QDomDocument xml = Object::xml(m_wildList1);
    QFile file("wildList.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapWildList::loading()
{
    QDomDocument xml;
    QFile file("wildList.xml");
    
    m_wildList1->setName("Bar");
    m_wildList1->newEncounter();
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_wildList1->load(xml.firstChildElement("MapWildList"));
    
    QCOMPARE(m_wildList1->name(), QString("Foo"));
    QCOMPARE(m_wildList1->encounterCount(), 1);
}

void TestMapWildList::setName()
{
    m_wildList2->setName("Foo");
    m_wildList2->setName("Foo");
    
    QCOMPARE(m_wildList2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapWildList::encounters()
{
    QCOMPARE(m_wildList2->newEncounter()->id(), 0);
    QCOMPARE(m_wildList2->newEncounter()->id(), 1);
    QCOMPARE(m_wildList2->newEncounter()->id(), 2);
    
    QCOMPARE(m_wildList2->encounterCount(), 3);
    
    m_wildList2->deleteEncounter(0);
    
    QCOMPARE(m_wildList2->encounterCount(), 2);
    
    QCOMPARE(m_wildList2->newEncounter()->id(), 0);
    
    QCOMPARE(m_wildList2->encounterIndex(0), 2);
    
    m_wildList2->deleteEncounterById(1);
    
    QCOMPARE(m_wildList2->encounterIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapWildList::assignment()
{
    *m_wildList3 = *m_wildList2;
    
    QCOMPARE(m_wildList3->name(), QString("Foo"));
    QCOMPARE(m_wildList3->encounterCount(), 2);
}

QTEST_APPLESS_MAIN(TestMapWildList)
