/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestItem.h"

// Sigmod includes
#include "../Game.h"
#include "../Item.h"
#include "../ItemType.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestItem::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_item1 = m_game->newItem();
    m_item2 = m_game->newItem();
    m_item3 = m_game->newItem();
}

void TestItem::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestItem::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_item1);
    makeConnections(m_item2);
    makeConnections(m_item3);
}

void TestItem::cleanup()
{
    closeConnections(m_item1);
    closeConnections(m_item2);
    closeConnections(m_item3);
    
    TestSigmodObject::cleanup();
}

void TestItem::validation()
{
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_item1->setName("Foo");
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 5);
    
    m_game->newItemType();
    
    m_item1->setType(0);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_game->rules()->setMaxMoney(1000);
    
    m_item1->setPrice(500);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 6);
    
    m_item1->setPrice(10000);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_item1->setPrice(500);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_item1->setSellPrice(10000);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_item1->setSellPrice(250);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_game->rules()->setMaxTotalWeight(100);
    m_game->itemType(0)->setMaxWeight(50);
    
    m_item1->setWeight(20);
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 8);
    
    m_game->itemType(0)->setMaxWeight(10);
    
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->itemType(0)->setMaxWeight(50);
    
    m_item1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
}

void TestItem::saving()
{
    QDomDocument xml = Object::xml(m_item1);
    QFile file("item.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestItem::loading()
{
    QDomDocument xml;
    QFile file("item.xml");
    
    m_game->newItemType();
    
    m_item1->setName("Bar");
    m_item1->setType(1);
    m_item1->setPrice(20);
    m_item1->setSellPrice(10);
    m_item1->setWeight(10);
    m_item1->setDescription("An item.");
    m_item1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_item1->load(xml.firstChildElement("Item"));
    
    QCOMPARE(m_item1->name(), QString("Foo"));
    QCOMPARE(m_item1->type(), 0);
    QCOMPARE(m_item1->price(), 500);
    QCOMPARE(m_item1->sellPrice(), 250);
    QCOMPARE(m_item1->weight(), 20);
    QCOMPARE(m_item1->description(), QString(""));
    QCOMPARE(m_item1->script(), Script());
}

void TestItem::setName()
{
    m_item2->setName("Foo");
    m_item2->setName("Foo");
    
    QCOMPARE(m_item2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestItem::setType()
{
    m_item2->setType(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newItemType();
    
    m_item2->setType(2);
    m_item2->setType(2);
    
    QCOMPARE(m_item2->type(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestItem::setPrice()
{
    m_item2->setPrice(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_item2->setPrice(2000);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_item2->setPrice(100);
    m_item2->setPrice(100);
    
    QCOMPARE(m_item2->price(), 100);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestItem::setSellPrice()
{
    m_item2->setSellPrice(-2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_item2->setSellPrice(200);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_item2->setSellPrice(50);
    m_item2->setSellPrice(50);
    
    QCOMPARE(m_item2->sellPrice(), 50);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestItem::setWeight()
{
    m_item2->setWeight(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_item2->setWeight(200);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_game->itemType(2)->setMaxWeight(50);
    
    m_item2->setWeight(20);
    m_item2->setWeight(20);
    
    QCOMPARE(m_item2->weight(), 20);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestItem::setDescription()
{
    m_item2->setDescription("An item.");
    m_item2->setDescription("An item.");
    
    QCOMPARE(m_item2->description(), QString("An item."));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestItem::setScript()
{
    m_item2->setScript(Script("python", "import os"));
    m_item2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_item2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestItem::assignment()
{
    *m_item3 = *m_item2;
    
    QCOMPARE(m_item3->name(), QString("Foo"));
    QCOMPARE(m_item3->type(), 2);
    QCOMPARE(m_item3->price(), 100);
    QCOMPARE(m_item3->sellPrice(), 50);
    QCOMPARE(m_item3->weight(), 20);
    QCOMPARE(m_item3->description(), QString("An item."));
    QCOMPARE(m_item3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestItem)
