/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapWildListEncounter.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapWildList.h"
#include "../MapWildListEncounter.h"
#include "../Rules.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestMapWildListEncounter::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    MapWildList* wildList = m_game->newMap()->newWildList();
    
    m_encounter1 = wildList->newEncounter();
    m_encounter2 = wildList->newEncounter();
    m_encounter3 = wildList->newEncounter();
}

void TestMapWildListEncounter::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapWildListEncounter::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_encounter1);
    makeConnections(m_encounter2);
    makeConnections(m_encounter3);
}

void TestMapWildListEncounter::cleanup()
{
    closeConnections(m_encounter1);
    closeConnections(m_encounter2);
    closeConnections(m_encounter3);
    
    TestSigmodObject::cleanup();
}

void TestMapWildListEncounter::validation()
{
    m_encounter1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSpecies();
    
    m_encounter1->setSpecies(0);
    m_encounter1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxLevel(100);
    
    m_encounter1->setLevel(50);
    m_encounter1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->rules()->setMaxLevel(40);
    
    m_encounter1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapWildListEncounter::saving()
{
    QDomDocument xml = Object::xml(m_encounter1);
    QFile file("encounter.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapWildListEncounter::loading()
{
    QDomDocument xml;
    QFile file("encounter.xml");
    
    m_game->newSpecies();
    
    m_encounter1->setSpecies(1);
    m_encounter1->setLevel(5);
    m_encounter1->setWeight(10);
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_encounter1->load(xml.firstChildElement("MapWildListEncounter"));
    
    QCOMPARE(m_encounter1->species(), 0);
    QCOMPARE(m_encounter1->level(), 50);
    QCOMPARE(m_encounter1->weight(), 1);
}

void TestMapWildListEncounter::setSpecies()
{
    m_encounter2->setSpecies(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newSpecies();
    
    m_encounter2->setSpecies(2);
    m_encounter2->setSpecies(2);
    
    QCOMPARE(m_encounter2->species(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapWildListEncounter::setLevel()
{
    m_encounter2->setLevel(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_encounter2->setLevel(50);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_encounter2->setLevel(10);
    m_encounter2->setLevel(10);
    
    QCOMPARE(m_encounter2->level(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestMapWildListEncounter::setWeight()
{
    m_encounter2->setWeight(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_encounter2->setWeight(10);
    m_encounter2->setWeight(10);
    
    QCOMPARE(m_encounter2->weight(), 10);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapWildListEncounter::assignment()
{
    *m_encounter3 = *m_encounter2;
    
    QCOMPARE(m_encounter3->species(), 2);
    QCOMPARE(m_encounter3->level(), 10);
    QCOMPARE(m_encounter3->weight(), 10);
}

QTEST_APPLESS_MAIN(TestMapWildListEncounter)
