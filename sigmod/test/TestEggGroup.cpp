/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestEggGroup.h"

// Sigmod includes
#include "../EggGroup.h"
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestEggGroup::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_eggGroup1 = m_game->newEggGroup();
    m_eggGroup2 = m_game->newEggGroup();
    m_eggGroup3 = m_game->newEggGroup();
}

void TestEggGroup::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestEggGroup::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_eggGroup1);
    makeConnections(m_eggGroup2);
    makeConnections(m_eggGroup3);
}

void TestEggGroup::cleanup()
{
    closeConnections(m_eggGroup1);
    closeConnections(m_eggGroup2);
    closeConnections(m_eggGroup3);
    
    TestSigmodObject::cleanup();
}

void TestEggGroup::validation()
{
    m_eggGroup1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_eggGroup1->setName("Foo");
    m_eggGroup1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestEggGroup::saving()
{
    QDomDocument xml = Object::xml(m_eggGroup1);
    QFile file("eggGroup.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestEggGroup::loading()
{
    QDomDocument xml;
    QFile file("eggGroup.xml");
    
    m_eggGroup1->setName("Bar");
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_eggGroup1->load(xml.firstChildElement("EggGroup"));
    
    QCOMPARE(m_eggGroup1->name(), QString("Foo"));
}

void TestEggGroup::setName()
{
    m_eggGroup2->setName("Foo");
    m_eggGroup2->setName("Foo");
    
    QCOMPARE(m_eggGroup2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestEggGroup::assignment()
{
    *m_eggGroup3 = *m_eggGroup2;
    
    QCOMPARE(m_eggGroup3->name(), QString("Foo"));
}

QTEST_APPLESS_MAIN(TestEggGroup)
