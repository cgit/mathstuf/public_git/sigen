/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestWeather.h"

// Sigmod includes
#include "../Game.h"
#include "../Weather.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestWeather::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_weather1 = m_game->newWeather();
    m_weather2 = m_game->newWeather();
    m_weather3 = m_game->newWeather();
}

void TestWeather::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestWeather::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_weather1);
    makeConnections(m_weather2);
    makeConnections(m_weather3);
}

void TestWeather::cleanup()
{
    closeConnections(m_weather1);
    closeConnections(m_weather2);
    closeConnections(m_weather3);
    
    TestSigmodObject::cleanup();
}

void TestWeather::validation()
{
    m_weather1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_weather1->setName("Foo");
    m_weather1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestWeather::saving()
{
    QDomDocument xml = Object::xml(m_weather1);
    QFile file("weather.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestWeather::loading()
{
    QDomDocument xml;
    QFile file("weather.xml");
    
    m_weather1->setName("Bar");
    m_weather1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_weather1->load(xml.firstChildElement("Weather"));
    
    QCOMPARE(m_weather1->name(), QString("Foo"));
    QCOMPARE(m_weather1->script(), Script());
}

void TestWeather::setName()
{
    m_weather2->setName("Foo");
    m_weather2->setName("Foo");
    
    QCOMPARE(m_weather2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestWeather::setScript()
{
    m_weather2->setScript(Script("python", "import os"));
    m_weather2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_weather2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestWeather::assignment()
{
    *m_weather3 = *m_weather2;
    
    QCOMPARE(m_weather3->name(), QString("Foo"));
    QCOMPARE(m_weather3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestWeather)
