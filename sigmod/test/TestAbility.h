/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTABILITY
#define SIGMOD_TESTABILITY

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class Ability;
}

class TestAbility : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setName();
        void setPriority();
        void setDescription();
        void setBattleScript();
        void setWorldScript();
        void setPriorityScript();
        
        void assignment();
    private:
        Sigmod::Ability* m_ability1;
        Sigmod::Ability* m_ability2;
        Sigmod::Ability* m_ability3;
};

#endif
