/*
 * Copyright 2008 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTSIGMODOBJECT
#define SIGMOD_TESTSIGMODOBJECT

// Test includes
#include "Global.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QString>

// Forward declarations
namespace Sigmod
{
class Game;
class Object;
}

class SIGMODTEST_EXPORT TestSigmodObject : public QObject
{
    Q_OBJECT
    
    protected:
        void makeConnections(Sigmod::Object* object);
        void closeConnections(Sigmod::Object* object);
        
        Sigmod::Game* m_game;
        
        QList<QString> m_warnings;
        QList<QString> m_errors;
        int m_changedCount;
    protected slots:
        void warning(const QString& warning);
        void error(const QString& warning);
        void changed();
        
        virtual void initTestCase();
        virtual void cleanupTestCase();
        
        virtual void init();
        virtual void cleanup();
        
        virtual void validation() = 0;
        virtual void saving() = 0;
        virtual void loading() = 0;
        
        virtual void assignment() = 0;
};

#endif
