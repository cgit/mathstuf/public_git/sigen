/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestSound.h"

// Sigmod includes
#include "../Game.h"
#include "../Sound.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigmod;

void TestSound::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_sound1 = m_game->newSound();
    m_sound2 = m_game->newSound();
    m_sound3 = m_game->newSound();
    
    m_sound1->setType(Sound::Music);
}

void TestSound::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestSound::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_sound1);
    makeConnections(m_sound2);
    makeConnections(m_sound3);
}

void TestSound::cleanup()
{
    closeConnections(m_sound1);
    closeConnections(m_sound2);
    closeConnections(m_sound3);
    
    TestSigmodObject::cleanup();
}

void TestSound::validation()
{
    m_sound1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_sound1->setName("Foo");
    m_sound1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_sound1->setData("blah");
    m_sound1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestSound::saving()
{
    QDomDocument xml = Object::xml(m_sound1);
    QFile file("sound.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestSound::loading()
{
    QDomDocument xml;
    QFile file("sound.xml");
    
    m_sound1->setName("Bar");
    m_sound1->setType(Sound::SoundEffect);
    m_sound1->setData("blargh");
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_sound1->load(xml.firstChildElement("Sound"));
    
    QCOMPARE(m_sound1->name(), QString("Foo"));
    QCOMPARE(m_sound1->type(), Sound::Music);
    QCOMPARE(m_sound1->data(), QByteArray("blah"));
}

void TestSound::setName()
{
    m_sound2->setName("Foo");
    m_sound2->setName("Foo");
    
    QCOMPARE(m_sound2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSound::setType()
{
    m_sound2->setType(Sound::Music);
    m_sound2->setType(Sound::Music);
    
    QCOMPARE(m_sound2->type(), Sound::Music);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSound::setData()
{
    m_sound2->setData("blah");
    m_sound2->setData("blah");
    
    QCOMPARE(m_sound2->data(), QByteArray("blah"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestSound::assignment()
{
    *m_sound3 = *m_sound2;
    
    QCOMPARE(m_sound3->name(), QString("Foo"));
    QCOMPARE(m_sound3->type(), Sound::Music);
    QCOMPARE(m_sound3->data(), QByteArray("blah"));
}

QTEST_APPLESS_MAIN(TestSound)
