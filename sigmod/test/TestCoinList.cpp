/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestCoinList.h"

// Sigmod includes
#include "../CoinList.h"
#include "../CoinListItem.h"
#include "../Game.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestCoinList::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_coinList1 = m_game->newCoinList();
    m_coinList2 = m_game->newCoinList();
    m_coinList3 = m_game->newCoinList();
}

void TestCoinList::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestCoinList::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_coinList1);
    makeConnections(m_coinList2);
    makeConnections(m_coinList3);
}

void TestCoinList::cleanup()
{
    closeConnections(m_coinList1);
    closeConnections(m_coinList2);
    closeConnections(m_coinList3);
    
    TestSigmodObject::cleanup();
}

void TestCoinList::validation()
{
    m_coinList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_coinList1->setName("Foo");
    m_coinList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_coinList1->newItem();
    m_coinList1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
}

void TestCoinList::saving()
{
    QDomDocument xml = Object::xml(m_coinList1);
    QFile file("coinList.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestCoinList::loading()
{
    QDomDocument xml;
    QFile file("coinList.xml");
    
    m_coinList1->setName("Bar");
    m_coinList1->setScript(Script("python", "import os"));
    m_coinList1->newItem();
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_coinList1->load(xml.firstChildElement("CoinList"));
    
    QCOMPARE(m_coinList1->name(), QString("Foo"));
    QCOMPARE(m_coinList1->script(), Script());
    QCOMPARE(m_coinList1->itemCount(), 1);
}

void TestCoinList::setName()
{
    m_coinList2->setName("Foo");
    m_coinList2->setName("Foo");
    
    QCOMPARE(m_coinList2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinList::setScript()
{
    m_coinList2->setScript(Script("python", "import os"));
    m_coinList2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_coinList2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinList::items()
{
    QCOMPARE(m_coinList2->newItem()->id(), 0);
    QCOMPARE(m_coinList2->newItem()->id(), 1);
    QCOMPARE(m_coinList2->newItem()->id(), 2);
    
    QCOMPARE(m_coinList2->itemCount(), 3);
    
    m_coinList2->deleteItem(0);
    
    QCOMPARE(m_coinList2->itemCount(), 2);
    
    QCOMPARE(m_coinList2->newItem()->id(), 0);
    
    QCOMPARE(m_coinList2->itemIndex(0), 2);
    
    m_coinList2->deleteItemById(1);
    
    QCOMPARE(m_coinList2->itemIndex(0), 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestCoinList::assignment()
{
    *m_coinList3 = *m_coinList2;
    
    QCOMPARE(m_coinList3->name(), QString("Foo"));
    QCOMPARE(m_coinList3->script(), Script("python", "import os"));
    QCOMPARE(m_coinList3->itemCount(), 2);
}

QTEST_APPLESS_MAIN(TestCoinList)
