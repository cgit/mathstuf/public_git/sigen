/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestMapWarp.h"

// Sigmod includes
#include "../Game.h"
#include "../Map.h"
#include "../MapWarp.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestMapWarp::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    Map* map = m_game->newMap();
    
    m_warp1 = map->newWarp();
    m_warp2 = map->newWarp();
    m_warp3 = map->newWarp();
}

void TestMapWarp::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestMapWarp::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_warp1);
    makeConnections(m_warp2);
    makeConnections(m_warp3);
}

void TestMapWarp::cleanup()
{
    closeConnections(m_warp1);
    closeConnections(m_warp2);
    closeConnections(m_warp3);
    
    TestSigmodObject::cleanup();
}

void TestMapWarp::validation()
{
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_warp1->setName("Foo");
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 7);
    
    m_game->map(0)->setWidth(20);
    m_game->map(0)->setHeight(20);
    
    m_warp1->setArea(QRect(0, 0, 5, 5));
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 9);
    
    m_game->map(0)->setWidth(3);
    m_game->map(0)->setHeight(3);
    
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 12);
    
    m_game->map(0)->setWidth(20);
    m_game->map(0)->setHeight(20);
    
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 14);
    
    m_warp1->setToMap(0);
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 15);
    
    m_warp1->setToWarp(2);
    m_warp1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 15);
}

void TestMapWarp::saving()
{
    QDomDocument xml = Object::xml(m_warp1);
    QFile file("mapWarp.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestMapWarp::loading()
{
    QDomDocument xml;
    QFile file("mapWarp.xml");
    
    m_game->newMap()->newWarp();
    
    m_warp1->setName("Bar");
    m_warp1->setArea(QRect(1, 1, 3, 3));
    m_warp1->setToMap(1);
    m_warp1->setToWarp(0);
    m_warp1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_warp1->load(xml.firstChildElement("MapWarp"));
    
    QCOMPARE(m_warp1->name(), QString("Foo"));
    QCOMPARE(m_warp1->area(), QRect(0, 0, 5, 5));
    QCOMPARE(m_warp1->type(), MapWarp::Door);
    QCOMPARE(m_warp1->toMap(), 0);
    QCOMPARE(m_warp1->toWarp(), 2);
    QCOMPARE(m_warp1->script(), Script());
}

void TestMapWarp::setName()
{
    m_warp2->setName("Foo");
    m_warp2->setName("Foo");
    
    QCOMPARE(m_warp2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapWarp::setArea()
{
    m_warp2->setArea(QRect(-1, 0, 5, 5));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_warp2->setArea(QRect(0, -1, 5, 5));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_warp2->setArea(QRect(0, 0, 25, 5));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 3);
    
    m_warp2->setArea(QRect(0, 0, 5, 25));
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
    
    m_warp2->setArea(QRect(0, 0, 5, 5));
    m_warp2->setArea(QRect(0, 0, 5, 5));
    
    QCOMPARE(m_warp2->area(), QRect(0, 0, 5, 5));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 4);
}

void TestMapWarp::setToMap()
{
    m_warp2->setToMap(2);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->newMap();
    
    m_warp2->setToMap(2);
    m_warp2->setToMap(2);
    
    QCOMPARE(m_warp2->toMap(), 2);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapWarp::setToWarp()
{
    m_warp2->setToWarp(0);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_game->map(2)->newWarp();
    
    m_warp2->setToWarp(0);
    m_warp2->setToWarp(0);
    
    QCOMPARE(m_warp2->toWarp(), 0);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestMapWarp::setScript()
{
    m_warp2->setScript(Script("python", "import os"));
    m_warp2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_warp2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestMapWarp::assignment()
{
    *m_warp3 = *m_warp2;
    
    QCOMPARE(m_warp3->name(), QString("Foo"));
    QCOMPARE(m_warp3->area(), QRect(0, 0, 5, 5));
    QCOMPARE(m_warp3->toMap(), 2);
    QCOMPARE(m_warp3->toWarp(), 0);
    QCOMPARE(m_warp3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestMapWarp)
