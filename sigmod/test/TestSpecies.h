/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_TESTSPECIES
#define SIGMOD_TESTSPECIES

// Test includes
#include "TestSigmodObject.h"

// Forward declarations
namespace Sigmod
{
class Species;
}

class TestSpecies : public TestSigmodObject
{
    Q_OBJECT
    
    private slots:
        void initTestCase();
        void cleanupTestCase();
        
        void init();
        void cleanup();
        
        void validation();
        void saving();
        void loading();
        
        void setName();
        void setBaseStat();
        void setEffortValue();
        void setGrowth();
        void setExperienceValue();
        void setCatchValue();
        void setMaxHoldWeight();
        void setRunChance();
        void setFleeChance();
        void setItemChance();
        void setEncyclopediaNumber();
        void setWeight();
        void setHeight();
        void setEncyclopediaEntry();
        void setFrontMaleSprite();
        void setBackMaleSprite();
        void setFrontFemaleSprite();
        void setBackFemaleSprite();
        void setGenderFactor();
        void setSkin();
        void setEggSpecies();
        void setEggSteps();
        void setType();
        void setEggGroup();
        void setEvolution();
        void setAbility();
        void setItem();
        
        void moves();
        
        void assignment();
    private:
        Sigmod::Species* m_species1;
        Sigmod::Species* m_species2;
        Sigmod::Species* m_species3;
};

#endif
