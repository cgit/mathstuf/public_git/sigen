/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestTime.h"

// Sigmod includes
#include "../Game.h"
#include "../Time.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestTime::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_time1 = m_game->newTime();
    m_time2 = m_game->newTime();
    m_time3 = m_game->newTime();
}

void TestTime::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestTime::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_time1);
    makeConnections(m_time2);
    makeConnections(m_time3);
}

void TestTime::cleanup()
{
    closeConnections(m_time1);
    closeConnections(m_time2);
    closeConnections(m_time3);
    
    TestSigmodObject::cleanup();
}

void TestTime::validation()
{
    m_time1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_time1->setName("Foo");
    m_time1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestTime::saving()
{
    QDomDocument xml = Object::xml(m_time1);
    QFile file("time.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestTime::loading()
{
    QDomDocument xml;
    QFile file("time.xml");
    
    m_time1->setName("Bar");
    m_time1->setHour(12);
    m_time1->setMinute(30);
    m_time1->setScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_time1->load(xml.firstChildElement("Time"));
    
    QCOMPARE(m_time1->name(), QString("Foo"));
    QCOMPARE(m_time1->hour(), 0);
    QCOMPARE(m_time1->minute(), 0);
    QCOMPARE(m_time1->script(), Script());
}

void TestTime::setName()
{
    m_time2->setName("Foo");
    m_time2->setName("Foo");
    
    QCOMPARE(m_time2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTime::setHour()
{
    m_time2->setHour(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_time2->setHour(24);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_time2->setHour(12);
    m_time2->setHour(12);
    
    QCOMPARE(m_time2->hour(), 12);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestTime::setMinute()
{
    m_time2->setMinute(-1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_time2->setMinute(60);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
    
    m_time2->setMinute(30);
    m_time2->setMinute(30);
    
    QCOMPARE(m_time2->minute(), 30);
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 2);
}

void TestTime::setScript()
{
    m_time2->setScript(Script("python", "import os"));
    m_time2->setScript(Script("python", "import os"));
    
    QCOMPARE(m_time2->script(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestTime::assignment()
{
    *m_time3 = *m_time2;
    
    QCOMPARE(m_time3->name(), QString("Foo"));
    QCOMPARE(m_time3->hour(), 12);
    QCOMPARE(m_time3->minute(), 30);
    QCOMPARE(m_time3->script(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestTime)
