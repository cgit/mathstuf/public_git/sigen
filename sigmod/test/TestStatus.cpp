/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "TestStatus.h"

// Sigmod includes
#include "../Game.h"
#include "../Status.h"

// Qt includes
#include <QtCore/QFile>
#include <QtTest/QTest>

using namespace Sigcore;
using namespace Sigmod;

void TestStatus::initTestCase()
{
    TestSigmodObject::initTestCase();
    
    m_status1 = m_game->newStatus();
    m_status2 = m_game->newStatus();
    m_status3 = m_game->newStatus();
}

void TestStatus::cleanupTestCase()
{
    TestSigmodObject::cleanupTestCase();
}

void TestStatus::init()
{
    TestSigmodObject::init();
    
    makeConnections(m_status1);
    makeConnections(m_status2);
    makeConnections(m_status3);
}

void TestStatus::cleanup()
{
    closeConnections(m_status1);
    closeConnections(m_status2);
    closeConnections(m_status3);
    
    TestSigmodObject::cleanup();
}

void TestStatus::validation()
{
    m_status1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
    
    m_status1->setName("Foo");
    m_status1->validate();
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 1);
}

void TestStatus::saving()
{
    QDomDocument xml = Object::xml(m_status1);
    QFile file("status.xml");
    
    QVERIFY(file.open(QIODevice::WriteOnly));
    file.write(xml.toByteArray());
    file.close();
}

void TestStatus::loading()
{
    QDomDocument xml;
    QFile file("status.xml");
    
    m_status1->setName("Bar");
    m_status1->setBattleScript(Script("python", "import os"));
    m_status1->setWorldScript(Script("python", "import os"));
    
    QVERIFY(file.open(QIODevice::ReadOnly));
    QVERIFY(xml.setContent(&file));
    m_status1->load(xml.firstChildElement("Status"));
    
    QCOMPARE(m_status1->name(), QString("Foo"));
    QCOMPARE(m_status1->battleScript(), Script());
    QCOMPARE(m_status1->worldScript(), Script());
}

void TestStatus::setName()
{
    m_status2->setName("Foo");
    m_status2->setName("Foo");
    
    QCOMPARE(m_status2->name(), QString("Foo"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestStatus::setBattleScript()
{
    m_status2->setBattleScript(Script("python", "import os"));
    m_status2->setBattleScript(Script("python", "import os"));
    
    QCOMPARE(m_status2->battleScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestStatus::setWorldScript()
{
    m_status2->setWorldScript(Script("python", "import os"));
    m_status2->setWorldScript(Script("python", "import os"));
    
    QCOMPARE(m_status2->worldScript(), Script("python", "import os"));
    
    QCOMPARE(m_changedCount, 1);
    
    QCOMPARE(m_warnings.size(), 0);
    QCOMPARE(m_errors.size(), 0);
}

void TestStatus::assignment()
{
    *m_status3 = *m_status2;
    
    QCOMPARE(m_status3->name(), QString("Foo"));
    QCOMPARE(m_status3->battleScript(), Script("python", "import os"));
    QCOMPARE(m_status3->worldScript(), Script("python", "import os"));
}

QTEST_APPLESS_MAIN(TestStatus)
