/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Ability.cpp
 */

// Header include
#include "Ability.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Ability::Ability(const Ability& ability) :
        Object(ability.parent(), ability.id())
{
    *this = ability;
}

Ability::Ability(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_priority(0),
        m_description(""),
        m_battleScript("", ""),
        m_worldScript("", ""),
        m_priorityScript("", "")
{
}

Ability::Ability(const Ability& ability, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = ability;
}

Ability::Ability(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Ability::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void Ability::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(priority);
    LOAD(description);
    LOAD(battleScript);
    LOAD(worldScript);
    LOAD(priorityScript);
}

QDomElement Ability::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(priority);
    SAVE(description);
    SAVE(battleScript);
    SAVE(worldScript);
    SAVE(priorityScript);
    return xml;
}

SETTER(Ability, QString&, Name, name)
SETTER(Ability, int, Priority, priority)
SETTER(Ability, QString&, Description, description)
SETTER(Ability, Script&, BattleScript, battleScript)
SETTER(Ability, Script&, WorldScript, worldScript)
SETTER(Ability, Script&, PriorityScript, priorityScript)

GETTER(Ability, QString, name)
GETTER(Ability, int, priority)
GETTER(Ability, QString, description)
GETTER(Ability, Script, battleScript)
GETTER(Ability, Script, worldScript)
GETTER(Ability, Script, priorityScript)

CHECK(Ability, QString&, name)
CHECK(Ability, int, priority)
CHECK(Ability, QString&, description)
CHECK(Ability, Script&, battleScript)
CHECK(Ability, Script&, worldScript)
CHECK(Ability, Script&, priorityScript)

Ability& Ability::operator=(const Ability& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(priority);
    COPY(description);
    COPY(battleScript);
    COPY(worldScript);
    COPY(priorityScript);
    return *this;
}
