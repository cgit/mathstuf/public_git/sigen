/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Sprite.h
 */

#ifndef SIGMOD_SPRITE
#define SIGMOD_SPRITE

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QByteArray>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Sprite Sprite.h sigmod/Sprite.h
 * \brief Class for holding an image.
 * 
 * This class is used to hold an image and give it a name for easy reference.
 */
class SIGMOD_EXPORT Sprite : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param sprite The sprite to copy.
         */
        Sprite(const Sprite& sprite);
        /**
         * Create a new sprite belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the sprite.
         * \param id The id number for the sprite.
         */
        Sprite(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p sprite as a child of \p parent with id \p id.
         * 
         * \param sprite The sprite to copy the data from.
         * \param parent The parent of the sprite.
         * \param id The id number for the sprite.
         */
        Sprite(const Sprite& sprite, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the sprite.
         * \param id The id number for the sprite.
         */
        Sprite(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the sprite's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the ability in XML format.
         * 
         * \return The XML structure representing the sprite.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the sprite. This is only used internally.
         * 
         * \param name The name of the sprite.
         */
        void setName(const QString& name);
        /**
         * Sets the data for the sprite. When stored, it will be base64 encoded to ensure that no
         * non-printable characters appear in the sigmod file.
         * 
         * \param sprite The data for the sprite.
         */
        void setSprite(const QByteArray& sprite);
        
        /**
         * \sa setName
         * 
         * \return The name of the skin.
         */
        QString name() const;
        /**
         * \sa setSprite
         * 
         * \return The data for the sprite.
         */
        QByteArray sprite() const;
        
        bool nameCheck(const QString& name) const;
        bool spriteCheck(const QByteArray& sprite) const;
        
        Sprite& operator=(const Sprite& rhs);
    private:
        QString m_name;
        QByteArray m_sprite;
};
}

#endif
