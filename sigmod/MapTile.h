/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapTile.h
 */

#ifndef SIGMOD_MAPTILE
#define SIGMOD_MAPTILE

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QPoint>

namespace Sigmod
{
class Map;

/**
 * \class Sigmod::MapTile MapTile.h sigmod/MapTile.h
 * \brief Class describing a tile on a map.
 * 
 * All tiles for a map must be declared with an instance of this class.
 */
class SIGMOD_EXPORT MapTile : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param tile The tile to copy.
         */
        MapTile(const MapTile& tile);
        /**
         * Create a new tile belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        MapTile(const Map* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p tile as a child of \p parent with id \p id.
         * 
         * \param tile The tile to copy the data from.
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        MapTile(const MapTile& tile, const Map* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        MapTile(const QDomElement& xml, const Map* parent, const int id = -1);
        
        /**
         * Check to make sure the tile's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the tile in XML format.
         * 
         * \return The XML structure representing the tile.
         */
        QDomElement save() const;
        
        /**
         * Sets the id of the tile that represents the tile.
         * 
         * \param tile The id of the tile.
         */
        void setTile(const int tile);
        /**
         * Sets the position of the tile on the map.
         * 
         * \param position The position of the tile.
         */
        void setPosition(const QPoint& position);
        /**
         * Sets the z-index of the tile on the map. Higher numbers are drawn on top
         * of lower numbers.
         * 
         * \param zIndex The z-index of the tile.
         */
        void setZIndex(const int zIndex);
        
        /**
         * \sa setTile
         * 
         * \return The id of the tile used for the image.
         */
        int tile() const;
        /**
         * \sa setPosition
         * 
         * \return The position of the tile.
         */
        QPoint position() const;
        /**
         * \sa setZIndex
         * 
         * \return The z-index of the tile.
         */
        int zIndex() const;
        
        bool tileCheck(const int tile) const;
        bool positionCheck(const QPoint& position) const;
        bool zIndexCheck(const int zIndex) const;
        
        MapTile& operator=(const MapTile& rhs);
    private:
        int m_tile;
        QPoint m_position;
        int m_zIndex;
};
}

#endif
