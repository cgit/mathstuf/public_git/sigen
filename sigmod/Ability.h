/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Ability.h
 */

#ifndef SIGMOD_ABILITY
#define SIGMOD_ABILITY

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Ability Ability.h sigmod/Ability.h
 * \brief Class describing an ability.
 * 
 * Abilities typically work as longer-lasting effects during a battle. Some common uses are:
 * 
 *  - Nullifying status effects
 *  - Manipulating the weather
 *  - Providing support in times of need
 *  - Added status effects under different conditions
 * 
 * They can also function as helpers in the world by doing things such as:
 * 
 *  - Changing the chances of a random encounter
 *  - Increasing the chances of different leveled enemies
 *  - Make trainers ignore the player
 * 
 * Abilities should be well balanced; they should not do too much, yet still be useful.
 */
class SIGMOD_EXPORT Ability : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param ability The ability to copy.
         */
        Ability(const Ability& ability);
        /**
         * Create a new ability belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the ability.
         * \param id The id number for the ability.
         */
        Ability(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p ability as a child of \p parent with id \p id.
         * 
         * \param ability The ability to copy the data from.
         * \param parent The parent of the ability.
         * \param id The id number for the ability.
         */
        Ability(const Ability& ability, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the ability.
         * \param id The id number for the ability.
         */
        Ability(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the ability's values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the ability in XML format.
         * 
         * \return The XML structure representing the ability.
         */
        QDomElement save() const;
        
        /**
         * Set the name of the ability used in the game.
         * 
         * \param name The name of the ability.
         */
        void setName(const QString& name);
        /**
         * Set the priority of the ability. The priority is used by the battle engine determine which abilities get executed first.
         * Lower values go before higher values. Use the \link Ability::setPriorityScript priority script \endlink for how to modify
         * it based on the environment in the arena.
         * 
         * \param priority The priority of the ability.
         */
        void setPriority(const int priority);
        /**
         * Set the description of the ability. The description should be a snippet of text describing the ability to the player.
         * 
         * \param description The description of the ability.
         */
        void setDescription(const QString& description);
        /**
         * Set the script for the ability when in battle. This script defines the behavior of the ability when in a battle.
         * The following objects are available to the script:
         * 
         *  - \b ability -- The \link Sigscript::AbilityWrapper ability \endlink which the script is using.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which owns the instance of the ability.
         *  - \b client -- The \link Sigencore::Client client \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the ability is being used in.
         * 
         * \param battleScript The script for the ability when in battle.
         */
        void setBattleScript(const Sigcore::Script& battleScript);
        /**
         * Set the script for the ability when on the world map. This script defines the behavior of the ability when on the world map.
         * The following objects are available to the script:
         * 
         *  - \b ability -- The \link Sigscript::AbilityWrapper ability \endlink which the script is using.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which owns the instance of the ability.
         *  - \b client -- The \link Sigencore::Client client \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the player is on.
         * 
         * \param worldScript The script for the ability when on the world map.
         */
        void setWorldScript(const Sigcore::Script& worldScript);
        /**
         * Set the script that determines the priority of the ability. This script allows the ability to change its priority based on what
         * is happening in the \link Sigencore::Arena arena \endlink. The following objects are available to the script:
         * 
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which owns the instance of the ability.
         *  - \b client -- The \link Sigencore::Client client \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the ability is being used in.
         * 
         * To override the priority given by the ability, set the \p ability-priority-%name (where "%name" is the name of the ability)
         * value in \p owner. Setting it in the \link Sigscript::SigmodWrapper sigmod wrapper \endlink will cause all instances of the
         * ability to have the new priority.
         * 
         * \param priorityScript The script that determines the priority of the ability.
         */
        void setPriorityScript(const Sigcore::Script& priorityScript);
        
        /**
         * \sa setName
         * 
         * \return The name of the ability.
         */
        QString name() const;
        /**
         * \sa setPriority
         * 
         * \return The priority of the ability.
         */
        int priority() const;
        /**
         * \sa setDescription
         * 
         * \return The description of the ability.
         */
        QString description() const;
        /**
         * \sa setBattleScript
         * 
         * \return The script that controls the ability in battle.
         */
        Sigcore::Script battleScript() const;
        /**
         * \sa setWorldScript
         * 
         * \return The script that controls the ability on the world map.
         */
        Sigcore::Script worldScript() const;
        /**
         * \sa setPriorityScript
         * 
         * \return The script that changes the priority when necessary.
         */
        Sigcore::Script priorityScript() const;
        
        bool nameCheck(const QString& name) const;
        bool priorityCheck(const int priority) const;
        bool descriptionCheck(const QString& description) const;
        bool battleScriptCheck(const Sigcore::Script& battleScript) const;
        bool worldScriptCheck(const Sigcore::Script& worldScript) const;
        bool priorityScriptCheck(const Sigcore::Script& priorityScript) const;
        
        Ability& operator=(const Ability& rhs);
    private:
        QString m_name;
        int m_priority;
        QString m_description;
        Sigcore::Script m_battleScript;
        Sigcore::Script m_worldScript;
        Sigcore::Script m_priorityScript;
};
}

#endif
