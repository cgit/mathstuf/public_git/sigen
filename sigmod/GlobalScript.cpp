/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/GlobalScript.cpp
 */

// Header include
#include "GlobalScript.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

GlobalScript::GlobalScript(const GlobalScript& globalScript) :
        Object(globalScript.parent(), globalScript.id())
{
    *this = globalScript;
}

GlobalScript::GlobalScript(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_script("", "")
{
}

GlobalScript::GlobalScript(const GlobalScript& globalScript, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = globalScript;
}

GlobalScript::GlobalScript(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void GlobalScript::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void GlobalScript::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(script);
}

QDomElement GlobalScript::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(script);
    return xml;
}

SETTER(GlobalScript, QString&, Name, name)
SETTER(GlobalScript, Script&, Script, script)

GETTER(GlobalScript, QString, name)
GETTER(GlobalScript, Script, script)

CHECK(GlobalScript, QString&, name)
CHECK(GlobalScript, Script&, script)

GlobalScript& GlobalScript::operator=(const GlobalScript& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(script);
    return *this;
}
