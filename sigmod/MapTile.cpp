/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapTile.cpp
 */

// Header include
#include "MapTile.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Map.h"
#include "Tile.h"

using namespace Sigmod;

MapTile::MapTile(const MapTile& tile) :
        Object(tile.parent(), tile.id())
{
    *this = tile;
}

MapTile::MapTile(const Map* parent, const int id) :
        Object(parent, id),
        m_tile(-1),
        m_position(0, 0),
        m_zIndex(0)
{
}

MapTile::MapTile(const MapTile& tile, const Map* parent, const int id) :
        Object(parent, id)
{
    *this = tile;
}

MapTile::MapTile(const QDomElement& xml, const Map* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void MapTile::validate()
{
    TEST_BEGIN();
    TEST(tile);
    TEST(position);
    TEST_END();
}

void MapTile::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(tile);
    LOAD(position);
    LOAD(zIndex);
}

QDomElement MapTile::save() const
{
    SAVE_CREATE();
    SAVE(tile);
    SAVE(position);
    SAVE(zIndex);
    return xml;
}

SETTER(MapTile, int, Tile, tile)
SETTER(MapTile, QPoint&, Position, position)
SETTER(MapTile, int, ZIndex, zIndex)

GETTER(MapTile, int, tile)
GETTER(MapTile, QPoint, position)
GETTER(MapTile, int, zIndex)

CHECK_INDEX(MapTile, int, tile, game(), tile)
CHECK_BEGIN(MapTile, QPoint&, position)
    const Map* map = qobject_cast<const Map*>(parent());
    TBOUNDS_MOD(position_x, 0, map->width() - 1, position.x())
    TBOUNDS_MOD(position_y, 0, map->height() - 1, position.y())
CHECK_END()
CHECK(MapTile, int, zIndex)

MapTile& MapTile::operator=(const MapTile& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(tile);
    COPY(position);
    COPY(zIndex);
    return *this;
}
