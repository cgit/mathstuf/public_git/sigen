/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapWildListEncounter.cpp
 */

// Header include
#include "MapWildListEncounter.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "MapWildList.h"
#include "Rules.h"
#include "Species.h"

using namespace Sigmod;

MapWildListEncounter::MapWildListEncounter(const MapWildListEncounter& encounter) :
        Object(encounter.parent(), encounter.id())
{
    *this = encounter;
}

MapWildListEncounter::MapWildListEncounter(const MapWildList* parent, const int id) :
        Object(parent, id),
        m_species(INT_MAX),
        m_level(1),
        m_weight(1)
{
}

MapWildListEncounter::MapWildListEncounter(const MapWildListEncounter& encounter, const MapWildList* parent, const int id) :
        Object(parent, id)
{
    *this = encounter;
}

MapWildListEncounter::MapWildListEncounter(const QDomElement& xml, const MapWildList* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void MapWildListEncounter::validate()
{
    TEST_BEGIN();
    TEST(species);
    TEST(level);
    TEST(weight);
    TEST_END();
}

void MapWildListEncounter::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(species);
    LOAD(level);
    LOAD(weight);
}

QDomElement MapWildListEncounter::save() const
{
    SAVE_CREATE();
    SAVE(species);
    SAVE(level);
    SAVE(weight);
    return xml;
}

SETTER(MapWildListEncounter, int, Species, species)
SETTER(MapWildListEncounter, int, Level, level)
SETTER(MapWildListEncounter, int, Weight, weight)

GETTER(MapWildListEncounter, int, species)
GETTER(MapWildListEncounter, int, level)
GETTER(MapWildListEncounter, int, weight)

CHECK_INDEX(MapWildListEncounter, int, species, game(), species)
CHECK_BOUNDS(MapWildListEncounter, int, level, 1, game()->rules()->maxLevel())
CHECK_BOUNDS(MapWildListEncounter, int, weight, 1, INT_MAX)

MapWildListEncounter& MapWildListEncounter::operator=(const MapWildListEncounter& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(species);
    COPY(level);
    COPY(weight);
    return *this;
}
