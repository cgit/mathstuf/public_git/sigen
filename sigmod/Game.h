/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Game.h
 */

#ifndef SIGMOD_GAME
#define SIGMOD_GAME

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Matrix.h>
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QMap>
#include <QtCore/QPoint>

namespace Sigmod
{
// Forward declarations
class Ability;
class Author;
class Badge;
class CoinList;
class EggGroup;
class GlobalScript;
class Item;
class ItemType;
class Map;
class Move;
class Nature;
class Rules;
class Skin;
class Sound;
class Species;
class Sprite;
class Status;
class Store;
class Tile;
class Time;
class Trainer;
class Type;
class Weather;

/**
 * \class Sigmod::Game Game.h sigmod/Game.h
 * \brief Main class that describes a Sigmod.
 * 
 * The Game class describes an entire set of objects for use with the game engine.
 * Games will refuse to run if the \link Game::validate validate \endlink method
 * returns any errors. Warnings will not stop the engine from running the Sigmod,
 * but they should be minimized.
 */
class SIGMOD_EXPORT Game : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Default constructor.
         */
        Game();
        /**
         * Copy constructor.
         * 
         * \param sigmod The game to copy.
         */
        Game(const Game& game);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         */
        Game(const QDomElement& xml);
        /**
         * Destructor.
         */
        ~Game();
        
        /**
         * Check to make sure the game is valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the badge in XML format.
         * 
         * \return The XML structure representing the badge.
         */
        QDomElement save() const;
        
        void setTitle(const QString& title);
        void setVersion(const QString& version);
        void setDescription(const QString& description);
        void setSinglePlayer(const bool singlePlayer);
        void setStartScript(const Sigcore::Script& startScript);
        void setTypechart(const int attack, const int defense, const Sigcore::Fraction& multiplier);
        void setMapPosition(const int map, const QPoint& position, const bool remove = false);
        void setRules(const Rules& rules);
        void setRules(const QDomElement& xml);
        
        QString title() const;
        QString version() const;
        QString description() const;
        bool singlePlayer() const;
        Sigcore::Script startScript() const;
        const Sigcore::Matrix<Sigcore::Fraction>* typechart() const;
        Sigcore::Matrix<Sigcore::Fraction>* typechart();
        Sigcore::Fraction typechart(const int attack, const int defense) const;
        QPoint mapPosition(const int map) const;
        QMap<int, QPoint> mapPosition() const;
        const Rules* rules() const;
        Rules* rules();
        
        bool titleCheck(const QString& title) const;
        bool versionCheck(const QString& version) const;
        bool descriptionCheck(const QString& description) const;
        bool singlePlayerCheck(const bool singlePlayer) const;
        bool startScriptCheck(const Sigcore::Script& startScript) const;
        bool mapPositionCheck(const QPoint& position) const;
        bool typechartCheck(const Sigcore::Fraction& multiplier) const;
        
        const Ability* ability(const int index) const;
        Ability* ability(const int index);
        const Ability* abilityById(const int id) const;
        Ability* abilityById(const int id);
        int abilityIndex(const int id) const;
        int abilityCount() const;
        Ability* newAbility();
        Ability* newAbility(const QDomElement& xml);
        Ability* newAbility(const Ability& ability);
        void deleteAbility(const int index);
        void deleteAbilityById(const int id);
        
        const Author* author(const int index) const;
        Author* author(const int index);
        const Author* authorById(const int id) const;
        Author* authorById(const int id);
        int authorIndex(const int id) const;
        int authorCount() const;
        Author* newAuthor();
        Author* newAuthor(const QDomElement& xml);
        Author* newAuthor(const Author& author);
        void deleteAuthor(const int index);
        void deleteAuthorById(const int id);
        
        const Badge* badge(const int index) const;
        Badge* badge(const int index);
        const Badge* badgeById(const int id) const;
        Badge* badgeById(const int id);
        int badgeIndex(const int id) const;
        int badgeCount() const;
        Badge* newBadge();
        Badge* newBadge(const QDomElement& xml);
        Badge* newBadge(const Badge& badge);
        void deleteBadge(const int index);
        void deleteBadgeById(const int id);
        
        const CoinList* coinList(const int index) const;
        CoinList* coinList(const int index);
        const CoinList* coinListById(const int id) const;
        CoinList* coinListById(const int id);
        int coinListIndex(const int id) const;
        int coinListCount() const;
        CoinList* newCoinList();
        CoinList* newCoinList(const QDomElement& xml);
        CoinList* newCoinList(const CoinList& coinList);
        void deleteCoinList(const int index);
        void deleteCoinListById(const int id);
        
        const EggGroup* eggGroup(const int index) const;
        EggGroup* eggGroup(const int index);
        const EggGroup* eggGroupById(const int id) const;
        EggGroup* eggGroupById(const int id);
        int eggGroupIndex(const int id) const;
        int eggGroupCount() const;
        EggGroup* newEggGroup();
        EggGroup* newEggGroup(const QDomElement& xml);
        EggGroup* newEggGroup(const EggGroup& eggGroup);
        void deleteEggGroup(const int index);
        void deleteEggGroupById(const int id);
        
        const GlobalScript* globalScript(const int index) const;
        GlobalScript* globalScript(const int index);
        const GlobalScript* globalScriptById(const int id) const;
        GlobalScript* globalScriptById(const int id);
        int globalScriptIndex(const int id) const;
        int globalScriptCount() const;
        GlobalScript* newGlobalScript();
        GlobalScript* newGlobalScript(const QDomElement& xml);
        GlobalScript* newGlobalScript(const GlobalScript& globalScript);
        void deleteGlobalScript(const int index);
        void deleteGlobalScriptById(const int id);
        
        const Item* item(const int index) const;
        Item* item(const int index);
        const Item* itemById(const int id) const;
        Item* itemById(const int id);
        int itemIndex(const int id) const;
        int itemCount() const;
        Item* newItem();
        Item* newItem(const QDomElement& xml);
        Item* newItem(const Item& item);
        void deleteItem(const int index);
        void deleteItemById(const int id);
        
        const ItemType* itemType(const int index) const;
        ItemType* itemType(const int index);
        const ItemType* itemTypeById(const int id) const;
        ItemType* itemTypeById(const int id);
        int itemTypeIndex(const int id) const;
        int itemTypeCount() const;
        ItemType* newItemType();
        ItemType* newItemType(const QDomElement& xml);
        ItemType* newItemType(const ItemType& itemType);
        void deleteItemType(const int index);
        void deleteItemTypeById(const int id);
        
        const Map* map(const int index) const;
        Map* map(const int index);
        const Map* mapById(const int id) const;
        Map* mapById(const int id);
        int mapIndex(const int id) const;
        int mapCount() const;
        Map* newMap();
        Map* newMap(const QDomElement& xml);
        Map* newMap(const Map& map);
        void deleteMap(const int index);
        void deleteMapById(const int id);
        
        const Move* move(const int index) const;
        Move* move(const int index);
        const Move* moveById(const int id) const;
        Move* moveById(const int id);
        int moveIndex(const int id) const;
        int moveCount() const;
        Move* newMove();
        Move* newMove(const QDomElement& xml);
        Move* newMove(const Move& move);
        void deleteMove(const int index);
        void deleteMoveById(const int id);
        
        const Nature* nature(const int index) const;
        Nature* nature(const int index);
        const Nature* natureById(const int id) const;
        Nature* natureById(const int id);
        int natureIndex(const int id) const;
        int natureCount() const;
        Nature* newNature();
        Nature* newNature(const QDomElement& xml);
        Nature* newNature(const Nature& nature);
        void deleteNature(const int index);
        void deleteNatureById(const int id);
        
        const Skin* skin(const int index) const;
        Skin* skin(const int index);
        const Skin* skinById(const int id) const;
        Skin* skinById(const int id);
        int skinIndex(const int id) const;
        int skinCount() const;
        Skin* newSkin();
        Skin* newSkin(const QDomElement& xml);
        Skin* newSkin(const Skin& status);
        void deleteSkin(const int index);
        void deleteSkinById(const int id);
        
        const Sound* sound(const int index) const;
        Sound* sound(const int index);
        const Sound* soundById(const int id) const;
        Sound* soundById(const int id);
        int soundIndex(const int id) const;
        int soundCount() const;
        Sound* newSound();
        Sound* newSound(const QDomElement& xml);
        Sound* newSound(const Sound& sound);
        void deleteSound(const int index);
        void deleteSoundById(const int id);
        
        const Species* species(const int index) const;
        Species* species(const int index);
        const Species* speciesById(const int id) const;
        Species* speciesById(const int id);
        int speciesIndex(const int id) const;
        int speciesCount() const;
        Species* newSpecies();
        Species* newSpecies(const QDomElement& xml);
        Species* newSpecies(const Species& species);
        void deleteSpecies(const int index);
        void deleteSpeciesById(const int id);
        
        const Sprite* sprite(const int index) const;
        Sprite* sprite(const int index);
        const Sprite* spriteById(const int id) const;
        Sprite* spriteById(const int id);
        int spriteIndex(const int id) const;
        int spriteCount() const;
        Sprite* newSprite();
        Sprite* newSprite(const QDomElement& xml);
        Sprite* newSprite(const Sprite& sprite);
        void deleteSprite(const int index);
        void deleteSpriteById(const int id);
        
        const Status* status(const int index) const;
        Status* status(const int index);
        const Status* statusById(const int id) const;
        Status* statusById(const int id);
        int statusIndex(const int id) const;
        int statusCount() const;
        Status* newStatus();
        Status* newStatus(const QDomElement& xml);
        Status* newStatus(const Status& status);
        void deleteStatus(const int index);
        void deleteStatusById(const int id);
        
        const Store* store(const int index) const;
        Store* store(const int index);
        const Store* storeById(const int id) const;
        Store* storeById(const int id);
        int storeIndex(const int id) const;
        int storeCount() const;
        Store* newStore();
        Store* newStore(const QDomElement& xml);
        Store* newStore(const Store& store);
        void deleteStore(const int index);
        void deleteStoreById(const int id);
        
        const Tile* tile(const int index) const;
        Tile* tile(const int index);
        const Tile* tileById(const int id) const;
        Tile* tileById(const int id);
        int tileIndex(const int id) const;
        int tileCount() const;
        Tile* newTile();
        Tile* newTile(const QDomElement& xml);
        Tile* newTile(const Tile& tile);
        void deleteTile(const int index);
        void deleteTileById(const int id);
        
        const Time* time(const int index) const;
        Time* time(const int index);
        const Time* timeById(const int id) const;
        Time* timeById(const int id);
        int timeIndex(const int id) const;
        int timeCount() const;
        Time* newTime();
        Time* newTime(const QDomElement& xml);
        Time* newTime(const Time& time);
        void deleteTime(const int index);
        void deleteTimeById(const int id);
        
        const Trainer* trainer(const int index) const;
        Trainer* trainer(const int index);
        const Trainer* trainerById(const int id) const;
        Trainer* trainerById(const int id);
        int trainerIndex(const int id) const;
        int trainerCount() const;
        Trainer* newTrainer();
        Trainer* newTrainer(const QDomElement& xml);
        Trainer* newTrainer(const Trainer& trainer);
        void deleteTrainer(const int index);
        void deleteTrainerById(const int id);
        
        const Type* type(const int index) const;
        Type* type(const int index);
        const Type* typeById(const int id) const;
        Type* typeById(const int id);
        int typeIndex(const int id) const;
        int typeCount() const;
        Type* newType();
        Type* newType(const QDomElement& xml);
        Type* newType(const Type& type);
        void deleteType(const int index);
        void deleteTypeById(const int id);
        
        const Weather* weather(const int index) const;
        Weather* weather(const int index);
        const Weather* weatherById(const int id) const;
        Weather* weatherById(const int id);
        int weatherIndex(const int id) const;
        int weatherCount() const;
        Weather* newWeather();
        Weather* newWeather(const QDomElement& xml);
        Weather* newWeather(const Weather& weather);
        void deleteWeather(const int index);
        void deleteWeatherById(const int id);
        
        Game& operator=(const Game& rhs);
    private:
        int newAbilityId() const;
        Ability* newAbility(Ability* ability);
        
        int newAuthorId() const;
        Author* newAuthor(Author* author);
        
        int newBadgeId() const;
        Badge* newBadge(Badge* badge);
        
        int newCoinListId() const;
        CoinList* newCoinList(CoinList* coinList);
        
        int newEggGroupId() const;
        EggGroup* newEggGroup(EggGroup* eggGroup);
        
        int newItemId() const;
        Item* newItem(Item* item);
        
        int newItemTypeId() const;
        ItemType* newItemType(ItemType* itemType);
        
        int newMapId() const;
        Map* newMap(Map* map);
        
        int newMoveId() const;
        Move* newMove(Move* move);
        
        int newNatureId() const;
        Nature* newNature(Nature* nature);
        
        int newGlobalScriptId() const;
        GlobalScript* newGlobalScript(GlobalScript* globalScript);
        
        int newSkinId() const;
        Skin* newSkin(Skin* skin);
        
        int newSoundId() const;
        Sound* newSound(Sound* sound);
        
        int newSpeciesId() const;
        Species* newSpecies(Species* species);
        
        int newSpriteId() const;
        Sprite* newSprite(Sprite* sprite);
        
        int newStatusId() const;
        Status* newStatus(Status* status);
        
        int newStoreId() const;
        Store* newStore(Store* store);
        
        int newTileId() const;
        Tile* newTile(Tile* tile);
        
        int newTimeId() const;
        Time* newTime(Time* time);
        
        int newTrainerId() const;
        Trainer* newTrainer(Trainer* trainer);
        
        int newTypeId() const;
        Type* newType(Type* type);
        
        int newWeatherId() const;
        Weather* newWeather(Weather* weather);
        
        void clear();
        
        QString m_title;
        QString m_version;
        QString m_description;
        bool m_singlePlayer;
        Sigcore::Script m_startScript;
        Sigcore::Matrix<Sigcore::Fraction> m_typechart;
        QMap<int, QPoint> m_mapPosition;
        Rules* m_rules;
        QList<Ability*> m_abilities;
        QList<Author*> m_authors;
        QList<Badge*> m_badges;
        QList<CoinList*> m_coinLists;
        QList<EggGroup*> m_eggGroups;
        QList<GlobalScript*> m_globalScripts;
        QList<Item*> m_items;
        QList<ItemType*> m_itemTypes;
        QList<Map*> m_maps;
        QList<Move*> m_moves;
        QList<Nature*> m_natures;
        QList<Skin*> m_skins;
        QList<Sound*> m_sounds;
        QList<Species*> m_species;
        QList<Sprite*> m_sprites;
        QList<Status*> m_status;
        QList<Store*> m_stores;
        QList<Tile*> m_tiles;
        QList<Time*> m_times;
        QList<Trainer*> m_trainers;
        QList<Type*> m_types;
        QList<Weather*> m_weathers;
};
}

#endif
