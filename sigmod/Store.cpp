/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Store.cpp
 */

// Header include
#include "Store.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

Store::Store(const Store& store) :
        Object(store.parent(), store.id())
{
    *this = store;
}

Store::Store(const Game* parent, const int id) :
        Object(parent, id),
        m_name("")
{
}

Store::Store(const Store& store, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = store;
}

Store::Store(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Store::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (m_item.size())
        TEST_LIST(item);
    else
        emit(error("No items in the store"));
    TEST_END();
}

void Store::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD_LIST(item);
}

QDomElement Store::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE_LIST(item);
    return xml;
}

SETTER(Store, QString&, Name, name)
SETTER_LIST(Store, Item, item)

GETTER(Store, QString, name)
GETTER_LIST(Store, item)

CHECK(Store, QString&, name)
CHECK_INDEX(Store, int, item, game(), item)

Store& Store::operator=(const Store& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY(item);
    return *this;
}

void Store::clear()
{
    m_item.clear();
}
