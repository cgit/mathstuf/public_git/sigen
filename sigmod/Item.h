/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Item.h
 */

#ifndef SIGMOD_ITEM
#define SIGMOD_ITEM

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Item Item.h sigmod/Item.h
 * \brief Items are used to objects that can be carried by the player in the game.
 * 
 * Items are carried by the player until used. Items do a wide range of things from
 * healing to catching to increasing stats (both permanent and temporarily) or to act
 * as keys.
 */
class SIGMOD_EXPORT Item : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param item The item to copy.
         */
        Item(const Item& item);
        /**
         * Create a new item belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        Item(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p item as a child of \p parent with id \p id.
         * 
         * \param item The item to copy the data from.
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        Item(const Item& item, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the item.
         * \param id The id number for the item.
         */
        Item(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the item's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the item in XML format.
         * 
         * \return The XML structure representing the item.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the item used in the game.
         * 
         * \param name The name of the item.
         */
        void setName(const QString& name);
        /**
         * Sets the \link ItemType type \endlink of the item.
         * 
         * \param type The id of the type the item is.
         */
        void setType(const int type);
        /**
         * Sets how much the item is sold for at a \link Store store \endlink. If set to -1, it cannot be sold.
         * 
         * \param price The price of the item.
         */
        void setPrice(const int price);
        /**
         * Sets how much the item can be sold for at a \link Store store \endlink.
         * 
         * \param sellPrice The selling price of the item.
         */
        void setSellPrice(const int sellPrice);
        /**
         * Sets the weight of the item.
         * 
         * \param weight The weight of the item.
         */
        void setWeight(const int weight);
        /**
         * Sets the description of the item. It should be describe the item to the player.
         * 
         * \param description The description of the item.
         */
        void setDescription(const QString& description);
        /**
         * Sets the script that determines the behavior of the item. The following objects are available to the script:
         * 
         *  - \b item -- The \link Sigscript::ItemWrapper item \endlink that the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which is holding the item.
         *  - \b player -- The \link Sigencore::Player client \endlink which owns \b item.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the item is being used in.
         *  - \b targetN -- The \link Sigencore::TeamMember targets \endlink of the item (target0, target1, etc.).
         * 
         * When used and destroyed in the process, the item should call the <b>consumed(Sigscript::ItemWrapper*)</b> slot on
         * either \b owner if held and \b player otherwise.
         * 
         * \param script 
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the item.
         */
        QString name() const;
        /**
         * \sa setType
         * 
         * \return The id of the \link ItemType type \endlink of the item.
         */
        int type() const;
        /**
         * \sa setPrice
         * 
         * \return How much the item costs at a \link Store store \endlink.
         */
        int price() const;
        /**
         * \sa setSellPrice
         * 
         * \return How much the item can be sold for at a \link Store store \endlink.
         */
        int sellPrice() const;
        /**
         * \sa setWeight
         * 
         * \return How much the item weighs.
         */
        int weight() const;
        /**
         * \sa setDescription
         * 
         * \return A short description of the item.
         */
        QString description() const;
        /**
         * \sa setScript
         * 
         * \return The script that defines the behavior of the item.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool typeCheck(const int type) const;
        bool priceCheck(const int price) const;
        bool sellPriceCheck(const int sellPrice) const;
        bool weightCheck(const int weight) const;
        bool descriptionCheck(const QString& description) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        Item& operator=(const Item& rhs);
    private:
        QString m_name;
        int m_type;
        int m_price;
        int m_sellPrice;
        int m_weight;
        QString m_description;
        Sigcore::Script m_script;
};
}

#endif
