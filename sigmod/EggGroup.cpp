/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/EggGroup.cpp
 */

// Header include
#include "EggGroup.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

EggGroup::EggGroup(const EggGroup& eggGroup) :
        Object(eggGroup.parent(), eggGroup.id())
{
    *this = eggGroup;
}

EggGroup::EggGroup(const Game* parent, const int id) :
        Object(parent, id),
        m_name("")
{
}

EggGroup::EggGroup(const EggGroup& eggGroup, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = eggGroup;
}

EggGroup::EggGroup(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void EggGroup::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_END();
}

void EggGroup::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
}

QDomElement EggGroup::save() const
{
    SAVE_CREATE();
    SAVE(name);
    return xml;
}

SETTER(EggGroup, QString&, Name, name)

GETTER(EggGroup, QString, name)

CHECK(EggGroup, QString&, name)

EggGroup& EggGroup::operator=(const EggGroup& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    return *this;
}
