/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/GlobalScript.h
 */

#ifndef SIGMOD_GLOBALSCRIPT
#define SIGMOD_GLOBALSCRIPT

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::GlobalScript GlobalScript.h sigmod/GlobalScript.h
 * \brief A script that contains common code for use by other scripts.
 * 
 * Global scripts allow for common actions to be triggered by other scripts in the sigmod.
 * Built-in scripts can be overridden as well if the script is given a name that tells the
 * engine to use it in place of another.
 * 
 * \todo List built-in script names and describe each.
 */
class SIGMOD_EXPORT GlobalScript : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param globalScript The script to copy.
         */
        GlobalScript(const GlobalScript& globalScript);
        /**
         * Create a new script belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the script.
         * \param id The id number for the script.
         */
        GlobalScript(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p globalScript as a child of \p parent with id \p id.
         * 
         * \param globalScript The item to copy the data from.
         * \param parent The parent of the script.
         * \param id The id number for the script.
         */
        GlobalScript(const GlobalScript& globalScript, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the script.
         * \param id The id number for the script.
         */
        GlobalScript(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the script's values are valid.
         * \note This does not check the script for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the item in XML format.
         * 
         * \return The XML structure representing the script.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the script. This is only used internally.
         * 
         * \param name The name of the script.
         */
        void setName(const QString& name);
        /**
         * Sets the script that contains common code that is available for use by other scripts. The following
         * objects are available to the script (\p NULL if unavailable):
         * 
         *  - \b script -- The \link Sigscript::GlobalScriptWrapper wrapper \endlink for the script.
         *  - \b player -- The \link Sigencore::Player player \endlink the script is active.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink that is active.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Sigscript::MapWrapper map \endlink that is active.
         * 
         * Other objects may be available to the script depending on its context. Also, objects may be added to the script by
         * giving the script more before it is started.
         * 
         * \param script The script.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the script.
         */
        QString name() const;
        /**
         * \sa setScript
         * 
         * \return The script.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        GlobalScript& operator=(const GlobalScript& rhs);
    private:
        QString m_name;
        Sigcore::Script m_script;
};
}

#endif
