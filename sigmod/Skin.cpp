/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Skin.cpp
 */

// Header include
#include "Skin.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigcore;
using namespace Sigmod;

Skin::Skin(const Skin& skin) :
        Object(skin.parent(), skin.id())
{
    *this = skin;
}

Skin::Skin(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_size(32, 32),
        m_script("", "")
{
}

Skin::Skin(const Skin& skin, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = skin;
}

Skin::Skin(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Skin::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(size);
    TEST_END();
}

void Skin::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(size);
    LOAD(script);
}

QDomElement Skin::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(size);
    SAVE(script);
    return xml;
}

SETTER(Skin, QString&, Name, name)
SETTER(Skin, QSize&, Size, size)
SETTER(Skin, Script&, Script, script)

GETTER(Skin, QString, name)
GETTER(Skin, QSize, size)
GETTER(Skin, Script, script)

CHECK(Skin, QString&, name)
CHECK(Skin, QSize&, size)
CHECK(Skin, Script&, script)

Skin& Skin::operator=(const Skin& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(size);
    COPY(script);
    return *this;
}
