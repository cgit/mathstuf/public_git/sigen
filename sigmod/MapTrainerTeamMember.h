/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapTrainerTeamMember.h
 */

#ifndef SIGMOD_MAPTRAINERTEAMMEMBER
#define SIGMOD_MAPTRAINERTEAMMEMBER

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>
#include <QtCore/QMap>

namespace Sigmod
{
// Forward dclarations
class MapTrainer;

/**
 * \class Sigmod::MapTrainerTeamMember MapTrainerTeamMember.h sigmod/MapTrainerTeamMember.h
 * \brief Class describing a team member on a trainer's team.
 * 
 * Each member of a trainer's team can be tuned to present specific challenges to a player.
 * If nothing is customized, or not fully customized, it will be completed with information
 * as a randomly encountered creature.
 */
class SIGMOD_EXPORT MapTrainerTeamMember : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param teamMember The team member to copy.
         */
        MapTrainerTeamMember(const MapTrainerTeamMember& teamMember);
        /**
         * Create a new team member belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the team member.
         * \param id The id number for the team member.
         */
        MapTrainerTeamMember(const MapTrainer* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p teamMember as a child of \p parent with id \p id.
         * 
         * \param teamMember The team member to copy the data from.
         * \param parent The parent of the team member.
         * \param id The id number for the team member.
         */
        MapTrainerTeamMember(const MapTrainerTeamMember& teamMember, const MapTrainer* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the team member.
         * \param id The id number for the team member.
         */
        MapTrainerTeamMember(const QDomElement& xml, const MapTrainer* parent, const int id = -1);
        
        /**
         * Check to make sure the team member's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the team member in XML format.
         * 
         * \return The XML structure representing the team member.
         */
        QDomElement save() const;
        
        /**
         * Sets the id of the species of the team member.
         * 
         * \param species The id of the species of the team member.
         */
        void setSpecies(const int species);
        /**
         * Sets the level of the team member.
         * 
         * \param level The level of the team member.
         */
        void setLevel(const int level);
        /**
         * Sets the state for the ability.
         * 
         * \param ability The id of the ability.
         * \param state Whether to turn the ability on or off.
         */
        void setAbility(const int ability, const bool state);
        /**
         * Sets the number of copies the team member holds of a certain item.
         * 
         * \param item The id of the item the team member is holding.
         * \param count The number of copies of \p item the team member holds.
         */
        void setItem(const int item, const int count);
        /**
         * Sets the state for the move.
         * 
         * \param move The id of the move.
         * \param state Whether to turn the move on or off.
         */
        void setMove(const int move, const bool state);
        /**
         * Sets the state for the nature.
         * 
         * \param nature The id of the nature.
         * \param state Whether to turn the nature on or off.
         */
        void setNature(const int nature, const bool state);
        
        /**
         * \sa setSpecies
         * 
         * \return The id of the species of the team member.
         */
        int species() const;
        /**
         * \sa setLevel
         * 
         * \return The level of the team member.
         */
        int level() const;
        /**
         * \sa setAbility
         * 
         * \param ability The id of the ability to check.
         * \return Whether the team member has the ability or not.
         */
        bool ability(const int ability) const;
        /**
         * \sa setAbility
         * 
         * \return The list of id of abilities the team member has.
         */
        QList<int> ability() const;
        /**
         * \sa setItem
         * 
         * \param item The id of the item to check for.
         * \return How many of the item the team member is carrying.
         */
        int item(const int item) const;
        /**
         * \sa setItem
         * 
         * \return The map of item ids to the amount the team member is carrying.
         */
        QMap<int, int> item() const;
        /**
         * \sa setMove
         * 
         * \param move The id of the move to check for.
         * \return Whether the team member knows the move or not.
         */
        bool move(const int move) const;
        /**
         * \sa setMove
         * 
         * \return The list of ids of moves the team member has.
         */
        QList<int> move() const;
        /**
         * \sa setNature
         * 
         * \param nature The id of the nature to check for.
         * \return Whether the team member has the nature or not.
         */
        bool nature(const int nature) const;
        /**
         * \sa setNature
         * 
         * \return The list of ids of natures the team member has.
         */
        QList<int> nature() const;
        
        bool speciesCheck(const int species) const;
        bool levelCheck(const int level) const;
        bool abilityCheck(const int ability) const;
        bool itemCheck(const int item, const int count) const;
        bool moveCheck(const int move) const;
        bool natureCheck(const int nature) const;

        MapTrainerTeamMember& operator=(const MapTrainerTeamMember& p);
    protected:
        // BUG: GCC bug #57 (template in default argument)
        // http://gcc.gnu.org/bugzilla/show_bug.cgi?id=57
        int heldWeight(const QMap<int, int>& items = (QMap<int, int>())) const;
        bool checkWeight(const int item, const int count) const;
        
        bool canLearn(const int move) const;
    private:
        void clear();
        
        int m_species;
        int m_level;
        QList<int> m_ability;
        QMap<int, int> m_item;
        QList<int> m_move;
        QList<int> m_nature;
};
}

#endif
