/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Time.h
 */

#ifndef SIGMOD_TIME
#define SIGMOD_TIME

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Time Time.h sigmod/Time.h
 * \brief A marker for the start of some time period.
 * 
 * Time can be used to change the properties of things throughout the day.
 * Encounter lists can change, species may change colors, or moves have different
 * effects.
 */
class SIGMOD_EXPORT Time : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param time The time to copy.
         */
        Time(const Time& time);
        /**
         * Create a new time belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the time.
         * \param id The id number for the time.
         */
        Time(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p time as a child of \p parent with id \p id.
         * 
         * \param time The time to copy the data from.
         * \param parent The parent of the time.
         * \param id The id number for the time.
         */
        Time(const Time& time, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the time.
         * \param id The id number for the time.
         */
        Time(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the time's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the time in XML format.
         * 
         * \return The XML structure representing the time.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the time. Used internally only.
         * 
         * \param name The name of the time.
         */
        void setName(const QString& name);
        /**
         * Sets the hour that the time starts at.
         * 
         * \param hour The hour the time starts at.
         */
        void setHour(const int hour);
        /**
         * Sets the minute that the time starts at.
         * 
         * \param minutes The minute the time starts at.
         */
        void setMinute(const int minutes);
        /**
         * Sets the script executed when the time starts. The following objects are available to the script:
         * 
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the ability is being used in (if in battle).
         *  - \b map -- The \link Sigscript::MapWrapper wrapper \endlink for the \link Map map \endlink the player is on.
         * 
         * \param script The script that gets run when the time starts.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the time.
         */
        QString name() const;
        /**
         * \sa setHour
         * 
         * \return The hour that the time starts at.
         */
        int hour() const;
        /**
         * \sa setMinute
         * 
         * \return The minute that the time starts at.
         */
        int minute() const;
        /**
         * \sa setScript
         * 
         * \return The script to get executed when the time starts.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool hourCheck(const int hour) const;
        bool minuteCheck(const int minutes) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        Time& operator=(const Time& rhs);
    private:
        QString m_name;
        int m_hour;
        int m_minute;
        Sigcore::Script m_script;
};
}

#endif
