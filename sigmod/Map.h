/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SIGMOD_MAP
#define SIGMOD_MAP

// Sigcore includes
#include <sigcore/Matrix.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>

namespace Sigmod
{
// Forward declarations
class Game;
class MapEffect;
class MapTile;
class MapTrainer;
class MapWarp;
class MapWildList;

class SIGMOD_EXPORT Map : public Object
{
    Q_OBJECT
    Q_ENUMS(Type)
    
    public:
        Map(const Map& map);
        Map(const Game* parent, const int id);
        Map(const Map& map, const Game* parent, const int id);
        Map(const QDomElement& xml, const Game* parent, const int id = -1);
        ~Map();
        
        void validate();
        
        void load(const QDomElement& xml);
        QDomElement save() const;
        
        void setName(const QString& name);
        void setWidth(const int width);
        void setHeight(const int height);
        void setIsWorld(const bool isWorld);
        
        QString name() const;
        int width() const;
        int height() const;
        bool isWorld() const;
        
        bool nameCheck(const QString& name) const;
        bool widthCheck(const int width) const;
        bool heightCheck(const int height) const;
        bool isWorldCheck(const bool isWorld) const;
        
        const MapEffect* effect(const int index) const;
        MapEffect* effect(const int index);
        const MapEffect* effectById(const int id) const;
        MapEffect* effectById(const int id);
        int effectIndex(const int id) const;
        int effectCount() const;
        MapEffect* newEffect();
        MapEffect* newEffect(const QDomElement& xml);
        MapEffect* newEffect(const MapEffect& effect);
        void deleteEffect(const int index);
        void deleteEffectById(const int id);
        
        const MapTile* tile(const int index) const;
        MapTile* tile(const int index);
        const MapTile* tileById(const int id) const;
        MapTile* tileById(const int id);
        int tileIndex(const int id) const;
        int tileCount() const;
        MapTile* newTile();
        MapTile* newTile(const QDomElement& xml);
        MapTile* newTile(const MapTile& tile);
        void deleteTile(const int index);
        void deleteTileById(const int id);
        
        const MapTrainer* trainer(const int index) const;
        MapTrainer* trainer(const int index);
        const MapTrainer* trainerById(const int id) const;
        MapTrainer* trainerById(const int id);
        int trainerIndex(const int id) const;
        int trainerCount() const;
        MapTrainer* newTrainer();
        MapTrainer* newTrainer(const QDomElement& xml);
        MapTrainer* newTrainer(const MapTrainer& trainer);
        void deleteTrainer(const int index);
        void deleteTrainerById(const int id);
        
        const MapWarp* warp(const int index) const;
        MapWarp* warp(const int index);
        const MapWarp* warpById(const int id) const;
        MapWarp* warpById(const int id);
        int warpIndex(const int id) const;
        int warpCount() const;
        MapWarp* newWarp();
        MapWarp* newWarp(const QDomElement& xml);
        MapWarp* newWarp(const MapWarp& warp);
        void deleteWarp(const int index);
        void deleteWarpById(const int id);
        
        const MapWildList* wildList(const int index) const;
        MapWildList* wildList(const int index);
        const MapWildList* wildListById(const int id) const;
        MapWildList* wildListById(const int id);
        int wildListIndex(const int id) const;
        int wildListCount() const;
        MapWildList* newWildList();
        MapWildList* newWildList(const QDomElement& xml);
        MapWildList* newWildList(const MapWildList& wildList);
        void deleteWildList(const int index);
        void deleteWildListById(const int id);
        
        Map& operator=(const Map& rhs);
    private:
        int newEffectId() const;
        MapEffect* newEffect(MapEffect* effect);
        
        int newTileId() const;
        MapTile* newTile(MapTile* tile);
        
        int newTrainerId() const;
        MapTrainer* newTrainer(MapTrainer* trainer);
        
        int newWarpId() const;
        MapWarp* newWarp(MapWarp* warp);
        
        int newWildListId() const;
        MapWildList* newWildList(MapWildList* wildList);
        
        void clear();
        
        QString m_name;
        int m_width;
        int m_height;
        bool m_isWorld;
        QList<MapEffect*> m_effects;
        QList<MapTile*> m_tiles;
        QList<MapTrainer*> m_trainers;
        QList<MapWarp*> m_warps;
        QList<MapWildList*> m_wildLists;
};
}

#endif
