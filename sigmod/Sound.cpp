/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Sound.cpp
 */

// Header include
#include "Sound.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

const QStringList Sound::TypeStr = QStringList() << "Sound Effect" << "Music";

Sound::Sound(const Sound& sound) :
        Object(sound.parent(), sound.id())
{
    *this = sound;
}

Sound::Sound(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_type(SoundEffect),
        m_data()
{
}

Sound::Sound(const Sound& sound, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = sound;
}

Sound::Sound(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Sound::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (m_data.isEmpty())
        emit(error("Data is empty"));
    TEST_END();
}

void Sound::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD_ENUM(type, Type);
    LOAD(data);
}

QDomElement Sound::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE_ENUM(type, Type);
    SAVE(data);
    return xml;
}

SETTER(Sound, QString&, Name, name)
SETTER(Sound, Type, Type, type)
SETTER(Sound, QByteArray&, Data, data)

GETTER(Sound, QString, name)
GETTER(Sound, Sound::Type, type)
GETTER(Sound, QByteArray, data)

CHECK(Sound, QString&, name)
CHECK(Sound, Type, type)
CHECK(Sound, QByteArray&, data)

Sound& Sound::operator=(const Sound& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY(type);
    COPY(data);
    return *this;
}
