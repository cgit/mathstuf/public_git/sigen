/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Sprite.cpp
 */

// Header include
#include "Sprite.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

Sprite::Sprite(const Sprite& sprite) :
        Object(sprite.parent(), sprite.id())
{
    *this = sprite;
}

Sprite::Sprite(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_sprite()
{
}

Sprite::Sprite(const Sprite& sprite, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = sprite;
}

Sprite::Sprite(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Sprite::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    if (m_sprite.isEmpty())
        emit(error("Sprite is empty"));
    TEST_END();
}

void Sprite::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(sprite);
}

QDomElement Sprite::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(sprite);
    return xml;
}

SETTER(Sprite, QString&, Name, name)
SETTER(Sprite, QByteArray&, Sprite, sprite)

GETTER(Sprite, QString, name)
GETTER(Sprite, QByteArray, sprite)

CHECK(Sprite, QString&, name)
CHECK(Sprite, QByteArray&, sprite)

Sprite& Sprite::operator=(const Sprite& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(sprite);
    return *this;
}
