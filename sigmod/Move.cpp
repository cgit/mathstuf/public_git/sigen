/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Move.cpp
 */

// Header include
#include "Move.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigcore;
using namespace Sigmod;

Move::Move(const Move& move) :
        Object(move.parent(), move.id())
{
    *this = move;
}

Move::Move(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_accuracy(1, 1),
        m_power(0),
        m_type(INT_MAX),
        m_special(false),
        m_powerPoints(0),
        m_priority(0),
        m_description(""),
        m_battleScript("", ""),
        m_worldScript("", ""),
        m_priorityScript("", "")
{
}

Move::Move(const Move& move, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = move;
}

Move::Move(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

void Move::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(accuracy);
    TEST(power);
    TEST(type);
    TEST(powerPoints);
    TEST_END();
}

void Move::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(accuracy);
    LOAD(power);
    LOAD(type);
    LOAD(special);
    LOAD(powerPoints);
    LOAD(priority);
    LOAD(description);
    LOAD(battleScript);
    LOAD(worldScript);
    LOAD(priorityScript);
}

QDomElement Move::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(accuracy);
    SAVE(power);
    SAVE(type);
    SAVE(special);
    SAVE(powerPoints);
    SAVE(priority);
    SAVE(description);
    SAVE(battleScript);
    SAVE(worldScript);
    SAVE(priorityScript);
    return xml;
}

SETTER(Move, QString&, Name, name)
SETTER(Move, Fraction&, Accuracy, accuracy)
SETTER(Move, int, Power, power)
SETTER(Move, int, Type, type)
SETTER(Move, bool, Special, special)
SETTER(Move, int, PowerPoints, powerPoints)
SETTER(Move, int, Priority, priority)
SETTER(Move, QString&, Description, description)
SETTER(Move, Script&, BattleScript, battleScript)
SETTER(Move, Script&, WorldScript, worldScript)
SETTER(Move, Script&, PriorityScript, priorityScript)

GETTER(Move, QString, name)
GETTER(Move, Fraction, accuracy)
GETTER(Move, int, power)
GETTER(Move, int, type)
GETTER(Move, bool, special)
GETTER(Move, int, powerPoints)
GETTER(Move, int, priority)
GETTER(Move, QString, description)
GETTER(Move, Script, battleScript)
GETTER(Move, Script, worldScript)
GETTER(Move, Script, priorityScript)

CHECK(Move, QString&, name)
CHECK_BEGIN(Move, Fraction&, accuracy)
    TBOUNDS(accuracy, 0, 1);
    TBOUNDS_MOD(accuracy, 1, INT_MAX, accuracy.numerator());
CHECK_END()
CHECK_BOUNDS(Move, int, power, 0, INT_MAX)
CHECK_INDEX(Move, int, type, game(), type)
CHECK(Move, bool, special)
CHECK_BOUNDS(Move, int, powerPoints, 1, INT_MAX)
CHECK(Move, int, priority)
CHECK(Move, QString&, description)
CHECK(Move, Script&, battleScript)
CHECK(Move, Script&, worldScript)
CHECK(Move, Script&, priorityScript)

Move& Move::operator=(const Move& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(accuracy);
    COPY(power);
    COPY(type);
    COPY(special);
    COPY(powerPoints);
    COPY(priority);
    COPY(description);
    COPY(battleScript);
    COPY(worldScript);
    COPY(priorityScript);
    return *this;
}
