/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapWarp.cpp
 */

#ifndef SIGMOD_MAPWARP
#define SIGMOD_MAPWARP

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtGui/QPainterPath>

namespace Sigmod
{
// Forward declarations
class Map;

/**
 * \class Sigmod::MapWarp MapWarp.h sigmod/MapWarp.h
 * \brief Class describing a warp on a map.
 * 
 * Warps transport players from one map to another. Every map must have at least
 * one warp (for access to it).
 */
class SIGMOD_EXPORT MapWarp : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param warp The warp to copy.
         */
        MapWarp(const MapWarp& warp);
        /**
         * Create a new warp belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the warp.
         * \param id The id number for the warp.
         */
        MapWarp(const Map* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p warp as a child of \p parent with id \p id.
         * 
         * \param warp The warp to copy the data from.
         * \param parent The parent of the warp.
         * \param id The id number for the warp.
         */
        MapWarp(const MapWarp& warp, const Map* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the ability.
         * \param id The id number for the ability.
         */
        MapWarp(const QDomElement& xml, const Map* parent, const int id = -1);
        
        /**
         * Check to make sure the warp's values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the warp in XML format.
         * 
         * \return The XML structure representing the warp.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the warp. This is only used internally.
         * 
         * \param name The name of the warp.
         */
        void setName(const QString& name);
        /**
         * Sets the position of the warp on the map.
         * 
         * \param position The position of the warp on the map.
         */
        void setPosition(const QPoint& position);
        /**
         * Sets the area of the map the warp takes up.
         * 
         * \param area The area of the map the warp takes up.
         */
        void setArea(const QPainterPath& area);
        /**
         * Sets the id of the target map of the warp.
         * 
         * \param toMap The id of the target map.
         */
        void setToMap(const int toMap);
        /**
         * Sets the id of the target warp on the target map.
         * 
         * \param toWarp The id of the target warp on the target map.
         */
        void setToWarp(const int toWarp);
        /**
         * Sets the script for the warp. This script controls the behavior of the warp
         * and whether it is activated or not. The following objects are available
         * to the script:
         * 
         *  - \b client -- The \link Sigencore::Client client \endlink which entered the warp.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the \b client is on.
         * 
         * \param script The script for the warp.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the warp.
         */
        QString name() const;
        /**
         * \sa setPosition
         * 
         * \return The position of the warp on the map.
         */
        QPoint position() const;
        /**
         * \sa setArea
         * 
         * \return The area that the warp takes up.
         */
        QPainterPath area() const;
        /**
         * \sa setToMap
         * 
         * \return The id of the target map of the warp.
         */
        int toMap() const;
        /**
         * \sa setToWarp
         * 
         * \return The id of the target warp on the target map.
         */
        int toWarp() const;
        /**
         * \sa setScript
         * 
         * \return The script that controls the warp.
         */
        Sigcore::Script script() const;
        
        bool nameCheck(const QString& name) const;
        bool positionCheck(const QPoint& point) const;
        bool areaCheck(const QPainterPath& area) const;
        bool toMapCheck(const int toMap) const;
        bool toWarpCheck(const int toWarp) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        MapWarp& operator=(const MapWarp& rhs);
    private:
        QString m_name;
        QPoint m_position;
        QPainterPath m_area;
        int m_toMap;
        int m_toWarp;
        Sigcore::Script m_script;
};
}

#endif
