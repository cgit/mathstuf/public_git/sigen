/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Nature.cpp
 */

// Header include
#include "Nature.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "Rules.h"

using namespace Sigcore;
using namespace Sigmod;

Nature::Nature(const Nature& nature) :
        Object(nature.parent(), nature.id()),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    *this = nature;
}

Nature::Nature(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_stat(ST_SpecialDefense - ST_Attack + 1),
        m_weight(1)
{
    for (int i = 0; i < m_stat.size(); ++i)
        m_stat[i].set(1, 1);
}

Nature::Nature(const Nature& nature, const Game* parent, const int id) :
        Object(parent, id),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    *this = nature;
}

Nature::Nature(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id),
        m_stat(ST_SpecialDefense - ST_Attack + 1)
{
    LOAD_ID();
    load(xml);
}

void Nature::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST_ARRAY_INDEX_OFF(stat, ST_Attack, ST_Attack);
    TEST_ARRAY_INDEX_OFF(stat, ST_Defense, ST_Attack);
    TEST_ARRAY_INDEX_OFF(stat, ST_Speed, ST_Attack);
    if (game()->rules()->specialSplit())
    {
        TEST_ARRAY_INDEX_OFF(stat, ST_SpecialAttack, ST_Attack);
        TEST_ARRAY_INDEX_OFF(stat, ST_SpecialDefense, ST_Attack);
    }
    else
        TEST_ARRAY_INDEX_OFF(stat, ST_Special, ST_Attack);
    TEST(weight);
    TEST_END();
}

void Nature::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD_ARRAY(stat);
    LOAD(weight);
}

QDomElement Nature::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE_ARRAY(stat);
    SAVE(weight);
    return xml;
}

SETTER(Nature, QString&, Name, name)
SETTER_ARRAY(Nature, Fraction&, Stat, stat, multiplier, Stat, stat, ST_Attack)
SETTER(Nature, int, Weight, weight)

GETTER(Nature, QString, name)
GETTER_ARRAY(Nature, Fraction, stat, multiplier, Stat, stat, ST_Attack)
GETTER(Nature, int, weight)

CHECK(Nature, QString&, name)
CHECK_BEGIN_ARRAY(Nature, Fraction&, stat, multiplier, Stat, stat)
    switch (stat)
    {
        case ST_SpecialDefense:
            if (game()->rules()->specialSplit())
        case ST_Attack:
        case ST_Defense:
        case ST_Speed:
        case ST_Special:
            break;
        default:
            EBOUNDS(stat, "Attack", game()->rules()->specialSplit() ? "SpecialDefense" : "Special");
            return false;
    }
    TBOUNDS(multiplier, 0, INT_MAX)
    TBOUNDS_MOD(multipler_numerator, 1, INT_MAX, multiplier.numerator())
CHECK_END()
CHECK_BOUNDS(Nature, int, weight, 1, INT_MAX)

Nature& Nature::operator=(const Nature& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(name);
    COPY(stat);
    COPY(weight);
    return *this;
}
