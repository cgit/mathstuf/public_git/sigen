/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/EggGroup.h
 */

#ifndef SIGMOD_EGGGROUP
#define SIGMOD_EGGGROUP

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::EggGroup EggGroup.h sigmod/EggGroup.h
 * \brief A group of species that can breed with each other.
 */
class SIGMOD_EXPORT EggGroup : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param eggGroup The group to copy.
         */
        EggGroup(const EggGroup& eggGroup);
        /**
         * Create a new egg group belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the group.
         * \param id The id number for the group.
         */
        EggGroup(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p eggGroup as a child of \p parent with id \p id.
         * 
         * \param eggGroup The group to copy the data from.
         * \param parent The parent of the group.
         * \param id The id number for the group.
         */
        EggGroup(const EggGroup& eggGroup, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the group.
         * \param id The id number for the group.
         */
        EggGroup(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the group's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the group in XML format.
         * 
         * \return The XML structure representing the group.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the egg group. This is only used internally.
         * 
         * \param name The name of the group.
         */
        void setName(const QString& name);
        
        /**
         * \sa setName
         * 
         * \return The name of the group.
         */
        QString name() const;
        
        bool nameCheck(const QString& name) const;
        
        EggGroup& operator=(const EggGroup& rhs);
    private:
        QString m_name;
};
}

#endif
