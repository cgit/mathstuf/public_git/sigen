/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Badge.h
 */

#ifndef SIGMOD_BADGE
#define SIGMOD_BADGE

// Sigcore includes
#include <sigcore/Fraction.h>

// Sigmod includes
#include "Object.h"
#include "Stat.h"

// Qt includes
#include <QtCore/QVarLengthArray>

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Badge Badge.h sigmod/Badge.h
 * \brief Class describing a badge.
 * 
 * Badges are used to show a trainer's worthiness. Badges can raise the maximum level that a player 
 * can train a team member to without fear of it becoming unruly and ignoring commands. Badges may also boost stats.
 */
class SIGMOD_EXPORT Badge : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param badge The badge to copy.
         */
        Badge(const Badge& badge);
        /**
         * Create a new badge belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Badge(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p badge as a child of \p parent and id \p id.
         * 
         * \param badge The badge to copy the data from.
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Badge(const Badge& badge, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the badge.
         * \param id The id number for the badge.
         */
        Badge(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the badge's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the badge in XML format.
         * 
         * \return The XML structure representing the badge.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the badge to be used in the game.
         * 
         * \param name The name of the badge.
         */
        void setName(const QString& name);
        /**
         * Sets the \p id of the sprite that is to be used for the badge before it is obtained.
         * 
         * \param face The \p id of the sprite to be used before the badge is obtained.
         */
        void setFace(const int face);
        /**
         * Sets the \p id of the sprite that is to be used for the badge once it is obtained.
         * 
         * \param badge The \p id of the sprite to be used after the badge is obtained.
         */
        void setBadge(const int badge);
        /**
         * Sets the maximum level at which team members of the player are guaranteed to obey the player.
         * Above this level, team members may not necessarily obey its trainer.
         * 
         * \param obey The maximum level that is guaranteed to obey the player once the badge is obtained.
         */
        void setObey(const int obey);
        /**
         * Sets a multiplier for a stat once the badge is obtained.
         * 
         * \param stat The stat to set the multiplier for.
         * \param multiplier The multiplier for the stat once the badge is obtained.
         */
        void setStat(const Stat stat, const Sigcore::Fraction& multiplier);
        
        /**
         * \sa setName
         * 
         * \return The name of the badge.
         */
        QString name() const;
        /**
         * \sa setFace
         * 
         * \return The \p id of the sprite that represents the badge before it is obtained.
         */
        int face() const;
        /**
         * \sa setBadge
         * 
         * \return The \p id of the sprite that represents the badge once it has been obtained.
         */
        int badge() const;
        /**
         * \sa setObey
         * 
         * \return The maximum level that is guaranteed to obey the player once the badge is obtained.
         */
        int obey() const;
        /**
         * \sa setStat
         * 
         * \param stat The stat to get the multiplier for.
         * 
         * \return The multiplier for \p stat.
         */
        Sigcore::Fraction stat(const Stat stat) const;
        
        bool nameCheck(const QString& name) const;
        bool faceCheck(const int face) const;
        bool badgeCheck(const int badge) const;
        bool obeyCheck(const int obey) const;
        bool statCheck(const Stat stat, const Sigcore::Fraction& multiplier) const;
        
        Badge& operator=(const Badge& rhs);
    private:
        QString m_name;
        int m_face;
        int m_badge;
        int m_obey;
        QVarLengthArray<Sigcore::Fraction, ST_SpecialDefense - ST_Attack + 1> m_stat;
};
}

#endif
