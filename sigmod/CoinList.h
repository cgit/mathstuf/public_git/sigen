/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/CoinList.h
 */

#ifndef SIGMOD_COINLIST
#define SIGMOD_COINLIST

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

// Qt includes
#include <QtCore/QList>

namespace Sigmod
{
// Forward declarations
class CoinListItem;
class Game;

/**
 * \class Sigmod::CoinList CoinList.h sigmod/CoinList.h
 * \brief A coin list contains a list of items that can be bought using coins.
 * 
 * The difference between a coin list and a \link Store store \endlink is that team members
 * cannot be bought at stores. Since money is typically easier to come by than coins are,
 * things that appear in a coin list tend to be rare and valuable.
 */
class SIGMOD_EXPORT CoinList : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param coinList The coin list to copy.
         */
        CoinList(const CoinList& coinList);
        /**
         * Create a new coin list belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the coin list.
         * \param id The id number for the coin list.
         */
        CoinList(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p coinList as a child of \p parent with id \p id.
         * 
         * \param coinList The coin list to copy the data from.
         * \param parent The parent of the coin list.
         * \param id The id number for the coin list.
         */
        CoinList(const CoinList& coinList, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the coin list.
         * \param id The id number for the coin list.
         */
        CoinList(const QDomElement& xml, const Game* parent, const int id = -1);
        /**
         * Destructor.
         */
        ~CoinList();
        
        /**
         * Check to make sure the coin list's values are valid.
         * \note This does not check the script for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the coin list in XML format.
         * 
         * \return The XML structure representing the coin list.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the coin list. This is only used internally.
         * 
         * \param name The name of the coin list.
         */
        void setName(const QString& name);
        /**
         * Sets the script that controls the list of items and purchasing of them. The following
         * objects are provided to the script:
         * 
         *  - \b list -- The instance of the \link Sigscript::CoinListWrapper coin list \endlink that is being used.
         *  - \b player -- The \link Sigencore::Player player \endlink which activated the list.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         * 
         * \param script The script that gets executed when the player activates the list.
         */
        void setScript(const Sigcore::Script& script);
        
        /**
         * \sa setName
         * 
         * \return The name of the coin list.
         */
        QString name() const;
        /**
         * \sa setScript
         * 
         * \return The script for the coin list.
         */
        Sigcore::Script script() const;
        
        /**
         * \param index The index of the item to get.
         * \return Pointer to the item or NULL if \p index is invalid.
         */
        const CoinListItem* item(const int index) const;
        /**
         * \param index The index of the item to get.
         * \return Pointer to the item or NULL if \p index is invalid.
         */
        CoinListItem* item(const int index);
        /**
         * \param id The id of the item to get.
         * \return Pointer to the item or NULL if \p id is invalid.
         */
        const CoinListItem* itemById(const int id) const;
        /**
         * \param id The id of the item to get.
         * \return Pointer to the item or NULL if \p id is invalid.
         */
        CoinListItem* itemById(const int id);
        /**
         * \param id The id of the item to get.
         * \return The index of the item or INT_MAX if \p id is invalid.
         */
        int itemIndex(const int id) const;
        /**
         * \return The number of items in the coin list.
         */
        int itemCount() const;
        /**
         * \return Pointer to a newly created item.
         */
        CoinListItem* newItem();
        /**
         * \param xml The XML of the item to add.
         * \return Pointer to a newly created item from \p xml.
         */
        CoinListItem* newItem(const QDomElement& xml);
        /**
         * \param item A copy of the item to add.
         * \return Pointer to a newly created item from \p item.
         */
        CoinListItem* newItem(const CoinListItem& item);
        /**
         * Deletes the item at index \p index.
         * 
         * \param index The index of the item to delete.
         */
        void deleteItem(const int index);
        /**
         * Deletes the item with id \p id.
         * 
         * \param id The id of the item to delete.
         */
        void deleteItemById(const int id);
        
        bool nameCheck(const QString& name) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        
        CoinList& operator=(const CoinList& rhs);
    private:
        int newItemId() const;
        CoinListItem* newItem(CoinListItem* item);
        
        void clear();
        
        QString m_name;
        Sigcore::Script m_script;
        QList<CoinListItem*> m_items;
};
}

#endif
