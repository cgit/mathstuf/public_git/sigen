/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Map.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"
#include "MapEffect.h"
#include "MapTile.h"
#include "MapTrainer.h"
#include "MapWarp.h"
#include "MapWildList.h"

// Qt includes
#include <QtCore/QSet>

using namespace Sigmod;

Map::Map(const Map& map) :
        Object(map.parent(), map.id())
{
    *this = map;
}

Map::Map(const Game* parent, const int id) :
        Object(parent, id),
        m_name(""),
        m_width(1),
        m_height(1),
        m_isWorld(false)
{
}

Map::Map(const Map& map, const Game* parent, const int id) :
        Object(parent, id)
{
    *this = map;
}

Map::Map(const QDomElement& xml, const Game* parent, const int id) :
        Object(parent, id)
{
    LOAD_ID();
    load(xml);
}

Map::~Map()
{
    clear();
}

void Map::validate()
{
    TEST_BEGIN();
    if (m_name.isEmpty())
        emit(error("Name is empty"));
    TEST(width);
    TEST(height);
    QSet<int> idChecker;
    QSet<QString> nameChecker;
    if (!effectCount())
        emit(warning("There are no effects"));
    TEST_SUB_BEGIN(MapEffect, effects);
        TEST_SUB("effect", id);
        TEST_SUB("effect", name);
    TEST_SUB_END();
    idChecker.clear();
    nameChecker.clear();
    if (!tileCount())
        emit(warning("There are no tiles"));
    TEST_SUB_BEGIN(MapTile, tiles);
        TEST_SUB("tile", id);
    TEST_SUB_END();
    idChecker.clear();
    if (!trainerCount())
        emit(warning("There are no trainers"));
    TEST_SUB_BEGIN(MapTrainer, trainers);
        TEST_SUB("trainer", id);
        TEST_SUB("trainer", name);
    TEST_SUB_END();
    idChecker.clear();
    nameChecker.clear();
    if (!warpCount())
        emit(error("There are no warps"));
    TEST_SUB_BEGIN(MapWarp, warps);
        TEST_SUB("warp", id);
        TEST_SUB("warp", name);
    TEST_SUB_END();
    idChecker.clear();
    if (!wildListCount())
        emit(warning("There are no wild lists"));
    TEST_SUB_BEGIN(MapWildList, wildLists);
        TEST_SUB("wild list", id);
    TEST_SUB_END();
    TEST_END();
}

void Map::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(name);
    LOAD(width);
    LOAD(height);
    LOAD(isWorld);
    LOAD_SUB(newEffect, MapEffect);
    LOAD_SUB(newTile, MapTile);
    LOAD_SUB(newTrainer, MapTrainer);
    LOAD_SUB(newWarp, MapWarp);
    LOAD_SUB(newWildList, MapWildList);
}

QDomElement Map::save() const
{
    SAVE_CREATE();
    SAVE(name);
    SAVE(width);
    SAVE(height);
    SAVE(isWorld);
    SAVE_SUB(MapEffect, effects);
    SAVE_SUB(MapTile, tiles);
    SAVE_SUB(MapTrainer, trainers);
    SAVE_SUB(MapWarp, warps);
    SAVE_SUB(MapWildList, wildLists);
    return xml;
}

SETTER(Map, QString&, Name, name)
SETTER(Map, int, Width, width)
SETTER(Map, int, Height, height)
SETTER(Map, bool, IsWorld, isWorld)

GETTER(Map, QString, name)
GETTER(Map, int, width)
GETTER(Map, int, height)
GETTER(Map, bool, isWorld)

CHECK(Map, QString&, name)
CHECK_BOUNDS(Map, int, width, 1, INT_MAX)
CHECK_BOUNDS(Map, int, height, 1, INT_MAX)
CHECK(Map, bool, isWorld)

SUBCLASS(Map, Effect, effect, effects)
SUBCLASS(Map, Tile, tile, tiles)
SUBCLASS(Map, Trainer, trainer, trainers)
SUBCLASS(Map, Warp, warp, warps)
SUBCLASS(Map, WildList, wildList, wildLists)

Map& Map::operator=(const Map& rhs)
{
    if (this == &rhs)
        return *this;
    clear();
    COPY(name);
    COPY(width);
    COPY(height);
    COPY(isWorld);
    COPY_SUB(MapEffect, effects);
    COPY_SUB(MapTile, tiles);
    COPY_SUB(MapTrainer, trainers);
    COPY_SUB(MapWarp, warps);
    COPY_SUB(MapWildList, wildLists);
    return *this;
}

void Map::clear()
{
    SUBCLASS_CLEAR(effects);
    SUBCLASS_CLEAR(tiles);
    SUBCLASS_CLEAR(trainers);
    SUBCLASS_CLEAR(warps);
    SUBCLASS_CLEAR(wildLists);
}
