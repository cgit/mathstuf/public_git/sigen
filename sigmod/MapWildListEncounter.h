/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/MapWildListEncounter.h
 */

#ifndef SIGMOD_MAPWILDLISTENCOUNTER
#define SIGMOD_MAPWILDLISTENCOUNTER

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class MapWildList;

/**
 * \class Sigmod::MapWildListEncounter MapWildListEncounter.h sigmod/MapWildListEncounter.h
 * \brief Class describing a random encounter.
 * 
 * Encounters are random battles in the world. While in areas that creatures roam wild, players
 * can encounter them. This gives a level and a relative weighting to other creatures that can
 * be encountered in an area.
 */
class SIGMOD_EXPORT MapWildListEncounter: public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param encounter The encounter to copy.
         */
        MapWildListEncounter(const MapWildListEncounter& encounter);
        /**
         * Create a new encounter belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the encounter.
         * \param id The id number for the encounter.
         */
        MapWildListEncounter(const MapWildList* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p encounter as a child of \p parent with id \p id.
         * 
         * \param encounter The encounter to copy the data from.
         * \param parent The parent of the encounter.
         * \param id The id number for the encounter.
         */
        MapWildListEncounter(const MapWildListEncounter& encounter, const MapWildList* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the encounter.
         * \param id The id number for the encounter.
         */
        MapWildListEncounter(const QDomElement& xml, const MapWildList* parent, const int id = -1);
        
        /**
         * Check to make sure the encounter's values are valid.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the encounter in XML format.
         * 
         * \return The XML structure representing the encounter.
         */
        QDomElement save() const;
        
        /**
         * Sets the id of the species that can be encountered.
         * 
         * \param species The id of the species that can be encountered.
         */
        void setSpecies(const int species);
        /**
         * Sets the level at which the species will apear.
         * 
         * \param level The level at which the species will appear.
         */
        void setLevel(const int level);
        /**
         * Sets the weight of the encounter.
         * 
         * \param weight The weighting of the encounter.
         */
        void setWeight(const int weight);
        
        /**
         * \sa setSpecies
         * 
         * \return The id of the species that can be encountered.
         */
        int species() const;
        /**
         * \sa setLevel
         * 
         * \return The level at which the species will appear.
         */
        int level() const;
        /**
         * \sa setWeight
         * 
         * \return The weighting of the encounter.
         */
        int weight() const;
        
        bool speciesCheck(const int species) const;
        bool levelCheck(const int level) const;
        bool weightCheck(const int weight) const;
        
        MapWildListEncounter& operator=(const MapWildListEncounter& rhs);
    private:
        int m_species;
        int m_level;
        int m_weight;
};
}

#endif
