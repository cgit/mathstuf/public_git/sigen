/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/XmlReader.h
 */

#ifndef SIGMOD_XMLREADER
#define SIGMOD_XMLREADER

// Sigcore includes
#include <sigcore/Fraction.h>
#include <sigcore/Matrix.h>
#include <sigcore/Script.h>

// Sigmod includes
#include "Global.h"

// Qt includes
#include <QtCore/QBuffer>
#include <QtCore/QPoint>
#include <QtCore/QStringList>
#include <QtCore/QVariant>
#include <QtCore/QVarLengthArray>
#include <QtGui/QPainterPath>
#include <QtXml/QDomElement>

// C includes
#include <climits>

namespace Sigmod
{
template<typename T> inline SIGMOD_NO_EXPORT void loadValue(const QDomElement& xml, T* value)
{
    *value = QVariant(xml.firstChild().toText().data()).value<T>();
}

template<> inline SIGMOD_NO_EXPORT void loadValue<bool>(const QDomElement& xml, bool* value)
{
    *value = (xml.firstChild().toText().data() == "true");
}

template<> inline SIGMOD_NO_EXPORT void loadValue<int>(const QDomElement& xml, int* value)
{
    *value = xml.firstChild().toText().data().toInt();
}

template<> inline SIGMOD_NO_EXPORT void loadValue<Sigcore::Fraction>(const QDomElement& xml, Sigcore::Fraction* value)
{
    value->set(xml.attribute("numerator", "1").toInt(), xml.attribute("denominator", "1").toInt());
    value->reduce();
}

template<> inline SIGMOD_NO_EXPORT void loadValue<QPoint>(const QDomElement& xml, QPoint* value)
{
    value->setX(xml.attribute("x", "0").toInt());
    value->setY(xml.attribute("y", "0").toInt());
}

template<> inline SIGMOD_NO_EXPORT void loadValue<QPainterPath>(const QDomElement& xml, QPainterPath* value)
{
    QDataStream stream(QByteArray::fromBase64(xml.firstChild().toText().data().toUtf8()));
    stream >> *value;
}

template<> inline SIGMOD_NO_EXPORT void loadValue<QByteArray>(const QDomElement& xml, QByteArray* value)
{
    *value = QByteArray::fromBase64(xml.firstChild().toText().data().toUtf8());
}

template<> inline SIGMOD_NO_EXPORT void loadValue<Sigcore::Script>(const QDomElement& xml, Sigcore::Script* value)
{
    value->setInterpreter(xml.attribute("interpreter", ""));
    value->setScript(xml.firstChild().toText().data());
}

template<typename T> inline SIGMOD_NO_EXPORT void loadEnum(const QDomElement& xml, T* value, const QStringList& array)
{
    *value = static_cast<T>(array.indexOf(xml.firstChild().toText().data()));
}

template<typename T, int S> inline SIGMOD_NO_EXPORT void loadArray(const QDomElement& xml, QVarLengthArray<T, S>* value)
{
    QDomElement node = xml.firstChildElement("element");
    while (!node.isNull())
    {
        int index = node.attribute("index", "-1").toInt();
        if ((0 <= index) && (index < value->size()))
            loadValue(node, value->data() + index);
        node = node.nextSiblingElement("element");
    }
}

inline SIGMOD_NO_EXPORT void loadList(const QDomElement& xml, QList<int>* value)
{
    QDomElement node = xml.firstChildElement("element");
    int element;
    while (!node.isNull())
    {
        loadValue(node, &element);
        if (!value->contains(element))
            value->append(element);
        node = node.nextSiblingElement("element");
    }
}

template<typename T> inline SIGMOD_NO_EXPORT void loadMap(const QDomElement& xml, QMap<int, T>* value, const QString& valueName)
{
    QDomElement node = xml.firstChildElement("element");
    int key;
    T keyValue;
    while (!node.isNull())
    {
        loadValue(node.firstChildElement("key"), &key);
        loadValue(node.firstChildElement(valueName), &keyValue);
        (*value)[key] = keyValue;
        node = node.nextSiblingElement("element");
    }
}

template<typename T> inline SIGMOD_NO_EXPORT void loadMatrix(const QDomElement& xml, Sigcore::Matrix<T>* value)
{
    int height = xml.attribute("height", "1").toInt();
    int width = xml.attribute("width", "1").toInt();
    value->resize(width, height);
    QDomElement node = xml.firstChildElement("element");
    while (!node.isNull())
    {
        int row = node.attribute("row", "-1").toInt();
        int column = node.attribute("column", "-1").toInt();
        if ((0 <= row) && (row < height) && (0 <= column) && (column < width))
            loadValue(node, &value->operator()(row, column));
        node = node.nextSiblingElement("element");
    }
}

template<typename T> inline SIGMOD_NO_EXPORT QDomElement saveValue(const QString& name, const T& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(QVariant::fromValue<T>(value).toString()));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<bool>(const QString& name, const bool& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(value ? "true" : "false"));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<int>(const QString& name, const int& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(QString::number(value)));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<QString>(const QString& name, const QString& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(value));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<Sigcore::Fraction>(const QString& name, const Sigcore::Fraction& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.setAttribute("numerator", value.numerator());
    element.setAttribute("denominator", value.denominator());
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<QPoint>(const QString& name, const QPoint& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.setAttribute("x", value.x());
    element.setAttribute("y", value.y());
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<QPainterPath>(const QString& name, const QPainterPath& value)
{
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);
    stream << value;
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(array.toBase64()));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<QByteArray>(const QString& name, const QByteArray& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.appendChild(doc.createTextNode(value.toBase64()));
    return element;
}

template<> inline SIGMOD_NO_EXPORT QDomElement saveValue<Sigcore::Script>(const QString& name, const Sigcore::Script& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.setAttribute("interpreter", value.interpreter());
    element.appendChild(doc.createCDATASection(value.script()));
    return element;
}

template<typename T> inline SIGMOD_NO_EXPORT QDomElement saveEnum(const QString& name, const T& value, const QStringList& array)
{
    return saveValue<QString>(name, array[value]);
}

template<typename T, int S> inline SIGMOD_NO_EXPORT QDomElement saveArray(const QString& name, const QVarLengthArray<T, S>& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    for (int i = 0; i < value.size(); ++i)
    {
        QDomElement valueElement = saveValue("element", value[i]);
        element.appendChild(valueElement);
        valueElement.setAttribute("index", QString::number(i));
    }
    return element;
}

inline SIGMOD_NO_EXPORT QDomElement saveList(const QString& name, const QList<int>& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    for (int i = 0; i < value.size(); ++i)
        element.appendChild(saveValue("element", value.at(i)));
    return element;
}

template<typename T> inline SIGMOD_NO_EXPORT QDomElement saveMap(const QString& name, const QMap<int, T>& value, const QString& valueName)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    const QList<int> keys = value.keys();
    foreach (int key, keys)
    {
        QDomElement data = doc.createElement("element");
        data.appendChild(saveValue("key", key));
        data.appendChild(saveValue(valueName, value[key]));
        element.appendChild(data);
    }
    return element;
}

template<typename T> inline SIGMOD_NO_EXPORT QDomElement saveMatrix(const QString& name, const Sigcore::Matrix<T>& value)
{
    QDomDocument doc;
    QDomElement element = doc.createElement(name);
    element.setAttribute("height", value.height());
    element.setAttribute("width", value.width());
    for (int i = 0; i < value.height(); ++i)
    {
        for (int j = 0; j < value.width(); ++j)
        {
            QDomElement valueElement = saveValue("element", value(i, j));
            valueElement.setAttribute("row", QString::number(i));
            valueElement.setAttribute("column", QString::number(j));
            element.appendChild(valueElement);
        }
    }
    return element;
}

}

#endif
