/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

// Header include
#include "Rules.h"

// Sigmod includes
#include "Game.h"
#include "Macros.h"

using namespace Sigmod;

const QStringList Rules::DVStr = QStringList() << "16" << "32";

Rules::Rules(const Rules& rules) :
        Object(rules.parent(), 0)
{
    *this = rules;
}

Rules::Rules(const Game* parent) :
        Object(parent, 0),
        m_genderAllowed(false),
        m_breedingAllowed(false),
        m_criticalDomains(false),
        m_numBoxes(0),
        m_boxSize(1),
        m_maxParty(1),
        m_maxFight(1),
        m_maxPlayers(2),
        m_maxHeldItems(0),
        m_maxAbilities(0),
        m_maxNatures(0),
        m_maxMoves(1),
        m_maxLevel(1),
        m_maxStages(6),
        m_maxMoney(0),
        m_maxTotalWeight(0),
        m_specialSplit(false),
        m_specialDVSplit(false),
        m_maxTotalEV(0),
        m_maxEVPerStat(0)
{
}

Rules::Rules(const Rules& rules, const Game* parent) :
        Object(parent, 0)
{
    *this = rules;
}

Rules::Rules(const QDomElement& xml, const Game* parent) :
        Object(parent, 0)
{
    load(xml);
}

void Rules::validate()
{
    TEST_BEGIN();
    TEST(breedingAllowed);
    TEST(numBoxes);
    TEST(boxSize);
    TEST(maxParty);
    TEST(maxFight);
    TEST(maxPlayers);
    TEST(maxHeldItems);
    TEST(maxAbilities);
    TEST(maxNatures);
    TEST(maxMoves);
    TEST(maxLevel);
    TEST(maxStages);
    if (!m_maxMoney)
        emit(warning("Player cannot carry any money"));
    else
        TEST(maxMoney);
    TEST(maxTotalWeight);
    TEST(maxTotalEV);
    TEST(maxEVPerStat);
    TEST_END();
}

void Rules::load(const QDomElement& xml)
{
    LOAD_BEGIN();
    LOAD(genderAllowed);
    LOAD(breedingAllowed);
    LOAD(criticalDomains);
    LOAD(numBoxes);
    LOAD(boxSize);
    LOAD(maxParty);
    LOAD(maxFight);
    LOAD(maxPlayers);
    LOAD(maxHeldItems);
    LOAD(maxAbilities);
    LOAD(maxNatures);
    LOAD(maxMoves);
    LOAD(maxLevel);
    LOAD(maxStages);
    LOAD(maxMoney);
    LOAD(maxTotalWeight);
    LOAD(specialSplit);
    LOAD(specialDVSplit);
    LOAD(maxTotalEV);
    LOAD(maxEVPerStat);
}

QDomElement Rules::save() const
{
    QDomDocument doc;
    QDomElement xml = doc.createElement(className());
    SAVE(genderAllowed);
    SAVE(breedingAllowed);
    SAVE(criticalDomains);
    SAVE(numBoxes);
    SAVE(boxSize);
    SAVE(maxParty);
    SAVE(maxFight);
    SAVE(maxPlayers);
    SAVE(maxHeldItems);
    SAVE(maxAbilities);
    SAVE(maxNatures);
    SAVE(maxMoves);
    SAVE(maxLevel);
    SAVE(maxStages);
    SAVE(maxMoney);
    SAVE(maxTotalWeight);
    SAVE(specialSplit);
    SAVE(specialDVSplit);
    SAVE(maxTotalEV);
    SAVE(maxEVPerStat);
    return xml;
}

SETTER(Rules, bool, GenderAllowed, genderAllowed)
SETTER(Rules, bool, BreedingAllowed, breedingAllowed)
SETTER(Rules, bool, CriticalDomains, criticalDomains)
SETTER(Rules, int, NumBoxes, numBoxes)
SETTER(Rules, int, BoxSize, boxSize)
SETTER(Rules, int, MaxParty, maxParty)
SETTER(Rules, int, MaxFight, maxFight)
SETTER(Rules, int, MaxPlayers, maxPlayers)
SETTER(Rules, int, MaxHeldItems, maxHeldItems)
SETTER(Rules, int, MaxAbilities, maxAbilities)
SETTER(Rules, int, MaxNatures, maxNatures)
SETTER(Rules, int, MaxMoves, maxMoves)
SETTER(Rules, int, MaxLevel, maxLevel)
SETTER(Rules, int, MaxStages, maxStages)
SETTER(Rules, int, MaxMoney, maxMoney)
SETTER(Rules, int, MaxTotalWeight, maxTotalWeight)
SETTER(Rules, bool, SpecialSplit, specialSplit)
SETTER(Rules, bool, SpecialDVSplit, specialDVSplit)
SETTER(Rules, int, MaxTotalEV, maxTotalEV)
SETTER(Rules, int, MaxEVPerStat, maxEVPerStat)

GETTER(Rules, bool, genderAllowed)
GETTER(Rules, bool, breedingAllowed)
GETTER(Rules, bool, criticalDomains)
GETTER(Rules, int, numBoxes)
GETTER(Rules, int, boxSize)
GETTER(Rules, int, maxParty)
GETTER(Rules, int, maxFight)
GETTER(Rules, int, maxPlayers)
GETTER(Rules, int, maxHeldItems)
GETTER(Rules, int, maxAbilities)
GETTER(Rules, int, maxNatures)
GETTER(Rules, int, maxMoves)
GETTER(Rules, int, maxLevel)
GETTER(Rules, int, maxStages)
GETTER(Rules, int, maxMoney)
GETTER(Rules, int, maxTotalWeight)
GETTER(Rules, bool, specialSplit)
GETTER(Rules, bool, specialDVSplit)
GETTER(Rules, int, maxTotalEV)
GETTER(Rules, int, maxEVPerStat)

CHECK(Rules, bool, genderAllowed)
CHECK_BEGIN(Rules, bool, breedingAllowed)
    if (!m_genderAllowed && breedingAllowed)
    {
        ERROR("Cannot breed when genders are not allowed");
        return false;
    }
CHECK_END()
CHECK(Rules, bool, criticalDomains)
CHECK_BOUNDS(Rules, int, numBoxes, -1, INT_MAX)
CHECK_BEGIN(Rules, int, boxSize)
    if (m_numBoxes)
        TBOUNDS(boxSize, 0, INT_MAX);
CHECK_END()
CHECK_BOUNDS(Rules, int, maxParty, 1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxFight, 1, m_maxParty)
CHECK_BOUNDS(Rules, int, maxPlayers, 2, INT_MAX)
CHECK_BOUNDS(Rules, int, maxHeldItems, -1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxAbilities, -1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxNatures, -1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxMoves, 1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxLevel, 1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxStages, -1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxMoney, -1, INT_MAX)
CHECK_BOUNDS(Rules, int, maxTotalWeight, -1, INT_MAX)
CHECK(Rules, bool, specialSplit)
CHECK_BEGIN(Rules, bool, specialDVSplit)
    if (!m_specialSplit && specialDVSplit)
    {
        ERROR("Cannot split special DV when special is not split");
        return false;
    }
CHECK_END()
CHECK_BEGIN(Rules, int, maxTotalEV)
    TBOUNDS(maxTotalEV, -1, INT_MAX);
CHECK_END()
CHECK_BEGIN(Rules, int, maxEVPerStat)
    if (m_maxTotalEV)
        TBOUNDS(maxEVPerStat, 0, (0 < m_maxTotalEV) ? m_maxTotalEV : INT_MAX);
CHECK_END()

Rules& Rules::operator=(const Rules& rhs)
{
    if (this == &rhs)
        return *this;
    COPY(genderAllowed);
    COPY(breedingAllowed);
    COPY(criticalDomains);
    COPY(numBoxes);
    COPY(boxSize);
    COPY(maxParty);
    COPY(maxFight);
    COPY(maxPlayers);
    COPY(maxHeldItems);
    COPY(maxAbilities);
    COPY(maxNatures);
    COPY(maxMoves);
    COPY(maxLevel);
    COPY(maxStages);
    COPY(maxMoney);
    COPY(maxTotalWeight);
    COPY(specialSplit);
    COPY(specialDVSplit);
    COPY(maxTotalEV);
    COPY(maxEVPerStat);
    return *this;
}
