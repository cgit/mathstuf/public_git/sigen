/*
 * Copyright 2008-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Status.h
 */

#ifndef SIGMOD_STATUS
#define SIGMOD_STATUS

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Status Status.h sigmod/Status.h
 * \brief Class describing a status.
 * 
 * A status effect can change things drastically for a creature that is inflicted with it.
 * It may not be able to attack, receive items, switch, or may receive damage at certain
 * intervals. Status effects that are only applicable in battle should clear themselves
 * from the creature after the battle ends.
 */
class SIGMOD_EXPORT Status : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param status The status to copy.
         */
        Status(const Status& status);
        /**
         * Create a new status belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the status.
         * \param id The id number for the status.
         */
        Status(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p status as a child of \p parent with id \p id.
         * 
         * \param status The status to copy the data from.
         * \param parent The parent of the status.
         * \param id The id number for the status.
         */
        Status(const Status& status, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the status.
         * \param id The id number for the status.
         */
        Status(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the status' values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the status in XML format.
         * 
         * \return The XML structure representing the status.
         */
        QDomElement save() const;
        
        void setName(const QString& name);
        /**
         * Set the script for the status when in battle. This script defines the behavior of the status when in a battle.
         * The following objects are available to the script:
         * 
         *  - \b status -- The \link Sigscript::StatusWrapper status \endlink the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which is affected by the status.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b arena -- The \link Sigencore::Arena arena \endlink the \b owner is in.
         * 
         * \param battleScript The script for the status when in battle.
         */
        void setBattleScript(const Sigcore::Script& battleScript);
        /**
         * Set the script for the status when on the world map. This script defines the behavior of the status when on the world map.
         * The following objects are available to the script:
         * 
         *  - \b status -- The \link Sigscript::StatusWrapper status \endlink the script controls.
         *  - \b owner -- The \link Sigencore::TeamMember team member \endlink which is affected by the status.
         *  - \b player -- The \link Sigencore::Player player \endlink which owns \b owner.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink \b player is on.
         * 
         * \param worldScript The script for the status when on the world map.
         */
        void setWorldScript(const Sigcore::Script& worldScript);
        
        /**
         * \sa setName
         * 
         * \return The name of the status.
         */
        QString name() const;
        /**
         * \sa setBattleScript
         * 
         * \return The script for the status in battle.
         */
        Sigcore::Script battleScript() const;
        /**
         * \sa setWorldScript
         * 
         * \return The script for the status in the world map.
         */
        Sigcore::Script worldScript() const;
        
        bool nameCheck(const QString& name) const;
        bool battleScriptCheck(const Sigcore::Script& battleScript) const;
        bool worldScriptCheck(const Sigcore::Script& worldScript) const;
        
        Status& operator=(const Status& rhs);
    private:
        QString m_name;
        Sigcore::Script m_battleScript;
        Sigcore::Script m_worldScript;
};
}

#endif
