/*
 * Copyright 2007-2009 Ben Boeckel <MathStuf@gmail.com>
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * \file sigmod/Tile.h
 */

#ifndef SIGMOD_TILE
#define SIGMOD_TILE

// Sigcore includes
#include <sigcore/Script.h>

// Sigmod includes
#include "Object.h"

namespace Sigmod
{
// Forward declarations
class Game;

/**
 * \class Sigmod::Tile Tile.h sigmod/Tile.h
 * \brief Class describing a tile.
 * 
 * Tiles are static map objects. They can change their images, but not their position
 * or size (determined by the initial image used for it). If a tile is set to be
 * \p walkable, it can be walked over by objects (trainers, effects, and players).
 * However, any transparent parts of the image used will never collide.
 */
class SIGMOD_EXPORT Tile : public Object
{
    Q_OBJECT
    
    public:
        /**
         * Copy constructor.
         * 
         * \param tile The tile to copy.
         */
        Tile(const Tile& tile);
        /**
         * Create a new tile belonging to \p parent and id \p id.
         * 
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        Tile(const Game* parent, const int id);
        /**
         * Data copy constructor. Copies the data from \p tile as a child of \p parent with id \p id.
         * 
         * \param tile The tile to copy the data from.
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        Tile(const Tile& tile, const Game* parent, const int id);
        /**
         * XML data constructor.
         * 
         * \param xml The XML structure to extract the data from.
         * \param parent The parent of the tile.
         * \param id The id number for the tile.
         */
        Tile(const QDomElement& xml, const Game* parent, const int id = -1);
        
        /**
         * Check to make sure the tile's values are valid.
         * \note This does not check the scripts for validity.
         */
        void validate();
        
        /**
         * Load data from XML.
         * 
         * \param xml The XML structure to extract data from.
         */
        void load(const QDomElement& xml);
        /**
         * Get the data for the tile in XML format.
         * 
         * \return The XML structure representing the tile.
         */
        QDomElement save() const;
        
        /**
         * Sets the name of the tile. This is only used internally.
         * 
         * \param name The name of the tile.
         */
        void setName(const QString& name);
        /**
         * Sets whether the tile can be walked through by objects or not.
         * 
         * \param walkable Whether the tile can be walked through.
         */
        void setWalkable(const bool walkable);
        /**
         * Sets the script for the tile. The following objects are available to the script:
         * 
         *  - \b tileinfo -- The \link Sigscript::TileWrapper wrapper \endlink for the tile.
         *  - \b sigmod -- The \link Sigscript::SigmodWrapper wrapper \endlink for the \link Sigmod sigmod \endlink in use.
         *  - \b world -- The \link Sigworld::Map map \endlink for the \link Map map \endlink the \b player is on.
         *  - \b tile -- The \link Sigworld::Tile tile \endlink the script controls.
         *  
         *  The \b tile object has a <i>entered(\link Sigencore::Player* \endlink)</i> signal which the script can connect to
         *  in order to interact with the player. In order to animate \b tile, it is recommended to use a \link QTimeLine \endlink
         *  in order to control the animation. Then, providing a function that converts accepts the frame number, set the corresponding
         *  sprite in \b tile.
         * 
         * \param script The script for the tile.
         */
        void setScript(const Sigcore::Script& script);
        /**
         * Sets the preview sprite for the tile. It is only used while editing maps.
         * 
         * \param preview The id of the sprite.
         */
        void setPreview(const int preview);
        
        /**
         * \sa setName
         * 
         * \return The name of the tile.
         */
        QString name() const;
        /**
         * \sa setWalkable
         * 
         * \return Whether the til ecan be walked through or not.
         */
        bool walkable() const;
        /**
         * \sa setScript
         * 
         * \return The script for the tile.
         */
        Sigcore::Script script() const;
        /**
         * \sa setPreview
         * 
         * \return The sprite to use for a preview of the tile.
         */
        int preview() const;
        
        bool nameCheck(const QString& name) const;
        bool walkableCheck(const bool walkable) const;
        bool scriptCheck(const Sigcore::Script& script) const;
        bool previewCheck(const int preview) const;
        
        Tile& operator=(const Tile& rhs);
    private:
        QString m_name;
        bool m_walkable;
        Sigcore::Script m_script;
        int m_preview;
};
}

#endif
